﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Visi.Generic.Data;
using Visi.Models;
using Visi.Views;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace Visi.ViewModels
{
    public class MainViewModel
    {
        public ObservableCollection<HomeMenuItem> TopItems { get; } = new ObservableCollection<HomeMenuItem>();
        public ObservableCollection<HomeMenuItem> BottomItems { get; } = new ObservableCollection<HomeMenuItem>();
        public event EventHandler ShowOptions;
        public Frame CurrentPage { get; set; }

        public MainViewModel()
        {
            TopItems.Add(new HomeMenuItem() { Title = "Contacts", Icon = "\uE71E", Action = CommandExecute, MenuType = MenuType.Contacts });
            TopItems.Add(new HomeMenuItem() { Title = "Profile", Icon = "\uE71F", Action = CommandExecute, MenuType = MenuType.Profile });
            TopItems.Add(new HomeMenuItem() { Title = "Store", Icon = "\uE71F", Action = CommandExecute, MenuType = MenuType.Store });
            TopItems.Add(new HomeMenuItem() { Title = "About", Icon = "\uE71F", Action = CommandExecute, MenuType = MenuType.About });
            BottomItems.Add(new HomeMenuItem() { Title = "Options", Icon = "\uE713", Action = CommandExecute, MenuType = MenuType.Options });
        }


        private void CommandExecute(HomeMenuItem naviItem)
        {            
            switch (naviItem.MenuType)
            {
                case MenuType.Contacts:
                    CurrentPage.Navigate(typeof(Views.AccountContacts));
                    break;
                case MenuType.Profile:
                    CurrentPage.Navigate(typeof(ProfilePage));
                    break;
                case MenuType.Store:
                    CurrentPage.Navigate(typeof(StorePage));
                    break;
                case MenuType.About:
                    CurrentPage.Navigate(typeof(AboutPage));
                    break;
                case MenuType.Options:
                    ShowOptions?.Invoke(this, EventArgs.Empty);
                    break;
            }

            
        }

    }
}
