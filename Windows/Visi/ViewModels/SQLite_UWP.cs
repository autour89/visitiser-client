﻿using SQLite;
using System.IO;
using Visi.Generic.Interfaces;
using Windows.Storage;

namespace Visi.ViewModels
{
    public class SQLite_UWP : ISQLite
    {
        private SQLiteConnection sql;
        public SQLite_UWP()
        {
        }
        #region ISQLite implementation
        public SQLiteConnection GetConnection()
        {
            var sqliteFilename = "MessengerSQLite2.db3";
            string path = Path.Combine(ApplicationData.Current.LocalFolder.Path, sqliteFilename);

            var conn = new SQLiteConnection(path);

            // Return the database connection 
            return conn;
        }
        #endregion
    }
}
