﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Visi.Generic.Data;
using Visi.Generic.Models;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Visi.Views
{
    public sealed partial class MessagesPage : Page
    {
        private IHubProxy HubProxy { get; set; }
        public HubConnection Connection { get; set; }
        private ContentDB db;
        private int RecipientId { get; set; }
        public MessagesPage()
        {
            this.InitializeComponent();

            NavigationCacheMode = NavigationCacheMode.Required;

            db = new ContentDB();

            ConnectAsync();

        }


        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var result = (int)e.Parameter;

            RecipientId = result;

            base.OnNavigatedTo(e);

        }


        private async Task ConnectAsync()
        {
            Connection = new HubConnection(Configuration.BaseUrl, "userid=" + db.UserAccount.Id);
            Connection.Closed += Connection_Closed; ;

            HubProxy = Connection.CreateHubProxy("MessengerHub");

            HubProxy.On<string, string>("AddMessage", (name, message) =>
               CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
               {
                   inbox.Text += String.Format("{0}: {1}\r", name, message + Environment.NewLine);
               }));

            //HubProxy.On<string, string>("AddMessage", (name, message) =>
            //   Window.Current.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => {
            //      //EditorTextConsole.Text += String.Format("{0}: {1}\r", name, message + Environment.NewLine);
            //   }));



            try
            {
                await Connection.Start();
            }
            catch (HttpRequestException)
            {
                inbox.Text = "Unable to connect to server: Start server before connecting clients.";

                return;
            }

            inbox.Text = "Connected to server at " + Configuration.BaseUrl + "\r";
        }

        private void Connection_Closed()
        {
            //var dispatcher = Application.Current.Dispatcher;
        }

        private void msgsend_Click(object sender, RoutedEventArgs e)
        {

            HubProxy.Invoke("SendPrivateMessage", RecipientId, db.UserAccount.Id, db.UserAccount.Name, msgbox.Text);

            msgbox.Text = String.Empty;


        }
    }
}
