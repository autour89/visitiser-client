﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Visi.Generic.Data;
using Visi.Generic.Helpers;
using Visi.Generic.Models;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace Visi.Views
{
    public sealed partial class AccountContacts : Page
    {
        private RestApi access;
        private ContentDB db;
        
        public AccountContacts()
        {
            this.InitializeComponent();

            NavigationCacheMode = NavigationCacheMode.Required;

            access = new RestApi(Configuration.BaseUrl);

            db = new ContentDB();


            AccountList();

            myContacts.SelectionChanged += MyContacts_SelectionChanged;
        }

        

        private async void AccountList()
        {

            var result = access.TemplateGet(ApiUrl.GetAllContacts);

            var data = JsonConvert.DeserializeObject<List<AccountData>>(result);

            myContacts.ItemsSource = data.Where(x=>x.Id != db.UserAccount.Id).ToList();
        }

        private void filtersearch_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {

        }

        private void MyContacts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = sender as ListView;

           var accnt = (AccountData)item.SelectedItem;

            this.Frame.Navigate(typeof(MessagesPage), accnt.Id);

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var b = "";

            base.OnNavigatedTo(e);

            NavigationCacheMode = NavigationCacheMode.Enabled;

        }

    }
}
