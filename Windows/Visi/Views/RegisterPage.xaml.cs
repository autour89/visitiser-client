﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Visi.Generic.Data;
using Visi.Generic.Helpers;
using Visi.Generic.Models;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Visi.Views
{
    public sealed partial class RegisterPage : Page
    {
        private RestApi access;
        private ContentDB db;
        public RegisterPage()
        {
            this.InitializeComponent();

            access = new RestApi(Configuration.BaseUrl);
            db = new ContentDB();
             SecurityCode = "1111";

        }

        private string SecurityCode { get; set; }
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            var accnt = new RegisterViewModel
            {
                Name = NickName_fld.Text,
                PhoneNumber = PhNmbr_fld.Text
            };

            var r = access.TemplatePost(ApiUrl.CreateAccount, accnt);

            if (r != null)
            {
                var reg = JsonConvert.DeserializeObject<VisitizerProfile>(r);

                var ins = "";
                ContentDB.Account_Data.InsertItem(reg);

                this.Frame.Navigate(typeof(Layouts.LayoutPage));
            }
        }
    }
}
