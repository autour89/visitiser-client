﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Visi.Generic.Data;
using Visi.Models;
using Visi.ViewModels;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Visi.Views.Layouts
{
    public sealed partial class LayoutPage : Page
    {
        private MainViewModel mainViewModel;
        private ContentDB db;
        private static readonly object _object = new object();

        public LayoutPage()
        {

            this.InitializeComponent();

            NavigationCacheMode = NavigationCacheMode.Required;

            SystemNavigationManager.GetForCurrentView().BackRequested += OnBackRequested;

            db = new ContentDB();
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            await CheckAcount();
        }

       
        private void MainViewModel_ShowOptions(object sender, EventArgs e)
        {
            optionsSplitView.IsPaneOpen = true;
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            if (this.ContentFrame.CanGoBack)
            {
                e.Handled = true;
                this.ContentFrame.GoBack();
            }
        }

        private void ContentFrame_Navigated(object sender, NavigationEventArgs e)
        {
            // Each time a navigation event occurs, update the Back button's visibility
            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility =
                ((Frame)sender).CanGoBack ?
                AppViewBackButtonVisibility.Visible :
                AppViewBackButtonVisibility.Collapsed;
        }

        

        public Frame ContentFrame
        {
            get { return frame; }
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            MySplitView.IsPaneOpen = !MySplitView.IsPaneOpen;
        }

        private void NavigationItem_Click(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            var naviItem = (HomeMenuItem)button.Tag;
            if (naviItem != null && naviItem.Action != null)
                naviItem.Execute();
        }



        private async Task CheckAcount()
        {
           
            if (db.UserAccount == null)
                await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                Frame.Navigate(typeof(RegisterPage));
            });
            else
                SetMenu();
           
        }

        private void SetMenu()
        {
            this.DataContext = mainViewModel = new MainViewModel();

            this.ContentFrame.Navigated += ContentFrame_Navigated;
            this.ContentFrame.Navigate(typeof(AccountContacts));

            mainViewModel.CurrentPage = this.ContentFrame;

            mainViewModel.ShowOptions += MainViewModel_ShowOptions;
            
        }


    }
}
