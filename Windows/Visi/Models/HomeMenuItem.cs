﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Visi.Models
{
    public enum MenuType
    {
        Contacts,
        Profile,
        Store,
        About,
        Options
    }
    public class HomeMenuItem : INotifyPropertyChanged
    {
        private string title;
        private string icon;
        private bool isSelected;
        private Action<HomeMenuItem> action;
        public event PropertyChangedEventHandler PropertyChanged;

        public MenuType MenuType { get; set; }

        public string Icon
        {
            get { return icon; }

            set
            {
                icon = value;
                OnPropertyChanged();
            }
        }
        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                OnPropertyChanged();
            }
        }

        public Action<HomeMenuItem> Action
        {
            get { return action; }
            set
            {
                action = value;
            }
        }

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                isSelected = value;
                OnPropertyChanged();
            }
        }

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Execute()
        {
            Action(this);
        }
    }
}
