using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Content.Res;

namespace Visi.Settings.Actions.view
{
    public class SettingParamView : LinearLayout
    {
        Context cx;
        private TypedArray typeArray;
        private TextView _paramTitle;
        private TextView _paramName;
        private CheckBox _paramCheckBox;

        public SettingParamView(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            cx = context;
            typeArray = context.ObtainStyledAttributes(attrs, Resource.Styleable.SettingParamView, 0, 0);
            Initialize();
        }

        public SettingParamView(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            cx = context;
            typeArray = context.ObtainStyledAttributes(attrs, Resource.Styleable.SettingParamView, 0, 0);
            Initialize();
        }

        private void Initialize()
        {
            Inflate(Context, Resource.Layout.Setting_parameter_view, this);

            var _boxTitle = typeArray.GetString(Resource.Styleable.SettingParamView_param_title);
            var _Param_text = typeArray.GetString(Resource.Styleable.SettingParamView_param_titleName);

            _paramTitle = FindViewById<TextView>(Resource.Id.sett_title);
            _paramName = FindViewById<TextView>(Resource.Id.para_name);

            _paramTitle.Text = _boxTitle;
            _paramName.Text = _Param_text;

        }

        public bool ParamCheckBox
        {
            set
            {
                _paramCheckBox.Checked = value;
            }
            get
            {
                return _paramCheckBox.Checked;
            }
        }

    }
}