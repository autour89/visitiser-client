using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Visi.Resources.Actions.activity;
using Visi.Settings.Actions.view;
using Android.Content.PM;

namespace Visi.Settings.Actions.activity
{
    [Activity(Label = "",Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class ConfidentialityActivity : BaseActivity
    {
        private SettingParamView param_isonline;
        private SettingParamView param_useapp;
        private SettingParamView param_seen;

        protected override int LayoutResource => Resource.Layout.Profile_confidentiality;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Toolbar = FindViewById<Toolbar>(Resource.Id.profile_toolbar);
            this.SetToolbar();

            var title_toolbar = FindViewById<TextView>(Resource.Id.profile_toolbar_text);
            title_toolbar.Text = GetString(Resource.String.Title_confidentials);
        }
    }
}