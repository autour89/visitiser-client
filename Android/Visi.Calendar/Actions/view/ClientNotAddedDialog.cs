﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Visi.Calendar.Actions.view
{
    public class ClientNotAddedDialog : Dialog
    {
        private Context cx;

        private ToggleButton choose_client_again, create_as_not_working_time, leave_empty;
        private Button btn_ok, btn_cancel;

        public ClientNotAddedDialog(Context context) : base(context)
        {
            var feature = this.Window;
            feature.RequestFeature(WindowFeatures.NoTitle);
            cx = context;
            Initialize();
        }

        private void Initialize()
        {
            SetContentView(Resource.Layout.Client_not_added_dialog);

            choose_client_again = FindViewById<ToggleButton>(Resource.Id.tb_add_or_create_client);
            create_as_not_working_time = FindViewById<ToggleButton>(Resource.Id.tb_create_as_time_not_working);
            leave_empty = FindViewById<ToggleButton>(Resource.Id.tb_leave_field_empty);

            btn_ok = FindViewById<Button>(Resource.Id.client_not_choosed_ok);
            btn_cancel = FindViewById<Button>(Resource.Id.client_not_choosed_cancel);

            choose_client_again.Click += Handle_Click;
            create_as_not_working_time.Click += Handle_Click;
            leave_empty.Click += Handle_Click;
            btn_ok.Click += Handle_Click;
            btn_cancel.Click += Handle_Click;

        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var toggle = sender as ToggleButton;
            var button = sender as Button;

            if (toggle != null)
            {
                if (toggle.Id.Equals(Resource.Id.tb_add_or_create_client))
                {
                    choose_client_again.Checked = true;
                    create_as_not_working_time.Checked = false;
                    leave_empty.Checked = false;
                }
                else if (toggle.Id.Equals(Resource.Id.tb_create_as_time_not_working))
                {
                    choose_client_again.Checked = false;
                    create_as_not_working_time.Checked = true;
                    leave_empty.Checked = false;
                }
                else if (toggle.Id.Equals(Resource.Id.tb_leave_field_empty))
                {
                    choose_client_again.Checked = false;
                    create_as_not_working_time.Checked = false;
                    leave_empty.Checked = true;
                }
            }
            else if (button != null)
            {
                if (button.Id.Equals(Resource.Id.client_not_choosed_ok))
                {
                    this.Dismiss();
                }
                else if (button.Id.Equals(Resource.Id.client_not_choosed_cancel))
                {
                    this.Dismiss();
                }
            }
        }
    }
}