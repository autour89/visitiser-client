﻿
using System;

using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Android.Support.V4.Widget;
using Android.Animation;
using Android.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Visi.Calendar.Presenter;
using Visi.Calendar.Actions.adapter;
using Visi.Generic.Models;

namespace Visi.Calendar.Actions.view
{
    public class DayCalendarView : FrameLayout
    {
        private DayCalendarPresenter cntrlr;
        private bool isLinerOpen = false;
        private bool animate = false;
        private bool setLinearMin = false;
        private const int numberOfColumns = 7;

        private Context cx;

        private RecyclerView dayGrid, calView, timetable;
        private DayCalendarScrollListener days_scrollListener;
        private TimetableScrollListener timetable_scrollListener;
        private NestedScrollView page_vertical_scroll;

        private GridLayoutManager calendar_lManager;
        private LinearLayoutManager timetableLm;
        private LinearLayout calendarLinear;
        private RelativeLayout month_relative, editgroup_checkboxes;
        private TextView monthText;
        private EventDialogView eventDialog;

        private DayNamesAdapter dayTitleAdapter;
        private DayCalendarAdapter calviewAdapter;
        private TimetableAdapter timetableAdapter;

        private ToggleButton plural_edit;
        private Button belate_sms_btn, cancel_sms_btn;
        private Dialog cancel_sms_dialog, belate_sms_dialog;

        public DayCalendarView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            cx = context;
            Initialize();
        }

        public DayCalendarView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
        {
            cx = context;
            Initialize();
        }

        private void Initialize()
        {
            Inflate(cx, Resource.Layout.Day_calendar_view, this);

            MaxHeight = (int)Resources.GetDimension(Resource.Dimension.calendar_linear_max_height);
            MinHeight = (int)cx.Resources.GetDimension(Resource.Dimension.calendar_linear_min_height);

            cntrlr = new DayCalendarPresenter();

            CalToolbar = FindViewById<Toolbar>(Resource.Id.calendar_toolbar);
            dayGrid = FindViewById<RecyclerView>(Resource.Id.day_title_grid);
            calView = FindViewById<RecyclerView>(Resource.Id.day_calendar_grid);
            timetable = FindViewById<RecyclerView>(Resource.Id.timetable_grid);
            page_vertical_scroll = FindViewById<NestedScrollView>(Resource.Id.scroll_page);

            calendarLinear = FindViewById<LinearLayout>(Resource.Id.grid_linear);
            month_relative = FindViewById<RelativeLayout>(Resource.Id.grid_relative);
            monthText = FindViewById<TextView>(Resource.Id.month_text);
            plural_edit = FindViewById<ToggleButton>(Resource.Id.plural_edit);
            belate_sms_btn = FindViewById<Button>(Resource.Id.belate_sms_btn);
            cancel_sms_btn = FindViewById<Button>(Resource.Id.cancel_sms_btn);
            editgroup_checkboxes = FindViewById<RelativeLayout>(Resource.Id.editgroup_checkboxes);

            calendar_lManager = new GridLayoutManager(cx, numberOfColumns);
            var day_lManager = new GridLayoutManager(cx, numberOfColumns);
            timetableLm = new LinearLayoutManager(cx, LinearLayoutManager.Horizontal, false);
            timetable_scrollListener = new TimetableScrollListener(timetableLm);
            days_scrollListener = new DayCalendarScrollListener(calendar_lManager);
            eventDialog = new EventDialogView(cx, null);

            dayGrid.SetLayoutManager(day_lManager);
            calView.SetLayoutManager(calendar_lManager);
            timetable.SetLayoutManager(timetableLm);

            timetable.AddOnScrollListener(timetable_scrollListener);
            calView.AddOnScrollListener(days_scrollListener);

            editgroup_checkboxes.Visibility = ViewStates.Gone;
            monthText.BringToFront();

            eventDialog.DismissEvent += Handle_DismissEvent;
            page_vertical_scroll.ScrollChange += Handle_ScrollChange;
            timetable_scrollListener.ScrollStateEvent += Handle_ScrollStateEvent;
            days_scrollListener.ScrollStateEvent += Handle_ScrollStateEvent;
            days_scrollListener.ScrolledEvent += Handle_ScrolledEvent;
            plural_edit.Click += Handle_Click;
            belate_sms_btn.Click += Handle_Click;
            cancel_sms_btn.Click += Handle_Click;

        }

        public Toolbar CalToolbar { get; private set; }
        public RecyclerView CalView => calView;
        public TextView MonthText => monthText;
        public GridLayoutManager CalendarFrame => calendar_lManager;
        public int MaxHeight { get; set; }
        public int MinHeight { get; set; }

        public void SetPage()
        {
            new Handler().Post(() =>
            {
                dayTitleAdapter = new DayNamesAdapter(this.Context, cntrlr.ListDays);
                timetableAdapter = new TimetableAdapter(cx, cntrlr.TimetableDate);
                calviewAdapter = new DayCalendarAdapter(cx, cntrlr.TimetableDate.DatesRange);

                calviewAdapter.ItemClick += Handle_ItemClick;
                timetableAdapter.ItemTouch += Timetable_ItemTouch;
                timetableAdapter.EventClick += Handle_EventClick;

                dayGrid.SetAdapter(dayTitleAdapter);
                calView.SetAdapter(calviewAdapter);
                timetable.SetAdapter(timetableAdapter);

                animate = false;
                SetLinearHeight(MinHeight / 2);
                SetTimetablePosition();

                calView.Post(() =>
                {
                    SetLinearHeight(MinHeight);
                });

            });

        }

        private void Handle_EventClick(object sender, int e)
        {
            var eview = sender as EventView;
            var event_info = eview.EventInfo;

            var editeventDialog = new EventDialogView(cx, null);
            editeventDialog.IsEditEvent = true;
            editeventDialog.DismissEvent += Handle_DismissEvent;

            var date_info = new DateRangeModel
            {
                Year = event_info.Start_DT.Year,
                Month = event_info.Start_DT.Month,
                DayOfMonth = event_info.Start_DT.Day,
                HourOfDay = event_info.Start_DT.Hour,
                Minutes = event_info.Start_DT.Minute,
                Duration = event_info.Duration
            };
            editeventDialog.IsEditEvent = true;
            editeventDialog.EventData = event_info;
            editeventDialog.Show();

        }

        private void Timetable_ItemTouch(object sender, int e)
        {
            OpenEventDialog(e);
        }

        private void Handle_ScrolledEvent(object sender, int e)
        {
            var recycler = sender as RecyclerView;

            if (recycler != null)
            {
                if (recycler.Id.Equals(Resource.Id.day_calendar_grid))
                {
                    if (!animate)
                    {
                        var count = recycler.ChildCount;
                        var middlePosition = recycler.GetChildAt(count / 2);

                        if (middlePosition != null)
                        {
                            var currMonth = CalendarFrame.GetPosition(middlePosition);
                            SetMonthTitle(currMonth);
                        }
                    }
                }
            }
        }

        private void Handle_ScrollStateEvent(object sender, int e)
        {
            var recycler = sender as RecyclerView;

            if (recycler != null)
            {
                switch (e)
                {
                    case 0:
                        if (recycler.Id.Equals(Resource.Id.day_calendar_grid))
                        {
                            var get = recycler.GetChildAt(1);
                            var position = CalendarFrame.GetPosition(get);
                            recycler.ScrollToPosition(position);
                            MonthText.Visibility = ViewStates.Gone;
                        }
                        else if (recycler.Id.Equals(Resource.Id.timetable_grid))
                        {
                            var pos = timetableLm.FindFirstVisibleItemPosition();
                            timetableLm.ScrollToPositionWithOffset(pos, 0);
                            ChangeDay(pos);
                        }
                        break;

                    case 1:
                        if (recycler.Id.Equals(Resource.Id.day_calendar_grid))
                        {
                            if (!isLinerOpen)
                            {
                                animate = true;
                                isLinerOpen = true;
                                SetLinearHeight(MaxHeight);
                                //MonthText.Visibility = ViewStates.Visible;
                            }
                            if (!animate)
                            {
                                if (MonthText.Visibility == ViewStates.Gone)
                                    MonthText.Visibility = ViewStates.Visible;
                            }
                        }
                        else if (recycler.Id.Equals(Resource.Id.timetable_grid))
                        {

                        }
                        break;
                }

            }
        }

        private void Handle_ScrollChange(object sender, NestedScrollView.ScrollChangeEventArgs e)
        {
            var scrollview = sender as NestedScrollView;

            if (isLinerOpen)
            {
                animate = true;
                setLinearMin = true;
                isLinerOpen = false;
                SetLinearHeight(MinHeight);
            }
        }

        private void Handle_ItemClick(object sender, int e)
        {
            ChangeDay(e);
            timetable.ScrollToPosition(e);
        }


        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            var toggle = sender as ToggleButton;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.belate_sms_btn))
                {
                    belate_sms_dialog = new Dialog(cx);
                    belate_sms_dialog.RequestWindowFeature(Convert.ToInt32(WindowFeatures.NoTitle));
                    belate_sms_dialog.SetContentView(Resource.Layout.Belate_sms_dialog);

                    belate_sms_dialog.Show();
                }
                else if (button.Id.Equals(Resource.Id.cancel_sms_btn))
                {
                    cancel_sms_dialog = new Dialog(cx);
                    cancel_sms_dialog.RequestWindowFeature(Convert.ToInt32(WindowFeatures.NoTitle));
                    cancel_sms_dialog.SetContentView(Resource.Layout.Cancel_sms_dialog);

                    cancel_sms_dialog.Show();
                }
            }
            if (toggle != null)
            {
                if (toggle.Id.Equals(Resource.Id.plural_edit))
                {
                    if (toggle.Checked == true)
                    {

                        editgroup_checkboxes.Visibility = ViewStates.Visible;
                    }
                    else
                    {
                        editgroup_checkboxes.Visibility = ViewStates.Gone;
                    }
                }
            }
        }

        private void Handle_DismissEvent(object sender, EventArgs e)
        {
            var dialog = sender as EventDialogView;

            if (dialog.IsCreatedOrUpdated)
            {
                cntrlr.UpdateTimetable();
                timetableAdapter.NotifyDataSetChanged();
                dialog.IsCreatedOrUpdated = false;
            }
            dialog.IsEditEvent = false;
            dialog.Invalidate();
        }

        private void Animator_AnimationEnd(object sender, EventArgs e)
        {
            animate = false;

            if (setLinearMin)
            {
                var position = cntrlr.CurrentSelectedDay;
                calView.ScrollToPosition(position);
                //calendar_lManager.ScrollToPositionWithOffset(position,0);
                setLinearMin = false;
            }
        }

        private void Animator_Update(object sender, ValueAnimator.AnimatorUpdateEventArgs e)
        {
            var linearParams = calendarLinear.LayoutParameters;
            linearParams.Height = (int)e.Animation.AnimatedValue;
            calendarLinear.LayoutParameters = linearParams;
        }

        private void SetTimetablePosition()
        {
            var position = cntrlr.CurrentSelectedDay;
            calView.ScrollToPosition(position);
            timetable.ScrollToPosition(position);
        }

        private void ChangeDay(int position)
        {
            var prev = cntrlr.CurrentSelectedDay;
            cntrlr.ChangeSelectedDay(position);
            calView.ScrollToPosition(position);
            calviewAdapter.NotifyItemRangeChanged(prev, 1, 0);
            calviewAdapter.NotifyItemRangeChanged(position, 1, 0);
        }

        private void SetMonthTitle(int position)
        {
            var date = cntrlr.TimetableDate.DatesRange[position];

            if (!date.Month.Equals(cntrlr.CurrentMonth))
            {
                switch (cntrlr.IsCurrentYear(position))
                {
                    case true:
                        monthText.Text = date.MonthOfYear;
                        break;
                    case false:
                        monthText.Text = date.MonthOfYear + " " + date.Year;
                        break;
                }

                cntrlr.CurrentMonth = date.Month;
            }
        }

        private void SetLinearHeight(int value)
        {
            if (!animate)
            {
                var linearParams = calendarLinear.LayoutParameters;
                linearParams.Height = value;
                calendarLinear.LayoutParameters = linearParams;
                return;
            }
            ValueAnimator animator = null;
            if (value.Equals(MinHeight))
                animator = ValueAnimator.OfFloat(MaxHeight, MinHeight);
            else if (value.Equals(MaxHeight))
                animator = ValueAnimator.OfFloat(MinHeight, MaxHeight);

            animator.SetDuration(500);

            animator.Update += Animator_Update;
            animator.AnimationEnd += Animator_AnimationEnd;

            animator.Start();
        }

        private void OpenEventDialog(int hour)
        {
            if (hour > 0)
                cntrlr.TimetableDate.DatesRange[cntrlr.CurrentSelectedDay].HourOfDay = hour;

            //eventDialog.EventDate = cntrlr.TimetableDate.DatesRange[cntrlr.CurrentSelectedDay];
            eventDialog.EventData = cntrlr.CreateEvent;
            eventDialog.Show();
        }

    }

}


