﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;

namespace Visi.Calendar.Actions.view
{
    public class CustomRecyclerView : RecyclerView
    {
        public CustomRecyclerView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Initialize();
        }

        public CustomRecyclerView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
        {
            Initialize();
        }

        private void Initialize()
        {

        }

        public override bool Fling(int velocityX, int velocityY)
        {
            velocityY = velocityY / 4;

            return base.Fling(velocityX, velocityY);
        }
    }
}