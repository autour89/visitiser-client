﻿using System;
using System.Collections.Generic;

using Android.Content;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Android.Content.Res;
using Android.Graphics;

using Visi.Calendar.Model;
using Visi.Calendar.Actions.adapter;
using Visi.Calendar.Interfaces;
using Visi.Calendar.Presenter;

namespace Visi.Calendar.Actions.view
{
    public class WheelView : FrameLayout, IWheel
    {
        private WheelViewController viewController;
        private int selectedPosition;

        private Context cx;
        private TypedArray typeArray;

        private CustomRecyclerView listData;
        private WheelViewAdapter listAdapter;

        private FrameLayout rootLinear;
        private WheelViewListener onScrollListener;
        private LinearLayoutManager lManager;

        public WheelView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            cx = context;
            typeArray = context.ObtainStyledAttributes(attrs, Resource.Styleable.WheelView, 0, 0);
            Initialize();
        }

        public WheelView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
        {
            cx = context;
            typeArray = context.ObtainStyledAttributes(attrs, Resource.Styleable.WheelView, 0, 0);
            Initialize();
        }

        private void Initialize()
        {
            Inflate(cx, Resource.Layout.Wheel_view_layout, this);

            viewController = new WheelViewController();

            //var _textSize = typeArray.GetDimension(Resource.Styleable.WheelView_control_textSize, -1);
            var _textColor = typeArray.GetColor(Resource.Styleable.WheelView_control_textColor, -1);
            var _selected_textColor = typeArray.GetColor(Resource.Styleable.WheelView_selected_textColor, -1);

            //if (!_textSize.Equals(-1))
            //    TextSize = _textSize;
            if (_textColor != null && !_textColor.Equals(-1))
                TextColor = _textColor;
            if (_selected_textColor != null && !_selected_textColor.Equals(-1))
                SelectedTextColor = _selected_textColor;


            rootLinear = FindViewById<FrameLayout>(Resource.Id.root_view);
            listData = FindViewById<CustomRecyclerView>(Resource.Id.data_list);

            lManager = new LinearLayoutManager(cx, LinearLayoutManager.Vertical, false);
            lManager.SmoothScrollbarEnabled = true;
            listData.SetLayoutManager(lManager);
            onScrollListener = new WheelViewListener(lManager);

            listData.AddOnScrollListener(onScrollListener);
            onScrollListener.ScrollStateEvent += OnScrollListener_ScrollStateEvent;
            onScrollListener.ScrolledEvent += OnScrollListener_ScrolledEvent; ;
        }

        private void OnScrollListener_ScrolledEvent(object sender, int e)
        {
            CurrentItemPosition = (e + 1) % viewController.Data.Count;
            listAdapter.CurrentItem = e + 1;
        }

        private void OnScrollListener_ScrollStateEvent(object sender, int e)
        {
            OnHandleScrollStateEvent(this, e);
        }

        public int CurrentItemPosition { get; set; }
        public int SelectedItemPosition
        {
            set
            {
                var ind = value;
                if (ind >= viewController.Data.Count)
                {
                    ind -= viewController.Data.Count;
                }
                SetDateScrollCurrent(ind);
                ChangeCurrentState();
                listAdapter.SelectedItem = listAdapter.CurrentItem;
                selectedPosition = ind;
            }
            get { return selectedPosition; }
        }

        public void SetDateScrollCurrent(int ind)
        {
            var curr_inAdapter = listAdapter.CurrentItem % viewController.Data.Count;
            var zero_res = listAdapter.CurrentItem - curr_inAdapter;
            var result_index = zero_res + ind;
            listAdapter.CurrentItem = result_index;
            CurrentItemPosition = result_index % viewController.Data.Count;
        }

        public float TextSize { get; set; }
        public Color TextColor { get; set; }
        public Color SelectedTextColor { get; set; }


        public delegate void ScrollStateEventHandler(object sender, int e);
        public event ScrollStateEventHandler ScrollStateEvent;
        void OnHandleScrollStateEvent(object sender, int position)
        {
            if (ScrollStateEvent != null)
                ScrollStateEvent(sender, position);
        }

        ///// <summary>
        /// Set Height of this Control
        /// </summary>
        public int NewHeight
        {
            set
            {
                var lp = rootLinear.LayoutParameters;
                lp.Height = value;
                rootLinear.LayoutParameters = lp;
            }
        }

        /// <summary>
        /// Set Width of this Control
        /// </summary>
        public int NewWidth
        {
            set
            {
                var lp = rootLinear.LayoutParameters;
                lp.Width = value;
                rootLinear.LayoutParameters = lp;
            }
        }

        /// <summary>
        ///Adapter Data
        /// </summary>
        public List<string> Data
        {
            set
            {
                viewController.StringModel = value;
                ActivateView();
            }
        }

        public List<string> StringObj
        {
            set
            {
                viewController.StringModel = value;
                ActivateView();
            }
        }



        /// <summary>
        ///  Change text color of item when scroll ended, or user selected new item
        /// </summary>
        public void ChangeCurrentState()
        {
            viewController.Data[SelectedItemPosition].IsCurrent = false;
            viewController.Data[CurrentItemPosition].IsCurrent = true;

            listAdapter.NotifyDataSetChanged();
            lManager.ScrollToPositionWithOffset(listAdapter.CurrentItem - 1, 0);
        }

        /*
         * Attach all configs to control
         */
        private void ActivateView()
        {
            if (viewController.Data != null)
            {
                listAdapter = new WheelViewAdapter(cx, viewController.Data);
                //listAdapter.TextSize = TextSize;
                listAdapter.TextColor = TextColor;
                listAdapter.SelectedTextColor = SelectedTextColor;

                listData.SetAdapter(listAdapter);
                InitListPos();
            }
        }

        /*
        * Init first middle position starting from 0
        */
        private void InitListPos()
        {
            var maxMiddle = Int32.MaxValue / 2;
            var curMiddlePosition = maxMiddle % viewController.Data.Count;
            var curPosition = (maxMiddle - curMiddlePosition) - 1;

            listAdapter.CurrentItem = curPosition;
            CurrentItemPosition = curPosition % viewController.Data.Count;
            selectedPosition = curPosition % viewController.Data.Count;
            SelectedItemPosition = curPosition % viewController.Data.Count;
        }

    }
}


