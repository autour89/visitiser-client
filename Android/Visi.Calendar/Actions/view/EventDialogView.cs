﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

using Android.App;
using Android.Content;
using Android.Util;
using Android.Views;
using Android.Widget;

using Visi.Calendar.Presenter;
using Visi.Generic.Models;
using Visi.Generic.DAL.DataModel;
using Newtonsoft.Json;

namespace Visi.Calendar.Actions.view
{
    public class EventDialogView : Dialog
    {
        private EventDialogController dController;
        private Events eventData;


        private Context cx;

        private Button newEvent, detailsAction, cancelAction;
        private EventTimeView date_time_select;
        private DescriptionCreateEvent descriptionBlock;

        public EventDialogView(Context context, IAttributeSet attrs) : base(context)
        {
            cx = context;
            Window.RequestFeature(WindowFeatures.NoTitle);
            Initialize();
        }

        private void Initialize()
        {
            SetContentView(Resource.Layout.Event_dialog_view_layout);

            dController = new EventDialogController();
            eventData = new Events();

            IsCreatedOrUpdated = false;
            IsEditEvent = false;

            date_time_select = FindViewById<EventTimeView>(Resource.Id.date_select_event_block);
            descriptionBlock = FindViewById<DescriptionCreateEvent>(Resource.Id.description_event_block);
            newEvent = FindViewById<Button>(Resource.Id.new_event_action);
            detailsAction = FindViewById<Button>(Resource.Id.details_create_event_action);
            cancelAction = FindViewById<Button>(Resource.Id.cancel_create_event_action);

            descriptionBlock.Cx = cx;

            newEvent.Click += Handle_Click;
            detailsAction.Click += Handle_Click;
            cancelAction.Click += Handle_Click;
        }

        public bool IsEditEvent { get; set; }
        public bool IsCreatedOrUpdated { get; set; }

        public Events EventData
        {
            set
            {
                eventData = value;
                dController.EventData = eventData;

                var ff3 = new DateRangeModel
                {
                    Year = eventData.Start_DT.Year,
                    Month = eventData.Start_DT.Month,
                    DayOfMonth = eventData.Start_DT.Day,
                    HourOfDay = eventData.Start_DT.Hour,
                    Minutes = eventData.Start_DT.Minute,
                    Duration = eventData.Duration
                };

                date_time_select.CurrentDate = ff3;

                if (IsEditEvent)
                {
                    descriptionBlock.EventDescription = eventData.Description;
                    var user = JsonConvert.DeserializeObject<AccountInfo>(eventData.EventMembers.LastOrDefault().MemberInfo);
                    descriptionBlock.ClientInfo = user;
                }

            }
            private get
            {
                return eventData;
            }
        }

        public void Invalidate()
        {
            descriptionBlock.Clean();
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.new_event_action))
                {
                    if (!descriptionBlock.IsMemberExist())
                    {
                        var dialog = new ClientNotAddedDialog(cx);
                        dialog.Show();
                    }
                    else
                    {
                        if (!IsEditEvent)
                            CreateEvent();
                        else
                            SaveEditEvent();
                    }
                }
                else if (button.Id.Equals(Resource.Id.details_create_event_action))
                {

                }
                else if (button.Id.Equals(Resource.Id.cancel_create_event_action))
                {
                    this.Dismiss();
                }
            }
        }

        private void CreateEvent()
        {
            var myCal = CultureInfo.InvariantCulture.Calendar;

            var date_sel = date_time_select.DateSelected;
            var time_sel = date_time_select.TimeSelected;
            var dur = date_time_select.Duration;
            var end_date = myCal.AddHours(date_sel, dur.Hours);
            end_date = myCal.AddMinutes(date_sel, dur.Minutes);

            if (dController.CanCreateEvent(date_sel, end_date))
            {
                var memb = descriptionBlock.MembersInfo;
                var ev_desc = descriptionBlock.EventDescription;

                var m = new List<EventMembers>();

                memb.ForEach(x =>
                {
                    m.Add(new EventMembers
                    {
                        AccountContactId = x.Id,
                        MemberInfo = JsonConvert.SerializeObject(x)
                    });
                });


                dController.EventData.Start_DT = date_sel;
                dController.EventData.End_DT = end_date;
                dController.EventData.Duration = dur;
                dController.EventData.EventMembers = m;
                dController.EventData.Description = ev_desc;
                dController.EventData.CreatorId = m[0].AccountContactId;
                dController.EventData.OwnerId = m[0].AccountContactId;

                dController.NewEvent();
                IsCreatedOrUpdated = true;

                this.Dismiss();
            }

        }

        private void SaveEditEvent()
        {
            var myCal = CultureInfo.InvariantCulture.Calendar;

            var date_sel = date_time_select.DateSelected;
            var dur = date_time_select.Duration;
            var end_date = myCal.AddHours(date_sel, dur.Hours);
            end_date = myCal.AddMinutes(date_sel, dur.Minutes);

            if (dController.CanCreateEvent(date_sel, end_date))
            {
                var memb = descriptionBlock.MembersInfo;
                var ev_desc = descriptionBlock.EventDescription;

                var m = new List<EventMembers>();

                memb.ForEach(x =>
                {
                    m.Add(new EventMembers
                    {
                        EventId = dController.EventData.EventId,
                        AccountContactId = x.Id,
                        MemberInfo = JsonConvert.SerializeObject(x)
                    });
                });

                dController.EventData.Start_DT = date_sel;
                dController.EventData.End_DT = end_date;
                dController.EventData.Duration = dur;
                dController.EventData.EventMembers = m;
                dController.EventData.Description = ev_desc;

                dController.UpdateEvent();

                IsCreatedOrUpdated = true;
                this.Dismiss();
            }

        }

    }
}

