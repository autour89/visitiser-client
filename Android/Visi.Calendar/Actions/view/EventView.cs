﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Android.Graphics;
using Visi.Generic.DAL.DataModel;

namespace Visi.Calendar.Actions.view
{
    public class EventView : FrameLayout
    {
        private Context cx;
        private TextView eventName;
        private LinearLayout linearRoot;
        private AbsoluteLayout.LayoutParams lp;

        public delegate void ClickEventHandler(object sender, int e);

        public event ClickEventHandler ItemClickEvent;

        void OnHandleClickEvent(object sender, int position)
        {
            if (ItemClickEvent != null)
                ItemClickEvent(sender, position);
        }

        public EventView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            cx = context;
            Initialize();
        }

        private void Initialize()
        {
            Inflate(cx, Resource.Layout.EventView_layout, this);

            linearRoot = FindViewById<LinearLayout>(Resource.Id.linear_event_root);
            eventName = FindViewById<TextView>(Resource.Id.event_name);

            lp = new AbsoluteLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent, 1, 0);

            this.Click += Handle_Click;
        }

        public Events EventInfo { get; set; }

        private void Handle_Click(object sender, EventArgs e)
        {
            if (EventInfo != null)
                OnHandleClickEvent(this, EventInfo.EventId);
            else
                OnHandleClickEvent(this, 0);
        }

        public TextView EventName => eventName;

        public void SetHeight(double value)
        {
            var height = value * 60;
            linearRoot.LayoutParameters.Height = Convert.ToInt32(height);
        }

        public void SetWidth(int value)
        {
            linearRoot.LayoutParameters.Width = value;
        }


        public void SetHourPosition(double time)
        {
            var pos = time * 60;
            lp.Y = Convert.ToInt32(pos);
            this.LayoutParameters = lp;
        }

        public void SetEventX(int value)
        {
            lp.X = value;
            this.LayoutParameters = lp;
        }

        public void SetBackgroud(Color color)
        {
            this.SetBackgroundColor(color);
            this.Alpha = 384;
        }

    }
}