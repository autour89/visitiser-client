﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Visi.Calendar.Actions.view
{
    public class AddEventClientDialog : Dialog
    {
        private Context cx;

        private Button newEvent, detailsAction, cancelAction;
        private EventTimeView date_time_select;
        private DescriptionCreateEvent descriptionBlock;

        public AddEventClientDialog(Context context, IAttributeSet attrs) : base(context)
        {
            cx = context;
            var feature = this.Window;
            feature.RequestFeature(WindowFeatures.NoTitle);

            Initialize();
        }

        private void Initialize()
        {
            SetContentView(Resource.Layout.Add_event_client_dialog_layout);




        }

    }
}