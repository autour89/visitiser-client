﻿
using System;
using System.Collections.Generic;
using System.Linq;

using Android.Content;
using Android.Util;
using Android.Views;
using Android.Widget;

using Visi.Calendar.Presenter;
using Visi.Generic.Models;
using Android.Graphics;

namespace Visi.Calendar.Actions.view
{
    public class EventTimeView : FrameLayout
    {
        private EventTimeController eventController;
        private Context cx;

        private WheelView pickerOne, pickerTwo;
        private ToggleButton tb_pickdatetime_mode_date, tb_pickdatetime_mode_durability, tb_pickdatetime_mode_start, tb_pickdatetime_mode_end;
        private LinearLayout toggle_group;
        private TextView wheelDots;
        private View stick1_r, stick2_r, stick3_r;

        public EventTimeView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            cx = context;
            Initialize();
        }

        public EventTimeView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
        {
            cx = context;
            Initialize();
        }

        private void Initialize()
        {
            Inflate(cx, Resource.Layout.Event_date_select_layout, this);

            eventController = new EventTimeController();

            wheelDots = FindViewById<TextView>(Resource.Id.dotsForWheels);
            tb_pickdatetime_mode_date = FindViewById<ToggleButton>(Resource.Id.tb_pickdatetime_mode_date);
            tb_pickdatetime_mode_durability = FindViewById<ToggleButton>(Resource.Id.tb_pickdatetime_mode_durability);
            tb_pickdatetime_mode_start = FindViewById<ToggleButton>(Resource.Id.tb_pickdatetime_mode_start);
            tb_pickdatetime_mode_end = FindViewById<ToggleButton>(Resource.Id.tb_pickdatetime_mode_end);
            toggle_group = FindViewById<LinearLayout>(Resource.Id.toggle_group);
            pickerOne = FindViewById<WheelView>(Resource.Id.cycleViewOne);
            pickerTwo = FindViewById<WheelView>(Resource.Id.cycleViewTwo);

            tb_pickdatetime_mode_date.Click += Handle_Click; ;
            tb_pickdatetime_mode_durability.Click += Handle_Click;
            tb_pickdatetime_mode_start.Click += Handle_Click;
            tb_pickdatetime_mode_end.Click += Handle_Click;
            pickerOne.ScrollStateEvent += WheelPicker_ScrollStateEvent;
            pickerTwo.ScrollStateEvent += WheelPicker_ScrollStateEvent;

            stick1_r = FindViewById<View>(Resource.Id.stick1_r);
            stick2_r = FindViewById<View>(Resource.Id.stick2_r);
            stick3_r = FindViewById<View>(Resource.Id.stick3_r);
        }

        public DateRangeModel CurrentDate
        {
            set
            {
                eventController.DateModel = value;
                eventController.CreateTimeModel();
                eventController.NowIndex();
                eventController.InitSelect();
                ChangeVisibilityBlock(3);
                InitTimeBlock();
            }
        }

        public DateTime DateSelected
        {
            get
            {
                var obj = eventController.DateSelect;
                var mnth = obj.Data1 as List<DateTime>;
                var dy = obj.Data2 as List<DateTime>;

                var dt_mnth = mnth[obj.NowIndex1];
                var dt_dy = dy[obj.NowIndex2];

                var obj_time = eventController.TimeSelect;
                var hr = obj_time.Data1 as List<string>;
                var mn = obj_time.Data2 as List<string>;

                var dt_hr = hr[obj_time.NowIndex1];
                var dt_mn = mn[obj_time.NowIndex2];

                return new DateTime(dt_mnth.Year, dt_mnth.Month, dt_dy.Day, Convert.ToInt32(dt_hr), Convert.ToInt32(dt_mn), 0);
            }
        }

        public TimeSpan TimeSelected
        {
            get
            {
                var obj = eventController.TimeSelect;
                var hr = obj.Data1 as List<string>;
                var mn = obj.Data2 as List<string>;

                var dt_hr = hr[obj.NowIndex1];
                var dt_mn = mn[obj.NowIndex2];

                return new TimeSpan(Convert.ToInt32(dt_hr), Convert.ToInt32(dt_mn), 0);
            }
        }

        public TimeSpan Duration
        {
            get
            {
                var obj = eventController.DurationSelect;
                var hr = obj.Data1 as List<string>;
                var mn = obj.Data2 as List<string>;

                var dt_hr = hr[obj.NowIndex1];
                var dt_mn = mn[obj.NowIndex2];

                return new TimeSpan(Convert.ToInt32(dt_hr), Convert.ToInt32(dt_mn), 0);
            }
        }

        private void WheelPicker_ScrollStateEvent(object sender, int e)
        {
            var wheelpicker = sender as WheelView;

            if (wheelpicker != null)
            {
                switch (e)
                {
                    case 0:
                        var pos1 = pickerOne.CurrentItemPosition;
                        var pos2 = pickerTwo.CurrentItemPosition;

                        if (wheelpicker.Id.Equals(Resource.Id.cycleViewOne))
                        {
                            if (eventController.IsDateSet)
                            {
                                var modelMonth = eventController.DateSelect.Data1 as List<DateTime>;//> MonthsDT[pos1];
                                var modelDays = eventController.DateSelect.Data2 as List<DateTime>;//> MonthsDT[pos1];

                                var date1 = modelMonth[pos1];
                                var date2 = modelDays[pos2];

                                if (date2.Day > eventController.DaysInMonth(pos1))
                                {
                                    pickerTwo.SelectedItemPosition = pos2 + 1;
                                    eventController.DateSelect.NowIndex2 = pos2 + 1;
                                }
                                else
                                {
                                    var ind = modelDays.FindIndex(x => x.Month.Equals(date1.Month) && x.Year.Equals(date1.Year) && x.Day.Equals(date2.Day));
                                    pickerTwo.SelectedItemPosition = ind;
                                    eventController.DateSelect.NowIndex2 = ind;

                                }
                                wheelpicker.SelectedItemPosition = wheelpicker.CurrentItemPosition;
                                eventController.DateSelect.NowIndex1 = wheelpicker.CurrentItemPosition;

                            }
                            else if (eventController.IsTime)
                            {
                                wheelpicker.SelectedItemPosition = wheelpicker.CurrentItemPosition;
                                eventController.TimeSelect.NowIndex1 = wheelpicker.CurrentItemPosition;
                                ChangeTime();
                            }
                            else if (eventController.IsDuration)
                            {
                                wheelpicker.SelectedItemPosition = wheelpicker.CurrentItemPosition;
                                eventController.DurationSelect.NowIndex1 = wheelpicker.CurrentItemPosition;
                                ChangeDiration();
                            }
                        }
                        else if (wheelpicker.Id.Equals(Resource.Id.cycleViewTwo))
                        {
                            if (eventController.IsDateSet)
                            {
                                var modelDays = eventController.DateSelect.Data2 as List<DateTime>;//> MonthsDT[pos1];

                                var date = modelDays[pos2];

                                if (!eventController.IsMonthEqual(pos1, pos2))
                                {
                                    var model1 = eventController.DateSelect.Data1 as List<DateTime>;
                                    var ind = model1.FindIndex(x => x.Month.Equals(date.Month));
                                    pickerOne.SelectedItemPosition = ind;
                                    eventController.DateSelect.NowIndex1 = ind;
                                }
                                wheelpicker.SelectedItemPosition = wheelpicker.CurrentItemPosition;
                                eventController.DateSelect.NowIndex2 = wheelpicker.CurrentItemPosition;
                            }
                            else if (eventController.IsTime)
                            {
                                wheelpicker.SelectedItemPosition = wheelpicker.CurrentItemPosition;
                                eventController.TimeSelect.NowIndex2 = wheelpicker.CurrentItemPosition;
                                ChangeTime();

                            }
                            else if (eventController.IsDuration)
                            {
                                wheelpicker.SelectedItemPosition = wheelpicker.CurrentItemPosition;
                                eventController.DurationSelect.NowIndex2 = wheelpicker.CurrentItemPosition;
                                ChangeDiration();
                            }
                        }
                        break;
                }
            }

        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var togglebutton = sender as ToggleButton;

            if (togglebutton != null)
            {
                if (togglebutton.Id.Equals(Resource.Id.tb_pickdatetime_mode_date))
                {
                    if (!eventController.IsDateSet)
                    {
                        var listM = eventController.DateSelect.Data1 as List<DateTime>;
                        var listD = eventController.DateSelect.Data2 as List<DateTime>;

                        pickerOne.Data = listM.Select(x => x.ToString("MMM")).ToList();
                        pickerTwo.Data = listD.Select(x => x.Day.ToString()).ToList();
                        pickerOne.SelectedItemPosition = eventController.DateSelect.NowIndex1;
                        pickerTwo.SelectedItemPosition = eventController.DateSelect.NowIndex2;
                    }
                    ChangeVisibilityBlock(1);
                }
                else if (togglebutton.Id.Equals(Resource.Id.tb_pickdatetime_mode_durability))
                {
                    if (!eventController.IsDuration)
                    {
                        if (wheelDots.Visibility == ViewStates.Gone)
                            wheelDots.Visibility = ViewStates.Visible;

                        pickerOne.Data = eventController.DurationSelect.Data1 as List<string>;
                        pickerTwo.Data = eventController.DurationSelect.Data2 as List<string>;
                        pickerOne.SelectedItemPosition = eventController.DurationSelect.NowIndex1;
                        pickerTwo.SelectedItemPosition = eventController.DurationSelect.NowIndex2;
                    }
                    ChangeVisibilityBlock(2);

                }
                else if (togglebutton.Id.Equals(Resource.Id.tb_pickdatetime_mode_start))
                {
                    if (!eventController.IsTime)
                    {

                        if (wheelDots.Visibility == ViewStates.Gone)
                            wheelDots.Visibility = ViewStates.Visible;

                        pickerOne.Data = eventController.TimeSelect.Data1 as List<string>;
                        pickerTwo.Data = eventController.TimeSelect.Data2 as List<string>;
                        pickerOne.SelectedItemPosition = eventController.TimeSelect.NowIndex1;
                        pickerTwo.SelectedItemPosition = eventController.TimeSelect.NowIndex2;
                    }
                    ChangeVisibilityBlock(3);

                }
                else if (togglebutton.Id.Equals(Resource.Id.tb_pickdatetime_mode_end))
                {
                    if (!eventController.IsDuration)
                    {
                        if (wheelDots.Visibility == ViewStates.Gone)
                            wheelDots.Visibility = ViewStates.Visible;

                        pickerOne.Data = eventController.DurationSelect.Data1 as List<string>;
                        pickerTwo.Data = eventController.DurationSelect.Data2 as List<string>;
                        pickerOne.SelectedItemPosition = eventController.DurationSelect.NowIndex1;
                        pickerTwo.SelectedItemPosition = eventController.DurationSelect.NowIndex2;
                    }
                    ChangeVisibilityBlock(4);

                }

            }
        }

        private void InitTimeBlock()
        {

            pickerOne.Data = eventController.TimeSelect.Data1 as List<string>;
            pickerTwo.Data = eventController.TimeSelect.Data2 as List<string>;
            pickerOne.SelectedItemPosition = eventController.TimeSelect.NowIndex1;
            pickerTwo.SelectedItemPosition = eventController.TimeSelect.NowIndex2;

            ChangeTime();
            ChangeDiration();

            var d = eventController.DateModel;
            var dt = new DateTime(d.Year, d.Month, d.DayOfMonth);
            tb_pickdatetime_mode_date.Text = dt.ToShortDateString() + ", " + dt.ToString("MMM");
            tb_pickdatetime_mode_date.TextOn = dt.ToShortDateString() + ", " + dt.ToString("MMM");
            tb_pickdatetime_mode_date.TextOff = dt.ToShortDateString() + ", " + dt.ToString("MMM");

        }

        private void ChangeVisibilityBlock(int flag)
        {
            switch (flag)
            {
                case 1:
                    tb_pickdatetime_mode_date.Checked = true;
                    tb_pickdatetime_mode_durability.Checked = false;
                    tb_pickdatetime_mode_start.Checked = false;
                    tb_pickdatetime_mode_end.Checked = false;
                    toggle_group.SetBackgroundResource(Resource.Drawable.shape_border_for_group_button);

                    wheelDots.Visibility = ViewStates.Gone;
                    eventController.IsDateSet = true;
                    eventController.IsDuration = false;
                    eventController.IsTime = false;



                    stick1_r.SetBackgroundColor(Color.ParseColor("#ffffff"));
                    stick2_r.SetBackgroundColor(Color.ParseColor("#b1bbc6"));
                    stick3_r.SetBackgroundColor(Color.ParseColor("#b1bbc6"));

                    break;
                case 2:
                    tb_pickdatetime_mode_date.Checked = false;
                    tb_pickdatetime_mode_durability.Checked = true;
                    tb_pickdatetime_mode_start.Checked = false;
                    tb_pickdatetime_mode_end.Checked = false;
                    toggle_group.SetBackgroundResource(Resource.Drawable.shape_border_for_group_button);
                    eventController.IsDateSet = false;
                    eventController.IsDuration = true;
                    eventController.IsTime = false;

                    stick1_r.SetBackgroundColor(Color.ParseColor("#b1bbc6"));
                    stick2_r.SetBackgroundColor(Color.ParseColor("#ffffff"));
                    stick3_r.SetBackgroundColor(Color.ParseColor("#b1bbc6"));

                    break;
                case 3:
                    tb_pickdatetime_mode_date.Checked = false;
                    tb_pickdatetime_mode_durability.Checked = false;
                    tb_pickdatetime_mode_start.Checked = true;
                    tb_pickdatetime_mode_end.Checked = false;

                    toggle_group.SetBackgroundResource(0);
                    eventController.IsDateSet = false;
                    eventController.IsDuration = false;
                    eventController.IsTime = true;

                    stick1_r.SetBackgroundColor(Color.ParseColor("#b1bbc6"));
                    stick2_r.SetBackgroundColor(Color.ParseColor("#b1bbc6"));
                    stick3_r.SetBackgroundColor(Color.ParseColor("#ffffff"));

                    break;
                case 4:
                    tb_pickdatetime_mode_date.Checked = false;
                    tb_pickdatetime_mode_durability.Checked = false;
                    tb_pickdatetime_mode_start.Checked = false;
                    tb_pickdatetime_mode_end.Checked = true;
                    toggle_group.SetBackgroundResource(0);
                    eventController.IsDateSet = false;
                    eventController.IsDuration = true;
                    eventController.IsTime = false;
                    stick3_r.SetBackgroundColor(Color.ParseColor("#ffffff"));


                    stick1_r.SetBackgroundColor(Color.ParseColor("#b1bbc6"));
                    stick2_r.SetBackgroundColor(Color.ParseColor("#b1bbc6"));

                    break;
            }
        }

        private void ChangeTime()
        {
            var tt = eventController.TimeSelect.Data1 as List<string>;
            var tt2 = eventController.TimeSelect.Data2 as List<string>;

            var t1 = tt[eventController.TimeSelect.NowIndex1];
            var t2 = tt2[eventController.TimeSelect.NowIndex2];

            tb_pickdatetime_mode_start.TextOn = t1 + " : " + t2;
            tb_pickdatetime_mode_start.TextOff = t1 + " : " + t2;
            tb_pickdatetime_mode_start.Text = t1 + " : " + t2;
        }

        private void ChangeDiration()
        {
            var tt = eventController.DurationSelect.Data1 as List<string>;
            var tt2 = eventController.DurationSelect.Data2 as List<string>;

            var t1 = tt[eventController.DurationSelect.NowIndex1];
            var t2 = tt2[eventController.DurationSelect.NowIndex2];

            //var time_array = eventController.TimeSelect.Data1 as List<string>;
            //var start_t = Convert.ToInt32(time_array[eventController.TimeSelect.NowIndex1]) + ;


            //EndTimeView
            tb_pickdatetime_mode_end.TextOn = t1 + " : " + t2;
            tb_pickdatetime_mode_end.TextOff = t1 + " : " + t2;
            tb_pickdatetime_mode_end.Text = t1 + " : " + t2;
        }


    }

}



//using System;
//using System.Collections.Generic;
//using System.Linq;

//using Android.Content;
//using Android.Util;
//using Android.Views;
//using Android.Widget;

//using Visi.Calendar.Presenter;
//using Visi.Generic.Models;
//using Android.Graphics;

//namespace Visi.Calendar.Actions.view
//{
//    public class EventTimeView : FrameLayout
//    {
//        private EventTimeController eventController;
//        private Context cx;

//        private WheelView pickerOne, pickerTwo;
//        private ToggleButton tb_pickdatetime_mode_date, tb_pickdatetime_mode_durability, tb_pickdatetime_mode_start, tb_pickdatetime_mode_end;
//        private LinearLayout toggle_group;
//        private TextView wheelDots;
//        private View stick1_r,  stick2_r, stick3_r;

//        public EventTimeView(Context context, IAttributeSet attrs) : base(context, attrs)
//        {
//            cx = context;
//            Initialize();
//        }

//        public EventTimeView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
//        {
//            cx = context;
//            Initialize();
//        }

//        private void Initialize()
//        {
//            Inflate(cx, Resource.Layout.Event_date_select_layout, this);

//            eventController = new EventTimeController();

//            wheelDots = FindViewById<TextView>(Resource.Id.dotsForWheels);
//            tb_pickdatetime_mode_date = FindViewById<ToggleButton>(Resource.Id.tb_pickdatetime_mode_date);
//            tb_pickdatetime_mode_durability = FindViewById<ToggleButton>(Resource.Id.tb_pickdatetime_mode_durability);
//            tb_pickdatetime_mode_start = FindViewById<ToggleButton>(Resource.Id.tb_pickdatetime_mode_start);
//            tb_pickdatetime_mode_end = FindViewById<ToggleButton>(Resource.Id.tb_pickdatetime_mode_end);
//            toggle_group = FindViewById<LinearLayout>(Resource.Id.toggle_group);
//            pickerOne = FindViewById<WheelView>(Resource.Id.cycleViewOne);
//            pickerTwo = FindViewById<WheelView>(Resource.Id.cycleViewTwo);            

//            tb_pickdatetime_mode_date.Click += Handle_Click; ;
//            tb_pickdatetime_mode_durability.Click += Handle_Click;
//            tb_pickdatetime_mode_start.Click += Handle_Click;
//            tb_pickdatetime_mode_end.Click += Handle_Click;
//            pickerOne.ScrollStateEvent += WheelPicker_ScrollStateEvent;
//            pickerTwo.ScrollStateEvent += WheelPicker_ScrollStateEvent;


//            tb_pickdatetime_mode_start.Checked = true;

//            stick1_r = FindViewById<View>(Resource.Id.stick1_r);
//            stick2_r = FindViewById<View>(Resource.Id.stick2_r);
//            stick3_r = FindViewById<View>(Resource.Id.stick3_r);
//        }

//        public DateRangeModel CurrentDate
//        {
//            set
//            {
//                eventController.DateModel = value;
//                eventController.CreateTimeModel();
//                eventController.NowIndex();
//                eventController.InitSelect();
//                ChangeVisibilityBlock(3);
//                InitTimeBlock();
//            }
//        }

//        public DateTime DateSelected
//        {
//            get
//            {
//                var obj = eventController.DateSelect;
//                var mnth = obj.Data1 as List<DateTime>;
//                var dy = obj.Data2 as List<DateTime>;

//                var dt_mnth = mnth[obj.NowIndex1];
//                var dt_dy = dy[obj.NowIndex2];

//                var obj_time = eventController.TimeSelect;
//                var hr = obj_time.Data1 as List<string>;
//                var mn = obj_time.Data2 as List<string>;

//                var dt_hr = hr[obj_time.NowIndex1];
//                var dt_mn = mn[obj_time.NowIndex2];

//                return new DateTime(dt_mnth.Year, dt_mnth.Month, dt_dy.Day, Convert.ToInt32(dt_hr),Convert.ToInt32(dt_mn),0);
//            }
//        }

//        public TimeSpan TimeSelected
//        {
//            get
//            {
//                var obj = eventController.TimeSelect;
//                var hr = obj.Data1 as List<string>;
//                var mn = obj.Data2 as List<string>;

//                var dt_hr = hr[obj.NowIndex1];
//                var dt_mn = mn[obj.NowIndex2];

//                return new TimeSpan(Convert.ToInt32(dt_hr),Convert.ToInt32(dt_mn),0);
//            }
//        }
//        public TimeSpan EndtimeSelected
//        {
//            get
//            {
//                var obj = eventController.EndtimeSelect;
//                var hr = obj.Data1 as List<string>;
//                var mn = obj.Data2 as List<string>;

//                var dt_hr = hr[obj.NowIndex1];
//                var dt_mn = mn[obj.NowIndex2];

//                return new TimeSpan(Convert.ToInt32(dt_hr), Convert.ToInt32(dt_mn), 0);
//            }
//        }

//        public TimeSpan Duration
//        {
//            get
//            {
//                var obj = eventController.DurationSelect;
//                var hr = obj.Data1 as List<string>;
//                var mn = obj.Data2 as List<string>;

//                var dt_hr = hr[obj.NowIndex1];
//                var dt_mn = mn[obj.NowIndex2];

//                return new TimeSpan(Convert.ToInt32(dt_hr), Convert.ToInt32(dt_mn), 0);
//            }
//        }

//        private void WheelPicker_ScrollStateEvent(object sender, int e)
//        {
//            var wheelpicker = sender as WheelView;

//            if (wheelpicker != null)
//            {
//                switch (e)
//                {
//                    case 0:
//                        var pos1 = pickerOne.CurrentItemPosition;
//                        var pos2 = pickerTwo.CurrentItemPosition;

//                        if (wheelpicker.Id.Equals(Resource.Id.cycleViewOne))
//                        {
//                            if (eventController.IsDateSet)
//                            {
//                                var modelMonth = eventController.DateSelect.Data1 as List<DateTime>;//> MonthsDT[pos1];
//                                var modelDays = eventController.DateSelect.Data2 as List<DateTime>;//> MonthsDT[pos1];

//                                var date1 = modelMonth[pos1];
//                                var date2 = modelDays[pos2];

//                                if (date2.Day > eventController.DaysInMonth(pos1))
//                                {
//                                    pickerTwo.SelectedItemPosition = pos2 + 1;
//                                    eventController.DateSelect.NowIndex2 = pos2 + 1;
//                                }
//                                else
//                                {

//                                    var ind = modelDays.FindIndex(x => x.Month.Equals(date1.Month) && x.Year.Equals(date1.Year) && x.Day.Equals(date2.Day));
//                                    pickerTwo.SelectedItemPosition = ind;
//                                    eventController.DateSelect.NowIndex2 = ind;

//                                }
//                                wheelpicker.SelectedItemPosition = wheelpicker.CurrentItemPosition;
//                                eventController.DateSelect.NowIndex1 = wheelpicker.CurrentItemPosition;

//                            }
//                            else if (eventController.IsTime)
//                            {
//                                wheelpicker.SelectedItemPosition = wheelpicker.CurrentItemPosition;
//                                eventController.TimeSelect.NowIndex1 = wheelpicker.CurrentItemPosition;

//                                ChangeStartTime();

//                            }
//                            else if (eventController.IsDuration)
//                            {
//                                wheelpicker.SelectedItemPosition = wheelpicker.CurrentItemPosition;
//                                eventController.DurationSelect.NowIndex1 = wheelpicker.CurrentItemPosition;
//                                ChangeEndTimeByDuration();
//                            }
//                            else if (eventController.IsEndtime)
//                            {
//                                wheelpicker.SelectedItemPosition = wheelpicker.CurrentItemPosition;
//                                eventController.EndtimeSelect.NowIndex1 = wheelpicker.CurrentItemPosition;
//                                ChangeEndTime();
//                            }

//                        }
//                        else if (wheelpicker.Id.Equals(Resource.Id.cycleViewTwo))
//                        {
//                            if (eventController.IsDateSet)
//                            {
//                                var modelDays = eventController.DateSelect.Data2 as List<DateTime>;//> MonthsDT[pos1];

//                                var date = modelDays[pos2];

//                                if (!eventController.IsMonthEqual(pos1, pos2))
//                                {
//                                    var model1 = eventController.DateSelect.Data1 as List<DateTime>;
//                                    var ind = model1.FindIndex(x => x.Month.Equals(date.Month));
//                                    pickerOne.SelectedItemPosition = ind;
//                                    eventController.DateSelect.NowIndex1 = ind;
//                                }
//                                wheelpicker.SelectedItemPosition = wheelpicker.CurrentItemPosition;
//                                eventController.DateSelect.NowIndex2 = wheelpicker.CurrentItemPosition;
//                            }
//                            else if (eventController.IsTime)
//                            {
//                                wheelpicker.SelectedItemPosition = wheelpicker.CurrentItemPosition;
//                                eventController.TimeSelect.NowIndex2 = wheelpicker.CurrentItemPosition;
//                                ChangeStartTime();
//                            }
//                            else if (eventController.IsDuration)
//                            {
//                                wheelpicker.SelectedItemPosition = wheelpicker.CurrentItemPosition;
//                                eventController.DurationSelect.NowIndex2 = wheelpicker.CurrentItemPosition;

//                                ChangeEndTimeByDuration(); 

//                            }
//                            else if (eventController.IsEndtime)
//                            {
//                                wheelpicker.SelectedItemPosition = wheelpicker.CurrentItemPosition;
//                                eventController.EndtimeSelect.NowIndex2 = wheelpicker.CurrentItemPosition;
//                                ChangeEndTime();
//                            }
//                        }
//                        break;
//                }
//            }

//        }

//        private void Handle_Click(object sender, EventArgs e)
//        {
//            var togglebutton = sender as ToggleButton;

//            if (togglebutton != null)
//            {
//                if (togglebutton.Id.Equals(Resource.Id.tb_pickdatetime_mode_date))
//                {                    
//                    if (!eventController.IsDateSet)
//                    {
//                        var listM = eventController.DateSelect.Data1 as List<DateTime>;
//                        var listD = eventController.DateSelect.Data2 as List<DateTime>;

//                        pickerOne.Data = listM.Select(x => x.ToString("MMM")).ToList();
//                        pickerTwo.Data = listD.Select(x => x.Day.ToString()).ToList();
//                        pickerOne.SelectedItemPosition = eventController.DateSelect.NowIndex1;
//                        pickerTwo.SelectedItemPosition = eventController.DateSelect.NowIndex2;
//                    }
//                    ChangeVisibilityBlock(1);
//                }
//                else if (togglebutton.Id.Equals(Resource.Id.tb_pickdatetime_mode_durability))
//                {
//                    if (!eventController.IsDuration)
//                    {
//                        if (wheelDots.Visibility == ViewStates.Gone)
//                            wheelDots.Visibility = ViewStates.Visible;

//                        pickerOne.Data = eventController.DurationSelect.Data1 as List<string>;
//                        pickerTwo.Data = eventController.DurationSelect.Data2 as List<string>;
//                        pickerOne.SelectedItemPosition = eventController.DurationSelect.NowIndex1;
//                        pickerTwo.SelectedItemPosition = eventController.DurationSelect.NowIndex2;
//                    }
//                    ChangeVisibilityBlock(2);

//                }
//                else if (togglebutton.Id.Equals(Resource.Id.tb_pickdatetime_mode_start))
//                {
//                    if (!eventController.IsTime)
//                    {

//                        if (wheelDots.Visibility == ViewStates.Gone)
//                            wheelDots.Visibility = ViewStates.Visible;

//                        pickerOne.Data = eventController.TimeSelect.Data1 as List<string>;
//                        pickerTwo.Data = eventController.TimeSelect.Data2 as List<string>;
//                        pickerOne.SelectedItemPosition = eventController.TimeSelect.NowIndex1;
//                        pickerTwo.SelectedItemPosition = eventController.TimeSelect.NowIndex2;
//                    }
//                    ChangeVisibilityBlock(3);

//                }
//                else if (togglebutton.Id.Equals(Resource.Id.tb_pickdatetime_mode_end))
//                {
//                    if (!eventController.IsEndtime)
//                    {
//                        if (wheelDots.Visibility == ViewStates.Gone)
//                            wheelDots.Visibility = ViewStates.Visible;

//                        pickerOne.Data = eventController.EndtimeSelect.Data1 as List<string>;
//                        pickerTwo.Data = eventController.EndtimeSelect.Data2 as List<string>;
//                        pickerOne.SelectedItemPosition = eventController.EndtimeSelect.NowIndex1;
//                        pickerTwo.SelectedItemPosition = eventController.EndtimeSelect.NowIndex2;
//                    }
//                    ChangeVisibilityBlock(4);

//                }

//            }
//        }

//        private void InitTimeBlock()
//        {

//            pickerOne.Data = eventController.TimeSelect.Data1 as List<string>;
//            pickerTwo.Data = eventController.TimeSelect.Data2 as List<string>;
//            pickerOne.SelectedItemPosition = eventController.TimeSelect.NowIndex1;
//            pickerTwo.SelectedItemPosition = eventController.TimeSelect.NowIndex2;

//            //tb_pickdatetime_mode_start.TextOn = "time";
//            //tb_pickdatetime_mode_start.TextOff = "time";
//            //tb_pickdatetime_mode_start.Text = "time";
//            ChangeStartTime();



//            tb_pickdatetime_mode_end.TextOn = "...";
//            tb_pickdatetime_mode_end.TextOff = "...";
//            tb_pickdatetime_mode_end.Text = "...";


//            var d = eventController.DateModel;
//            var dt = new DateTime(d.Year, d.Month, d.DayOfMonth);
//            tb_pickdatetime_mode_date.Text = dt.ToShortDateString() + ", " + dt.ToString("MMM");
//            tb_pickdatetime_mode_date.TextOn = dt.ToShortDateString() + ", " + dt.ToString("MMM");
//            tb_pickdatetime_mode_date.TextOff = dt.ToShortDateString() + ", " + dt.ToString("MMM");


//            //tb_pickdatetime_mode_start.TextOn = eventController.GetHourStr();
//            //tb_pickdatetime_mode_start.TextOff = eventController.GetHourStr();
//            //tb_pickdatetime_mode_start.Text = eventController.GetHourStr();

//            //tb_pickdatetime_mode_end.TextOn = eventController.GetDurationStr();
//            //tb_pickdatetime_mode_end.TextOff = eventController.GetDurationStr();
//            //tb_pickdatetime_mode_end.Text =  eventController.GetDurationStr();

//        }


//        private void ChangeStartTime()
//        {
//            var start_h = eventController.TimeSelect.Data1 as List<string>;
//            var start_m = eventController.TimeSelect.Data2 as List<string>;

//            var t1 = start_h[eventController.TimeSelect.NowIndex1];
//            var t2 = start_m[eventController.TimeSelect.NowIndex2];

//            eventController.DurationSelect.NowIndex1 = eventController.EndtimeSelect.NowIndex1 - eventController.TimeSelect.NowIndex1;
//            eventController.DurationSelect.NowIndex2 = eventController.EndtimeSelect.NowIndex2 - eventController.TimeSelect.NowIndex2;

//            tb_pickdatetime_mode_start.TextOn = t1 + " : " + t2;
//            tb_pickdatetime_mode_start.TextOff = t1 + " : " + t2;
//            tb_pickdatetime_mode_start.Text = t1 + " : " + t2;
//        }
//        private void ChangeEndTime()
//        {
//            var end_h = eventController.EndtimeSelect.Data1 as List<string>;
//            var end_m = eventController.EndtimeSelect.Data2 as List<string>;

//            var t1 = end_h[eventController.EndtimeSelect.NowIndex1];
//            var t2 = end_m[eventController.EndtimeSelect.NowIndex2];

//            eventController.DurationSelect.NowIndex1 = eventController.EndtimeSelect.NowIndex1 - eventController.TimeSelect.NowIndex1;
//            eventController.DurationSelect.NowIndex2 = eventController.EndtimeSelect.NowIndex2 - eventController.TimeSelect.NowIndex2;


//            tb_pickdatetime_mode_end.TextOn = t1 + " : " + t2;
//            tb_pickdatetime_mode_end.TextOff = t1 + " : " + t2;
//            tb_pickdatetime_mode_end.Text = t1 + " : " + t2;

//        }
//        private void ChangeEndTimeByDuration()
//        {
//            //eventController.DurationSelect.NowIndex1 = eventController.EndtimeSelect.NowIndex1 - eventController.TimeSelect.NowIndex1;
//            //eventController.DurationSelect.NowIndex2 = eventController.EndtimeSelect.NowIndex2 - eventController.TimeSelect.NowIndex2;

//            eventController.EndtimeSelect.NowIndex1 = eventController.DurationSelect.NowIndex1;// + eventController.DurationSelect.NowIndex1;
//            eventController.EndtimeSelect.NowIndex2 = eventController.DurationSelect.NowIndex2;// + eventController.DurationSelect.NowIndex2;
//            ChangeEndTime();

//        }

//        private void ChangeVisibilityBlock(int flag)
//        {
//            switch (flag)
//            {
//                case 1:
//                    tb_pickdatetime_mode_date.Checked = true;
//                    tb_pickdatetime_mode_durability.Checked = false;
//                    tb_pickdatetime_mode_start.Checked = false;
//                    tb_pickdatetime_mode_end.Checked = false;
//                    toggle_group.SetBackgroundResource(Resource.Drawable.shape_border_for_group_button);

//                    wheelDots.Visibility = ViewStates.Gone;
//                    eventController.IsDateSet = true;
//                    eventController.IsDuration = false;
//                    eventController.IsTime = false;
//                    eventController.IsEndtime = false;



//                    stick1_r.SetBackgroundColor(Color.ParseColor("#ffffff"));
//                    stick2_r.SetBackgroundColor(Color.ParseColor("#b1bbc6"));
//                    stick3_r.SetBackgroundColor(Color.ParseColor("#b1bbc6"));

//                    break;
//                case 2:
//                    tb_pickdatetime_mode_date.Checked = false;
//                    tb_pickdatetime_mode_durability.Checked = true;
//                    tb_pickdatetime_mode_start.Checked = false;
//                    tb_pickdatetime_mode_end.Checked = false;
//                    toggle_group.SetBackgroundResource(Resource.Drawable.shape_border_for_group_button);
//                    eventController.IsDateSet = false;
//                    eventController.IsDuration = true;
//                    eventController.IsTime = false;
//                    eventController.IsEndtime = false;

//                    stick1_r.SetBackgroundColor(Color.ParseColor("#b1bbc6"));
//                    stick2_r.SetBackgroundColor(Color.ParseColor("#ffffff"));
//                    stick3_r.SetBackgroundColor(Color.ParseColor("#b1bbc6"));

//                    break;
//                case 3:
//                    tb_pickdatetime_mode_date.Checked = false;
//                    tb_pickdatetime_mode_durability.Checked = false;
//                    tb_pickdatetime_mode_start.Checked = true;
//                    tb_pickdatetime_mode_end.Checked = false;

//                    toggle_group.SetBackgroundResource(0);
//                    eventController.IsDateSet = false;
//                    eventController.IsDuration = false;
//                    eventController.IsTime = true;
//                    eventController.IsEndtime = false;

//                    stick1_r.SetBackgroundColor(Color.ParseColor("#b1bbc6"));
//                    stick2_r.SetBackgroundColor(Color.ParseColor("#b1bbc6"));
//                    stick3_r.SetBackgroundColor(Color.ParseColor("#ffffff"));

//                    break;
//                case 4:
//                    tb_pickdatetime_mode_date.Checked = false;
//                    tb_pickdatetime_mode_durability.Checked = false;
//                    tb_pickdatetime_mode_start.Checked = false;
//                    tb_pickdatetime_mode_end.Checked = true;
//                    toggle_group.SetBackgroundResource(0);
//                    eventController.IsDateSet = false;
//                    eventController.IsDuration = false;
//                    eventController.IsTime = false;
//                    eventController.IsEndtime = true;
//                    stick3_r.SetBackgroundColor(Color.ParseColor("#ffffff"));


//                    stick1_r.SetBackgroundColor(Color.ParseColor("#b1bbc6"));
//                    stick2_r.SetBackgroundColor(Color.ParseColor("#b1bbc6"));

//                    break;
//            }
//        }

//    }


//}

