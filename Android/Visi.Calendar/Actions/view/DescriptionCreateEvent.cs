using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.Content;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;

using Visi.Calendar.Presenter;
using Android.Graphics;
using Android.Graphics.Drawables;
using Visi.Generic.Models;

namespace Visi.Calendar.Actions.view
{
    public class DescriptionCreateEvent : FrameLayout
    {
        private EventDescriptionController descriptionCntrlr;

        private Context cx;

        private Button button_add_client, button_create_note, button_create_sms;
        private LinearLayout linear_add_client_form, linear_add_note_form, linear_add_sms_form;

        private EditText userName, userPhone, userNote;
        private ImageView userImg;

        private Timer timer;
        public DescriptionCreateEvent(Context context) : base(context)
        {
            cx = context;
            Initialize();
        }
        public DescriptionCreateEvent(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            cx = context;
            Initialize();
        }

        public DescriptionCreateEvent(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
        {
            cx = context;
            Initialize();
        }

        private void Initialize()
        {

            Inflate(cx, Resource.Layout.Description_create_event_layout, this);

            descriptionCntrlr = new EventDescriptionController();
            IsSearchClient = true;

            userImg = FindViewById<ImageView>(Resource.Id.user_form_pic);
            userName = FindViewById<EditText>(Resource.Id.user_form_name);
            userPhone = FindViewById<EditText>(Resource.Id.user_form_phone);
            userNote = FindViewById<EditText>(Resource.Id.ce_add_note_text);

            button_add_client = FindViewById<Button>(Resource.Id.ce_add_client);
            button_create_note = FindViewById<Button>(Resource.Id.ce_create_note);
            button_create_sms = FindViewById<Button>(Resource.Id.ce_create_sms);

            linear_add_client_form = FindViewById<LinearLayout>(Resource.Id.ce_add_client_form);
            linear_add_note_form = FindViewById<LinearLayout>(Resource.Id.ce_add_note_form);
            linear_add_sms_form = FindViewById<LinearLayout>(Resource.Id.ce_add_sms_form);


            linear_add_client_form.Visibility = ViewStates.Visible;
            linear_add_note_form.Visibility = ViewStates.Gone;
            linear_add_sms_form.Visibility = ViewStates.Gone;


            button_add_client.Click += Handle_Click;
            button_create_note.Click += Handle_Click;
            button_create_sms.Click += Handle_Click;
            userPhone.TextChanged += Handle_TextChanged;
            userPhone.AfterTextChanged += Handle_AfterTextChanged;
        }

        public bool IsSearchClient { get; set; }
        public List<AccountInfo> MembersInfo
        {
            get
            {
                AccountInfo data;
                if (descriptionCntrlr.ClientInfo != null)
                {
                    data = new AccountInfo
                    {
                        Id = descriptionCntrlr.ClientInfo.Id,
                        Logo = descriptionCntrlr.ClientInfo.Logo,
                        Name = userName.Text,
                        PhoneNumber = userPhone.Text
                    };
                }
                else
                {
                    data = new AccountInfo
                    {
                        Name = userName.Text,
                        PhoneNumber = userPhone.Text
                    };
                }

                descriptionCntrlr.SetEventMembers(data);

                return descriptionCntrlr.MembersInfo;
            }
        }

        public AccountInfo ClientInfo
        {
            set
            {
                IsSearchClient = false;
                descriptionCntrlr.ClientInfo = value;
                userName.Text = value.Name;
                userPhone.Text = value.PhoneNumber;
            }
        }
        public string EventDescription
        {
            get { return userNote.Text; }
            set
            {
                userNote.Text = value;
            }
        }
        public string ClientName
        {
            get { return userName.Text; }
            set
            {
                userName.Text = value;
            }
        }
        public string ClientPhone
        {
            get { return userPhone.Text; }
            set
            {
                userPhone.Text = value;
            }
        }

        private string Term { get; set; }

        public Context Cx { get; set; }

        public void Clean()
        {
            userName.Text = "";
            userPhone.Text = "";
            userNote.Text = "";
            //userName.Invalidate();
            //userPhone.Invalidate();
            //userNote.Invalidate();

            descriptionCntrlr.ClientInfo = null;
            descriptionCntrlr.MembersInfo = null;
        }

        public bool IsMemberExist()
        {
            return (userName.Text.Length > 0 && userPhone.Text.Length > 5) ? true : false;
        }
        private void Handle_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            if (timer != null)
                timer.Dispose();
        }

        private void Handle_AfterTextChanged(object sender, Android.Text.AfterTextChangedEventArgs e)
        {
            if (IsSearchClient)
            {
                var term = (sender as EditText).Text;
                Term = term;

                timer = new Timer(x => LoadSearch(), null, 5000, Timeout.Infinite);
            }

        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.ce_add_client))
                {
                    if (linear_add_client_form.Visibility == ViewStates.Visible)
                        linear_add_client_form.Visibility = ViewStates.Gone;
                    else
                        linear_add_client_form.Visibility = ViewStates.Visible;

                    linear_add_note_form.Visibility = ViewStates.Gone;
                    linear_add_sms_form.Visibility = ViewStates.Gone;

                }
                else if (button.Id.Equals(Resource.Id.ce_create_note))
                {
                    linear_add_client_form.Visibility = ViewStates.Gone;


                    if (linear_add_note_form.Visibility == ViewStates.Visible)
                        linear_add_note_form.Visibility = ViewStates.Gone;
                    else
                        linear_add_note_form.Visibility = ViewStates.Visible;

                    linear_add_sms_form.Visibility = ViewStates.Gone;
                }
                else if (button.Id.Equals(Resource.Id.ce_create_sms))
                {

                    linear_add_client_form.Visibility = ViewStates.Gone;
                    linear_add_note_form.Visibility = ViewStates.Gone;

                    if (linear_add_sms_form.Visibility == ViewStates.Visible)
                        linear_add_sms_form.Visibility = ViewStates.Gone;
                    else
                        linear_add_sms_form.Visibility = ViewStates.Visible;
                }
            }

        }

        private void ViewHeight()
        {
            var f = linear_add_client_form.Height;
            var f1 = linear_add_note_form.Height;
            var f2 = linear_add_sms_form.Height;

            var i = 1;
        }
        private void LoadSearch()
        {
            ((AppCompatActivity)Cx).RunOnUiThread(() =>
            {
                descriptionCntrlr.SearchAccount(Term.ParsePhoneStr(HelperStr.PhoneContains));

                if (descriptionCntrlr.ClientInfo != null)
                {
                    userName.Text += descriptionCntrlr.ClientInfo.Name;

                    if (descriptionCntrlr.ClientInfo.Logo != null)
                    {
                        var bmp = BitmapFactory.DecodeByteArray(descriptionCntrlr.ClientInfo.Logo, 0, descriptionCntrlr.ClientInfo.Logo.Length);
                        var img = new BitmapDrawable(Resources, bmp);
                        userImg.SetImageDrawable(img);
                    }

                }
            });


        }
    }
}