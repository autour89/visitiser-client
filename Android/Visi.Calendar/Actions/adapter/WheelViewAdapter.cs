﻿using System;
using System.Collections.Generic;

using Android.Content;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Visi.Calendar.Model;
using Android.Graphics;
using Visi.Calendar.Actions.view;

namespace Visi.Calendar.Actions.adapter
{
    public class WheelViewAdapter : RecyclerView.Adapter
    {
        private Context cx;

        public WheelViewAdapter(Context cx, List<WheelModel> model) : base()
        {
            this.cx = cx;
            DataHolder = model;
        }

        public List<WheelModel> DataHolder { get; set; }

        public event EventHandler<View> ItemClick;

        public int CurrentItem { get; set; }
        public int SelectedItem { get; set; }
        public float TextSize { get; set; }
        public Color TextColor { get; set; }
        public Color SelectedTextColor { get; set; }

        void OnClick(View view)
        {
            if (ItemClick != null)
                ItemClick(this, view);
        }
        public override int ItemCount => Int32.MaxValue;

        public override long GetItemId(int position)
        {
            return base.GetItemId(position);
        }
        public override int GetItemViewType(int position)
        {
            return base.GetItemViewType(position);
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            holder.IsRecyclable = true;
            var viewholder = holder as WheelViewHolder;

            var data = DataHolder[position % DataHolder.Count];

            viewholder.TitleText.Text = data.Text;

            //if (TextSize > 0)
            //    viewholder.TitleText.TextSize = TextSize;

            if (data.IsCurrent)
            {
                if (SelectedTextColor != null)
                    viewholder.TitleText.SetTextColor(SelectedTextColor);
            }
            else
            {
                if (TextColor != null)
                    viewholder.TitleText.SetTextColor(TextColor);
            }
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var id = Resource.Layout.List_item_layout;
            var itemView = LayoutInflater.From(parent.Context).Inflate(id, parent, false);

            return new WheelViewHolder(itemView, OnClick);
        }

    }

    class WheelViewHolder : RecyclerView.ViewHolder
    {
        private View view;

        private TextView titleText;
        public WheelViewHolder(View itemView, Action<View> clickListener) : base(itemView)
        {
            view = itemView;
            titleText = view.FindViewById<TextView>(Resource.Id.title_text);

            view.Click += (sender, e) => clickListener(view);
        }

        public TextView TitleText => titleText;


        public void SetTextSize(float val)
        {
            titleText.SetTextSize(Android.Util.ComplexUnitType.Sp, val);
        }

        public void SetTextColor(Color val)
        {
            titleText.SetTextColor(val);
        }

    }


    public class WheelViewListener : RecyclerView.OnScrollListener
    {
        private LinearLayoutManager lmanager;
        public delegate void ScrolledEventHandler(object sender, int e);
        public delegate void ScrollStateEventHandler(object sender, int e);

        public event ScrollStateEventHandler ScrollStateEvent;
        public event ScrolledEventHandler ScrolledEvent;

        void OnHandleScrolledEvent(object sender, int position)
        {
            if (ScrolledEvent != null)
                ScrolledEvent(sender, position);
        }
        void OnHandleScrollstateEvent(object sender, int position)
        {
            if (ScrollStateEvent != null)
                ScrollStateEvent(sender, position);
        }
        public WheelViewListener(LinearLayoutManager lm)
        {
            lmanager = lm;
        }
        public override void OnScrolled(RecyclerView recyclerView, int dx, int dy)
        {
            base.OnScrolled(recyclerView, dx, dy);

            var pos = lmanager.FindFirstVisibleItemPosition();
            OnHandleScrolledEvent(this, pos);
        }

        public override void OnScrollStateChanged(RecyclerView recyclerView, int newState)
        {
            OnHandleScrollstateEvent(this, newState);

        }
    }


}