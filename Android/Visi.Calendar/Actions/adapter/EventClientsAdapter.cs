﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.Content;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Android.Graphics;

using Visi.Generic.Models;

namespace Visi.Calendar.Actions.adapter
{
    public class EventClientsAdapter : RecyclerView.Adapter
    {
        private Context cx;

        public EventClientsAdapter(Context cx, List<DateRangeModel> items) : base()
        {
            this.cx = cx;
            DataHolder = items;
        }

        public List<DateRangeModel> DataHolder { get; set; }


        public event EventHandler<int> ItemClick;

        public event EventHandler<int> HandleEvent;

        void OnClick(int position)
        {
            if (ItemClick != null)
                ItemClick(this, position);
        }

        void OnHandleEvent(object sender, int position)
        {
            if (HandleEvent != null)
                HandleEvent(sender, position);
        }


        public override int ItemCount => DataHolder.Count;

        public override long GetItemId(int position)
        {
            return base.GetItemId(position);
        }
        public override int GetItemViewType(int position)
        {
            return base.GetItemViewType(position);
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            holder.IsRecyclable = true;
            var viewholder = holder as EventClientsViewHolder;

            var data = DataHolder[position];




        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var id = Resource.Layout.Event_clients_list_layout;
            var itemView = LayoutInflater.From(parent.Context).Inflate(id, parent, false);

            return new DayCalendarViewHolder(itemView, OnHandleEvent, OnClick);
        }



    }

    class EventClientsViewHolder : RecyclerView.ViewHolder
    {
        private View view;
        private TextView dayTtitle, monthTitle;
        private LinearLayout squareLinear;

        public TextView TitleDay => dayTtitle;
        public TextView TitleMonth => monthTitle;

        public EventClientsViewHolder(View itemView, Action<object, int> handleEvent, Action<int> clickEvent) : base(itemView)
        {
            view = itemView;

            view.Tag = this;

            view.Id = Position;
            view.Click += (sender, e) => clickEvent(base.Position);

        }





    }



}