﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Android;

namespace Visi.Calendar.Actions.adapter
{
    public class DayNamesAdapter : RecyclerView.Adapter
    {
        private Context cx;

        public DayNamesAdapter(Context cx, List<string> items) : base()
        {
            this.cx = cx;
            Items = items;
        }

        public List<string> Items { get; set; }

        public override int ItemCount => Items.Count;

        public override long GetItemId(int position)
        {
            return base.GetItemId(position);
        }
        public override int GetItemViewType(int position)
        {
            return base.GetItemViewType(position);
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            holder.IsRecyclable = true;
            var viewholder = holder as DayNamesViewHolder;

            viewholder.TitleDay.Text = Items[position];

        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var id = Resource.Layout.Day_name_layout;
            var itemView = LayoutInflater.From(parent.Context).Inflate(id, parent, false);

            return new DayNamesViewHolder(itemView);
        }

    }

    class DayNamesViewHolder : RecyclerView.ViewHolder
    {
        private View view;
        private TextView dayTtitle;

        public TextView TitleDay => dayTtitle;

        public DayNamesViewHolder(View itemView) : base(itemView)
        {
            view = itemView;
            view.Tag = this;
            dayTtitle = view.FindViewById<TextView>(Resource.Id.day_name);
        }

    }

}


/*
public class DayNamesAdapter : BaseAdapter<string>
{

    private Context cx;

    public DayNamesAdapter(Context context, List<string> items) : base()
    {
        this.cx = context;
        Items = items;
    }
    List<string> Items { get; set; }
    public override int Count => Items.Count;
    public override string this[int position] => Items[position];

    public override long GetItemId(int position)
    {
        return position;
    }

    public override View GetView(int position, View convertView, ViewGroup parent)
    {
        var view = convertView;

        DayNamesViewHolder viewholder = null;

        if (view == null)
        {
            var inflater = (LayoutInflater)cx.GetSystemService(Context.LayoutInflaterService);

            //view = inflater.Inflate(Resource.Layout.day_name_layout, parent, false);

            viewholder = new DayNamesViewHolder(view);

            view.Tag = viewholder;
        }
        else
        {
            viewholder = (DayNamesViewHolder)view.Tag;
        }

        viewholder.TitleDay.Text = Items[position];

        return view;
    }

    public class DayNamesViewHolder : Java.Lang.Object
    {
        private View view;
        private TextView dayTtitle;

        public TextView TitleDay => dayTtitle;

        public DayNamesViewHolder(View v)
        {
            view = v;

            //dayTtitle = view.FindViewById<TextView>(Resource.Id.day_name);
        }

    }

    */

