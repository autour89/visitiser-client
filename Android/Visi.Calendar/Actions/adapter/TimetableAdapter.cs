﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Visi.Calendar.Model;
using Visi.Calendar.Actions.view;
using Android.Graphics;
using Visi.Generic.Models;
using Visi.Generic.DAL.DataModel;

namespace Visi.Calendar.Actions.adapter
{
    public class TimetableAdapter : RecyclerView.Adapter
    {
        private Context cx;

        public TimetableAdapter(Context cx, TimetableDate model) : base()
        {
            this.cx = cx;
            DataHolder = model;
        }

        public TimetableDate DataHolder { get; set; }

        public event EventHandler<View> ItemClick;

        public event EventHandler<int> ItemTouch;

        void OnClick(View view)
        {
            if (ItemClick != null)
                ItemClick(this, view);
        }

        void OnTouch(object sender, int position)
        {
            if (ItemTouch != null)
                ItemTouch(this, position);
        }

        public delegate void TimetableEventHandler(object sender, int e);

        public event TimetableEventHandler EventClick;

        void OnHandleClickEvent(object sender, int position)
        {
            if (EventClick != null)
                EventClick(sender, position);
        }

        public override int ItemCount => DataHolder.DatesRange.Count;

        public override long GetItemId(int position)
        {
            return base.GetItemId(position);
        }
        public override int GetItemViewType(int position)
        {
            return base.GetItemViewType(position);
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            holder.IsRecyclable = true;
            var viewholder = holder as TimetableViewHolder;

            var data = DataHolder.DatesRange[position];

            viewholder.SetTimeLabel(data.IsCurrent);

            viewholder.CleanEventsPanel();

            var list_ev = DataHolder.EventData.Where(x => x.Start_DT.Year.Equals(data.Year) && x.Start_DT.Month.Equals(data.Month) && x.Start_DT.Day.Equals(data.DayOfMonth)).ToList();

            foreach (var item in list_ev)
            {
                viewholder.SetEvents(item);
            }

        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var id = Resource.Layout.Adapter_timetable_layout;
            var itemView = LayoutInflater.From(parent.Context).Inflate(id, parent, false);

            return new TimetableViewHolder(itemView, OnClick, OnTouch, OnHandleClickEvent);
        }
    }

    class TimetableViewHolder : RecyclerView.ViewHolder
    {
        private View view;
        private AbsoluteLayout newAbsolute;
        private AbsoluteLayout timelabelPanel;
        private AbsoluteLayout.LayoutParams lp;

        private Action<object, int> evListener;

        public TimetableViewHolder(View itemView, Action<View> clickListener, Action<object, int> touchListener, Action<object, int> eventListener) : base(itemView)
        {
            view = itemView;
            newAbsolute = view.FindViewById<AbsoluteLayout>(Resource.Id.addEvent_here);
            timelabelPanel = view.FindViewById<AbsoluteLayout>(Resource.Id.timelabel_panel);

            lp = new AbsoluteLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent, 1, 0);

            newAbsolute.Touch += (sender, e) =>
            {
                if (e.Event.Action == MotionEventActions.Up)
                {
                    var y = e.Event.GetY();
                    var height = newAbsolute.Height;
                    var item = height / 24;
                    var pos = y / item;
                    var hour = Convert.ToInt32(Math.Floor(pos));

                    touchListener(newAbsolute, hour);
                }

            };
            view.Click += (sender, e) => clickListener(view);
            evListener = eventListener;
        }

        public AbsoluteLayout LabelPanel => timelabelPanel;

        public void SetEvents(Events ev)
        {
            var evOne = new EventView(view.Context, null);
            evOne.EventInfo = ev;
            evOne.SetHourPosition(ev.ToTime());
            evOne.SetHeight(ev.ToDuration());
            evOne.ItemClickEvent += (sender, e) => evListener(evOne, ev.EventId);

            var ev_title = view.Resources.GetString(Resource.String.Event_timetable_title);
            evOne.EventName.Text = ev_title;

            evOne.SetBackgroud(Color.Gray);
            newAbsolute.AddView(evOne);
        }

        public void SetTimeLabel(bool state)
        {
            var time = DateTime.Now;
            CanRemoveLabel();
            switch (state)
            {
                case true:
                    view.Post(() =>
                    {
                        var inflater = (LayoutInflater)this.view.Context.GetSystemService(Context.LayoutInflaterService);
                        var childLayout = inflater.Inflate(Resource.Layout.Timetable_label, new LinearLayout(view.Context), false);

                        var hgt = timelabelPanel.Height / (24 * 60);
                        var yY = ((time.Hour * 60) + time.Minute) * hgt;
                        //var pos = ToTime(time) * 60;
                        lp.Y = yY;//Convert.ToInt32(pos);
                        childLayout.LayoutParameters = lp;
                        timelabelPanel.AddView(childLayout);
                        childLayout.BringToFront();
                    });
                    break;
            }
        }

        private void CanRemoveLabel()
        {
            var v = LabelPanel.FindViewById<LinearLayout>(Resource.Id.time_label_linear);

            if (v != null)
                LabelPanel.RemoveView(v);
        }

        public void CleanEventsPanel()
        {
            if (newAbsolute.ChildCount > 0)
                newAbsolute.RemoveAllViews();
        }

    }

    class TimetableScrollListener : RecyclerView.OnScrollListener
    {
        private LinearLayoutManager lmanager;
        public delegate void ScrolledEventHandler(object sender, int e);
        public delegate void ScrollStateEventHandler(object sender, int e);

        public event ScrollStateEventHandler ScrollStateEvent;
        public event ScrolledEventHandler ScrolledEvent;


        void OnHandleScrolledEvent(object sender, int position)
        {
            if (ScrolledEvent != null)
                ScrolledEvent(sender, position);
        }
        void OnHandleScrollstateEvent(object sender, int position)
        {
            if (ScrollStateEvent != null)
                ScrollStateEvent(sender, position);
        }

        public TimetableScrollListener(LinearLayoutManager manager)
        {
            lmanager = manager;
        }

        public override void OnScrolled(RecyclerView recyclerView, int dx, int dy)
        {
            base.OnScrolled(recyclerView, dx, dy);

            var pos = lmanager.FindFirstVisibleItemPosition();

            OnHandleScrolledEvent(recyclerView, pos);
        }

        public override void OnScrollStateChanged(RecyclerView recyclerView, int newState)
        {
            OnHandleScrollstateEvent(recyclerView, newState);
        }
    }


}