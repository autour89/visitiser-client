﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.Graphics;

using Visi.Calendar.Model;

namespace Visi.Calendar.Presenter
{
    public class WheelViewController
    {
        private List<string> stringModel;
        private List<WheelModel> modelData;

        public WheelViewController()
        {
            stringModel = new List<string>();
            modelData = new List<WheelModel>();
        }

        public List<string> StringModel
        {
            set
            {
                stringModel = value;
                CreateStringModel();
            }
            get
            {
                return stringModel;
            }
        }
        public List<WheelModel> Data
        {
            set
            {
                modelData = value;
            }
            get
            {
                return modelData;
            }
        }

        public int MyProperty { get; set; }
        public void CreateStringModel()
        {
            var strHolder = new List<WheelModel>();

            foreach (var item in StringModel)
            {
                strHolder.Add(new WheelModel
                {
                    Text = item,
                    IsCurrent = false
                });
            }
            Data = strHolder;
        }


    }
}