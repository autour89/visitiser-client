﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

using Visi.Generic.Models;
using Visi.Generic.Interfaces;
using Visi.Generic.DAL.DataModel;
using Visi.Generic.Services;
using Visi.Generic.DAL.Services;
using Visi.Generic.Services.ApiClients;
using System.Threading.Tasks;

namespace Visi.Calendar.Presenter
{
    public class EventDescriptionController
    {
        private RestsharpApi _access;
        private IContentAccess<VisitizerProfile> profileContent;
        private IContentAccess<AccountPhones> profilePhones;
        private List<AccountInfo> membersInfo;


        public EventDescriptionController()
        {
            _access = RestsharpApi.Instance(Configuration.ConnectionString);
            profileContent = ContentDB.Account_Data;
            profilePhones = ContentDB.Account_Phones;

            ProfileData = profileContent.SelectAllWithChildren<VisitizerProfile>().FirstOrDefault();
            membersInfo = new List<AccountInfo>();
        }

        public VisitizerProfile ProfileData { get; private set; }
        public AccountInfo ClientInfo { get; set; }

        public List<AccountInfo> MembersInfo
        {
            set { membersInfo = value; }
            get { return membersInfo; }
        }
        public async Task SearchAccount(string term)
        {
            var info = new AccountInfo { PhoneNumber = term };

            var result = await _access.SendPostAsJson(ApiUrl.FindAccntByPhone, info);

            if (result.Length > 10)
                ClientInfo = JsonConvert.DeserializeObject<AccountInfo>(result);

        }

        public void SetEventMembers(AccountInfo data)
        {
            var ph = ProfileData.PhoneNumbers.FirstOrDefault();

            MembersInfo.Add(new AccountInfo
            {
                Id = ProfileData.Id,
                Email = ProfileData.Email,
                Logo = ProfileData.Logo,
                Name = ProfileData.Name,
                PhoneNumber = ProfileData.PhoneNumbers.FirstOrDefault().PhoneNumber
            });

            if (data != null)
            {
                MembersInfo.Add(data);
            }

        }

    }
}
