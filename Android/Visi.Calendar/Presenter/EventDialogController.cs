﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Visi.Generic.DAL.DataModel;
using Visi.Generic.DAL.Services;
using Visi.Generic.Helpers;
using Visi.Generic.Interfaces;
using Visi.Generic.Models;
using Visi.Generic.Services;
using Visi.Generic.Services.ApiClients;

namespace Visi.Calendar.Presenter
{
    public class EventDialogController
    {
        //private ContentDB tables_db;
        private RefitApi smartApi;
        private IContentAccess<Events> eventContent;
        private IContentAccess<EventMembers> eventMemContent;
        private Events eventData;
        public EventDialogController()
        {
            //tables_db = new ContentDB();
            smartApi = RefitApi.Instance(Configuration.ConnectionString);
            eventContent = ContentDB.EventData;
            eventMemContent = ContentDB.EventMembersData;
            eventData = new Events();

            //eventContent.DeleteAllItems();
            //eventMemContent.DeleteAllItems();
        }

        public IContentAccess<Events> EventModel => eventContent;


        public Events EventData
        {
            set { eventData = value; }
            get { return eventData; }
        }

        public void NewEvent()
        {
            eventContent.InsertItemWithChildren(EventData);
        }
        public void UpdateEvent()
        {
            var members = eventMemContent.SelectAllData<EventMembers>().Where(x => x.EventId.Equals(EventData.EventId)).ToList();

            foreach (var x in members)
                eventMemContent.DeleteItems(x);

            eventContent.UpdateItem(EventData);
            eventMemContent.InsertList(EventData.EventMembers);
        }

        public bool CanCreateEvent(DateTime startdate, DateTime end_date)
        {
            var val = eventContent.SelectAllData<Events>().Where(x => x.Start_DT >= startdate && x.End_DT <= end_date).ToList();

            return val.Count>0? false : true;
        }

    }
}

