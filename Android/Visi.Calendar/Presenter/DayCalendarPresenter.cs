﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Visi.Calendar.Model;
using Visi.Generic.DAL.DataModel;
using Visi.Generic.DAL.Services;
using Visi.Generic.Helpers;
using Visi.Generic.Interfaces;
using Visi.Generic.Models;


namespace Visi.Calendar.Presenter
{
    public class DayCalendarPresenter
    {
        private IContentAccess<Events> eventContent;
        private int currentMonth, currentSelected;
        private DateTime time = DateTime.Now;
        private List<DateRangeModel> datesModel;
        private CalendarViewHelper cal_dates;
        private TimetableDate timetableDate;

        public DayCalendarPresenter()
        {
            eventContent = ContentDB.EventData;
            cal_dates = CalendarViewHelper.Instance;
            datesModel = new List<DateRangeModel>();
            timetableDate = new TimetableDate();

            var listDates = cal_dates.DatesList(2, 2);
            datesModel = cal_dates.DaysValues(listDates);

            SetCurrentDaySelected();

            timetableDate = new TimetableDate
            {
                DatesRange = datesModel,
                EventData = eventContent.SelectAllWithChildren<Events>().ToList()
            };
            CurrentMonth = timetableDate.DatesRange[CurrentSelectedDay].Month;//DatesModel[CurrentSelectedDay].Month;

        }

        public TimetableDate TimetableDate
        {
            set { timetableDate = value; }
            get { return timetableDate; }
        }


        public List<string> ListDays => cal_dates.ListDays();

        public int CurrentSelectedDay
        {
            private set { currentSelected = value; }
            get { return currentSelected; }
        }

        public int CurrentMonth
        {
            set { currentMonth = value; }
            get { return currentMonth; }
        }

        private void SetCurrentDaySelected()
        {
            datesModel.ForEach(d =>
            {
                if (d.Year.Equals(time.Year) && d.Month.Equals(time.Month) && d.DayOfMonth.Equals(time.Day))
                {
                    d.IsDaySelected = true;
                }
            });
            CurrentSelectedDay = datesModel.FindIndex(d => d.IsDaySelected.Equals(true));
        }

        public bool IsCurrentYear(int position)
        {
            return true ? timetableDate.DatesRange[position].Year.Equals(time.Year) : false;
        }


        public void ChangeSelectedDay(int position)
        {
            timetableDate.DatesRange[CurrentSelectedDay].IsDaySelected = false;
            timetableDate.DatesRange[position].IsDaySelected = true;
            CurrentSelectedDay = position;
        }


        public void UpdateTimetable()
        {
            var dt = timetableDate.DatesRange[CurrentSelectedDay];
            timetableDate.EventData = eventContent.SelectAllWithChildren<Events>().ToList();
        }

        public Events CreateEvent
        {
            get
            {
                var date = TimetableDate.DatesRange[CurrentSelectedDay];

                return new Events
                {
                    Created_DT = DateTime.Now,
                    Start_DT = new DateTime(date.Year, date.Month, date.DayOfMonth, Convert.ToInt32(date.HourOfDay), Convert.ToInt32(date.Minutes), 0),
                    Duration = new TimeSpan(1, 0, 0)
                };

            }
        }

    }
}