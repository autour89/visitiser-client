﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using Visi.Calendar.Model;
using Visi.Generic.Models;

namespace Visi.Calendar.Presenter
{
    public class EventTimeController
    {
        private List<string> hoursStr,minutesStr;
        private List<DateTime> monthDT, dayDT;
        private TimeStateModel dateSelect, durationSelect, timeSelect, endtimeSelect;

        public EventTimeController()
        {
            dateSelect = new TimeStateModel();
            durationSelect = new TimeStateModel();
            timeSelect = new TimeStateModel();
            endtimeSelect = new TimeStateModel();

            minutesStr = new List<string>();
            hoursStr = new List<string>();
            dayDT = new List<DateTime>();
            monthDT = new List<DateTime>();

            CurrentDate = DateTime.Now;
        }

        public bool IsDateSet { get; set; }
        public bool IsDuration { get; set; }
        public bool IsTime { get; set; }
        public bool IsEndtime { get; set; }
        public DateTime CurrentDate { get; set; }
        public DateRangeModel DateModel { get; set; }
        public TimeStateModel DateSelect
        {
            set
            {
                dateSelect = value;
            }
            get { return dateSelect; }
        }
        public TimeStateModel DurationSelect
        {
            set
            {
                durationSelect = value;
            }
            get { return durationSelect; }
        }
        public TimeStateModel TimeSelect
        {
            set
            {
                timeSelect = value;
            }
            get { return timeSelect; }
        }
        public TimeStateModel EndtimeSelect
        {
            set
            {
                endtimeSelect = value;
            }
            get { return endtimeSelect; }
        }
        
        public void CreateTimeModel()
        {
            //var now = DateTime.Now;
            var myCal = CultureInfo.InvariantCulture.Calendar;
            var myDT = new DateTime(DateModel.Year, DateModel.Month, 1, new GregorianCalendar());
            var fromD = myCal.AddMonths(myDT, -6);
            var toD = myCal.AddMonths(myDT, 5);
            var dys = DateTime.DaysInMonth(toD.Year, toD.Month);
            toD = myCal.AddDays(toD, dys - 1);

            monthDT = MonthDatesRange(fromD, toD);
            dayDT = DateRange(fromD, toD).ToList();

            for (int i = 0; i < 60; i++)
            {
                if (i % 5 == 0)
                {
                    if (!(i.Equals(0) || i < 10))
                        minutesStr.Add(i.ToString());
                    else
                        minutesStr.Add("0" + i);
                }
                if (i < 24)
                    hoursStr.Add(i.ToString());
            }
        }
        public void NowIndex()
        {
            var myCal = CultureInfo.InvariantCulture.Calendar;

            if (CurrentDate.Minute > 0 && CurrentDate.Minute < 15)
            {
                TimeSelect.NowIndex1 = hoursStr.FindIndex(x => x.Equals(DateTime.Now.Hour.ToString()));
                TimeSelect.NowIndex2 = minutesStr.FindIndex(x => Convert.ToInt32(x).Equals(15));
            }
            else if (CurrentDate.Minute > 15 && CurrentDate.Minute < 30)
            {
                TimeSelect.NowIndex1 = hoursStr.FindIndex(x => x.Equals(DateTime.Now.Hour.ToString()));
                TimeSelect.NowIndex2 = minutesStr.FindIndex(x => Convert.ToInt32(x).Equals(30));
            }
            else if (CurrentDate.Minute > 30 && CurrentDate.Minute < 45)
            {
                TimeSelect.NowIndex1 = hoursStr.FindIndex(x => x.Equals(DateTime.Now.Hour.ToString()));
                TimeSelect.NowIndex2 = minutesStr.FindIndex(x => Convert.ToInt32(x).Equals(45));
            }
            else if (CurrentDate.Minute > 45)
            {
                CurrentDate = myCal.AddHours(CurrentDate, 1);
                TimeSelect.NowIndex1 = hoursStr.FindIndex(x => x.Equals(CurrentDate.Hour.ToString()));
                TimeSelect.NowIndex2 = minutesStr.FindIndex(x => Convert.ToInt32(x).Equals(0));
            }

        }

        public string EndTimeView(string text)
        {

            var ind = hoursStr.FindIndex(x => x.Equals(text));

            return hoursStr[ind];
        }
        public void InitSelect()
        {
            IsDateSet = false;
            IsDuration = false;
            IsTime = true;
            IsEndtime = false;

            dateSelect.Data1 = monthDT;
            dateSelect.Data2 = dayDT;
            dateSelect.NowIndex1 = monthDT.FindIndex(x => x.Year.Equals(DateModel.Year) && x.Month.Equals(DateModel.Month));// x => x.Date.Equals(CurrentDate.Date));
            dateSelect.NowIndex2 = dayDT.FindIndex(x => x.Year.Equals(DateModel.Year) && x.Month.Equals(DateModel.Month) && x.Day.Equals(DateModel.DayOfMonth));// x => x.Date.Equals(CurrentDate.Date));

            durationSelect.Data1 = hoursStr;
            durationSelect.Data2 = minutesStr;

            if(DateModel.Duration.Hours > 0)
            {
                durationSelect.NowIndex1 = hoursStr.FindIndex(x => x.Equals(DateModel.Duration.Hours.ToString()));

                if(DateModel.Duration.Minutes > 0)
                    durationSelect.NowIndex2 = minutesStr.FindIndex(x => x.Equals(DateModel.Duration.Minutes.ToString()));
                else
                    durationSelect.NowIndex2 = 0;
            }
            else
            {
                durationSelect.NowIndex1 = 1;
                durationSelect.NowIndex2 = 0;
            }


            timeSelect.Data1 = hoursStr;
            timeSelect.Data2 = minutesStr;

            if (DateModel.HourOfDay > 0)
            {
                timeSelect.NowIndex1 = DateModel.HourOfDay;
                if(DateModel.Minutes>0)
                    timeSelect.NowIndex2 = minutesStr.FindIndex(x=>x.Equals(DateModel.Minutes.ToString()));
                else
                    timeSelect.NowIndex2 = 0;

            }
            else
            {
                timeSelect.NowIndex1 = TimeSelect.NowIndex1;//hoursStr.FindIndex(x=>x.Equals(CurrentDate.Hour.ToString()));
                timeSelect.NowIndex2 = TimeSelect.NowIndex2;//minutesStr.FindIndex(x=>x.Equals(CurrentDate.Minute.ToString()));
            }
            endtimeSelect.Data1 = hoursStr;
            endtimeSelect.Data2 = minutesStr;

        }
        public bool IsMonthEqual(int posM, int posD)
        {
            if (monthDT[posM].Month.Equals(dayDT[posD].Month))
                return true;
            else
                return false;
        }
        public int DaysInMonth(int pos)
        {
            return DateTime.DaysInMonth(monthDT[pos].Year, monthDT[pos].Month);
        }

        private List<DateTime> MonthDatesRange(DateTime startdate, DateTime enddate)
        {
            var listDate = new List<DateTime>();

            while (startdate <= enddate)
            {
                listDate.Add(startdate);
                startdate = startdate.AddMonths(1);
            }

            return listDate;
        }
        private IEnumerable<DateTime> DateRange(DateTime fromDate, DateTime toDate)
        {
            return Enumerable.Range(0, toDate.Subtract(fromDate).Days + 1)
                             .Select(d => fromDate.AddDays(d));
        }

    }
}



