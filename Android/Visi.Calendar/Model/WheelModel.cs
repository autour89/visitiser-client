﻿using Android.Graphics;
using System;

namespace Visi.Calendar.Model
{
    public class WheelModel
    {
        public Color SelectedBackground { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
        public bool IsCurrent { get; set; }
        public bool IsBusy { get; set; }
    }
}