﻿using System;

namespace Visi.Calendar.Model
{
    public class Event
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public TimeSpan Duration { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public Double ToDuration()
        {
            return Convert.ToDouble(Duration.Hours + "," + Duration.Minutes);
        }

        public Double ToTime()
        {
            return Convert.ToDouble(StartTime.Hour + "," + StartTime.Minute);
        }
    }
}