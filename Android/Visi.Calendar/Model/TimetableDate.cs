﻿
using System.Collections.Generic;
using Visi.Generic.DAL.DataModel;
using Visi.Generic.Models;

namespace Visi.Calendar.Model
{
    public class TimetableDate
    {
        public List<DateRangeModel> DatesRange { get; set; }
        public List<Events> EventData { get; set; }

    }
}