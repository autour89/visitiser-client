﻿using System;

namespace Visi.Calendar.Model
{
    public class TimeStateModel
    {
        public Object Data1 { get; set; }
        public Object Data2 { get; set; }
        public int NowIndex1 { get; set; }
        public int NowIndex2 { get; set; }
    }
}