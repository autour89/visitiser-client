﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Visi.Calendar.Model;

namespace Visi.Calendar.Interfaces
{
    public interface IWheel
    {
        int CurrentItemPosition { get; set; }
        float TextSize { get; set; }
        Color TextColor { get; set; }
        int NewHeight { set; }
        int NewWidth { set; }
        List<string> Data { set;}
        void ChangeCurrentState();
    }
}