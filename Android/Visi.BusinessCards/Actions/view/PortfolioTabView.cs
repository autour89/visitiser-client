using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Visi.BusinessCards.Actions.view
{
    public class PortfolioTabView : LinearLayout
    {
        private Context cx;

        private LinearLayout linear_work_done;
        private Button add_work_done;

        private List<WorkDoneView> workDoneList;

        public PortfolioTabView(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            Initialize(context);
        }

        public PortfolioTabView(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            Initialize(context);
        }

        private void Initialize(Context context)
        {
            cx = context;
            Inflate(cx, Resource.Layout.Portfolio_tab_view, this);

            workDoneList = new List<WorkDoneView>();

            linear_work_done = FindViewById<LinearLayout>(Resource.Id.linear_for_work_done);
            add_work_done = FindViewById<Button>(Resource.Id.add_portfolio_link);

            add_work_done.Click += Handle_Click;
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.add_portfolio_link))
                {
                    var work_done = new WorkDoneView(cx, null);
                    work_done.Tag = workDoneList.Count + 1;
                    linear_work_done.AddView(work_done);
                    workDoneList.Add(work_done);
                }
            }
        }

        /// <summary>
        /// Remove work perfomed item
        /// </summary>
        /// <param name="id">id of work perfomed</param>
        public void RmWorkPerfomed(int id)
        {
            var view_search_res = workDoneList.Find(x => x.Tag.Equals(id));
            workDoneList.Remove(view_search_res);
            linear_work_done.RemoveView(view_search_res);
        }

        /// <summary>
        /// Remove image from portfolio
        /// </summary>
        /// <param name="id">id of image</param>
        /// <param name="parent_id">id of work perfomed</param>
        public void RmViewImage(int id, int parent_id)
        {
            var view_res = workDoneList.Find(t => t.Tag.Equals(parent_id));
            view_res.ImageRemove(id);
        }

        /// <summary>
        /// Add image to portfolio
        /// </summary>
        /// <param name="data">image path on device</param>
        public void AddWorkImage(Android.Net.Uri data)
        {
            var view_res = workDoneList.Find(t => t.Tag.Equals(WorkDoneCurrent));
            view_res.AddNewImage(data);
        }

        /// <summary>
        /// Work done id to be added
        /// </summary>
        public int WorkDoneCurrent { get; set; }

        /// <summary>
        /// Get work perfomed list
        /// </summary>
        public List<WorkDoneView> WorkPerfomed => workDoneList;
        
    }
}