using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.Content;
using Android.Util;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V7.Widget;
using Android.Widget;
using Android.Views;
using Visi.BusinessCards.Actions.adapter;

namespace Visi.BusinessCards.Actions.view
{
    public class BCardTabsView : CoordinatorLayout
    {
        private Context cx;

        //private RecyclerView bcard_titles;
        private GridView bcard_titles;
        
        private ViewPager bcard_tabs;
        //private TabTitleListViewHolder selected_view;
        private View selectedView;

        public BCardTabsView(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            cx = context;
            Initialize();
        }

        public BCardTabsView(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            cx = context;
            Initialize();
        }

        private void Initialize()
        {
            Inflate(cx, Resource.Layout.Bcard_tabs_view, this);

            bcard_titles = FindViewById<GridView>(Resource.Id.bcard_title_list);
            //var layoutManager = new LinearLayoutManager(cx, LinearLayoutManager.Horizontal, false);
            //bcard_titles.SetLayoutManager(layoutManager);

            bcard_tabs = FindViewById<ViewPager>(Resource.Id.bcard_page_list);

            var title_array = cx.Resources.GetStringArray(Resource.Array.bcard_titles);
            var title_adapter = new TitleListAdapter(cx, title_array.ToList());
            bcard_titles.SetAdapter(title_adapter);

            var page_adapter = new CreateBCardTabsAdapter(cx);
            bcard_tabs.Adapter = page_adapter;
            bcard_tabs.OffscreenPageLimit = title_array.Length;

            //var v = (TabTitleListViewHolder)bcard_titles.FindViewHolderForPosition(0);
            //selected_view = v;
            //if (selected_view != null)
            //    selected_view.SetViewBackground(1);

            bcard_titles.ItemClick += Handle_ItemClick;
            bcard_tabs.PageScrolled += Handle_PageScrolled;

        }

        private void Handle_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var gridview = sender as GridView;

            var pos = e.Position;

            if (gridview != null)
            {
                if (gridview.Id.Equals(Resource.Id.bcard_title_list))
                {
                    bcard_tabs.CurrentItem = pos;

                    SetTab(pos);
                }
            }
        }

        private void Handle_PageScrolled(object sender, ViewPager.PageScrolledEventArgs e)
        {
            var pos = e.Position;

            SetTab(pos);

        }

        private void SetTab(int pos)
        {
            if (selectedView != null)
                SetViewBackground(0, selectedView);

            var v = bcard_titles.GetChildAt(pos);

            SetViewBackground(1, v);

            selectedView = v;
        }

        public void SetViewBackground(int state, View itemView)
        {
            switch (state)
            {
                case 0:
                    itemView.SetBackgroundResource(Resource.Drawable.list_selector);
                    break;
                case 1:
                    itemView.SetBackgroundResource(Resource.Drawable.item_background_focused);
                    break;

            }
        }

        /// <summary>
        /// Get Data from Description tab
        /// </summary>
        public DescriptionBusinessCard DescriptiontData
        {
            get
            {
                var v = bcard_tabs.GetChildAt(0);

                return (DescriptionBusinessCard)v;
            }
        }

        /// <summary>
        /// Get Data from Contacts tab 
        /// </summary>
        public CantactDataView ContactData
        {
            get
            {
                var v = bcard_tabs.GetChildAt(1);

                return (CantactDataView)v;
            }
        }

        /// <summary>
        /// Get Data from work perfomed tab 
        /// </summary>
        public PortfolioTabView PortfolioData
        {
            get
            {
                var v = bcard_tabs.GetChildAt(3);

                return (PortfolioTabView)v;
            }
        }

    }
}