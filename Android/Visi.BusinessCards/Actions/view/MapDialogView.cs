using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FragmentManager = Android.Support.V4.App.FragmentManager;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Runtime;
using Android.Util;
using Android.Widget;
using Visi.Generic.Models;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Visi.BusinessCards.Actions.activity;

namespace Visi.BusinessCards.Actions.view
{
    public class MapDialogView : DialogFragment
    {
        private Context cx;
        private BCard_Adresses geoAddress;

        private SupportMapFragment mapFrag;
        private GoogleMap map;
        private Marker map_marker;

        private FragmentManager ft;

        public MapDialogView(Context context, BCard_Adresses location)
        {
            this.cx = context;
            geoAddress = location;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.Map_dialog_view, container);

            var displaymetrics = new DisplayMetrics();
            ((CreateBusinessCardActivity)cx).WindowManager.DefaultDisplay.GetMetrics(displaymetrics);
            int height = displaymetrics.HeightPixels;

            var feature = Dialog.Window;
            feature.RequestFeature(WindowFeatures.NoTitle);
            feature.SetGravity(GravityFlags.Top);

            ft =    ((CreateBusinessCardActivity)cx).SupportFragmentManager;

            mapFrag = (SupportMapFragment)ft.FindFragmentByTag("map_dialog");

            var paramss = mapFrag.View.LayoutParameters;
            paramss.Height = height / 2;
            view.LayoutParameters = paramss;

            //map = mapFrag.Map;
            map.MapType = GoogleMap.MapTypeNormal;
            var loc = new LatLng(geoAddress.Latitude, geoAddress.Longitude);
            map.AnimateCamera(CameraUpdateFactory.NewLatLngZoom(loc, 14));

            AddMapPin();

            return view;
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            var bgn = ft.BeginTransaction();
            bgn.Remove(mapFrag);
            bgn.Commit();
        }

        private void AddMapPin()
        {
            var marker = new MarkerOptions();

            marker.SetPosition(new LatLng(geoAddress.Latitude, geoAddress.Longitude));
            marker.SetTitle(geoAddress.Business_Card_Adress_NM);
            map.AddMarker(marker);
        }


    }
}