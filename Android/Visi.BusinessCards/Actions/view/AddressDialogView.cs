using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Newtonsoft.Json;

using Java.Util;
using Android.Locations;
using Android.App;
using Android.Content;
using Android.Widget;
using Android.Net;
using Android.Views;
using Android.OS;
using Visi.Generic.Models;
using Visi.BusinessCards.Actions.activity;

namespace Visi.BusinessCards.Actions.view
{
    public class AddressDialogView : Dialog
    {
        private Context cx;

        private Spinner countries;
        private EditText region_edit, city_edit, address_edit;
        private Button addAddress,dialogCancel;

        private List<CountryModel> countryList;
        private List<CountryModel> data_countries;
        private Geocoder gc;


        public AddressDialogView(Context context):base(context)
        {
            this.cx = context;

            var feature = this.Window;
            feature.RequestFeature(WindowFeatures.NoTitle);

            Initialize();
        }

        private void Initialize()
        {
            this.SetContentView(Resource.Layout.Add_master_address_dialog);

            countryList = new List<CountryModel>();
            data_countries = new List<CountryModel>();
            gc = new Geocoder(cx);

            countries = FindViewById<Spinner>(Resource.Id.country_spinner);
            region_edit = FindViewById<EditText>(Resource.Id.adding_address_dialog_region);
            city_edit = FindViewById<EditText>(Resource.Id.adding_address_dialog_city);
            address_edit = FindViewById<EditText>(Resource.Id.adding_address_dialog_address);

            addAddress = FindViewById<Button>(Resource.Id.address_dialog_ok);
            dialogCancel = FindViewById<Button>(Resource.Id.address_dialog_cancel);

            countryList = LocaleCountries();
            countryList = countryList.OrderBy(x => x.LocaleName).ToList();
            GluingJson();
            data_countries = data_countries.OrderBy(x => x.LocaleName).ToList();

            var names = data_countries.Select(c => c.LocaleName).ToList();
            var dataset = new ArrayAdapter(cx, Resource.Layout.support_simple_spinner_dropdown_item, names);
            //var adapter = new CountryListAdapter(cx, arangeby);
            //adapter.SetDropDownViewResource(Resource.Layout.content_adapter);
            countries.Adapter = dataset;

            SetFirstSelection();
            addAddress.Click += Handle_Click;
            dialogCancel.Click += Handle_Click;

        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.address_dialog_ok))
                {
                    SetLocation();
                    ((CreateBusinessCardActivity)cx).AddContactAddress();
                }
                else if (button.Id.Equals(Resource.Id.address_dialog_cancel))
                {
                    Dismiss();
                }
            }
        }
        /// <summary>
        /// Get selected country
        /// </summary>
        public string Country => Convert.ToString(countries.SelectedItem);
        /// <summary>
        /// Get region
        /// </summary>
        public string Region => region_edit.Text;
        /// <summary>
        /// Get city
        /// </summary>
        public string City => city_edit.Text;
        /// <summary>
        /// Get address
        /// </summary>
        public string Address => address_edit.Text;


        /// <summary>
        /// Get address with geo location
        /// </summary>
        public BCard_Adresses Location { get; private set; }


        private void SetLocation()
        {
            var address = Country + ", " + Region + ", " + City + ", " + Address;

            var dataset = new BCard_Adresses();            
            dataset.Business_Card_City = City;
            //dataset.Business_Card_Adress_NM = address;

            IList<Address> location = AddressLocation(address);

            if (location!=null && location.Count>0)
            {
                var addr_result = location.FirstOrDefault().CountryName + ", " + location.FirstOrDefault().AdminArea + ", " + location.FirstOrDefault().Locality + ", " + location.FirstOrDefault().Thoroughfare + " " + location.FirstOrDefault().SubThoroughfare;
                dataset.Business_Card_Adress_NM = addr_result;
                dataset.Latitude = location.FirstOrDefault().Latitude;
                dataset.Longitude = location.FirstOrDefault().Longitude;
            }
            else
            {
                dataset.Business_Card_Adress_NM = address;
            }

            Location = dataset;
        }
        private async void SetFirstSelection()
        {
            var id = 0;
            if (IsConnected())
            {
                var response = ApiSearch.DeviceLocation();

                var result = JsonConvert.DeserializeObject<DeviceCountry>(response);
                id = data_countries.FindIndex(x => x.Code.Equals(result.CountryCode));
                countries.SetSelection(id);
            }
        }

        private List<CountryModel> LocaleCountries()
        {
            var locales = Locale.GetAvailableLocales();

            var countries = new List<CountryModel>();
            var count_str = new List<string>();

            foreach (var locale in locales)
            {
                var country = new CountryModel();

                country.LocaleName = locale.DisplayCountry;
                try
                {
                    country.Iso3Code = locale.ISO3Country;
                }
                catch (Exception) { }

                if (locale.DisplayCountry.Trim().Length > 0 && !count_str.Contains(country.LocaleName))
                {
                    count_str.Add(locale.DisplayCountry);
                    countries.Add(country);
                }

            }

            
            return countries;
        }

        private void GluingJson()
        {
            string content;
            var assets = cx.Assets;
            using (StreamReader sr = new StreamReader(assets.Open("CountryCodes.json")))
            {
                content = sr.ReadToEnd();
            }

            data_countries = JsonConvert.DeserializeObject<List<CountryModel>>(content);


            countryList.ForEach(x =>
            {
                var ind = data_countries.FindIndex(r => r.Iso3Code.Equals(x.Iso3Code));

                if (ind > 0)
                    data_countries[ind].LocaleName = x.LocaleName;
            });


            data_countries.RemoveAll(c => c.LocaleName == null);
        }


        private bool IsConnected()
        {
            var connectivityManager = (ConnectivityManager)cx.GetSystemService(Context.ConnectivityService);
            var networkInfo = connectivityManager.ActiveNetworkInfo;

            return networkInfo != null && networkInfo.IsAvailable && networkInfo.IsConnected;
        }

        /*
         * Get location of an address
         */
        private IList<Address> AddressLocation(string address)
        {
            IList<Address> addr = null;
            try
            {
                addr = gc.GetFromLocationName(address, 1);
            }
            catch (Exception)
            {
            }

            return addr;
        }

    }
}