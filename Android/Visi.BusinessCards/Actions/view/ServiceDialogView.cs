using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Visi.BusinessCards.Actions.view
{
    public class ServiceDialogView : Dialog
    {
        private Context cx;

        public ServiceDialogView(Context context) : base(context)
        {
            this.cx = context;
            var feature = this.Window;
            feature.RequestFeature(WindowFeatures.NoTitle);

            Initialize();
        }

        private void Initialize()
        {
            this.SetContentView(Resource.Layout.Service_dialog_view);



        }
    }
}