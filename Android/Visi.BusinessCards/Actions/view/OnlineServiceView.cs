using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Content.Res;
using Visi.BusinessCards.Actions.activity;

namespace Visi.BusinessCards.Actions.view
{
    public class OnlineServiceView : LinearLayout
    {
        private Context cx;
        private Button rm_btn;
        private EditText method;
        private EditText address;
        public OnlineServiceView(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            cx = context;
            Initialize();
        }

        public OnlineServiceView(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            cx = context;
            Initialize();
        }

        private void Initialize()
        {
            Inflate(Context, Resource.Layout.Contact_address_view, this);

            method = FindViewById<EditText>(Resource.Id.bis_card_method);
            address = FindViewById<EditText>(Resource.Id.bis_card_addr);

            rm_btn = FindViewById<Button>(Resource.Id.bcard_rm_addr);

            rm_btn.Click += Handle_Click;


        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.bcard_rm_addr))
                {
                    var id = Convert.ToInt32(Tag);
                    ((CreateBusinessCardActivity)cx).RmInputField(FieldType,id);
                }
            }

        }

        public string FieldType { get; set; }
        public string Method => method.Text;
        public string Address => address.Text;

        /// <summary>
        /// check if one of input fields are empty or not
        /// </summary>
        public bool isEmpty()
        {
            var count = 0;

            if (method.Text.Length.Equals(0))
                count++;
            if (address.Text.Length.Equals(0))
                count++;

            return true ? count > 0 : false;
        }

    }
}