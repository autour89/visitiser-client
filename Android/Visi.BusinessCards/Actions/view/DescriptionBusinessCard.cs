using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Content.Res;
using Android.Graphics;
using Android.Provider;
using Android.Views.InputMethods;
using Android.Support.Design.Widget;

namespace Visi.BusinessCards.Actions.view
{
    public class DescriptionBusinessCard : LinearLayout
    {
        private Context cx;

        private EditText title_name;
        private EditText title_desc;
        private TextView img_link;
        private ImageView biscard_img;
        private TextInputLayout bacrd_name_src;

        public DescriptionBusinessCard(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            cx = context;
            Initialize();
        }

        public DescriptionBusinessCard(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {   
            cx = context;
            Initialize();
        }

        private void Initialize()
        {
            Inflate(Context, Resource.Layout.Description_item_view, this);

            bacrd_name_src = FindViewById<TextInputLayout>(Resource.Id.bcard_input_form);
            title_name = FindViewById<EditText>(Resource.Id.bis_card_name);
            title_desc = FindViewById<EditText>(Resource.Id.bis_card_desc);

            biscard_img = FindViewById<ImageView>(Resource.Id.bis_cardImage);
            img_link = FindViewById<TextView>(Resource.Id.add_image_link);

            IfFieldEmpty(title_name.Text.Length);
            title_name.TextChanged += Handle_TextChanged;
            title_name.EditorAction += Handle_EditorAction;
            img_link.Click += Handle_Click;
            
        }

        private void Handle_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            var editbox = sender as EditText;

            if (editbox != null)
            {
                if (editbox.Id.Equals(Resource.Id.bis_card_name))
                {
                    IfFieldEmpty(e.AfterCount);
                }
            }
        }

        private void Handle_EditorAction(object sender, TextView.EditorActionEventArgs e)
        {
            var editbox = sender as EditText;
            switch (e.ActionId)
            {
                case ImeAction.Next:
                    BCardDesc.RequestFocus();
                    break;
            }
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var textview = sender as TextView;

            if (textview != null)
            {
                if (textview.Id.Equals(Resource.Id.add_image_link))
                {
                    var imageIntent = new Intent();
                    imageIntent.SetType("image/*");
                    imageIntent.SetAction(Intent.ActionGetContent);
                    ((Activity)cx).StartActivityForResult(
                        Intent.CreateChooser(imageIntent, "Select Picture"), 1);
                }
            }
        }

        /// <summary>
        /// Change background resource if edittext length less then 1 character 
        /// </summary>
        /// <param name="count">count characters</param>
        public void IfFieldEmpty(int count)
        {
            if (count < 1)
            {
                bacrd_name_src.SetBackgroundResource(Resource.Drawable.item_border_alarm);
            }
            else
            {
                bacrd_name_src.SetBackgroundResource(Resource.Drawable.item_border);
            }
        }
        
        /// <summary>
        ///Set image for business card
        /// </summary>
        public void SetBisCardImage(Android.Net.Uri uri)
        {
            Bitmap bitmap = MediaStore.Images.Media.GetBitmap(cx.ContentResolver, uri);
            biscard_img.SetImageBitmap(bitmap);

            //var ob = new BitmapDrawable(Resources, bitmap);
            //biscard_img.SetBackgroundDrawable(ob);
        }

        /// <summary>
        ///Business card name
        /// </summary>
        public EditText BCardName => title_name;
        /// <summary>
        ///Business card full description
        /// </summary>
        public EditText BCardDesc => title_desc;
        /// <summary>
        ///Business card Logo
        /// </summary>
        public ImageView BCardImage => biscard_img;

    }
}