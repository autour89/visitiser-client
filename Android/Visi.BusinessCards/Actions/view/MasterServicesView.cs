using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;

namespace Visi.BusinessCards.Actions.view
{
    public class MasterServicesView : LinearLayout
    {
        private Context cx;

        private TextView addService;

        public MasterServicesView(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            Initialize(context);
        }

        public MasterServicesView(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            Initialize(context);
        }

        private void Initialize(Context context)
        {
            this.cx = context;
            Inflate(cx, Resource.Layout.Master_services_view, this);

            addService = FindViewById<TextView>(Resource.Id.add_service_link);

            addService.Click += Handle_Click;

        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var textview = sender as TextView;

            if (textview != null)
            {
                if (textview.Id.Equals(Resource.Id.add_service_link))
                {
                    //var ft = ((CreateBusinessCardActivity)cx).FragmentManager;
                    var serviceDialog = new ServiceDialogView(cx);
                    serviceDialog.Show();

                }
            }
        }
    }
}