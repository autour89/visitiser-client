using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;

namespace Visi.BusinessCards.Actions.view
{
    public class PromoActionView : LinearLayout
    {
        private Context cx;
        
        public PromoActionView(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            Initialize(context);
        }

        public PromoActionView(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            Initialize(context);
        }

        private void Initialize(Context context)
        {
            this.cx = context;
            Inflate(cx, Resource.Layout.Promo_action_view, this);

        }
    }
}