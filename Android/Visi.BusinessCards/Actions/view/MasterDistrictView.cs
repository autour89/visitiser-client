using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Visi.BusinessCards.Actions.view
{
    public class MasterDistrictView : LinearLayout
    {
        private Context cx;
        public MasterDistrictView(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            Initialize(context);
        }

        public MasterDistrictView(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            Initialize(context);
        }

        private void Initialize(Context context)
        {
            this.cx = context;
            Inflate(cx, Resource.Layout.Master_district_view,this);



        }
    }
}