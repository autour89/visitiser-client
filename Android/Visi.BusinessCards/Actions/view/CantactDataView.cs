using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.Content;
using Android.Util;
using Android.Widget;
using Android.Text;
using Android.Support.V7.Widget;
using Newtonsoft.Json;
using Android.App;
using Visi.Generic.Interfaces;
using Visi.Generic.Data;
using Visi.Generic.Models;
using Visi.BusinessCards.Actions.adapter;

namespace Visi.BusinessCards.Actions.view
{
    public class CantactDataView : LinearLayout
    {
        private Context cx;

        private LinearLayout emails_linear;
        private LinearLayout sites_linear;
        private LinearLayout online_service_linear;
        private LinearLayout address_linear;
        private LinearLayout district_linear;

        private List<OnlineServiceView> online_service_list;
        private List<FieldLinkListView> email_list;
        private List<FieldLinkListView> sites_list;
        private List<MasterAddressView> masterAddressList;
        private List<MasterDistrictView> masterDistrictList;

        private AddressDialogView address_dialog;
        private CheckBox isoffice_chk, isonsite_chk;
        private EditText skype_edit, facebook_edit;
        private Spinner phones_spiner;
        private RecyclerView bcard_phones_list;

        private List<string> BCardPhones;

        private Button clear_skype;
        private Button clear_fb;


        private IContentAccess<AccountPhones> AccountPhones => ContentDB.Account_Phones;

        public CantactDataView(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            cx = context;
            Initialize();
        }

        public CantactDataView(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            cx = context;
            Initialize();
        }

        private void Initialize()
        {
            Inflate(Context, Resource.Layout.Contact_data_view, this);

            BCardPhones = new List<string>();
            email_list = new List<FieldLinkListView>();
            sites_list = new List<FieldLinkListView>();
            online_service_list = new List<OnlineServiceView>();
            masterAddressList = new List<MasterAddressView>();
            masterDistrictList = new List<MasterDistrictView>();

            isoffice_chk = FindViewById<CheckBox>(Resource.Id.action_bcard_is_office);
            isonsite_chk = FindViewById<CheckBox>(Resource.Id.bcard_is_onsite);
            skype_edit = FindViewById<EditText>(Resource.Id.link_field_input_skype);
            facebook_edit = FindViewById<EditText>(Resource.Id.link_field_input_facebook);
            clear_skype = FindViewById<Button>(Resource.Id.link_field_skype_rm);
            clear_fb = FindViewById<Button>(Resource.Id.link_field_facebook_rm);
            phones_spiner = FindViewById<Spinner>(Resource.Id.bcard_phones_spiner);

            var phones_layoutManager = new LinearLayoutManager(cx, LinearLayoutManager.Vertical, false);
            bcard_phones_list = FindViewById<RecyclerView>(Resource.Id.bcard_phones_list);
            bcard_phones_list.SetLayoutManager(phones_layoutManager);


            emails_linear = FindViewById<LinearLayout>(Resource.Id.linear_for_emails);
            sites_linear = FindViewById<LinearLayout>(Resource.Id.linear_for_sites);
            online_service_linear = FindViewById<LinearLayout>(Resource.Id.linear_for_online_service);
            address_linear = FindViewById<LinearLayout>(Resource.Id.linear_for_address);
            district_linear = FindViewById<LinearLayout>(Resource.Id.linear_for_districts);

            var addr_link = FindViewById<TextView>(Resource.Id.add_address_link);
            var district_link = FindViewById<TextView>(Resource.Id.add_district_link);
            var online_srv_link = FindViewById<TextView>(Resource.Id.add_online_link);
            var email_link = FindViewById<TextView>(Resource.Id.add_email_link);
            var site_link = FindViewById<TextView>(Resource.Id.add_site_link);

            SetPhonesDropDown();
            phones_spiner.ItemSelected += List_ItemSelected;
            addr_link.Click += Handle_Click;
            district_link.Click += Handle_Click;
            online_srv_link.Click += Handle_Click;
            email_link.Click += Handle_Click;
            site_link.Click += Handle_Click;
            clear_skype.Click += Handle_Click;
            clear_fb.Click += Handle_Click;

        }

        private void List_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var item = JsonConvert.DeserializeObject<SpinnerItem>(Convert.ToString(e.View.Tag));

            if (!IsAdded(item.ItemText) && !item.IsHint)
            {
                BCardPhones.Add(item.ItemText);
                SetPhonesList();
            }
            phones_spiner.SetSelection(0);
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var textview = sender as TextView;
            var button = sender as Button;

            if (textview != null)
            {
                if (textview.Id.Equals(Resource.Id.add_address_link))
                {
                    address_dialog = new AddressDialogView(cx);
                    address_dialog.Show();

                }
                else if (textview.Id.Equals(Resource.Id.add_district_link))
                {

                }
                else if (textview.Id.Equals(Resource.Id.add_online_link))
                {
                    var addr = new OnlineServiceView(cx, null);
                    addr.FieldType = "onlineservice";
                    addr.Tag = online_service_list.Count + 1;
                    online_service_linear.AddView(addr);
                    online_service_list.Add(addr);
                }
                else if (textview.Id.Equals(Resource.Id.add_email_link))
                {
                    var email_view = new FieldLinkListView(cx, null);
                    email_view.FieldType = "email";
                    email_view.SetInputView(InputTypes.TextVariationEmailAddress);
                    email_view.Tag = email_list.Count + 1;
                    emails_linear.AddView(email_view);
                    email_list.Add(email_view);
                }
                else if (textview.Id.Equals(Resource.Id.add_site_link))
                {
                    var site_view = new FieldLinkListView(cx, null);
                    site_view.FieldType = "site";
                    site_view.Tag = sites_list.Count + 1;
                    sites_linear.AddView(site_view);
                    sites_list.Add(site_view);
                }
            }
            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.link_field_skype_rm))
                {
                    skype_edit.Text = "";
                }
                else if (button.Id.Equals(Resource.Id.link_field_facebook_rm))
                {
                    facebook_edit.Text = "";
                }
            }
        }

        public void RmInputField(string field_type, int id)
        {
            switch (field_type)
            {
                case "onlineservice":
                    var view_search_address = online_service_list.Find(x => x.Tag.Equals(id));
                    online_service_list.Remove(view_search_address);
                    online_service_linear.RemoveView(view_search_address);
                    break;
                case "email":
                    var view_search_email = email_list.Find(x => x.Tag.Equals(id));
                    email_list.Remove(view_search_email);
                    emails_linear.RemoveView(view_search_email);
                    break;
                case "site":
                    var view_search_site = sites_list.Find(x => x.Tag.Equals(id));
                    sites_list.Remove(view_search_site);
                    sites_linear.RemoveView(view_search_site);
                    break;
            }
        }


        /// <summary>
        /// is master can service in office 
        /// </summary>
        public CheckBox BCardIsOffice => isoffice_chk;
        /// <summary>
        /// is master can service onsite
        /// </summary>
        public CheckBox BCardIsOnsite => isonsite_chk;
        /// <summary>
        /// master skype
        /// </summary>
        public EditText BCardSkype => skype_edit;
        /// <summary>
        /// master facabook account
        /// </summary>
        public EditText BCardFacebook => facebook_edit;

        /// <summary>
        /// Get master addresses list
        /// </summary>
        public List<MasterAddressView> LocationAddresses => masterAddressList;

        /// <summary>
        /// master phones
        /// </summary>
        public List<BCard_Links> GetPhones()
        {
            var items = new List<BCard_Links>();

            BCardPhones.ForEach(item => items.Add(new BCard_Links
            {
                Name = item
            }));

            return items;
        }
        /// <summary>
        /// master emails
        /// </summary>
        public List<BCard_Links> GetEmails()
        {
            var emailslist = new List<BCard_Links>();
            email_list.ForEach(ph =>
            {
                emailslist.Add(new BCard_Links { Name = ph.LinkInput });
            });

            return emailslist;
        }

        /// <summary>
        /// master websites addresses
        /// </summary>
        public List<BCard_Links> GetSites()
        {
            var siteslist = new List<BCard_Links>();
            sites_list.ForEach(ph =>
            {
                siteslist.Add(new BCard_Links { Name = ph.LinkInput });
            });

            return siteslist;
        }

        /// <summary>
        ///Remove phone
        /// </summary>
        public void RmPhone(string phone)
        {
            BCardPhones.Remove(phone);
            SetPhonesList();
        }

        public void AddLocationAddress()
        {
            var dataset = address_dialog.Location;
            address_dialog.Dismiss();

            var addr = new MasterAddressView(cx, dataset);
            addr.Tag = masterAddressList.Count + 1;
            address_linear.AddView(addr);
            masterAddressList.Add(addr);
        }

        private void SetPhonesDropDown()
        {
            var list_ac_ph = AccountPhones.SelectAllData<AccountPhones>().Select(x => x.PhoneNumber).ToList();
            var list_items = new List<SpinnerItem>();

            list_items.Add(new SpinnerItem
            {
                ItemText = cx.GetString(Resource.String.Title_phones_list),
                IsHint = true
            });
            list_ac_ph.ForEach(item => list_items.Add(new SpinnerItem
            {
                ItemText = item,
                IsHint = false
            }));

            phones_spiner.Adapter = new PhonesSpinerAdapter(cx, list_items);

        }

        private void SetPhonesList()
        {
            var ph_adapter = new BCardPhoneAdapter(cx, BCardPhones);
            bcard_phones_list.SetAdapter(ph_adapter);
        }

        private bool IsAdded(string phone)
        {
            var find = BCardPhones.Find(title => title.Equals(phone));

            return true ? find != null : false;
        }

        

    }
}



