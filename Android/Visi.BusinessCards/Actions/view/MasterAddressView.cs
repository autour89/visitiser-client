using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Visi.Generic.Models;
using Visi.BusinessCards.Actions.activity;

namespace Visi.BusinessCards.Actions.view
{
    public class MasterAddressView : LinearLayout
    {
        private Context cx;

        private TextView titleAddress,showMap;
        private ImageButton editView, rmView;

        private BCard_Adresses LocationAddress { get; set; }

        public MasterAddressView(Context context, BCard_Adresses address) :base(context)
        {
            LocationAddress = address;
            Initialize(context);
        }


        private void Initialize(Context context)
        {
            this.cx = context;
            Inflate(cx, Resource.Layout.Master_address_view, this);

            titleAddress = FindViewById<TextView>(Resource.Id.name_address_view);
            showMap = FindViewById<TextView>(Resource.Id.show_map_action);
            editView = FindViewById<ImageButton>(Resource.Id.edit_address_action);
            rmView = FindViewById<ImageButton>(Resource.Id.remove_address_action);

            titleAddress.Text = LocationAddress.Business_Card_Adress_NM;

            showMap.Click += Handle_Click;
            editView.Click += Handle_Click;
            rmView.Click += Handle_Click;
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var imagebutton = sender as ImageButton;
            var textview = sender as TextView;

            if (imagebutton != null)
            {
                if (imagebutton.Id.Equals(Resource.Id.edit_address_action))
                {
                }
                else if (imagebutton.Id.Equals(Resource.Id.remove_address_action))
                {
                }
            }
            else if (textview != null)
            {
                if (textview.Id.Equals(Resource.Id.show_map_action))
                {
                    //show on the map
                    var ft = ((CreateBusinessCardActivity)cx).FragmentManager;
                    var map_dialog = new MapDialogView(cx, LocationAddress);
                    map_dialog.RetainInstance = true;
                    map_dialog.Show(ft, "");

                }
            }
        }
    }
}