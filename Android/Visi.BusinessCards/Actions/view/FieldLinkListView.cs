using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

using Android.Text;
using Visi.BusinessCards.Actions.activity;

namespace Visi.BusinessCards.Actions.view
{
    public class FieldLinkListView : LinearLayout
    {
        private Context cx;
        private EditText link_input;
        private Button rm_btn;
        public FieldLinkListView(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            cx = context;
            Initialize();
        }

        public FieldLinkListView(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            cx = context;
            Initialize();
        }

        private void Initialize()
        {
            Inflate(Context, Resource.Layout.Field_link_list_view, this);

            link_input = FindViewById<EditText>(Resource.Id.link_field_input);
            rm_btn = FindViewById<Button>(Resource.Id.link_field_rm);

            rm_btn.Click += Handle_Click;
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.link_field_rm))
                {
                    var id = Convert.ToInt32(Tag);

                    ((CreateBusinessCardActivity)cx).RmInputField(FieldType, id);
                }
            }
        }

        public string FieldType { get; set; }
        public string LinkInput => link_input.Text;

        public void SetInputView(InputTypes input)
        {
            link_input.InputType = input;
        }

        /// <summary>
        /// check input field are empty or not
        /// </summary>
        public bool isEmpty()
        {
            var count = 0;

            if (link_input.Text.Length.Equals(0))
                count++;

            return true ? count > 0 : false;
        }


    }
}