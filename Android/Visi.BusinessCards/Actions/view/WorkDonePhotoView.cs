using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Provider;
using Android.Graphics.Drawables;
using Visi.BusinessCards.Actions.activity;

namespace Visi.BusinessCards.Actions.view
{
    public class WorkDonePhotoView : LinearLayout
    {
        private Context cx;
        private ImageView img;
        private EditText img_desc;
        private ImageButton rm_view;

        public WorkDonePhotoView(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            Initialize(context);
        }

        public WorkDonePhotoView(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            Initialize(context);
        }

        private void Initialize(Context context)
        {
            this.cx = context;
            Inflate(cx, Resource.Layout.Work_done_photo_view, this);

            img = FindViewById<ImageView>(Resource.Id.work_perfomed_image);
            img_desc = FindViewById<EditText>(Resource.Id.image_description_box);
            rm_view = FindViewById<ImageButton>(Resource.Id.clear_image_description);

            rm_view.Click += Handle_Click;


            var imageIntent = new Intent();
            imageIntent.SetType("image/*");
            imageIntent.SetAction(Intent.ActionGetContent);
            ((Activity)cx).StartActivityForResult(
                Intent.CreateChooser(imageIntent, "Select Picture"), 2);

        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var imagebuttom = sender as ImageButton;

            if (imagebuttom != null)
            {
                if (imagebuttom.Id.Equals(Resource.Id.clear_image_description))
                {
                    var id = Convert.ToInt32(Tag);
                    ((CreateBusinessCardActivity)cx).RmWorkPerfomedPhoto(id,ParentId);
                }
            }
        }

        /// <summary>
        /// Get image signed text
        /// </summary>
        public string ImageDescription => img_desc.Text;

        /// <summary>
        ///Set image for work perfomed item
        /// </summary>
        public void SetImage(Android.Net.Uri uri)
        {
            var bitmap = MediaStore.Images.Media.GetBitmap(cx.ContentResolver, uri);
            img.SetImageBitmap(bitmap);
        }

        /// <summary>
        /// Get image of current view
        /// </summary>
        /// <returns>return image bytes</returns>
        public byte[] GetImageByte()
        {

            var bitmap = ((BitmapDrawable)img.Drawable).Bitmap;
            var img_bytes = ((CreateBusinessCardActivity)cx).GetImageBytes(bitmap);

            return img_bytes;
        }

        /// <summary>
        /// Get id of parent view
        /// </summary>
        public int ParentId { get; set; }
    }
}