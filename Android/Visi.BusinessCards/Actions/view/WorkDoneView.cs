using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Visi.BusinessCards.Actions.activity;

namespace Visi.BusinessCards.Actions.view
{
    public class WorkDoneView : LinearLayout
    {
        private Context cx;

        private Button add_work_done_image;
        private LinearLayout linear_work_done_image;

        private List<WorkDonePhotoView> workDoneImageList;
        private WorkDonePhotoView wait_forRes;

        public WorkDoneView(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            Initialize(context);
        }

        public WorkDoneView(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            Initialize(context);
        }

        private void Initialize(Context context)
        {
            this.cx = context;
            Inflate(cx, Resource.Layout.Work_done_view, this);

            workDoneImageList = new List<WorkDonePhotoView>();

            linear_work_done_image = FindViewById<LinearLayout>(Resource.Id.linear_for_work_done_image);
            add_work_done_image = FindViewById<Button>(Resource.Id.work_done_add_image);

            add_work_done_image.Click += Handle_Click;
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.work_done_add_image))
                {
                    if (workDoneImageList.Count <= 10)
                    {
                        var t = Convert.ToInt32(this.Tag);
                        ((CreateBusinessCardActivity)cx).SetWorkDoneTag(t);
                        wait_forRes = new WorkDonePhotoView(cx, null);
                        wait_forRes.ParentId = t;
                    }
                    
                }
            }
        }

        /// <summary>
        /// Add image to list.
        /// </summary>
        /// <param name="data">Image path.</param>
        public void AddNewImage(Android.Net.Uri data)
        {
            wait_forRes.SetImage(data);
            wait_forRes.Tag = workDoneImageList.Count + 1;
            linear_work_done_image.AddView(wait_forRes);
            workDoneImageList.Add(wait_forRes);
        }

        /// <summary>
        /// Remove image from list.
        /// </summary>
        /// <param name="id">id of item in images list.</param>
        public void ImageRemove(int id)
        {
            var view_search_res = workDoneImageList.Find(x => x.Tag.Equals(id));
            workDoneImageList.Remove(view_search_res);
            linear_work_done_image.RemoveView(view_search_res);
        }

        /// <summary>
        /// Get work perfomed images list.
        /// </summary>
        public List<WorkDonePhotoView> WorkDonePhoto => workDoneImageList;

    }
}