using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.View;
using Visi.BusinessCards.Actions.activity;
using Visi.BusinessCards.Actions.view;

namespace Visi.BusinessCards.Actions.adapter
{
    public class CreateBCardTabsAdapter : PagerAdapter
    {
        private Context cx;
        private View view;
        private const int TabsCount = 5;

        private Button publish_bcard, add_to_notvisible, rm_bcard;

        public CreateBCardTabsAdapter(Context context)
        {
            cx = context;

        }

        public override int Count => TabsCount;

        public override Java.Lang.Object InstantiateItem(ViewGroup container, int position)
        {
            var pager = (ViewPager)container;

            SetResource(position);
            //var inflater = (LayoutInflater)cx.GetSystemService(Context.LayoutInflaterService);
            //view = inflater.Inflate(resId,null);

            if (view != null)
            {

            }
            
            view.Tag = position;


            publish_bcard = view.FindViewById<Button>(Resource.Id.action_bcard_publish);
            add_to_notvisible = view.FindViewById<Button>(Resource.Id.action_bcard_not_active);
            rm_bcard = view.FindViewById<Button>(Resource.Id.action_bcard_remove);

            publish_bcard.Click += Handle_Click;
            add_to_notvisible.Click += Handle_Click;
            rm_bcard.Click += Handle_Click;

            pager.AddView(view);

            return view;
        }


        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.action_bcard_publish))
                {
                    ((CreateBusinessCardActivity)cx).SaveDate("action_bcard_publish");

                }
                else if (button.Id.Equals(Resource.Id.action_bcard_not_active))
                {
                    ((CreateBusinessCardActivity)cx).SaveDate("action_bcard_not_active");
                }
                else if (button.Id.Equals(Resource.Id.action_bcard_remove))
                {
                    ((CreateBusinessCardActivity)cx).SaveDate("action_bcard_not_active");
                }
            }
        }

        private void SetResource(int position)
        {
            switch (position)
            {
                case 0:
                    view = new DescriptionBusinessCard(cx,null);
                    break;
                case 1:
                    view = new CantactDataView(cx, null);
                    break;
                case 2:
                    view = new MasterServicesView(cx, null);
                    break;
                case 3:
                    view = new PortfolioTabView(cx, null);
                    break;
                case 4:
                    view = new PromoActionView(cx, null);
                    break;
            }
        }
        

        public override void DestroyItem(View container, int position, Java.Lang.Object view)
        {
            var viewPager = (ViewPager)container;
            viewPager.RemoveView(view as View);
        }

        public override bool IsViewFromObject(View view, Java.Lang.Object objectValue)
        {
            return view == objectValue;
        }

    }
}