using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Java.Lang;

namespace Visi.BusinessCards.Actions.adapter
{
    public class CountryListAdapter : ArrayAdapter<string>
    {
        private Context context;
        private List<string> Items;
        public CountryListAdapter(Context context, List<string> items) : base(context, 0, items)
        {
            this.context = context;
            this.Items = items;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            if (convertView == null)
            {
                convertView = LayoutInflater.From(context).Inflate(Resource.Layout.Content_adapter, parent, false);

                var title = convertView.FindViewById<TextView>(Resource.Id.content_text_title);

                title.Text = GetItem(position);
            }

            return convertView;
        }

    }
}



//public class CountryListAdapter : BaseAdapter<string>
//{
//    private Context _context;
//    private View view;
//    List<string> Items { get; set; }
//    public CountryListAdapter(Context cx, List<string> dataset) : base()
//    {
//        this._context = cx;
//        Items = dataset;
//    }

//    public override int Count => Items.Count;

//    public override string this[int position] => Items[position];

//    public override long GetItemId(int position)
//    {
//        return position;
//    }

//    public override View GetView(int position, View convertView, ViewGroup parent)
//    {
//        view = convertView;

//        if (view == null)
//        {
//            LayoutInflater inflater = (LayoutInflater)_context.GetSystemService(Context.LayoutInflaterService);

//            view = inflater.Inflate(Resource.Layout.content_adapter, parent, false);

//            var title = view.FindViewById<TextView>(Resource.Id.content_text_title);

//            title.Text = this[position];
//        }

//        return view;
//    }

//}



//namespace BusinessCards.Actions.adapter
//{
//    public class CountryListAdapter : ArrayAdapter<string>
//    {
//        private Context context;
//        public CountryListAdapter(Context context, List<string> items) : base(context, 0, items)
//        {
//            this.context = context;
//        }


//        public override View GetView(int position, View convertView, ViewGroup parent)
//        {

//            if (convertView == null)
//            {
//                convertView = LayoutInflater.From(context).Inflate(Resource.Layout.content_adapter, parent, false);

//                var title = convertView.FindViewById<TextView>(Resource.Id.content_text_title);

//                title.Text = GetItem(position);
//            }

//            return convertView;
//        }

//    }
//}