using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Visi.BusinessCards.Actions.activity;

namespace Visi.BusinessCards.Actions.adapter
{
    public class BCardPhoneAdapter : RecyclerView.Adapter
    {

        private List<string> items;
        private Context _cx;
        private View itemView;
        public BCardPhoneAdapter(Context context, List<string> items)
        {
            this.items = items;
            this._cx = context;
        }
        public override int ItemCount => items.Count;

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var item = items[position];

            holder.IsRecyclable = false;
            var viewholder = holder as BCardPhoneViewHolder;

            viewholder.IsRecyclable = false;
            viewholder.ItemView.Tag = item;
            viewholder.Title.Text = item;


        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var id = Resource.Layout.Phones_list_layout;
            itemView = LayoutInflater.From(parent.Context).Inflate(id, parent, false);

            return new BCardPhoneViewHolder(itemView);
        }


    }

    class BCardPhoneViewHolder : RecyclerView.ViewHolder
    {
        public TextView Title { get; private set; }
        private Button rm_ph;
        private Context _cx;
        private View item_v;

        public BCardPhoneViewHolder(View itemView) : base(itemView)
        {
            Title = itemView.FindViewById<TextView>(Resource.Id.emptytxt);
            rm_ph = itemView.FindViewById<Button>(Resource.Id.profile_remove_phone);
            _cx = itemView.Context;
            item_v = itemView;
            rm_ph.Click += Handle_Click;
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var item = Convert.ToString(item_v.Tag);
            ((CreateBusinessCardActivity)_cx).RmPhone(item);
        }

    }


}