using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Visi.Generic.Models;

namespace Visi.BusinessCards.Actions.adapter
{
    public class ServiceListAdapter : ArrayAdapter<ServicesModel>
    {
        private Context context;
        public ServiceListAdapter(Context context, List<ServicesModel> items) : base(context, 0, items)
        {
            this.context = context;
        }


        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            string sname = GetItem(position).ServiceName;

            if (convertView == null)
            {
                convertView = LayoutInflater.From(Context).Inflate(Resource.Layout.Add_to_service_list, parent, false);

            }
            convertView.Tag = GetItem(position).ServiceId;
            var srv_nm = convertView.FindViewById<TextView>(Resource.Id.list_srvc_name);

            srv_nm.Text = sname;

            return convertView;
        }
    }
}