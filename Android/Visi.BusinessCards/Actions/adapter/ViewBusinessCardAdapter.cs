using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Android.Graphics;
using Visi.Generic.Data;
using Visi.Generic.Models;

namespace Visi.BusinessCards.Actions.adapter
{
    public class ViewBusinessCardAdapter : RecyclerView.Adapter
    {

        private List<BusinessCard> items;
        private Context _cx;
        private View itemView;
        public ViewBusinessCardAdapter(Context context, List<BusinessCard> items)
        {
            this.items = items;
            this._cx = context;
        }
        public override int ItemCount => items.Count;

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var item = items[position];

            var viewholder = holder as ViewBusinessCardViewHolder;

            viewholder.Title.Text = items[position].Name;
            viewholder.IsRecyclable = false;

            var er = items[position].Logo;
            if (er != null)
            {
                var bmp = BitmapFactory.DecodeByteArray(er, 0, er.Length);
                viewholder.Logo.SetImageBitmap(bmp);
            }


            var aboutlist = new List<CountInfoModel>();
            aboutlist.Add(new CountInfoModel { DrawId = Resource.Drawable.ic_views_count, InfoTitle = _cx.GetString(Resource.String.Title_bcard_views), Info = "3" });
            aboutlist.Add(new CountInfoModel { DrawId = Resource.Drawable.ic_done_visit, InfoTitle = _cx.GetString(Resource.String.Title_bcard_done), Info = "3" });
            aboutlist.Add(new CountInfoModel { DrawId = Resource.Drawable.ic_in_favorites, InfoTitle = _cx.GetString(Resource.String.Title_bcard_infav), Info = "3" });
            aboutlist.Add(new CountInfoModel { DrawId = Resource.Drawable.ic_reservation_visit, InfoTitle = _cx.GetString(Resource.String.Title_bcard_reserv), Info = "3" });
            aboutlist.Add(new CountInfoModel { DrawId = Resource.Drawable.ic_messages, InfoTitle = _cx.GetString(Resource.String.Title_bcard_msgs), Info = "3" });
            aboutlist.Add(new CountInfoModel { DrawId = Resource.Drawable.ic_in_shares, InfoTitle = _cx.GetString(Resource.String.Title_bcard_shares), Info = "3" });

            var grid_adapter = new CountInfoAdapter(_cx, aboutlist);

            viewholder.Aboutinfo.Adapter = grid_adapter;


            viewholder.ItemView.Tag = position;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var id = Resource.Layout.Business_card_list_layout;
            itemView = LayoutInflater.From(parent.Context).Inflate(id, parent, false);

            return new ViewBusinessCardViewHolder(itemView);
        }


    }

    class ViewBusinessCardViewHolder : RecyclerView.ViewHolder
    {
        public ImageView Logo { get; private set; }
        public TextView Title { get; private set; }
        public GridView Aboutinfo { get; private set; }

        private Button ActionDelete { get; set; }
        
        public ViewBusinessCardViewHolder(View itemView) : base(itemView)
        {
            Logo = itemView.FindViewById<ImageView>(Resource.Id.business_card_pic);
            Title = itemView.FindViewById<TextView>(Resource.Id.business_card_title);
            Aboutinfo = itemView.FindViewById<GridView>(Resource.Id.action_about_info);

            ActionDelete = itemView.FindViewById<Button>(Resource.Id.bcard_action_delete);
            ActionDelete.Click += Handle_Click;
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.bcard_action_delete))
                {

                }
            }
        }

       
    }


}

