using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Visi.BusinessCards.Actions.activity;

namespace Visi.BusinessCards.Actions.adapter
{
    public class TabTitleListAdapter : RecyclerView.Adapter
    {
        private List<string> items;
        private Context _cx;
        private View itemView;

        public TabTitleListAdapter(Context context, List<string> items)
        {
            this._cx = context;
            this.items = items;
        }
        public override int ItemCount => items.Count;

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            holder.IsRecyclable = false;

            var item = items[position];

            var viewholder = holder as TabTitleListViewHolder;

            viewholder.Title.Text = item;

            viewholder.ItemView.Tag = position;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var id = Resource.Layout.Tab_title_list_adapter;
            itemView = LayoutInflater.From(parent.Context).Inflate(id, parent, false);

            return new TabTitleListViewHolder(itemView);
        }


    }

    public class TabTitleListViewHolder : RecyclerView.ViewHolder, View.IOnClickListener
    {
        public TextView Title { get; private set; }
        private View itemView;

        public TabTitleListViewHolder(View itemView) : base(itemView)
        {
            Title = itemView.FindViewById<TextView>(Resource.Id.title_text);
            this.itemView = itemView;
            itemView.SetOnClickListener(this);
        }

        public void OnClick(View v)
        {
            var position = Convert.ToInt32(v.Tag);

            SetViewBackground(1);

            ((CreateBusinessCardActivity)v.Context).ChangeTab(position);
        }


        public void SetViewBackground(int state)
        {
            switch (state)
            {
                case 0:
                    itemView.SetBackgroundResource(Resource.Drawable.list_selector);
                    break;
                case 1:
                    itemView.SetBackgroundResource(Resource.Drawable.item_background_focused);
                    break;

            }
        }
    }


}

