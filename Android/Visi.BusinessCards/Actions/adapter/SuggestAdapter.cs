using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Visi.Generic.Models;

namespace Visi.BusinessCards.Actions.adapter
{
    public class SuggestAdapter : ArrayAdapter<ServicesModel>
    {
        private Context context;
        public SuggestAdapter(Context context, List<ServicesModel> items) : base(context, 0, items)
        {
            this.context = context;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            string sname = GetItem(position).ToString();

            if (convertView == null)
            {
                convertView = LayoutInflater.From(Context).Inflate(Resource.Layout.Search_services_list, parent, false);


            }
            var srv_nm = convertView.FindViewById<TextView>(Resource.Id.service_name);

            convertView.Tag = GetItem(position).ServiceId;

            srv_nm.Text = sname;

            return convertView;
        }


    }
}