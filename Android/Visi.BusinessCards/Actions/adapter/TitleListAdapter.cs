using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Visi.BusinessCards.Actions.adapter
{
    public class TitleListAdapter : BaseAdapter<string>
    {

        private Context cx;
        private View row;

        List<string> Items { get; set; }

        public TitleListAdapter(Context context, List<string> items) : base()
        {
            this.cx = context;
            Items = items;
        }
       
        public override int Count => Items.Count;
        public override string this[int position] => Items[position];

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            row = convertView;
            if (row == null)
            {
                var inflater = (LayoutInflater)cx.GetSystemService(Context.LayoutInflaterService);

                row = inflater.Inflate(Resource.Layout.Tab_title_list_adapter, parent, false);
            }

            var textTitle = row.FindViewById<TextView>(Resource.Id.title_text);

            textTitle.Text = Items[position];

            return row;
        }


    }
}