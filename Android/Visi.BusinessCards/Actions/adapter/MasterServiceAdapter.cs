using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;

namespace Visi.BusinessCards.Actions.adapter
{
    public class MasterServiceAdapter : RecyclerView.Adapter
    {
        private List<string> items;
        private Context _cx;
        private View itemView;

        public MasterServiceAdapter(Context context, List<string> items)
        {
            this._cx = context;
            this.items = items;
        }
        public override int ItemCount => items.Count;

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            holder.IsRecyclable = false;

            var item = items[position];

            var viewholder = holder as MasterServiceViewHolder;


        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var id = Resource.Layout.Master_service_adapter;
            itemView = LayoutInflater.From(parent.Context).Inflate(id, parent, false);

            return new MasterServiceViewHolder(itemView);
        }


    }

    public class MasterServiceViewHolder : RecyclerView.ViewHolder
    {
        private View itemView;

        public TextView ColumnCount { get; private set; }
        public TextView ServiceDescription { get; private set; }
        public TextView ServicePrice { get; private set; }
        public ImageButton EditService { get; private set; }
        public ImageButton RemoveService { get; private set; }


        public MasterServiceViewHolder(View itemView) : base(itemView)
        {
            this.itemView = itemView;

            ColumnCount = itemView.FindViewById<TextView>(Resource.Id.count_master_service_title);
            ServiceDescription = itemView.FindViewById<TextView>(Resource.Id.description_master_service_title);
            ServicePrice = itemView.FindViewById<TextView>(Resource.Id.price_master_service_title);
            EditService = itemView.FindViewById<ImageButton>(Resource.Id.edit_master_service_action);
            RemoveService = itemView.FindViewById<ImageButton>(Resource.Id.remove_master_service_action);


            EditService.Click += Handle_Click;
            RemoveService.Click += Handle_Click;
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var imagebutton = sender as ImageButton;

            if (imagebutton != null)
            {
                if (imagebutton.Id.Equals(Resource.Id.edit_master_service_action))
                {


                }
                else if (imagebutton.Id.Equals(Resource.Id.remove_master_service_action))
                {

                }

            }

        }
    }
}