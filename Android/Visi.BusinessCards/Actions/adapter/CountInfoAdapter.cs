using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Visi.Generic.Models;

namespace Visi.BusinessCards.Actions.adapter
{
    public class CountInfoAdapter : BaseAdapter<CountInfoModel>
    {
        private Context _context;
        private View view;
        List<CountInfoModel> Items { get; set; }
        public CountInfoAdapter(Context cx, List<CountInfoModel> data) : base()
        {
            this._context = cx;
            Items = data;
        }

        public override int Count => Items.Count;

        public override CountInfoModel this[int position] => Items[position];

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            view = convertView;

            if (view == null)
            {
                LayoutInflater inflater = (LayoutInflater)_context.GetSystemService(Context.LayoutInflaterService);

                view = inflater.Inflate(Resource.Layout.About_info_view, parent, false);

                var about_info_img = view.FindViewById<ImageView>(Resource.Id.about_info_pic);
                var about_info_title = view.FindViewById<TextView>(Resource.Id.about_info_title);
                var about_info_msg = view.FindViewById<TextView>(Resource.Id.about_info_msg);


                about_info_img.SetImageResource(this[position].DrawId);
                about_info_title.Text = this[position].InfoTitle;
                about_info_msg.Text = this[position].Info;

            }

            return view;
        }

    }
}