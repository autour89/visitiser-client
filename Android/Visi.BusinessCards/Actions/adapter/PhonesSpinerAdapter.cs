using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace Visi.BusinessCards.Actions.adapter
{
    public class PhonesSpinerAdapter : BaseAdapter<SpinnerItem>
    {
        private Context _context;
        private View view;
        List<SpinnerItem> Items { get; set; }
        public PhonesSpinerAdapter(Context cx, List<SpinnerItem> data) : base()
        {
            this._context = cx;
            Items = data;
        }

        public override int Count => Items.Count;

        public override SpinnerItem this[int position] => Items[position];

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            view = convertView;

            if (view == null)
            {
                LayoutInflater inflater = (LayoutInflater)_context.GetSystemService(Context.LayoutInflaterService);

                view = inflater.Inflate(Resource.Layout.Content_text_layout, parent, false);

                var phone_title = view.FindViewById<TextView>(Resource.Id.content_text);

                var item_ser = JsonConvert.SerializeObject(this[position]);
                view.Tag = item_ser;
                phone_title.Text = this[position].ItemText;


            }

            return view;
        }

    }

    public class SpinnerItem
    {
        private string Text;
        private bool isHint;

        public SpinnerItem()
        {
        }
        public SpinnerItem(string strItem, bool flag)
        {
            this.isHint = flag;
            this.Text = strItem;
        }

        public string ItemText
        {
            get
            {
                return this.Text;
            }
            set
            {
                this.Text = value;
            }
        }
        public bool IsHint
        {
            get
            {
                return this.isHint;
            }
            set
            {
                this.isHint = value;
            }
        }
    }
}