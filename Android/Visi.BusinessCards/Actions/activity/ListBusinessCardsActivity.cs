using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Visi.Resources.Actions.activity;
using Visi.BusinessCards.Presenter;
using Visi.BusinessCards.Actions.adapter;

namespace Visi.BusinessCards.Actions.activity
{
    [Activity(Theme = "@style/AppTheme")]
    public class ListBusinessCardsActivity : BaseActivity
    {
        private ListBusinessCardsPresenter bcard_cntrlr;
        private TextView title_toolbar;
        private RecyclerView bsnscrd_list;
        protected override int LayoutResource => Resource.Layout.List_business_cards_activity;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Toolbar = FindViewById<Toolbar>(Resource.Id.profile_toolbar);
            title_toolbar = FindViewById<TextView>(Resource.Id.profile_toolbar_text);
            bcard_cntrlr = new ListBusinessCardsPresenter();

            bsnscrd_list = FindViewById<RecyclerView>(Resource.Id.active_bsnscards_list);
            var layoutManager = new LinearLayoutManager(this, LinearLayoutManager.Vertical, false);
            bsnscrd_list.SetLayoutManager(layoutManager);

            SetAdapter();

        }

        private void SetAdapter()
        {
            var data = Intent.GetStringExtra("Date");
            
            if (data.Equals("active"))
            {
                title_toolbar.Text = GetString(Resource.String.Title_active_bcards);
                
                var adapter = new ViewBusinessCardAdapter(this, bcard_cntrlr.ActiveBCard);
                bsnscrd_list.SetAdapter(adapter);
            }
            else if (data.Equals("inactive"))
            {
                title_toolbar.Text = GetString(Resource.String.Title_inactive_bcards);

                var adapter = new ViewBusinessCardAdapter(this, bcard_cntrlr.InActiveBCard);
                bsnscrd_list.SetAdapter(adapter);
            }
            this.SetToolbar();
        }
    }
}
