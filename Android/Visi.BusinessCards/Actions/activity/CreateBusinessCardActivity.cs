using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Toolbar = Android.Support.V7.Widget.Toolbar;

using Android.Locations;
using Android.Graphics;
using Visi.Resources.Actions.activity;
using Visi.BusinessCards.Presenter;
using Visi.BusinessCards.Actions.view;

namespace Visi.BusinessCards.Actions.activity
{
    [Activity(Theme = "@style/AppTheme")]
    public class CreateBusinessCardActivity : BaseActivity
    {
        private NewBusinessCardPresenter bcard_ctrlr;

        private Geocoder gc;
        private BCardTabsView bcardView;

        protected override int LayoutResource => Resource.Layout.Create_bcard_activity;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            bcard_ctrlr = new NewBusinessCardPresenter();

            Toolbar = FindViewById<Toolbar>(Resource.Id.profile_toolbar);

            //var title_toolbar = FindViewById<TextView>(Resource.Id.profile_toolbar_text);
            //title_toolbar.Text = GetString(Resource.String.title_create_bcard);

            //this.SetToolbar();

            bcardView = FindViewById<BCardTabsView>(Resource.Id.bcard_tabs_view);
        }



        public void ChangeTab(int position)
        {
        }

        /// <summary>
        /// command method for start collecting data
        /// </summary>
        /// <param name="kto">name who called this method</param>
        public void SaveDate(string kto)
        {
            var mention = 0;
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            switch (requestCode)
            {
                case 1:
                    if (resultCode.Equals(Result.Ok))
                    {
                        var desc_data = bcardView.DescriptiontData;
                        desc_data.SetBisCardImage(data.Data);
                    }
                    break;

                case 2:
                    if (resultCode.Equals(Result.Ok))
                    {
                        var workPerfomed_data = bcardView.PortfolioData;
                        workPerfomed_data.AddWorkImage(data.Data);
                    }
                    break;
            }

        }

        /// <summary>
        /// bridge from another user contol to remove some additional input 
        /// field dynamicly from layout
        /// </summary>
        public void RmInputField(string field_type, int id)
        {
            var data = bcardView.ContactData;
            data.RmInputField(field_type, id);
        }

        /// <summary>
        /// Remove image from work perfomed list
        /// </summary>
        /// <param name="id">item id</param>
        public void RmWorkPerfomedPhoto(int id,int parent_id)
        {
            var workPerfomed_data = bcardView.PortfolioData;
            workPerfomed_data.RmViewImage(id, parent_id);
        }

        /// <summary>
        /// remove selected phone from the list
        /// </summary>
        public void RmPhone(string phone)
        {
            var data = bcardView.ContactData;
            data.RmPhone(phone);
        }

        /// <summary>
        /// Set id of item in list
        /// </summary>
        /// <param name="id">number in list</param>
        public void SetWorkDoneTag(int id)
        {
            var workPerfomed_data = bcardView.PortfolioData;
            workPerfomed_data.WorkDoneCurrent = id;
        }

        public void AddContactAddress()
        {
            var data = bcardView.ContactData;
            data.AddLocationAddress();
        }

        /// <summary>
        /// Converting image to array of bytes
        /// </summary>
        /// <param name="bitmap">Image Bitmap resource</param>
        /// <returns></returns>
        public byte[] GetImageBytes(Bitmap bitmap)
        {
            using (var ms = new MemoryStream())
            {
                bitmap.Compress(Bitmap.CompressFormat.Jpeg, 90, ms);
                byte[] bitmapData = ms.ToArray();
                return bitmapData;
            }
        }

        /*
         * Get location of an address
         */
        private IList<Address> AddressLocation(string address)
        {
            var addr = gc.GetFromLocationName(address, 1);

            return addr;
        }

    }
}