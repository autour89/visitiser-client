//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//using Android.App;
//using Android.Content;
//using Android.OS;
//using Android.Runtime;
//using Android.Views;
//using Android.Widget;
//using Android.Locations;

//using Newtonsoft.Json;


//namespace Visi.BusinessCards.Actions.activity
//{
//    [Activity(Theme = "@style/AppTheme")]
//    public class ServicesActivity : BaseActivity
//    {
//        private AutoCompleteTextView autocompl;
//        private Task perdiodicTask;

//        private ListView servList;
//        List<string> list_phones;
//        private Button sv_bt;
//        private ListView adapter_phone;
//        private BusinessCardPresenter crd_pres;
//        private Geocoder gc;
//        private Address geo;

//        private BusinessCard obj;
//        protected override int LayoutResource => Resource.Layout.Suggest_layout;

//        public List<string> List_phone
//        {
//            get { return list_phones; }
//            set
//            {
//                if (list_phones != null)
//                    list_phones = value;
//            }
//        }
//        public string Term { get; set; }

//        /*
//         * Init Components
//         */
//        protected override void OnCreate(Bundle savedInstanceState)
//        {
//            base.OnCreate(savedInstanceState);

//            gc = new Geocoder(this);
//            crd_pres = BusinessCardPresenter.Instance;
//            list_phones = new List<String>();

//            var intent_data = Intent.GetStringExtra("mydata");
//            if (intent_data != null)
//                obj = JsonConvert.DeserializeObject<BusinessCard>(intent_data);

//            autocompl = (AutoCompleteTextView)FindViewById(Resource.Id.searchTxt);

//            servList = (ListView)FindViewById(Resource.Id.servs_list);

//            adapter_phone = FindViewById<ListView>(Resource.Id.list_phones);
//            adapter_phone.FastScrollEnabled = true;

//            var btn = (Button)FindViewById(Resource.Id.btnAdd);
//            sv_bt = (Button)FindViewById(Resource.Id.sv_bsns_card);

//            if (obj != null)
//                SetIfEdit();

//            servList.ItemLongClick += ServList_ItemLongClick;
//            autocompl.AfterTextChanged += Autocompl_AfterTextChanged;
//            autocompl.ItemClick += Autocompl_ItemClick;

//            sv_bt.Click += Handle_Click;
//            btn.Click += Handle_Click;
//        }


//        /*
//         * Add service to list
//         */
//        private void Autocompl_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
//        {
//            var tag = Convert.ToInt32(e.View.Tag);
//            var item = crd_pres.Items.Where(x => x.ServiceId.Equals(tag)).Select(y => y).FirstOrDefault();

//            crd_pres.CardServices.Add(new ServicesModel
//            {
//                ServiceId = item.ServiceId,
//                ServiceName = item.ServiceName
//            });
//            servList.Adapter = new ServiceListAdapter(this, crd_pres.CardServices);

//            autocompl.Text = "";
//        }

//        /*
//         * remove service item from the list
//         */
//        private void ServList_ItemLongClick(object sender, AdapterView.ItemLongClickEventArgs e)
//        {
//            var id = Convert.ToInt32(e.View.Tag);

//            var s = crd_pres.CardServices.Where(x => x.ServiceId.Equals(id)).FirstOrDefault();

//            crd_pres.CardServices.Remove(s);

//            servList.Adapter = new ServiceListAdapter(this, crd_pres.CardServices);
//        }

//        /*
//         * Save all changes on page
//         */
//        private void Handle_Click(object sender, System.EventArgs e)
//        {
//            var button = sender as Button;

//            if (button.Id.Equals(Resource.Id.add_new_number))
//            {
//                var nm = FindViewById<EditText>(Resource.Id.add_new_number);
//                List_phone.Add(nm.Text);
//                nm.Text = "";

//                adapter_phone.Adapter = new PhonesAdapter(this, List_phone);
//            }
//            else if (button.Id.Equals(Resource.Id.sv_bsns_card))
//            {
//                var bsns_crd_nm = FindViewById<EditText>(Resource.Id.bsns_card_name);
//                var b_card_descr = FindViewById<EditText>(Resource.Id.bsns_card_descr);
//                var p_email = FindViewById<EditText>(Resource.Id.bsns_card_email);
//                var address = FindViewById<EditText>(Resource.Id.bsnscard_addrs);

//                var phns = "";

//                list_phones.ForEach(x => phns += x + ",");

//                var cardid = 0;
//                if (obj != null)
//                    cardid = obj.Business_Card_ID;

//                var model = new BusinessCardModel
//                {
//                    AccountContactId = crd_pres.UserAccount.Id,
//                    Business_Card_ID = cardid,
//                    Business_Card_NM = bsns_crd_nm.Text,
//                    Business_Card_Descriprion = b_card_descr.Text,
//                    Business_Card_Email = p_email.Text,
//                    Business_Card_Adress = address.Text,
//                    Business_Card_Phones = phns,
//                    Services = crd_pres.CardServices
//                };


//                if (address.Text.Length > 10)
//                {
//                    GeoAddress(address.Text);

//                    if (geo != null && geo.Latitude > 0)
//                        model.Latitude = geo.Latitude;
//                    if (geo != null && geo.Longitude > 0)
//                        model.Longitude = geo.Longitude;
//                }

//                if (obj != null)
//                    crd_pres.UpdateCard(model);
//                else
//                    crd_pres.SetNewCard(model);

//                this.OnBackPressed();
//            }

//        }

//        /*
//         * run the code after delay for searching services 
//         */
//        private void Autocompl_AfterTextChanged(object sender, Android.Text.AfterTextChangedEventArgs e)
//        {
//            var term = (sender as AutoCompleteTextView).Text;

//            Term = term;

//            perdiodicTask = Task.Run(() => PeriodicTask.Run(LoadSearch, new TimeSpan(0, 0, 3), CancellationToken.None));
//        }


//        /*
//         * Load services async
//         */
//        private void LoadSearch()
//        {
//            RunOnUiThread(() =>
//            {
//                var data = crd_pres.FindServices(Term);

//                var sgs = new SuggestAdapter(this, data);

//                var autoCtextView = (AutoCompleteTextView)FindViewById(Resource.Id.searchTxt);
//                autoCtextView.Threshold = 1;
//                autoCtextView.Adapter = sgs;
//                autoCtextView.ShowDropDown();
//            });

//        }

//        /*
//         * set object data if instance not null and in edit mode
//         */
//        private void SetIfEdit()
//        {
//            var bsns_crd_nm = FindViewById<EditText>(Resource.Id.bsns_card_name);
//            var b_card_descr = FindViewById<EditText>(Resource.Id.bsns_card_descr);
//            var p_email = FindViewById<EditText>(Resource.Id.bsns_card_email);
//            var p_addr = FindViewById<EditText>(Resource.Id.bsnscard_addrs);

//            bsns_crd_nm.Text = obj.Business_Card_NM;
//            b_card_descr.Text = obj.Business_Card_Descriprion;
//            p_email.Text = obj.Business_Card_Email;
//            p_addr.Text = obj.Business_Card_Adress;

//            var srvc = new List<ServicesModel>();

//            var phns = obj.Business_Card_Phones.Split(new String[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();

//            List_phone = phns;
//            adapter_phone.Adapter = new PhonesAdapter(this, List_phone);



//            obj.Services.ForEach(x => srvc.Add(new ServicesModel
//            {
//                ServiceId = x.Serv_ID,
//                ServiceName = x.Serv_NM
//            }));
//            crd_pres.CardServices = srvc;
//            servList.Adapter = new ServiceListAdapter(this, crd_pres.CardServices);

//        }
//        /*
//         * Find coordinates by address
//         */
//        private Address GeoAddress(string address)
//        {
//            geo = gc.GetFromLocationName(address, 1).FirstOrDefault();

//            return geo;
//        }

//    }

//}