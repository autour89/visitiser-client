using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Graphics.Drawables;
using Android.Graphics;
using Android.Locations;
using Newtonsoft.Json;
using Visi.Resources.Actions.activity;
using Visi.BusinessCards.Presenter;
using Visi.BusinessCards.Actions.view;
using Visi.Generic.Data;

namespace Visi.BusinessCards.Actions.activity
{
    [Activity(Theme = "@style/AppTheme")]
    public class NewBusinessCardActivity : BaseActivity
    {
        private NewBusinessCardPresenter bcard_ctrlr;
        private Geocoder gc;

        private DescriptionBusinessCard descr_part;
        private CantactDataView contact_data;
        private Button publish_bcard,add_to_notvisible, rm_bcard;

        protected override int LayoutResource => Resource.Layout.New_business_card_activity;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            bcard_ctrlr = new NewBusinessCardPresenter();

            Toolbar = FindViewById<Toolbar>(Resource.Id.profile_toolbar);

            var title_toolbar = FindViewById<TextView>(Resource.Id.profile_toolbar_text);
            title_toolbar.Text = GetString(Resource.String.Title_create_bcard);

            this.SetToolbar();

            gc = new Geocoder(this);


            descr_part = FindViewById<DescriptionBusinessCard>(Resource.Id.descroption_of_card);
            contact_data = FindViewById<CantactDataView>(Resource.Id.contact_data_of_card);

            publish_bcard = FindViewById<Button>(Resource.Id.action_bcard_publish);
            add_to_notvisible = FindViewById<Button>(Resource.Id.action_bcard_not_active);
            rm_bcard = FindViewById<Button>(Resource.Id.action_bcard_remove);

            publish_bcard.Click += Handle_Click;

        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.action_bcard_publish))
                {
                    if (descr_part.BCardName.Text.Length > 5)
                    {
                        var task = Task.Run(() => SetNewData());
                        Task.WhenAll(task).ContinueWith(t => this.Finish());
                    }
                    else
                    {
                        var empty_name = GetString(Resource.String.Title_name_empty);
                        descr_part.BCardName.Hint = empty_name;
                        descr_part.IfFieldEmpty(descr_part.BCardName.Text.Length);
                    }
                    
                }
                else if (button.Id.Equals(Resource.Id.action_bcard_not_active))
                {

                }
                else if (button.Id.Equals(Resource.Id.action_bcard_remove))
                {

                }
            }
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            switch (requestCode)
            {
                case 1:
                    if (resultCode.Equals(Result.Ok))
                        descr_part.SetBisCardImage(data.Data);
                    break;
            }
            
        }

        /// <summary>
        /// bridge from another user contol to remove some additional input 
        /// field dynamicly from layout
        /// </summary>
        public void RmInputField(string field_type, int id)
        {
            contact_data.RmInputField(field_type, id);
        }

        /// <summary>
        /// remove selected phone from the list
        /// </summary>
        public void RmPhone(string phone)
        {
            contact_data.RmPhone(phone);
        }
        /// <summary>
        /// Sets foucus on EditText field
        /// </summary>
        /// <param name="id">id of field to compare with</param>
        public void SetFocus(int id)
        {
            if (id.Equals(Resource.Id.bis_card_name))
            {
                descr_part.BCardDesc.RequestFocus();
            }
        }

        /*
         * Set input fields to object
         */
        private void SetNewData()
        {

            var master_nm = descr_part.BCardName;
            var master_ds = descr_part.BCardDesc;

            var master_img = descr_part.BCardImage;
            var bitmap = ((BitmapDrawable)master_img.Drawable).Bitmap;
            var img_bytes = GetImageBytes(bitmap);


            //var addr_list = contact_data.LocationAddresses();

            //addr_list.ForEach(addr =>
            //{
            //    var location = AddressLocation(addr.Business_Card_City + ", " + addr.Business_Card_Adress_NM);

            //    if (location.Count() > 0)
            //    {
            //        addr.Latitude = location.FirstOrDefault().Latitude;
            //        addr.Longitude = location.FirstOrDefault().Longitude;
            //    }
            //});

            var phones_list = contact_data.GetPhones();
            var email_list = contact_data.GetEmails();
            var sites_list = contact_data.GetSites();


            //var addr_list_json = JsonConvert.SerializeObject(addr_list);
            var phones_list_json = JsonConvert.SerializeObject(phones_list);
            var email_list_json = JsonConvert.SerializeObject(email_list);
            var sites_list_json = JsonConvert.SerializeObject(sites_list);

            var master_skype = contact_data.BCardSkype;
            var master_fb = contact_data.BCardFacebook;
            var master_isoffice = contact_data.BCardIsOffice;
            var master_isonsite = contact_data.BCardIsOnsite;


            var bcard = new BusinessCard
            {
                Name = master_nm.Text,
                Descriprion = master_ds.Text,
                Logo = img_bytes,
                Card_Type = "A",
                //Addresses = addr_list_json,
                Emails = email_list_json,
                Phones = phones_list_json,
                Sites = sites_list_json,
                Facebook = master_fb.Text,
                Skype = master_skype.Text,
                IsOffice = master_isoffice.Checked,
                IsOnSite = master_isonsite.Checked,
                Created_DT = DateTime.Now
            };

            bcard_ctrlr.SaveNewBCard(bcard);

        }

        /*
         * Converting image to array of bytes
         */
        private byte[] GetImageBytes(Bitmap bitmap)
        {
            using (var ms = new MemoryStream())
            {
                bitmap.Compress(Bitmap.CompressFormat.Jpeg, 90, ms);
                byte[] bitmapData = ms.ToArray();
                return bitmapData;
            }
        }

        /*
         * Get location of an address
         */
        private IList<Address> AddressLocation(string address)
        {
            var addr = gc.GetFromLocationName(address, 1);
            
            return addr;
        }

    }
}

