using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Visi.Common.Helpers;
using Visi.Generic.Data;
using Visi.Generic.Helpers;
using Visi.Generic.Interfaces;
using Visi.Generic.Models;
using Visi.Generic.Services;

namespace Visi.BusinessCards.Presenter
{
    public class NewBusinessCardPresenter
    {
        private RestsharpApi access;
        private ContentDB tables_db;
        public IContentAccess<BusinessCard> bcard_table;
        public NewBusinessCardPresenter()
        {
            access = RestsharpApi.Instance(Configuration.ConnectionString);
            bcard_table = ContentDB.Bsns_Cards;
        }


        /// <summary>
        /// Save new business card in db
        /// </summary>
        public void SaveNewBCard(BusinessCard bcard)
        {

            bcard_table.InsertItem(bcard);
            //access.CreateJson(bcard, "/api/BusinessCards/NewBusinessCard/");

        }


    }
}