//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//using Newtonsoft.Json;
//using Visi.Generic.Helpers;
//using Visi.Generic.Data;
//using Visi.Generic.Interfaces;

//namespace Visi.BusinessCards.Presenter
//{
//    public class BusinessCardPresenter2
//    {
//        private RestApi access;
//        private ContentDB tables_db;
//        public List<ServicesModel> CardServices;
//        private static BusinessCardPresenter2 instance = null;
//        public BusinessCardPresenter2()
//        {
//            access = RestApi.ApiInstance;
//            tables_db = new ContentDB();
//            CardServices = new List<ServicesModel>();
//        }


//        public static BusinessCardPresenter2 Instance
//        {
//            get
//            {
//                if (instance == null)
//                    instance = new BusinessCardPresenter2();
//                return instance;
//            }
//        }

//        public List<ServicesModel> Items { get; set; }

//        //private List<ServicesModel> CardServices { get; set; }

//        public VisitizerProfile UserAccount { get { return tables_db.UserAccount; } }


//        /*
//         * set new business card in local and on the server
//         */
//        public void SetNewCard(BusinessCardModel model)
//        {
//            var result = access.CreateJson(model, "/api/Profile/NewBusinessCard/");

//            var data = JsonConvert.DeserializeObject<int>(result);

//            if (data != null)
//            {
//                var cmodel = new BusinessCard
//                {
//                    Business_Card_ID = data,
//                    AccountContactId = tables_db.UserAccount.Id,
//                    Business_Card_NM = model.Business_Card_NM,
//                    Business_Card_Descriprion = model.Business_Card_Descriprion,
//                    Business_Card_Email = model.Business_Card_Email,
//                    Business_Card_Adress = model.Business_Card_Adress,
//                    Business_Card_Phones = model.Business_Card_Phones,
//                    Latitude = model.Latitude,
//                    Longitude = model.Longitude
//                };
//                ContentDB.Bsns_Cards.InsertItem(cmodel);

//                var smodel = new List<BusinessCardServices>();
//                model.Services.ForEach(x => smodel.Add(new BusinessCardServices
//                {
//                    Business_Card_ID = data,
//                    Serv_NM = x.ServiceName,
//                    Serv_ID = x.ServiceId
//                }));
//                ContentDB.Card_Services.InsertList(smodel);
//            }

//            var d = ContentDB.Bsns_Cards.SelectAllData<BusinessCard>().ToList();
//        }

//        /*
//         * Update Business card info in DB
//         */
//        public void UpdateCard(BusinessCardModel model)
//        {
//            var result = access.CreateJson(model, "/api/Profile/UpdateBusinessCard/");

//            var cmodel = new BusinessCard
//            {
//                Business_Card_ID = model.Business_Card_ID,
//                AccountContactId = tables_db.UserAccount.Id,
//                Business_Card_NM = model.Business_Card_NM,
//                Business_Card_Descriprion = model.Business_Card_Descriprion,
//                Business_Card_Email = model.Business_Card_Email,
//                Business_Card_Adress = model.Business_Card_Adress,
//                Business_Card_Phones = model.Business_Card_Phones,
//                Latitude = model.Latitude,
//                Longitude = model.Longitude
//            };
//            ContentDB.Bsns_Cards.UpdateItem(cmodel);

//            var snewmodel = new List<BusinessCardServices>();
//            var current_serv = ContentDB.Card_Services.SelectAllData<BusinessCardServices>().Where(x => x.Business_Card_ID.Equals(model.Business_Card_ID)).ToList();

//            model.Services.ForEach(x => snewmodel.Add(new BusinessCardServices
//            {
//                Business_Card_ID = model.Business_Card_ID,
//                Serv_NM = x.ServiceName,
//                Serv_ID = x.ServiceId
//            }));

//            if (snewmodel.Count > current_serv.Count)
//            {
//                var rm = current_serv.Select(x => x.Serv_ID).ToList();
//                var list_to_upd = snewmodel.Where(item => !rm.Contains(item.Serv_ID)).ToList();
//                ContentDB.Card_Services.InsertList(list_to_upd);
//            }
//            else if (snewmodel.Count < current_serv.Count)
//            {
//                var rm = snewmodel.Select(x => x.Serv_ID).ToList();
//                var list_to_upd = current_serv.Where(item => !rm.Contains(item.Serv_ID)).ToList();

//                list_to_upd.ForEach(x => ContentDB.Card_Services.DeleteItems(x));
//            }

//            //var fd = TablesDB.Bsns_Cards.SelectWithChildren(model.Business_Card_ID);

//            //var d = 1;
//        }


//        /*
//         * Send Request to the server for searching services
//         */
//        public List<ServicesModel> FindServices(string term)
//        {
//            var result = access.CreateJson(term, "api/Profile/FindService/");

//            if (result != null)
//            {
//                var data = JsonConvert.DeserializeObject<List<ServicesModel>>(result);

//                Items = data;

//                return data;
//            }

//            return null;
//        }

//    }
//}