using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Visi.Generic.Helpers;
using Visi.Generic.Data;
using Visi.Generic.Interfaces;
using Visi.Common.Helpers;

namespace Visi.BusinessCards.Presenter
{
    public class ListBusinessCardsPresenter
    {
        private RestsharpApi access;
        private ContentDB tables_db;
        public IContentAccess<BusinessCard> bcard_table;
        public ListBusinessCardsPresenter()
        {
            bcard_table = ContentDB.Bsns_Cards;
        }

        public List<BusinessCard> ActiveBCard => bcard_table.SelectAllData<BusinessCard>().Where(bc => bc.Card_Type.Equals("A")).ToList();

        public List<BusinessCard> InActiveBCard => bcard_table.SelectAllData<BusinessCard>().Where(bc => bc.Card_Type.Equals("NA")).ToList();

    }
}