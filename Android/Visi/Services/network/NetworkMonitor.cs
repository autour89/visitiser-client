using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Widget;
using Android.Net;

namespace Visi.Services.network
{
    public class NetworkMonitor : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            //var comp = new ComponentName(context.PackageName,typeof(NetworkService).Name);
            //intent.PutExtra("IsNetworkConnected",IsConnected(context));
            //PeekService(context, intent.SetComponent(comp));

            if (!IsConnected(context))
                Toast.MakeText(context, "You are disconnected", ToastLength.Long).Show();
        }

        public static bool IsConnected(Context context)
        {
            var connectivityManager = (ConnectivityManager)context.GetSystemService(Context.ConnectivityService);
            var networkInfo = connectivityManager.ActiveNetworkInfo;

            return networkInfo != null && networkInfo.IsAvailable && networkInfo.IsConnected;
        }

    }
}

//[BroadcastReceiver(Name = "droid.visi.NetworkReceiver",
//    Label = "wifi receiver")]
//[IntentFilter(new string[] { "android.net.wifi.STATE_CHANGE", "android.net.conn.CONNECTIVITY_CHANGE" })]