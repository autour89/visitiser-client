using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;

using Visi.Presenter;
using Visi.Generic.Models;

namespace Visi.Services.messaging
{
    [Service]
    [IntentFilter(new String[] { "com.visitizer.MessagingService" })]
    public class MessagingService : Service
    {
        private MessageServicePresenter serviceController;
        private IBinder binder;
        public const string MessageUpdatedAction = "MessagesUpdated";

        public MessagingService()
        {
            serviceController = new MessageServicePresenter();
        }

        public override IBinder OnBind(Intent intent)
        {
            try
            {
                binder = new ServiceBinder<MessagingService>(this);
            }
            catch (Exception ex)
            {
                var err = new ErrorModel
                {
                    Time = DateTime.Now,
                    User = serviceController.UserId().ToString(),
                    //Exception = ex,
                    Detail = "OnBind method was called",
                    Source = "MessagingService",
                    Message = "something happ when you try create instance new ServiceBinder<MessagingService>(this)"
                };
                serviceController.ErrorInterceptor(err);
            }

            return binder;
        }

        public override void OnCreate()
        {
            base.OnCreate();

            try
            {
                serviceController.Msg_running = false;
                serviceController.SetData();
            }
            catch (Exception e)
            {
                var err = new ErrorModel
                {
                    Time = DateTime.Now,
                    User = serviceController.UserId().ToString(),
                    //Exception = e,
                    Detail = "OnCreate method was called",
                    Source = "MessagingService",
                    Message = "something happ when you try to Call SetData() function"
                };
                serviceController.ErrorInterceptor(err);
            }
            

        }

        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            
            try
            {
                if (!serviceController.Msg_running)
                    serviceController.Msg_running = true;

                if (serviceController.Hub != null)
                    serviceController.Hub.ToConnect();
            }
            catch (Exception ex)
            {
                var err = new ErrorModel
                {
                    Time = DateTime.Now,
                    User = serviceController.UserId().ToString(),
                    //Exception = ex,
                    Detail = "OnStartCommand method was called",
                    Source = "MessagingService",
                    Message = "something happ when you try to Call serviceController.Hub.ToConnect() function"
                };
                serviceController.ErrorInterceptor(err);
            }

            return base.OnStartCommand(intent, flags, startId);
        }

        public override void OnDestroy()
        {            
            try
            {
                if (serviceController.Hub != null)
                    serviceController.Hub.ToDisconnect();
            }
            catch (Exception ex)
            {
                var err = new ErrorModel
                {
                    Time = DateTime.Now,
                    User = serviceController.UserId().ToString(),
                    //Exception = ex,
                    Detail = "OnDestroy method was called",
                    Source = "MessagingService",
                    Message = "something happ when you try to Call serviceController.Hub.ToDisconnect() function"
                };
                serviceController.ErrorInterceptor(err);
            }

            base.OnDestroy();
        }

    }
}