using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;

using Visi.Messaging.Actions.fragment;

namespace Visi.Services.messaging
{
    public class MessagingServiceConnection : Java.Lang.Object, IServiceConnection
    {
        static readonly string TAG = typeof(MessagingServiceConnection).FullName;
        public bool IsConnected { get; private set; }
        public MessageFragment Binder { get; private set; }

        public MessagingServiceConnection()
        {
            IsConnected = false;
            Binder = null;
        }

        public void OnServiceConnected(ComponentName name, IBinder service)
        {
            Binder = service as MessageFragment;
            IsConnected = Binder != null;
        }

        public void OnServiceDisconnected(ComponentName name)
        {
            IsConnected = false;
            Binder = null;
        }
    }


    [BroadcastReceiver]
    [IntentFilter(new string[] { MessagingService.MessageUpdatedAction }, Priority = (int)IntentFilterPriority.LowPriority)]
    public class MessageNotificationReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            
        }
    }


    public class MesageReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {

        }
    }

}