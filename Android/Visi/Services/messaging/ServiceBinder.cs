
using Android.OS;

namespace Visi.Services.messaging
{
    public class ServiceBinder<T> : Binder
    {
        T service;
        public ServiceBinder(T service)
        {
            this.service = service;
        }

        public T MessageService => service;
    }
}