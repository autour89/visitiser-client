using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;

namespace Visi.Services.account
{
    [Service]
    [IntentFilter(new[] { "android.accounts.AccountAuthenticator" })]
    [MetaData("android.accounts.AccountAuthenticator", Resource = "@xml/authenticator")]
    public class AccountAuthenticatorService : Service
    {

        private static AccountAuthenticator sAccountAuthenticator = null;

        public static string ACCOUNT_TYPE = "com.visitizer.account";
        public static string ACCOUNT_NAME = "MyApp";
        private Context _context;

        public AccountAuthenticatorService() : base()
        {
        }

        public AccountAuthenticatorService(Context _context) : base()
        {
            this._context = _context;
        }

        public override IBinder OnBind(Intent intent)
        {
            IBinder binder = null;
            if (intent.Action == Android.Accounts.AccountManager.ActionAuthenticatorIntent)
                binder = Authenticator.IBinder;
            return binder;
        }

        private AccountAuthenticator Authenticator
        {
            get
            {
                if (sAccountAuthenticator == null)
                    sAccountAuthenticator = new AccountAuthenticator(this);
                return sAccountAuthenticator;
            }

        }

    }
}