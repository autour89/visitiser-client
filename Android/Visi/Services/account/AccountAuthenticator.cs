using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.Content;
using Android.OS;
using Android.Accounts;
using Android.Text;
using Android.Util;

namespace Visi.Services.account
{
    public class AccountAuthenticator : AbstractAccountAuthenticator
    {
        private Context mContext;
        public AccountAuthenticator(Context context): base(context)
        {
            mContext = context;
        }

        public override Bundle AddAccount(AccountAuthenticatorResponse response, string accountType, string authTokenType, string[] requiredFeatures, Bundle options)
        {
            try
            {
                //var intent = new Intent(mContext, typeof(RegisterActivity));

                //intent.PutExtra(AccountManager.KeyAccountAuthenticatorResponse, response);

                //var result = new Bundle();

                //if (options != null)
                //    result.PutAll(options);

                //result.PutParcelable(AccountManager.KeyIntent, intent);

                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            
        }

        public override Bundle ConfirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options)
        {
            return this.ConfirmCredentials(response, account, options);
        }

        public override Bundle EditProperties(AccountAuthenticatorResponse response, string accountType)
        {
            return null;
        }

        public override Bundle GetAuthToken(AccountAuthenticatorResponse response, Account account, string authTokenType, Bundle options)
        {
            var result = new Bundle();
            var am = AccountManager.Get(mContext.ApplicationContext);
            var authToken = am.PeekAuthToken(account, authTokenType);
            if (TextUtils.IsEmpty(authToken))
            {
                var password = am.GetPassword(account);
                if (!TextUtils.IsEmpty(password))
                {
                    //authToken = VisitAccount.TOKEN_FULL_ACCESS;//AuthTokenLoader.signIn(mContext, account.Name, password);
                }
            }
            if (!TextUtils.IsEmpty(authToken))
            {
                result.PutString(AccountManager.KeyAccountName, account.Name);
                result.PutString(AccountManager.KeyAccountType, account.Type);
                result.PutString(AccountManager.KeyAuthTokenLabel, authToken);
            }
            else
            {
                //var intent = new Intent(mContext, typeof(RegisterActivity));
                //intent.PutExtra(AccountManager.KeyAccountAuthenticatorResponse, response);
                //var bundle = new Bundle();
                //bundle.PutParcelable(AccountManager.KeyIntent, intent);
            }

            return result;
        }

        public override string GetAuthTokenLabel(string authTokenType)
        {
            return null;
        }

        public override Bundle HasFeatures(AccountAuthenticatorResponse response, Account account, string[] features)
        {
            var result = new Bundle();
            result.PutBoolean(AccountManager.KeyBooleanResult, false);
            return result;
        }

        public override Bundle UpdateCredentials(AccountAuthenticatorResponse response, Account account, string authTokenType, Bundle options)
        {
            return null;
        }
    }
}
