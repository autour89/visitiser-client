using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.OS;
using Android.Accounts;

namespace Visi.Services.account
{
    public class VisitiserAccount : Account
    {
        public static String TYPE = "com.visitizer.account";
        public static String TOKEN_FULL_ACCESS = "com.visitizer.TOKEN_FULL_ACCESS";
        public static String KEY_PASSWORD = "com.visitizer.KEY_PASSWORD";

        public VisitiserAccount(Parcel p) : base(p)
        {
        }

        public VisitiserAccount(string name) : base(name, TYPE)
        {
        }

    }
}