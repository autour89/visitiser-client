﻿using System;
using System.Collections.Generic;
using System.Linq;

using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;
using Android.Support.Design.Widget;
using Android.Content;
using Android.Runtime;
using Android.Support.V4.Widget;
using Android.Content.PM;
using Android.Content.Res;

using Visi.Resources.Actions.activity;
using Visi.Presenter;
using Visi.Actions.fragment;
using Visi.Services.messaging;
using Visi.Actions.activity.registration;
using Visi.Generic.Models;

namespace Visi.Actions.activity
{
    [Activity(Label = "Visitizer", Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait, ConfigurationChanges = ConfigChanges.Orientation| ConfigChanges.ScreenSize)]
    public class HomeActivity : BaseActivity
    {
        private HomePresenter hm_pres;

        private DrawerLayout drawerLayout;
        private NavigationView navigationView;
        
        private CalendarFragment calendar_pg;
        private ContactsFragment contactsFragment;
        private ProfileViewFragment profileFragment;

        bool isBound = false;
        private MesageReceiver msgReceiver;
        private MessagingServiceConnection servConnection;

        /*
         * Bounding with Layout by setting resource to base class property
         */
        protected override int LayoutResource => Resource.Layout.HomeTemplate;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            hm_pres = new HomePresenter();
            msgReceiver = new MesageReceiver();

            contactsFragment = new ContactsFragment();
            profileFragment = new ProfileViewFragment();
            calendar_pg = new CalendarFragment();

            //var err = new ErrorModel
            //{
            //    Time = DateTime.Now,
            //    User = hm_pres.UserId().ToString(),
            //    Detail = "Test Error 3",
            //    Source = "View Error",
            //    Message = "something happ"
            //};
            //hm_pres.ErrorInterceptor(err);

            CheckAcount();
        }

        /*
         * create service for SignalR library on main page loads
         * connecting to the server
         */
        protected override void OnStart()
        {
            base.OnStart();

            try
            {
                var intentFilter = new IntentFilter(MessagingService.MessageUpdatedAction) { Priority = (int)IntentFilterPriority.HighPriority };
                RegisterReceiver(msgReceiver, intentFilter);

                var serviceIntent = new Intent(this, typeof(MessagingService));
                servConnection = new MessagingServiceConnection();
                this.StartService(serviceIntent);
                BindService(serviceIntent, servConnection, Bind.AutoCreate);
            }
            catch (Exception ex)
            {
                var err = new ErrorModel
                {
                    Time = DateTime.Now,
                    User = hm_pres.UserId(),
                    Detail = "OnStart method was called",
                    Source = "HomeActivity Visi project",
                    Message = "something happ when you try create RegisterReceiver, MessagingServiceConnection, BindService "
                };
                hm_pres.ErrorInterceptor(err);
            }
        }


        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            switch (requestCode)
            {
                case 1:
                    if (resultCode == Result.Ok)
                    {
                        var header_text = data.GetStringExtra("Data");
                        var hdr = FindViewById<TextView>(Resource.Id.acc_nm);
                        hdr.Text = header_text;
                    }
                    break;

                case 3:
                    if(data != null)
                    {
                        var bytes = data.GetByteArrayExtra("Data");

                        if (bytes != null)
                        {
                            profileFragment.ResetField(requestCode, bytes);
                        }
                    }
                    break;
                case 4:
                    if (data != null)
                    {
                        var str = data.GetStringExtra("Data");
                        profileFragment.ResetField(requestCode, str);
                    }
                    break;
                case 5:
                    if (data != null)
                    {
                        var str = data.GetStringExtra("Data");
                        profileFragment.ResetField(requestCode, str);
                    }
                    break;
                case 6:
                    if (data != null)
                    {
                        var str = data.GetStringExtra("Data");
                        profileFragment.ResetField(requestCode, str);
                    }
                    break;
                case 7:
                    if (data != null)
                    {
                        var str = data.GetStringExtra("Data");
                        profileFragment.ResetField(requestCode, str);
                    }
                    break;

            }

        }

        public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);

            if (calendar_pg != null)
            {
                this.SupportFragmentManager.BeginTransaction().Detach(calendar_pg).Attach(calendar_pg).Commit();
            }
        }

        /*
         * unbounding with the service
         */
        protected override void OnStop()
        {
            if (isBound)
                isBound = false;

            try
            {
                UnregisterReceiver(msgReceiver);
            }
            catch (Exception ex)
            {
                var err = new ErrorModel
                {
                    Time = DateTime.Now,
                    User = hm_pres.UserId(),
                    //Exception = ex,
                    Detail = "OnStop method was called",
                    Source = "HomeActivity Visi project",
                    Message = "something happ when you try to UnregisterReceiver "
                };
                hm_pres.ErrorInterceptor(err);
            }

            base.OnStop();
        }

        
        /*
         * Retreive to register page if account does not exist else creating menu and other functionality
         */
        private void CheckAcount()
        {
            if (hm_pres.isAccountExist())
            {
                hm_pres.EmptyErrorCache();
                SetComponents();
                SetMenu();

                var ft = SupportFragmentManager;
                var frt = ft.BeginTransaction();
                frt.Replace(Resource.Id.mainLayout, profileFragment);
                frt.AddToBackStack(null);
                frt.Commit();
            }
            else
                NavRegister();
        }

        /*
         * Init components
         */
        private void SetComponents()
        {
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.main_drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            navigationView.NavigationItemSelected += Handle_NavigationItemSelected;
        }

        public void SetDrawer()
        {
            var drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, Toolbar, Resource.String.Drawer_open, Resource.String.Drawer_close);
            drawerLayout.SetDrawerListener(drawerToggle);
            drawerToggle.SyncState();
        }

        /*
         * Inflate menu items and set account info
         */
        private void SetMenu()
        {
            navigationView.Menu.Clear();
            navigationView.InflateMenu(Resource.Menu.navigation_menu);

            var navHeaderView = navigationView.InflateHeaderView(Resource.Layout.Nav_header);
            var hdr = (TextView)navHeaderView.FindViewById(Resource.Id.acc_nm);
            hdr.Text = hm_pres.DisplayName();
        }


        /*
         * Navigate to register page
         */
        private void NavRegister()
        {
            var intent = new Intent(this, typeof(WelcomeActivity));
            StartActivity(intent);
        }

        /*
         * Event for replacement one frame by another on menu item selected
         */
        private void Handle_NavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            var ft = SupportFragmentManager;
            var frt = ft.BeginTransaction();
            try
            {
                switch (e.MenuItem.ItemId)
                {
                    case (Resource.Id.nav_cntc):
                        contactsFragment = new ContactsFragment();
                        frt.Replace(Resource.Id.mainLayout, contactsFragment);
                        break;
                    case (Resource.Id.nav_conversations):
                        frt.Replace(Resource.Id.mainLayout, new AccountConversationsFragment());
                        break;
                    case (Resource.Id.nav_profile):
                        frt.Replace(Resource.Id.mainLayout, profileFragment);
                        break;
                    case (Resource.Id.nav_about):
                        frt.Replace(Resource.Id.mainLayout, calendar_pg);
                        break;
                }
                frt.AddToBackStack(null);
                frt.Commit();
                drawerLayout.CloseDrawers();
            }
            catch (Exception ex)
            {

                var err = new ErrorModel
                {
                    Time = DateTime.Now,
                    User = hm_pres.UserId(),
                    Exception = ex,
                    Detail = "Handle_NavigationItemSelected delegate was called",
                    Source = "HomeActivity Visi project",
                    Message = "something happ when you try to change fragment "
                };
                hm_pres.ErrorInterceptor(err);
            }
            
            
        }


        /*
         * method for finding all services running on device, finding your specific services by replacement Service.ClassName etc...
         */
        private IEnumerable<string> GetRunningServices()
        {
            var manager = (ActivityManager)GetSystemService(ActivityService);
            return manager.GetRunningServices(int.MaxValue).Select(
                service => service.Service.ClassName).ToList();
        }


    }
}



