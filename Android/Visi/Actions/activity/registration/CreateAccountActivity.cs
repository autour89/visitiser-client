using System;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Views.InputMethods;
using Android.Content.PM;
using Toolbar = Android.Support.V7.Widget.Toolbar;

using Newtonsoft.Json;
using Visi.Presenter;
using Visi.Generic.Models;
using Visi.Generic.Services;
using Visi.Resources.Actions.activity;

namespace Visi.Actions.activity.registration
{
    [Activity(Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class CreateAccountActivity : BaseActivity
    {
        private CreateAccountPresenter register;

        private EditText sec_code;
        private Button send_data, changeNumber, sendAgain;

        private Button socials;

        protected override int LayoutResource => Resource.Layout.Registration_security_code;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            this.Toolbar = FindViewById<Toolbar>(Resource.Id.Toolbar);
            this.SetToolbar();

            register = new CreateAccountPresenter();

            sec_code = FindViewById<EditText>(Resource.Id.register_securitycode_field);
            send_data = FindViewById<Button>(Resource.Id.register_confirm_button_code);
            changeNumber = FindViewById<Button>(Resource.Id.register_change_button);
            sendAgain = FindViewById<Button>(Resource.Id.register_sendagain_button);
            socials = FindViewById<Button>(Resource.Id.open_social);
            var view_phone = FindViewById<TextView>(Resource.Id.register_confirm_number);

            var intent_data = Intent.GetStringExtra("phoneData");

            if (intent_data != null)
            {
                register.PhoneData = JsonConvert.DeserializeObject<SecurityModel>(intent_data);
            }

            CanSendSecurirtyCode();
            view_phone.Text = register.PhoneData.Phone;

            sec_code.EditorAction += Handle_EditorAction;
            sec_code.TextChanged += Handle_TextChanged;
            send_data.Click += Handle_Click;
            changeNumber.Click += Handle_Click;
            sendAgain.Click += Handle_Click;
        }

        public override void OnBackPressed()
        {
            base.OnBackPressed();
            SetResult(Result.Canceled);
            Finish();
        }

        private void Handle_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            var codeText = e.Text;

            if (codeText.ToString().Length > 0)
            {
                CanSendSecurirtyCode();
            }
        }

        private void Handle_EditorAction(object sender, TextView.EditorActionEventArgs e)
        {
            switch (e.ActionId)
            {
                case ImeAction.Done:
                    CreateAccount();
                    break;
            }
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                switch (button.Id)
                {
                    case Resource.Id.register_change_button:
                        SetResult(Result.Canceled);
                        this.Finish();
                        break;
                    case Resource.Id.register_confirm_button_code:
                        CreateAccount();
                        break;
                    case Resource.Id.open_social:
                        StartActivity(new Intent(this, typeof(SocialsActivity)));
                        break;

                }

            }
        }

        private void CanSendSecurirtyCode()
        {
            if (sec_code.Text.Length == 4)
                send_data.Enabled = true;
            else
                send_data.Enabled = false;
        }


        private void CreateAccount()
        {
            if (ApplicationSettings.IsConnected)
            {
                Task.Run(async () =>
                {
                    await register.SetupNewAccount(sec_code.Text);

                }).ContinueWith(x =>
                {
                    StartApp();
                });
                //await register.SetupNewAccount(sec_code.Text);
                //StartApp();
            }
            else
            {
                var view = FindViewById(Android.Resource.Id.Content);
                var snackbar = Snackbar.Make(view, "No internet connection!", Snackbar.LengthLong);
                snackbar.SetAction("RETRY", this);
                snackbar.Show();
            }
        }

        private void StartApp()
        {
            RunOnUiThread(() =>
            {
                if (register.IsRegister)
                {
                    SetResult(Result.Ok);
                    Finish();
                    var intent = new Intent(this, typeof(HomeActivity));
                    StartActivity(intent);
                }
                else
                {
                    ViewAlert("Somethig was wrong while creating account");
                }
            });
        }

        private void ViewAlert(string message)
        {
            var alert = Toast.MakeText(this, message, ToastLength.Long);
            alert.SetGravity(GravityFlags.CenterVertical & GravityFlags.CenterHorizontal, 0, 0);
            alert.Show();
        }

        private string ParseNumber(string phone)
        {
            var ind = phone.IndexOf(')');

            var trim_result = phone.Substring(ind+1);

            return trim_result;
        }

    }
}

