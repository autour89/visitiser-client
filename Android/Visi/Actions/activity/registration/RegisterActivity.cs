using System;
using System.IO;

using Android.App;
using Android.OS;
using Android.Widget;
using Android.Views.InputMethods;

using System.Reactive.Linq;

using Visi.Presenter;
using Visi.Profile.Actions.adapter;
using Visi.Generic.Helpers;
using Android.Content;
using Visi.Resources.Actions.activity;
using Visi.Generic.Services;
using Android.Content.PM;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Runtime;
using System.Threading.Tasks;

namespace Visi.Actions.activity.registration
{
    [Activity(Theme = "@style/AppTheme",ScreenOrientation = ScreenOrientation.Portrait)]
    public class RegisterActivity : BaseActivity
    {
        private RegisterPresenter _registerPressenter;

        private Spinner countryList;
        private TextView contry_codeText;
        private EditText phoneNumber;
        private CountryListAdapter countriesAdapter;

        protected override int LayoutResource => Resource.Layout.Register_page;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            this.Toolbar = FindViewById<Toolbar>(Resource.Id.Toolbar);
            this.SetToolbar();

            countryList = FindViewById<Spinner>(Resource.Id.register_countriesList);
            contry_codeText = FindViewById<TextView>(Resource.Id.register_country_code);
            phoneNumber = FindViewById<EditText>(Resource.Id.register_add_phone);
            var btn_to_code_confirm = FindViewById<Button>(Resource.Id.register_next_btn);

            SetupCountriesList();

            var intent_data = Intent.GetStringExtra("phone");
            if (intent_data != null)
            {
                SetPhoneBox(intent_data);
            }

            countryList.ItemSelected += Handle_ItemSelected;
            phoneNumber.EditorAction += Handle_EditorAction;
            btn_to_code_confirm.Click += Handle_Click;
        }
        private void SetupCountriesList()
        {
            GetCountries();
            countriesAdapter = new CountryListAdapter(this, _registerPressenter.DataCountries);
            countryList.Adapter = countriesAdapter;
            SelectCountry();
        }

        public override void OnBackPressed()
        {
            base.OnBackPressed();
            SetResult(Result.Canceled);
            Finish();
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == 1)
            {
                if (resultCode == Result.Ok)
                {
                    SetResult(resultCode);
                    Finish();
                }
            }
        }

        private void Handle_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var id = e.Position;
            contry_codeText.Text = "(" + _registerPressenter.DataCountries[id].CallingCode + ")";
        }

        private void Handle_EditorAction(object sender, TextView.EditorActionEventArgs e)
        {
            switch (e.ActionId)
            {
                case ImeAction.Done:
                    SetNewPhone();
                    break;
            }
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.register_next_btn))
                {
                    SetNewPhone();
                }
            }
        }

        /*
         * set data to controls
         */
        private async Task SelectCountry()
        {
            int id = 0;
            if (ApplicationSettings.IsConnected)
            {
                await _registerPressenter.GetCountryInfo();
                id = _registerPressenter.DataCountries.FindIndex(x => x.Code.Equals(_registerPressenter.CountryInfo.CountryCode));
                SetSelection(id);
            }
            else
            {
                id = _registerPressenter.DataCountries.FindIndex(x => x.EnglishName.Contains("United States"));
                SetSelection(id);
            }
        }

        /*
         * wrapper for setting control data
         */
        private void SetSelection(int id)
        {
            countryList.SetSelection(id);
            contry_codeText.Text = "(" + _registerPressenter.DataCountries[id].CallingCode + ")";
        }

        /*
         * wrapper for set phone to editbox in case if user clicked change number at forward activity
         */
        private void SetPhoneBox(string phone)
        {
            phoneNumber.Text = phone;
        }

        /*
         * action on handling button press for add new phone to account
         */
        private void SetNewPhone()
        {
            var phone = contry_codeText.Text + " " + phoneNumber.Text;
            var id = countryList.SelectedItemPosition;
            _registerPressenter.SendSMS(id,phone.ParsePhoneStr(HelperStr.PhoneContains));

            var intent = new Intent(this, typeof(CreateAccountActivity));
            intent.PutExtra("phoneData", _registerPressenter.SerelizeData);
            StartActivityForResult(intent, 1);
        }


        /*
         * get txt file with countries list from assets folder
         */
        private void GetCountries()
        {
            string content;
            var assets = this.Assets;
            using (var sr = new StreamReader(assets.Open("CountryCodes.json")))
            {
                content = sr.ReadToEnd();
                _registerPressenter = new RegisterPresenter(content);
            }
        }


    }

}


