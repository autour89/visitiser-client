using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Widget;
using Visi.Resources.Actions.activity;

namespace Visi.Actions.activity.registration
{
    [Activity(Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class WelcomeActivity : BaseActivity
    {
        private TextView privacy;
        private Button next;
        private CheckBox click_privacy_ch;

        protected override int LayoutResource => Resource.Layout.Visi_welcome;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            privacy = FindViewById<TextView>(Resource.Id.action_privacy_policy);

            next = FindViewById<Button>(Resource.Id.visi_welcome_next);
            next.Enabled = false;

            click_privacy_ch = FindViewById<CheckBox>(Resource.Id.checkbox_privacy);
            click_privacy_ch.CheckedChange += Handle_CheckedChange;

            next.Click += Handle_Click;
            privacy.Click += Handle_Click;
            click_privacy_ch.Click += Handle_Click;
        }

        private void Handle_CheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            var checkbox = sender as CheckBox;

            if (checkbox != null)
            {
                switch (checkbox.Id)
                {
                    case Resource.Id.checkbox_privacy:
                        if (click_privacy_ch.Checked)
                        {
                            next.Enabled = true;
                        }
                        else
                        {
                            next.Enabled = false;
                        }

                        break;
                }
            }
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;
            var textview = sender as TextView;
            var checkbox = sender as CheckBox;

            if (button != null)
            {
                switch (button.Id)
                {
                    case Resource.Id.visi_welcome_next:
                        var intent = new Intent(this, typeof(RegisterActivity));
                        this.StartActivityForResult(intent, 1);
                        break;
                }
                
            }
            else if (textview != null)
            {
                switch (textview.Id)
                {
                    case Resource.Id.action_privacy_policy:
                        var intent = new Intent(this, typeof(PrivacyPolicyActivity));
                        this.StartActivityForResult(intent, 1);
                        break;
                }
            }

            
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if(requestCode == 1)
            {
                if(resultCode == Result.Ok)
                {
                    Finish();
                }
            }
        }

    }
}