using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Visi.Resources.Actions.activity;
using Android.Content.PM;

namespace Visi.Actions.activity.registration
{
    [Activity(Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class PrivacyPolicyActivity : BaseActivity
    {
        protected override int LayoutResource => Resource.Layout.Privacy_policy_layout;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            this.Toolbar = FindViewById<Toolbar>(Resource.Id.Toolbar);
            var privacy_txt = FindViewById<TextView>(Resource.Id.privacy_text);

            this.SetToolbar();

            string content;
            var lng = GetString(Resource.String.Text_privacy_and_policy);
            var path = lng + "/Privacy_policy.txt";
            var assets = this.Assets;
            using (var sr = new StreamReader(assets.Open(path)))
            {
                content = sr.ReadToEnd();
            }
            
            privacy_txt.Text = content;
        }

        public override void OnBackPressed()
        {
            base.OnBackPressed();
            SetResult(Result.Canceled);
            Finish();
        }

    }
}