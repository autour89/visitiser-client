using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.OS;
using Visi.Resources.Actions.activity;
using Android.Content.PM;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace Visi.Actions.activity.registration
{
    [Activity(Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class SocialsActivity : BaseActivity
    {
        protected override int LayoutResource => Resource.Layout.Socials;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Socials);

            this.Toolbar = FindViewById<Toolbar>(Resource.Id.Toolbar);
            this.SetToolbar();

        }
    }
}