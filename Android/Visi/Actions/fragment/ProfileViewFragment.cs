using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Fragment = Android.Support.V4.App.Fragment;
using Toolbar = Android.Support.V7.Widget.Toolbar;

using Visi.Profile.Actions.view;
using Visi.Actions.activity;

namespace Visi.Actions.fragment
{
    public class ProfileViewFragment : Fragment
    {
        private View view;
        private ProfileView profile_view;

        public ProfileViewFragment()
        {

        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override void OnResume()
        {
            base.OnResume();

            if (profile_view != null)
                profile_view.ResetView();
        }

        /*
         * Init page
         */
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.Profile_fragment, container, false);

            var toolbar = view.FindViewById<Toolbar>(Resource.Id.profile_toolbar);

            var activity = (HomeActivity)view.Context;
            activity.Toolbar = toolbar;
            activity.SetToolbar();
            activity.SetDrawer();

            profile_view = view.FindViewById<ProfileView>(Resource.Id.account_profile_view);

            return view;
        }


        public void ResetField(int requestCode, object data)
        {
            profile_view.ResetField(requestCode, data);
        }
        
    }
}