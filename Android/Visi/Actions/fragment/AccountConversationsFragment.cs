using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.OS;
using Android.Views;
using Android.Widget;
using Fragment = Android.Support.V4.App.Fragment;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Visi.Presenter;
using Visi.Actions.activity;
using Visi.Messaging.Actions.fragment;

namespace Visi.Actions.fragment
{
    public class AccountConversationsFragment : Fragment
    {
        private View view;
        private ListView listview;
        private ProgressBar progressbar;
        private AccountConversationsPresenter accnt_conv;
        
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            accnt_conv = new AccountConversationsPresenter();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.AccountConversationsLayout, container, false);

            var toolbar = view.FindViewById<Toolbar>(Resource.Id.convers_list_toolbar);
            var activity = (HomeActivity)view.Context;
            activity.Toolbar = toolbar;
            activity.SetToolbar();
            activity.SetDrawer();

            //var conv_view = view.FindViewById<ConversationsView>(Resource.Id.account_conversation_view);

            listview = (ListView)view.FindViewById(Resource.Id.accnt_convrstns);
            listview.ItemClick += Listview_ItemClick;

            return view;
        }


        /*
         * open selected chat
         */
        private void Listview_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var chatid = Convert.ToInt32(e.View.Tag);
            accnt_conv.GetLocalMsgs(chatid);

            var ft = this.FragmentManager.BeginTransaction();

            ft.Replace(Resource.Id.mainLayout, new MessageFragment(accnt_conv.ChatMsg, chatid));

            ft.AddToBackStack(null);
            ft.Commit();
        }

    }
}
