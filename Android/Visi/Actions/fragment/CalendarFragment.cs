
using Android.OS;
using Android.Views;
using Android.App;
using Android.Content.PM;
using Android.Content.Res;

using Fragment = Android.Support.V4.App.Fragment;
using Visi.Calendar.Actions.view;
using Visi.Actions.activity;

namespace Visi.Actions.fragment
{
    [Activity(ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize)]
    public class CalendarFragment : Fragment
    {
        private View view;
        private DayCalendarView day_calendar;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            //RetainInstance = true;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            if (view == null)
                view = inflater.Inflate(Resource.Layout.Calendar_fragment_layout, container, false);

            day_calendar = view.FindViewById<DayCalendarView>(Resource.Id.calendar_planner_view);
            day_calendar.SetPage();

            var activity = (HomeActivity)this.Context;
            activity.Toolbar = day_calendar.CalToolbar;
            activity.SetToolbar();
            activity.SetDrawer();

            return view;
        }


        public override void OnConfigurationChanged(Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);

            SetView();
        }

        private void SetView()
        {
            var id = Resource.Layout.Calendar_fragment_layout;
            var itemview = LayoutInflater.From(this.Context).Inflate(id, null, false);
            //var itemview = LayoutInflater.FromContext(this.Context).Inflate(id, null, false);

            day_calendar = itemview.FindViewById<DayCalendarView>(Resource.Id.calendar_planner_view);
            day_calendar.SetPage();
            var activity = (HomeActivity)this.Context;
            activity.Toolbar = day_calendar.CalToolbar;
            activity.SetToolbar();
            activity.SetDrawer();
            view = itemview;
        }

    }
}