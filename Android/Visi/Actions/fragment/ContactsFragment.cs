using System;
using System.Threading.Tasks;
using System.Collections.Generic;

using Android.OS;
using Android.Views;
using Android.Widget;

using Fragment = Android.Support.V4.App.Fragment;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using RecyclerView = Android.Support.V7.Widget.RecyclerView;
using LinearLayoutManager = Android.Support.V7.Widget.LinearLayoutManager;

using Visi.Actions.adapter;
using Visi.Presenter;
using Visi.Messaging.Actions.fragment;
using Visi.Actions.activity;

namespace Visi.Actions.fragment
{
    public class ContactsFragment : Fragment
    {
        private View view;
        private ProgressBar progressbar;
        private RecyclerView contactList;
        private LinearLayout textLinear;
        private ContactsAdapter contactsAdapter;
        private SearchView searchContact;

        private ContactsPresenter adrBook;

        public ContactsFragment()
        {
            adrBook = new ContactsPresenter();
        }
        
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.AccountContacts, container, false);

            var toolbar = view.FindViewById<Toolbar>(Resource.Id.Toolbar);

            var activity = (HomeActivity)view.Context;
            activity.Toolbar = toolbar;
            activity.SetToolbar();
            activity.SetDrawer();

            progressbar = (ProgressBar)view.FindViewById(Resource.Id.progressBar_contacts);
            progressbar.Visibility = ViewStates.Visible;

            searchContact = view.FindViewById<SearchView>(Resource.Id.accntSearch);
            textLinear = view.FindViewById<LinearLayout>(Resource.Id.empty_screen_linear);
            contactList = view.FindViewById<RecyclerView>(Resource.Id.accnt_list);

            var ltManager = new LinearLayoutManager(this.Context, LinearLayoutManager.Vertical, false);
            contactList.SetLayoutManager(ltManager);

            adrBook.GetAddressBook(view.Context);

            
            var task = Task.Run(() => 
            {
                adrBook.CheckContacts();
            });            
            Task.WhenAll(task).ContinueWith(t => SetData());

            searchContact.QueryTextSubmit += Acntsrch_QueryTextSubmit;
            searchContact.QueryTextChange += Acntsrch_QueryTextChange;
            searchContact.Close += Acntsrch_Close;

            return view;
        }

        private void Acntsrch_QueryTextChange(object sender, SearchView.QueryTextChangeEventArgs e)
        {
            var term = (sender as SearchView).Query;

            if (term.Length > 0)
            {
                contactsAdapter = new ContactsAdapter(this.Context, adrBook.FilterContacts(term));
                contactList.SetAdapter(contactsAdapter);
            }
        }

        /*
         * filtering contacts
         */
        private void Acntsrch_QueryTextSubmit(object sender, SearchView.QueryTextSubmitEventArgs e)
        {
            var term = (sender as SearchView).Query;

            if (term.Length > 0)
            {
                var data = adrBook.FindContact(term);
            }
        }

        /*
         * show all contacts
         */
        private void Acntsrch_Close(object sender, SearchView.CloseEventArgs e)
        {
            contactsAdapter = new ContactsAdapter(this.Context, adrBook.ContactList);
            contactList.SetAdapter(contactsAdapter);
        }


        private void Listview_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {

        }

        /*
        * selecting contact from the list
        */
        private void Handle_ItemClick(object sender, View v)
        {
            var id = Convert.ToInt32(v.Tag);

            if (id != 0)
            {
                var ft = this.FragmentManager.BeginTransaction();
                Activity.Intent.PutExtra("recId", id.ToString());
                ft.Replace(Resource.Id.mainLayout, new ConversationPagesFragment());

                ft.AddToBackStack(null);
                ft.Commit();
            }
        }


        private void LoadData()
        {
            adrBook.CheckContacts();
        }

        
        /*
        * Loading contacts list asynchronously without awaiting to task complete
        */
        private void SetData()
        {
            this.Activity.RunOnUiThread(() =>
            {
                progressbar.Visibility = ViewStates.Gone;
                adrBook.SetContactModel();
                if (adrBook.ContactList.Count > 0)
                {
                    textLinear.Visibility = ViewStates.Gone;
                    contactsAdapter = new ContactsAdapter(this.Context, adrBook.ContactList);
                    contactsAdapter.ItemClick += Handle_ItemClick;
                    contactList.SetAdapter(contactsAdapter);
                }
                else
                {
                    textLinear.Visibility = ViewStates.Visible;
                }
            });


        }

    }
}


