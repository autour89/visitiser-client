﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Visi.Generic.Models;

namespace Visi.Actions.adapter
{

    public class ContactsAdapter : RecyclerView.Adapter
    {
        private Context cx;
        private bool onBind;

        public ContactsAdapter(Context cx, List<ContactsModel> model) : base()
        {
            this.cx = cx;
            DataHolder = model;
        }

        public List<ContactsModel> DataHolder { get; set; }

        public event EventHandler<View> ItemClick;

        void OnClick(View view)
        {
            if (ItemClick != null)
                ItemClick(this, view);
        }

        public override int ItemCount => DataHolder.Count;

        public override long GetItemId(int position)
        {
            return base.GetItemId(position);
        }
        public override int GetItemViewType(int position)
        {
            return base.GetItemViewType(position);
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            holder.IsRecyclable = true;
            var viewholder = holder as ContactsViewHolder;

            var data = DataHolder[position];

            viewholder.ItemView.Tag = data.AccountContactId;
            viewholder.NickName.Text = data.Name;
            viewholder.PhoneNumber.Text = data.Number;

        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var id = Resource.Layout.Contacts_list;
            var itemView = LayoutInflater.From(parent.Context).Inflate(id, parent, false);

            return new ContactsViewHolder(itemView,OnClick);
        }

    }

    class ContactsViewHolder : RecyclerView.ViewHolder
    {
        private bool is_open = false;

        private View view;

        public TextView NickName { get; set; }
        public TextView PhoneNumber { get; set; }


        public ContactsViewHolder(View itemView, Action<View> clickListener) : base(itemView)
        {
            view = itemView;

            NickName = view.FindViewById<TextView>(Resource.Id.nickname);
            PhoneNumber = view.FindViewById<TextView>(Resource.Id.phone_number);

            view.Click += (sender, e) => clickListener(view);
        }



    }

}

