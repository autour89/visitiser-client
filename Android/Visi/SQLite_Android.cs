using Akavache;
using System;
using System.IO;

using Visi.Generic.Interfaces;

namespace Visi
{
    public class SQLite_Android : ISQLite
    {
        #region ISQLite implementation

        public string GetConnectionString()
        {
            var sqliteFilename = "MessengerSQLite.db3";
            string documentsPath = Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);


            return Path.Combine(documentsPath, sqliteFilename);
        }

        public string GetCacheConnectionString()
        {
            var cacheFilename = "visicache.db";
            string documentsPath = Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            return Path.Combine(documentsPath, cacheFilename);
        }

        public string GetCacheConnectionString(string key)
        {
            string documentsPath = Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string dbPath = Path.Combine(documentsPath, key);

            return Path.Combine(documentsPath, key);
        }

        #endregion
    }
}
