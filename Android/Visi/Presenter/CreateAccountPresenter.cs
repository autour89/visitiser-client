using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Visi.Generic.Helpers;
using Visi.Generic.DAL.DataModel;
using Visi.Generic.Models;
using System.Linq;
using Visi.Generic.Services;
using Visi.Generic.DAL.Services;
using Visi.Generic.Services.ApiClients;

namespace Visi.Presenter
{
    public class CreateAccountPresenter
    {
        private ContentDB tables_db;
        private string serverAccountResult;
        RestsharpApi accessApi;

        public bool IsRegister { get; private set; } = false;
        public SecurityModel PhoneData { get; set; }
       
        public CreateAccountPresenter()
        {
            accessApi = RestsharpApi.Instance(Configuration.ConnectionString);
            tables_db = new ContentDB();
        }

        public async Task SendPhone(object data, string code)
        {
            if (PhoneData.SecurityCode.Equals(code))
            {
                serverAccountResult = await accessApi.SendPostAsJson(ApiUrl.CreateAccount, data);
            }
        }

        /// <summary>
        /// Register new account
        /// </summary>
        public async Task SetupNewAccount(string security_code)
        {
            if (PhoneData.SecurityCode.Equals(security_code))
            {
                var data = new RegisterViewModel
                {
                    Name = "Your name",
                    PhoneNumber = PhoneData.Phone,
                    CountryCode = PhoneData.CountryCode
                };

                serverAccountResult = await accessApi.SendPostAsJson(ApiUrl.CreateAccount, data);

                AccountData accountResponse = null;

                if (serverAccountResult != null && serverAccountResult.Length > 0)
                {
                    accountResponse = JsonConvert.DeserializeObject<AccountData>(serverAccountResult);
                }
                if (accountResponse != null)
                {
                    WriteToDB(accountResponse);
                    IsRegister = true;
                }
            }
        }


        private void WriteToDB(AccountData data)
        {
            var account_data = new ContentAccess<VisitizerProfile>();
            var create_table_contacts = ContentDB.Account_Cont;
            var create_table_bc = ContentDB.Bsns_Cards;

            var profile = new VisitizerProfile
            {
                Id = data.Id,
                Name = data.Name,
                Gender = data.Sex,
                Birth_DT = data.Birth_DT,
                Address = data.Address,
                City = data.City,
                Email = data.Email,
                Logo = data.Logo
            };

            account_data.InsertItem(profile);
            
            var phones = new List<AccountPhones>();
            data.PhoneNumber.ForEach(x => phones.Add(new AccountPhones
            {
                AccountContactId = data.Id,
                CountryCode = x.CountryCode,
                PhoneNumber = x.PhoneNumber
            }));
            ContentDB.Account_Phones.InsertList(phones);
        }


    }
}

