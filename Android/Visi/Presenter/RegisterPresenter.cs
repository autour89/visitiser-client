using System;
using System.Linq;
using System.Collections.Generic;

using Newtonsoft.Json;

using Visi.Generic.Helpers;
using Visi.Generic.Models;
using System.Threading.Tasks;
using Visi.Generic.Services;
using Visi.Generic.Services.ApiClients;

namespace Visi.Presenter
{
    public class RegisterPresenter
    {
        private List<CountryModel> _countries;
        private List<CountryModel> _data_countries;
        private SecurityModel _phoneData;
        private Random _random;
        private RestsharpApi apiRest;

        public RegisterPresenter(string data)
        {
            _phoneData = new SecurityModel();
            _random = new Random();
            apiRest = new RestsharpApi(ApplicationSettings.SmsServiceEndPoint);
            if (Configuration.IsSmsService)
            {
                apiRest.SetAuthenticator(ApplicationSettings.SmsService_Username, ApplicationSettings.SmsService_Password);
            }

            SetLocaleCountries();
            GluingJson(data);
            CleanModel();
            //GetCountryInfo();
        }

        public List<CountryModel> DataCountries => _data_countries;

        public string SerelizeData => JsonConvert.SerializeObject(PhoneData);

        public SecurityModel PhoneData
        {
            get => _phoneData;
            private set =>  _phoneData = value; 
        }

        public DeviceCountry CountryInfo { get; set; }

        private void SetLocaleCountries()
        {
            var locales = Java.Util.Locale.GetAvailableLocales();

            _countries = new List<CountryModel>();
            var count_str = new List<string>();

            foreach (var locale in locales)
            {
                var country = new CountryModel();

                country.LocaleName = locale.DisplayCountry;
                try
                {
                    country.Iso3Code = locale.ISO3Country;
                }
                catch (Exception) { }

                if (locale.DisplayCountry.Trim().Length > 0 && !count_str.Contains(country.LocaleName))
                {
                    count_str.Add(locale.DisplayCountry);
                    _countries.Add(country);
                }
            }
        }

        private void GluingJson(string content)
        {
            _data_countries = JsonConvert.DeserializeObject<List<CountryModel>>(content);


            _countries.ForEach(x =>
            {
                var ind = _data_countries.FindIndex(r => r.Iso3Code.Equals(x.Iso3Code));

                if (ind > 0)
                    _data_countries[ind].LocaleName = x.LocaleName;
            });
        }

        private void CleanModel()
        {
            _data_countries.RemoveAll(c => c.LocaleName == null);
            _data_countries = _data_countries.OrderBy(x => x.LocaleName).ToList();
        }

        public async Task SendSMS(int code, string phone)
        {
            PhoneData.Phone = phone;
            PhoneData.CountryCode = DataCountries[code].CallingCode;

            if (Configuration.IsSmsService)
            {
                PhoneData.SecurityCode = GenerateCode();

                var data = new SmsModel
                {
                    sender = ApplicationSettings.ApplicationOwner,
                    recipient = PhoneData.Phone,
                    message = PhoneData.SecurityCode,
                    dlr = 1
                };
                await apiRest.SendPostAsJson("sms", data);
            }
            else
            {
                PhoneData.SecurityCode = "1111";
            }
        }

        private string GenerateCode()
        {
            return _random.Next(0, 9999).ToString("D4");
        }

        public async Task GetCountryInfo()
        {
            if (ApplicationSettings.IsConnected)
            {
                var api = new RestsharpApi(ApplicationSettings.IpApi);
                var response = await api.GetRequest("json");
                if (response.Length > 1)
                {
                    CountryInfo = JsonConvert.DeserializeObject<DeviceCountry>(response);
                }
            }
        }
    }
}

   