using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Visi.Generic.Helpers;
using Visi.Generic.DAL.DataModel;
using Visi.Generic.Models;
using Visi.Generic.Services;
using Visi.Generic.DAL.Services;
using Visi.Generic.Services.ApiClients;
using System.Threading.Tasks;

namespace Visi.Presenter
{
    public class ContactsAdapterPresenter
    {
        private List<AccountContacts> items;
        private RestsharpApi access;
        private ContentDB tables_db;
        public ContactsAdapterPresenter()
        {
            access = RestsharpApi.Instance(Configuration.ConnectionString);
            tables_db = new ContentDB();
        }

        public List<AccountContacts> Items
        {
            get { return items; }
            set { items = value; }
        }


        /*
         * Add cantact to our DB
         */
        public async Task SyncNewContact(int userid)
        {
            ContentDB.Account_Cont.InsertItem(new AccountContacts { AccountContactId = userid });

            var model = new MessageModel
            {
                Account1 = tables_db.UserAccount.Id,
                Account2 = userid
            };

            await access.PostRequest(ApiUrl.AddToContacts, model);
        }

    }
}