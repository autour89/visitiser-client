using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Visi.Generic.DAL.DataModel;
using Visi.Generic.Helpers;
using Visi.Generic.Models;
using Newtonsoft.Json;
using Visi.Generic.Services;
using Visi.Generic.DAL.Services;
using Visi.Generic.Services.Synchronization;
using Visi.Generic.Services.ApiClients;

namespace Visi.Presenter
{
    public class HomePresenter
    {
        private ContentDB tables_db;
        private CacheHelper cache;
        private RefitApi smartApi;

        public HomePresenter()
        {

            tables_db = new ContentDB();
            cache = CacheHelper.Instance;
            smartApi = RefitApi.Instance(Configuration.ConnectionString);
        }

        public bool isAccountExist()
        {
            return true ? tables_db.UserAccount != null : false;
        }

        public string DisplayName()
        {
            return tables_db.UserAccount.Name;
        }


        public string UserId()
        {
            return tables_db.UserAccount.Id.ToString();
        }

        /// <summary>
        /// Send Error report to server or cach data if no internet connection
        /// </summary>
        public void ErrorInterceptor(ErrorModel data)
        {
            if (ApplicationSettings.IsConnected)
            {
                smartApi.SendReport(data);
            }
            else
            {
                cache.ReportErrors(data);
            }
        }

        public void EmptyErrorCache()
        {
            if (ApplicationSettings.IsConnected)
            {
                var key = cache.GetKey("errors");

                if (key != null)
                {
                    var cache_item = cache.GetData<List<ErrorModel>>(key);
                    smartApi.SendReport(cache_item);
                    cache.EmptyCache(key);
                }
            }
            
        }

    }
}