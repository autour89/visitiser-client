﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Visi.Generic.Helpers;
using Visi.Generic.Models;
using Visi.Generic.Interfaces;
using Visi.Generic.DAL.DataModel;
using Visi.Generic.Services;
using Visi.Generic.DAL.Services;
using Visi.Generic.Services.Synchronization;
using Visi.Generic.Services.ApiClients;
using Visi.Generic.Services.SignalR;

namespace Visi.Presenter
{
    public class MessageServicePresenter
    {
        private CacheHelper cache;
        private RefitApi smartApi;
        private IHubTon hub;
        private ContentDB tables_db;

        private Dictionary<string, string> qrstring = new Dictionary<string, string>();


        public MessageServicePresenter()
        {
            cache = CacheHelper.Instance;
            smartApi = RefitApi.Instance(Configuration.ConnectionString);
        }

        public IHubTon Hub => hub;

        public bool Msg_running { get; set; }

        public void SetData()
        {
            tables_db = new ContentDB();

            if (isAccountExist())
            {
                QueryStr.Add("userid", tables_db.UserAccount.Id.ToString());
                hub = HubTon.Instance(QueryStr);
            }
        }

        public bool isAccountExist()
        {
            return true ? tables_db.UserAccount != null : false;
        }

        public int UserId()
        {
            return tables_db.UserAccount.Id;
        }

        public Dictionary<string, string> QueryStr
        {
            get { return qrstring; }
            set { qrstring = value; }
        }


        /// <summary>
        /// Send Error report to server or cach data if no internet connection
        /// </summary>
        public void ErrorInterceptor(ErrorModel data)
        {
            if (ApplicationSettings.IsConnected)
            {
                smartApi.SendReport(data);
            }
            else
            {
                cache.ReportErrors(data);
            }
        }

    }
}