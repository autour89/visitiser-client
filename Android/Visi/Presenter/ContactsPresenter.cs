using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using Android.Content;
using Android.Provider;
using Android.Database;

using Newtonsoft.Json;

using Visi.Generic.Helpers;
using Visi.Generic.DAL.DataModel;
using Visi.Generic.Models;
using Visi.Generic.Services;
using Visi.Generic.DAL.Services;
using Visi.Generic.Services.ApiClients;

namespace Visi.Presenter
{
    public class ContactsPresenter
    {
        private RestsharpApi access;
        private ContentDB tables_db;
        private DataSearch<ContactsModel> local_srch;
        private List<AccountContacts> list_cont;
        private List<AccountContacts> addressBook;
        private List<ContactsModel> dataHolder;

        public ContactsPresenter()
        {
            access = RestsharpApi.Instance(Configuration.ConnectionString);
            tables_db = new ContentDB();
            local_srch = new DataSearch<ContactsModel>();
            list_cont = new List<AccountContacts>();
            addressBook = new List<AccountContacts>();
            dataHolder = new List<ContactsModel>();

            list_cont = ContentDB.Account_Cont.SelectAllData<AccountContacts>();
        }

        public List<ContactsModel> ContactList => dataHolder;
        public List<AccountContacts> AddressBook => addressBook;

        /*
         *  check for new data in address book if added new -> add it to account contacts
         */
        public async Task CheckContacts()
        {
            list_cont = ContentDB.Account_Cont.SelectAllData<AccountContacts>();

            list_cont = list_cont.Concat(AddressBook).Distinct().GroupBy(x => x.Name).Select(g => g.First()).ToList();

            var check_ifex_num = AddressBook.Select(book => book.PhoneNumber).ToList();


            var list_to_rm = list_cont.Where(item => !check_ifex_num.Contains(item.PhoneNumber)).ToList();

            list_cont.RemoveAll(item => !check_ifex_num.Contains(item.PhoneNumber));

            var model_to_rm = new AccountModel { Items = list_to_rm, UserId = tables_db.UserAccount.Id };

            if (list_to_rm.Count > 0)
            {
                await access.SendPostAsJson(ApiUrl.RemoveFromContacts, model_to_rm);
            }

            var to_check = list_cont.Where(x => x.ContactStatus.Equals("new")).Select(x => x).ToList();

            if (to_check.Count > 0)
            {
                var model_to_check = new AccountModel { Items = to_check, UserId = tables_db.UserAccount.Id };

                var result = await access.SendPostAsJson(ApiUrl.CheckContacts, model_to_check);

                var data = JsonConvert.DeserializeObject<List<AccountContacts>>(result);

                if (data != null)
                {
                    var uniqueList = data.Concat(list_cont).Distinct().GroupBy(x => x.Name).Select(g => g.First()).ToList();

                    list_cont = uniqueList;

                    list_cont.Where(x => x.ContactStatus.Equals("new")).ToList().ForEach(x => x.ContactStatus = "added");

                    ContentDB.Account_Cont.DeleteAllItems();
                    ContentDB.Account_Cont.InsertList(list_cont);
                }

            }
        }

        private async Task IsContactExists()
        {
            var to_check = list_cont.Where(x => x.ContactStatus.Equals("new")).Select(x => x).ToList();

            if (to_check.Count > 0)
            {
                var model_to_check = new AccountModel { Items = to_check, UserId = tables_db.UserAccount.Id };

                var result = await access.SendPostAsJson(ApiUrl.CheckContacts, model_to_check);

                var data = JsonConvert.DeserializeObject<List<AccountContacts>>(result);

                if (data != null)
                {
                    var uniqueList = data.Concat(list_cont).Distinct().GroupBy(x => x.Name).Select(g => g.First()).ToList();

                    list_cont = uniqueList;
                    //List_Cont = Enumerable.Union(List_Cont.Where(x => !data.Any(r => r.PhoneNumber == x.PhoneNumber)), data).ToList();

                    list_cont.Where(x => x.ContactStatus.Equals("new")).ToList().ForEach(x => x.ContactStatus = "added");

                    ContentDB.Account_Cont.DeleteAllItems();
                    ContentDB.Account_Cont.InsertList(list_cont);
                }
                
            }
        }

        /*
         * searhing for new contats in our server side
         */
        public async Task<List<AccountContacts>> FindContact(string term)
        {
            var result = await access.SendPostAsBody(ApiUrl.FindAccnt, term);

            if (result != null)
            {
                var data = JsonConvert.DeserializeObject<List<AccountContacts>>(result);

                return data;
            }

            return null;
        }

        /*
         *  filtering contacts list 
         */
        public List<ContactsModel> FilterContacts(string term)
        {
            var links = term.Split(new String[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries);

            var search_res = local_srch.SearchFilter(links);

            return search_res;
        }

        /*
         *  get device address book
         */
        public void GetAddressBook(Context context)
        {
            var uri = ContactsContract.CommonDataKinds.Phone.ContentUri;

            ICursor cursor = context.ContentResolver.Query(uri, null, null, null, null);
            while (cursor.MoveToNext())
            {
                int name_idx = cursor.GetColumnIndex(ContactsContract.Contacts.InterfaceConsts.DisplayName);
                int phone_idx = cursor.GetColumnIndex(ContactsContract.CommonDataKinds.Phone.Number);
                
                string name = cursor.GetString(name_idx);
                string phone = cursor.GetString(phone_idx);
                

                addressBook.Add(new AccountContacts
                {
                    Name = name,
                    PhoneNumber = phone,
                    ContactStatus = "new"
                });
            }
            cursor.Close();
        }


        public void SetContactModel()
        {
            list_cont.ForEach(x =>
            {
                dataHolder.Add(new ContactsModel
                {
                    Number = x.PhoneNumber,
                    Name = x.Name,
                    AccountContactId = x.AccountContactId,
                    IsContact = false 
                });
            });

            local_srch.Data = dataHolder;
        }


    }
}