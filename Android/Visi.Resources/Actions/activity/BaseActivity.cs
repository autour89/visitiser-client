
using Android.OS;
using Android.Views;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Content;

namespace Visi.Resources.Actions.activity
{
    public abstract class BaseActivity : AppCompatActivity, View.IOnClickListener
    {
        public Toolbar Toolbar { get; set; }

        protected abstract int LayoutResource { get; }

        protected int ActionBarIcon
        {
            set => Toolbar.SetNavigationIcon(value);
        }
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(LayoutResource);

        }

        public void SetToolbar()
        {
            SetSupportActionBar(Toolbar);
            Toolbar.SetNavigationOnClickListener(this);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
        }
        public void OnClick(View v)
        {
            this.OnBackPressed();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;

            if (id == Resource.Id.action_settings_bar)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        /// <summary>
        /// in case if device change its configuration
        /// </summary>
        public void ResetContentView()
        {
            SetContentView(LayoutResource);
        }

    }
}