using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Visi.Messaging.Presenter;
using Visi.Messaging.Actions.adapter;

namespace Visi.Messaging.Actions.view
{
    public class ConversationsView : LinearLayout
    {
        private Context _context;
        private ListView listview;
        private ProgressBar progressbar;
        private SearchView convesation_srch;
        private ConversationsPresenter converstn;
        public ConversationsView(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            _context = context;
            Initialize();
        }

        public ConversationsView(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            _context = context;
            Initialize();
        }

        private void Initialize()
        {
            Inflate(Context, Resource.Layout.Conversations_list_layout, this);

            converstn = new ConversationsPresenter();

            listview = (ListView)FindViewById(Resource.Id.accnt_convrstns);
            progressbar = (ProgressBar)FindViewById(Resource.Id.progressBar);
            convesation_srch = FindViewById<SearchView>(Resource.Id.converstnSrch);


            convesation_srch.QueryTextSubmit += Convesation_srch_QueryTextSubmit;
            convesation_srch.Close += Convesation_srch_Close;


            LoadData();

        }


        /*
         * Loading data asynchronously
         */
        private void LoadData()
        {
            Task[] tasks = new Task[1]
            {
                converstn.NewConversations()
            };

            Task.WhenAll(tasks).ContinueWith(t => SetData());
            progressbar.Visibility = ViewStates.Visible;
        }

        /*
         * show account cinversations
         */
        private void SetData()
        {
            ((Activity)_context).RunOnUiThread(() =>
            {
                progressbar.Visibility = ViewStates.Gone;
                var adp = new ConversationsAdapter(_context, converstn.Items);
                listview.Adapter = adp;
            });
        }


        /*
        * filtering conversation list
        */
        private void Convesation_srch_QueryTextSubmit(object sender, SearchView.QueryTextSubmitEventArgs e)
        {
            var term = (sender as SearchView).Query;

            var data = converstn.FilterConvers(term);

            listview.Adapter = new ConversationsAdapter(_context, data);
        }

        /*
         * if search result not match set all conversations
         */
        private void Convesation_srch_Close(object sender, SearchView.CloseEventArgs e)
        {
            listview.Adapter = new ConversationsAdapter(_context, converstn.Items);
        }

    }
}