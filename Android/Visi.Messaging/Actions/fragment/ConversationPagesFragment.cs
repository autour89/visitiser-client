using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Fragment = Android.Support.V4.App.Fragment;
using Android.Support.V4.View;
using Android.Support.Design.Widget;
using Visi.Messaging.Actions.adapter;
using Visi.Messaging.Presenter;

namespace Visi.Messaging.Actions.fragment
{
    public class ConversationPagesFragment : Fragment
    {
        private View view;
        private CnversationPagesAdapter adapter;
        private ViewPager pager;
        private TabLayout tabs;
        private ConversationPagesPresenter convrstns;

        private string obj;
        public ConversationPagesFragment()
        {
            convrstns = new ConversationPagesPresenter();
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.TabsLayout, container, false);

            var intent_data = Activity.Intent.GetStringExtra("recId");

            convrstns.RecipientId = Convert.ToInt32(intent_data);
            convrstns.GetConversations();

            SetComp();
            //SetData();

            return view;
        }

        private void SetData()
        {
            convrstns.GetAllConversations();

            SetComp();
        }

        /*
         * Show all chats with selected account in Tabs
         */
        private void SetComp()
        {
            adapter = new CnversationPagesAdapter(this.FragmentManager, convrstns.Convrstns);

            pager = view.FindViewById<ViewPager>(Resource.Id.viewpager);
            // Give the TabLayout the ViewPager
            tabs = view.FindViewById<TabLayout>(Resource.Id.sliding_tabs);
            pager.Adapter = adapter;
            tabs.SetupWithViewPager(pager);

            var pageMargin = (int)TypedValue.ApplyDimension(ComplexUnitType.Dip, 4, Resources.DisplayMetrics);
            pager.PageMargin = pageMargin;
            pager.CurrentItem = 1;

            tabs.TabSelected += Tabs_TabSelected;
            tabs.TabReselected += Tabs_TabReselected;
        }
        private void Tabs_TabSelected(object sender, TabLayout.TabSelectedEventArgs e)
        {
            pager.CurrentItem = e.Tab.Position;
        }

        private void Tabs_TabReselected(object sender, TabLayout.TabReselectedEventArgs e)
        {
            pager.CurrentItem = e.Tab.Position;
        }

        private void Tabs_ScrollChange(object sender, View.ScrollChangeEventArgs e)
        {

        }


    }
}

