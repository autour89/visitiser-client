using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Microsoft.AspNet.SignalR.Client;
using System.Threading.Tasks;
using Newtonsoft.Json;

using Fragment = Android.Support.V4.App.Fragment;
using Android.OS;
using Android.Views;
using Android.Content;
using Android.Widget;
using Android.App;

using Microsoft.AspNet.SignalR.Client;
using Visi.Generic.DAL.DataModel;
using Visi.Generic.Interfaces;
using Visi.Messaging.Presenter;
using Visi.Messaging.Actions.adapter;
using Visi.Messaging.Actions.activity;
using Visi.Generic.Models;
using Visi.Generic.Helpers;
using Visi.Generic.Services.SignalR;

namespace Visi.Messaging.Actions.fragment
{
    public class MessageFragment : Fragment
    {
        private View view;
        private List<ChatMessages> data_model;

        private IHubTon hub;
        private MessagingPresenter msg_pres;

        private Context _context;
        private ListView msgs_listview;
        private EditText msg_box;

        private bool isEdit = false;
        private int msgId = 0;

        public MessageFragment(List<ChatMessages> data, int id)
        {
            ChatId = id;
            data_model = data;
            msg_pres = new MessagingPresenter(data_model, id);
        }
        public int ChatId { get; set; }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.Inflate(Resource.Layout.Messaging_view_layout, container, false);

            

            var chatmsgs = view.FindViewById<Button>(Resource.Id.sendMsg);
            var _showvcardlistBtn = view.FindViewById<Button>(Resource.Id.sendServ);
            var chatpic = view.FindViewById<Button>(Resource.Id.sendPic);
            var chatdoc = view.FindViewById<Button>(Resource.Id.sendDoc);

            msg_box = view.FindViewById<EditText>(Resource.Id.msg_body);
            msgs_listview = (ListView)view.FindViewById(Resource.Id.conv_msg_list);

            if (msg_pres != null)
            {
                msgs_listview.Adapter = new MessageAdapter(this.Context, msg_pres.Items);
                RegisterForContextMenu(msgs_listview);
            }

            msgs_listview.ItemClick += Msgs_listview_ItemClick;
            msg_box.TextChanged += Msg_box_TextChanged;


            _showvcardlistBtn.Click += Handle_Click;
            chatmsgs.Click += Handle_Click;
            chatpic.Click += Handle_Click;
            chatdoc.Click += Handle_Click;


            return view;
        }


        private void Msgs_listview_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var b = Convert.ToInt32(e.View.Tag);

            var msg = msg_pres.Items.Where(x => x.MessageId.Equals(b)).Select(x => x).FirstOrDefault();

            if (msg.MessageType.Equals("service"))
            {
                var intent = new Intent(this.Context, typeof(ShowVisitCardActivity));
                intent.PutExtra("data", msg.Message);
                StartActivity(intent);
            }
        }

        public override void OnActivityResult(int requestCode, int resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            switch (requestCode)
            {
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    if (resultCode.Equals(Result.Ok))
                    {
                        var result = Convert.ToInt32(data.GetStringExtra("data"));

                        msg_pres.BusinessCardId = result;
                        msg_pres.MessageType = "service";
                        LoadData();
                    }
                    break;
            }

        }

        private void Msg_box_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            var length = msg_box.Text.Length;

            if (length.Equals(0) || length.Equals(1))
                msg_pres.MessageType = "message";
        }

        /*
         * create context menu for chat message selected
         */
        public override void OnCreateContextMenu(IContextMenu menu, View v, IContextMenuContextMenuInfo menuInfo)
        {
            if (v.Id == Resource.Id.conv_msg_list)
            {
                var info = (AdapterView.AdapterContextMenuInfo)menuInfo;

                var msg = msg_pres.Items[info.Position];

                if (msg.MessageType.Equals("message"))
                {
                    menu.SetHeaderTitle(Resource.String.Select_option);
                    var menuItems = Resources.GetStringArray(Resource.Array.menu);
                    for (var i = 0; i < menuItems.Length; i++)
                        menu.Add(Menu.None, i, i, menuItems[i]);
                }
            }
        }

        /*
         * action with chat message selected
         */
        public override bool OnContextItemSelected(IMenuItem item)
        {
            var info = (AdapterView.AdapterContextMenuInfo)item.MenuInfo;


            var menuItemIndex = item.ItemId;
            var menuItems = Resources.GetStringArray(Resource.Array.menu);
            var menuItemName = menuItems[menuItemIndex];

            var id = Convert.ToInt32(info.Id);

            switch (menuItemIndex)
            {
                case 0:
                    //edit
                    var msg = msg_pres.Items[id];

                    if (msg.MessageType.Equals("message"))
                    {
                        isEdit = true;
                        msgId = msg.MessageId;
                        var item_edit = JsonConvert.DeserializeObject<MessageM>(msg.Message);
                        msg_box.Text = item_edit.Message;
                    }
                    break;
                case 1:
                    //delete
                    msg_pres.RemoveMessaage(id);
                    break;
                case 2:
                    //back
                    msg_box.Text = "";
                    isEdit = false;
                    break;
            }

            msgs_listview.Adapter = new MessageAdapter(this.Context, msg_pres.Items);

            return true;
        }


        /*
         * set changes or send new message
         */
        private void Handle_Click(object sender, System.EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.sendMsg))
                {
                    if (msg_box.Text.Length > 0)
                    {
                        if (isEdit)
                        {
                            msg_pres.EditMessaage(msgId, msg_box.Text);
                            isEdit = false;
                            msgs_listview.Adapter = new MessageAdapter(_context, msg_pres.Items);
                        }
                        else
                        {
                            if (msg_box.Text.Length > 2)
                            {
                                msg_pres.MessageType = "message";
                                msg_pres.TextMessage = msg_box.Text;
                                LoadData();
                            }
                        }

                        msg_box.Text = "";
                    }
                }
                else if (button.Id.Equals(Resource.Id.sendPic))
                {
                    var img_intent = new Intent(Intent.ActionGetContent);
                    img_intent.SetType("image/*");
                    StartActivityForResult(img_intent, 1);
                }
                else if (button.Id.Equals(Resource.Id.sendDoc))
                {
                    var doc_intent = new Intent(Intent.ActionGetContent);
                    doc_intent.SetType("*/*");
                    doc_intent.AddCategory(Intent.CategoryOpenable);
                    StartActivityForResult(Intent.CreateChooser(doc_intent, "Choose file to upload..."), 2);
                }
                else if (button.Id.Equals(Resource.Id.sendServ))
                {
                    var visitcard_intent = new Intent(this.Context, typeof(ShowVisitCardActivity));
                    StartActivityForResult(visitcard_intent, 3);
                }
            }
        }

        /*
         * Invoking action for settong new message to message list 
         */
        private void InvokeAsync()
        {
            hub = HubTon.Instance(msg_pres.QueryStr);

            hub.HubProxy.On<int>("NewMessages", (chatId) =>
            Activity.RunOnUiThread(() =>
            {
                if (chatId.Equals(msg_pres.ChatId))
                {
                    msg_pres.UpdateMessageList();
                    msgs_listview.Adapter = new MessageAdapter(this.Context, msg_pres.Items);
                }
            }));

        }

        /*
         * Async call for updating chat messages list
         */
        private void LoadData()
        {
            var task = Task.Run(() => msg_pres.SendNewMessage());
            Task.WhenAll(task).ContinueWith(t => hub.HubProxy.Invoke("SendPrivateMessage", msg_pres.ChatId));
        }



    }
}