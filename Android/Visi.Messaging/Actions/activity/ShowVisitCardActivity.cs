using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Visi.Resources.Actions.activity;
using Visi.Generic.Models;
using Android.Content.PM;

namespace Visi.Messaging.Actions.activity
{
    [Activity(Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class ShowVisitCardActivity : BaseActivity
    {
        private string obj;

        protected override int LayoutResource => Resource.Layout.Open_selected_business_card_activity;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            var intent_data = Intent.GetStringExtra("data");
            if (intent_data != null)
                SetMsg(intent_data);

            var srv_nm = FindViewById<Button>(Resource.Id.info_vcard_save);
            srv_nm.Click += Handle_Click;

        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.info_vcard_save))
                {
                    return;
                }
            }
        }

        private void SetMsg(string msg)
        {

            var visit_card = JsonConvert.DeserializeObject<VisitCardM>(msg);

            var srv_nm = FindViewById<TextView>(Resource.Id.info_vcard_name);
            var srv_descr = FindViewById<TextView>(Resource.Id.info_vcard_desc);
            var srv_srcvs = FindViewById<TextView>(Resource.Id.info_vcard_services);
            var srv_addr = FindViewById<TextView>(Resource.Id.info_vcard_address);
            var srv_cnt = FindViewById<TextView>(Resource.Id.info_vcard_contacts);

            var service = visit_card.Services;
            //visit_card.Services.ForEach(x => service += x + ", ");

            srv_nm.Text = visit_card.Name;
            srv_descr.Text = visit_card.Description;
            srv_srcvs.Text = service;
            srv_cnt.Text = visit_card.Phones;
            srv_addr.Text = visit_card.Adress;

        }


    }
}