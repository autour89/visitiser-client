using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Newtonsoft.Json;
using Visi.Resources.Actions.activity;
using Visi.Messaging.Presenter;
using Visi.Messaging.Actions.adapter;
using Android.Content.PM;

namespace Visi.Messaging.Actions.activity
{
    [Activity(Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class VisitCardListActivity : BaseActivity
    {
        private VisitCardListPresenter vcard_pres;

        private ListView vcard_listview;
        protected override int LayoutResource => Resource.Layout.Visit_cart_list_activity_layout;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            vcard_pres = new VisitCardListPresenter();

            var acntsrch = FindViewById<SearchView>(Resource.Id.visit_card_srch);

            acntsrch.QueryTextChange += Acntsrch_QueryTextChange;
            acntsrch.Close += Acntsrch_Close;

            vcard_listview = (ListView)FindViewById(Resource.Id.visit_card_list);

            var adapter = new ChatBusinessCardAdapter(this, vcard_pres.Cards);

            vcard_listview.Adapter = adapter;

            vcard_listview.ItemClick += Vcard_listview_ItemClick;

        }
        private void Acntsrch_QueryTextChange(object sender, SearchView.QueryTextChangeEventArgs e)
        {
            var term = (sender as SearchView).Query;

            if (term.Length > 0)
            {
                vcard_listview.Adapter = new ChatBusinessCardAdapter(this, vcard_pres.FilterContacts(term));
            }
        }

        private void Acntsrch_Close(object sender, SearchView.CloseEventArgs e)
        {
            vcard_listview.Adapter = new ChatBusinessCardAdapter(this, vcard_pres.Cards);
        }

        private void Vcard_listview_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var tag = Convert.ToInt32(e.View.Tag);
            var ser = JsonConvert.SerializeObject(tag);
            Intent returnIntent = new Intent();
            returnIntent.PutExtra("data", ser);
            SetResult(Result.Ok, returnIntent);
            Finish();
        }

    }
}