using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Visi.Generic.DAL.DataModel;
using Visi.Messaging.Actions.view;
using Visi.Generic.Models;

namespace Visi.Messaging.Actions.adapter
{
    public class MessageAdapter : BaseAdapter<ChatMessages>
    {
        private Context context;
        private View view;

        private int _pool = 0;
        private bool isside = true;
        public List<ChatMessages> Items;
        private string MessageType { get; set; }

        public MessageAdapter(Context _cx, List<ChatMessages> items) : base()
        {
            this.context = _cx;
            Items = items;
            _pool = items.Select(x => x.SenderId).FirstOrDefault();
        }

        public override ChatMessages this[int position]
        {
            get { return Items[position]; }
        }

        public override int Count
        {
            get { return Items.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            view = convertView;

            if (view == null)
            {
                var inflater = (LayoutInflater)context.GetSystemService(Context.LayoutInflaterService);

                view = inflater.Inflate(Resource.Layout.Message_list_frame_layout, parent, false);

            }

            MessageType = Items[position].MessageType;

            var msg_draw = SetTypeLayout();

            if (_pool != Items[position].SenderId)
                isSide();
            if (isside)
                msg_draw.Gravity = GravityFlags.Right;
            else
                msg_draw.Gravity = GravityFlags.Left;

            view.Tag = Items[position].MessageId;

            view.FindViewById<TextView>(Resource.Id.msg_date).Text = Items[position].MessageDT.ToShortTimeString();

            view.FindViewById<TextView>(Resource.Id.sender_name).Text = Items[position].SenderName;

            SetMessage(position);

            _pool = Items[position].SenderId;

            return view;
        }

        private bool isSide()
        {
            isside = !isside;

            return isside;
        }

        private FrameLayout.LayoutParams SetTypeLayout()
        {
            var msg_v = view.FindViewById<ChatView>(Resource.Id.customview_msg);
            var bsns_card_v = view.FindViewById<BusinessCardView>(Resource.Id.customview_bsns_card);
            var pic_v = view.FindViewById<PictureView>(Resource.Id.customview_pic);

            FrameLayout.LayoutParams castLayoutParams = null;
            switch (MessageType)
            {
                case "message":
                    msg_v.Visibility = ViewStates.Visible;
                    bsns_card_v.Visibility = ViewStates.Gone;
                    pic_v.Visibility = ViewStates.Gone;
                    var lp_msg = msg_v.LayoutParameters;
                    castLayoutParams = (FrameLayout.LayoutParams)lp_msg;
                    break;
                case "service":
                    bsns_card_v.Visibility = ViewStates.Visible;
                    msg_v.Visibility = ViewStates.Gone;
                    pic_v.Visibility = ViewStates.Gone;

                    var lp_srv = bsns_card_v.LayoutParameters;
                    castLayoutParams = (FrameLayout.LayoutParams)lp_srv;
                    break;
                case "picture":
                    pic_v.Visibility = ViewStates.Visible;
                    msg_v.Visibility = ViewStates.Gone;
                    bsns_card_v.Visibility = ViewStates.Gone;

                    var lp_pic = pic_v.LayoutParameters;
                    castLayoutParams = (FrameLayout.LayoutParams)lp_pic;
                    break;
            }

            return castLayoutParams;
        }

        private void SetMessage(int position)
        {
            switch (MessageType)
            {
                case "message":
                    var msg = view.FindViewById<TextView>(Resource.Id.msg);
                    var m = JsonConvert.DeserializeObject<MessageM>(Items[position].Message);
                    msg.Text = m.Message;
                    break;
                case "service":
                    var srv_nm = view.FindViewById<TextView>(Resource.Id.msg_bsns_card_name);
                    var srv_descr = view.FindViewById<TextView>(Resource.Id.msg_bsns_card_desc);
                    var srv_srcvs = view.FindViewById<TextView>(Resource.Id.msg_bsns_card_services);
                    var srv_addr = view.FindViewById<TextView>(Resource.Id.msg_bsns_card_address);
                    var srv_cnt = view.FindViewById<TextView>(Resource.Id.msg_bsns_card_contacts);

                    var visit_card = JsonConvert.DeserializeObject<VisitCardM>(Items[position].Message);

                    var service = visit_card.Services;
                    //visit_card.Services.ForEach(x => service += x + ", ");

                    srv_nm.Text = visit_card.Name;
                    srv_descr.Text = visit_card.Description;
                    srv_srcvs.Text = service;
                    srv_cnt.Text = visit_card.Phones;
                    srv_addr.Text = visit_card.Adress;

                    break;
                case "picture":
                    break;
            }
        }

    }
}


