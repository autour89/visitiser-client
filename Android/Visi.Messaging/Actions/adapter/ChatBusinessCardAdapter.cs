using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Visi.Generic.DAL.DataModel;

namespace Visi.Messaging.Actions.adapter
{
    public class ChatBusinessCardAdapter : BaseAdapter<BusinessCard>
    {
        private Activity context;
        private View view;
        public List<BusinessCard> Items { get; set; }


        public ChatBusinessCardAdapter(Activity activity, List<BusinessCard> items) : base()
        {
            this.context = activity;
            Items = items;

        }

        public override BusinessCard this[int position]
        {
            get { return Items[position]; }
        }

        public override int Count
        {
            get { return Items.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            view = convertView;

            if (view == null)
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.Chat_business_cart_list, parent, false);
            }


            view.Tag = this[position].Business_Card_ID;

            var srvc_name = view.FindViewById<TextView>(Resource.Id.list_chat_bsns_card_name);

            srvc_name.Text = this[position].Name;

            return view;
        }


    }
}