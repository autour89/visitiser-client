using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Java.Lang;
using Android.Support.V4.App;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;
using Visi.Generic.DAL.DataModel;
using Visi.Messaging.Actions.fragment;

namespace Visi.Messaging.Actions.adapter
{
    public class CnversationPagesAdapter : FragmentStatePagerAdapter
    {
        private List<string> Titles = new List<string>();

        private List<Fragment> fragments;
        private List<Conversation> conv_msgs;

        public CnversationPagesAdapter(FragmentManager fm, List<Conversation> msgs) : base(fm)
        {
            conv_msgs = msgs;

            this.fragments = new List<Fragment>();

            conv_msgs.ForEach(current_chat => { fragments.Add(new MessageFragment(current_chat.Messages, current_chat.ConversationId)); Titles.Add(current_chat.ConversationName); });
        }

        public override ICharSequence GetPageTitleFormatted(int position)
        {
            return new Java.Lang.String(Titles[position]);
        }

        #region implemented abstract members of PagerAdapter

        public override int Count
        {
            get { return Titles.Count; }
        }

        #endregion

        #region implemented abstract members of FragmentPagerAdapter

        public override Fragment GetItem(int position)
        {
            //return ChatPageFragment.NewInstance(position);
            return fragments[position];
        }

        #endregion
    }
}