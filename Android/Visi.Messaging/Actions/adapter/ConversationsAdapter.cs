using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Visi.Generic.DAL.DataModel;

namespace Visi.Messaging.Actions.adapter
{

    public class ConversationsAdapter : BaseAdapter<Conversation>
    {
        private List<Conversation> items;
        private Context context;
        private View view;

        public ConversationsAdapter(Context _cx, List<Conversation> items) : base()
        {
            this.context = _cx;
            this.items = items;
        }
        public override Conversation this[int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            view = convertView;

            if (view == null)
            {
                var inflater = (LayoutInflater)context.GetSystemService(Context.LayoutInflaterService);

                view = inflater.Inflate(Resource.Layout.Accountconversations_list, parent, false);
            }

            view.Tag = items[position].ConversationId;

            view.FindViewById<TextView>(Resource.Id.conv_name).Text = items[position].ConversationName;


            return view;
        }

        private string ItemText(int position)
        {
            //string type_m = items[position].ChatSnippetType;
            //var msg = items[position].ChatSnippet;

            //switch (type_m)
            //{
            //    case "message":
            //        var obj_msg = JsonConvert.DeserializeObject<MessageM>(msg);

            //        return obj_msg.Message;
            //    case "service":
            //        var obj_card = JsonConvert.DeserializeObject<VisitCardM>(msg);

            //        return obj_card.Name;
            //    default:
            //        return "";
            //}
            return "";

        }

    }
}

