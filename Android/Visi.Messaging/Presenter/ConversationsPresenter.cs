using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Visi.Generic.DAL.DataModel;
using Visi.Generic.DAL.Services;
using Visi.Generic.Helpers;
using Visi.Generic.Models;
using Visi.Generic.Services;
using Visi.Generic.Services.ApiClients;

namespace Visi.Messaging.Presenter
{
    public class ConversationsPresenter
    {
        private RestsharpApi access;
        private List<Conversation> items;
        private List<ChatMessages> chatmsg;
        private ContentDB tables_db;
        private DataSearch<ChatMessages> local_srch;
        public ConversationsPresenter()
        {
            access = RestsharpApi.Instance(Configuration.ConnectionString);
            tables_db = new ContentDB();
            local_srch = new DataSearch<ChatMessages>();

            local_srch.Data = ChatMsg;
        }

        

        public List<Conversation> Items
        {
            get
            {
                return items;
            }
            set
            {
                items = value;
            }
        }

        public List<ChatMessages> ChatMsg
        {
            get
            {
                return chatmsg;
            }
            set
            {
                chatmsg = value;
            }
        }


        /*
         *  Get list of account conversations from local storage
         */
        public Task NewConversations()
        {
            var t = Task.Run(() =>
            {
                Items = ContentDB.User_Conv.SelectAllData<Conversation>();

                UpdateConversation();
            });

            return t;
        }

        /*
         * Get chat messages and synchronize changes with server
         */
        public void GetLocalMsgs(int chatid)
        {
            var local_conv = ContentDB.Conv_Msgs.SelectAllData<ChatMessages>().Where(x => x.ConversationId.Equals(chatid)).ToList();


            ChatMsg = local_conv;

            UpdateIfMessages(chatid);

        }

        private async Task UpdateConversation()
        {
            var ids = Items.Select(x => x.ConversationId).ToList();
            var model = new AccountModel
            {
                UserId = tables_db.UserAccount.Id,
                Ids = ids
            };

            var result = "";
            try
            {
                result = await access.SendPostAsJson(ApiUrl.NewConversations, model);
            }
            catch (Exception)
            {
            }

            if (result.Length > 10)
            {
                var data = JsonConvert.DeserializeObject<List<Conversation>>(result);

                if (data != null)
                {
                    ContentDB.User_Conv.InsertList(data);

                    Items = Items.Concat(data).Distinct().GroupBy(x => x.ConversationId).Select(g => g.First()).ToList();
                }
            }


        }

        private async Task UpdateIfMessages(int chatid)
        {
            var ids = ChatMsg.Select(x => x.MessageId).ToList();
            var model = new AccountModel
            {
                UserId = tables_db.UserAccount.Id,
                Ids = ids,
                ChatId = chatid
            };
            var result = "";
            try
            {
                result = await access.SendPostAsJson(ApiUrl.CheckMessages, model);
            }
            catch (Exception)
            {
            }

            if (result.Length > 10)
            {
                var data = JsonConvert.DeserializeObject<List<ChatMessages>>(result);

                if (data != null)
                {
                    ContentDB.Conv_Msgs.InsertList(data);

                    ChatMsg = ContentDB.Conv_Msgs.SelectAllData<ChatMessages>().Where(x => x.ConversationId.Equals(chatid)).ToList();
                }
            }

        }


        /*
         * Obsolete method for searching in account conversations on server
         */
        public async Task<List<ChaListModel>> SearchConversation(string term)
        {
            var model = new MessageModel { Account1 = tables_db.UserAccount.Id, Message = term };

            var result = await access.PostRequest("api/Messages/SearchConversation/", model);

            if (result != null)
            {
                var data = JsonConvert.DeserializeObject<List<ChaListModel>>(result);

                return data;
            }

            return null;
        }

        /*
         * New method for searching messages by keywords in my chats
         */
        public List<Conversation> FilterConvers(string term)
        {
            var links = term.Split(new String[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries);

            var chats = ContentDB.User_Conv.SelectAllData<Conversation>().Select(x => x).ToList();

            var chatlist = new List<Conversation>();

            foreach (var chat in chats)
            {

                var data = ContentDB.Conv_Msgs.SelectAllData<ChatMessages>().Where(x => x.ConversationId.Equals(chat.ConversationId)).Where(local_srch.Predicate(links).Compile()).Select(x => x).ToList().LastOrDefault();

                if (data != null)
                {
                    chatlist.Add(new Conversation
                    {
                        ConversationId = chat.ConversationId,
                        ConversationName = chat.ConversationName
                    });
                }

            }

            return chatlist;
        }

    }
}