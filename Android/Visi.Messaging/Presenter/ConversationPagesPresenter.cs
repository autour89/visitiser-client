using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;
using Visi.Generic.DAL.DataModel;
using Visi.Generic.Models;
using Visi.Generic.Services;
using Visi.Generic.DAL.Services;
using Visi.Generic.Services.ApiClients;
using System.Threading.Tasks;

namespace Visi.Messaging.Presenter
{
    public class ConversationPagesPresenter
    {
        private RestsharpApi access;
        private ContentDB tables_db;

        public ConversationPagesPresenter()
        {
            access = RestsharpApi.Instance(Configuration.ConnectionString);
            tables_db = new ContentDB();
        }

        public int RecipientId { get; set; }
        public List<Conversation> Convrstns { get; set; }

        /*
         *  Obsolete method for getting chat lists on account contact item click event
         */
        //[Obsolete("Don't use OldMethod, use NewMethod instead", true)]
        [Obsolete]
        public void GetAllConversations()
        {
            //var model = new MessageModel { Account1 = tables_db.UserAccount.Id, Account2 = RecipientId };
            //var result = access.TemplatePost(ApiUrl.GetAllMessages, model);

            //var data = JsonConvert.DeserializeObject<List<ChaListModel>>(result);

            //Convrstns = data;
        }


        public async Task GetConversations()
        {
            var model = new MessageModel { Account1 = tables_db.UserAccount.Id, Account2 = RecipientId };
            var result = await access.PostRequest(ApiUrl.AccontConversations, model);

            var data = JsonConvert.DeserializeObject<List<int>>(result);


            if (data != null)
            {

                var ewr = ContentDB.User_Conv.SelectAllWithChildren<Conversation>().Where(x => data.Contains(x.ConversationId)).ToList();


                Convrstns = ewr;

                if (Convrstns.Count == 0)
                    Convrstns.Add(new Conversation { Messages = new List<ChatMessages>(), ConversationId = data.FirstOrDefault(), ConversationName = ContentDB.Account_Data.SelectAllData<VisitizerProfile>().FirstOrDefault().Name });
            }


        }

    }
}