using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Visi.Generic.DAL.DataModel;
using Visi.Generic.DAL.Services;
using Visi.Generic.Helpers;

namespace Visi.Messaging.Presenter
{
    public class VisitCardListPresenter
    {
        private ContentDB tables_db;
        private DataSearch<BusinessCard> local_srch;

        public VisitCardListPresenter()
        {
            tables_db = new ContentDB();

            Cards = ContentDB.Bsns_Cards.SelectAllWithChildren<BusinessCard>();
            local_srch = new DataSearch<BusinessCard>();

            local_srch.Data = Cards;
        }

        public List<BusinessCard> Cards { get; set; }

        /*
         *  filtering visit cards list 
         */
        public List<BusinessCard> FilterContacts(string term)
        {
            var links = term.Split(new String[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries);

            var search_res = local_srch.SearchFilter(links);

            return search_res;
        }

    }
}