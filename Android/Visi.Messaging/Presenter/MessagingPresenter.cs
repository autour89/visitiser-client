using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Visi.Generic.DAL.DataModel;
using Visi.Generic.DAL.Services;
using Visi.Generic.Helpers;
using Visi.Generic.Models;
using Visi.Generic.Services;
using Visi.Generic.Services.ApiClients;

namespace Visi.Messaging.Presenter
{
    public class MessagingPresenter
    {
        private RestsharpApi access;
        private ContentDB tables_db;
        private Dictionary<string, string> qrstring;
        public MessagingPresenter()
        {

        }
        public MessagingPresenter(List<ChatMessages> model, int id)
        {
            access = RestsharpApi.Instance(Configuration.ConnectionString);
            tables_db = new ContentDB();

            Items = model;
            ChatId = id;

            qrstring = new Dictionary<string, string>();

            qrstring.Add("userid", tables_db.UserAccount.Id.ToString());

            QueryStr = qrstring;

            MessageType = "message";

            Cards = ContentDB.Bsns_Cards.SelectAllWithChildren<BusinessCard>();
        }

        public List<BusinessCard> Cards { get; set; }
        public List<ChatMessages> Items { get; set; }
        public int ChatId { get; set; }
        public int RecipientId { get; set; }


        public string MessageType { get; set; }
        public string TextMessage { get; set; }
        public byte[] Picture { get; set; }
        public int BusinessCardId { get; set; }
        public Dictionary<string, string> QueryStr
        {
            get { return qrstring; }
            set { qrstring = value; }
        }

        /*
         *  Send message to server async
         */
        public async Task SendNewMessage()
        {
            var msg = CreateMessage();

            var dt = DateTime.Now;
            var model = new MessageModel { SenderId = tables_db.UserAccount.Id, ChatId = ChatId, Message = msg, MessageType = MessageType, MessageDT = dt };

            await access.SendPostAsJson(ApiUrl.NewMessage, model);
        }

        private string CreateMessage()
        {
            switch (MessageType)
            {
                case "message":
                    var item = new MessageM { Message = TextMessage };
                    return JsonConvert.SerializeObject(item);
                case "service":
                    var visit_card = Cards.Where(x => x.Business_Card_ID.Equals(BusinessCardId)).Select(card => card).FirstOrDefault();

                    var card_item = new VisitCardM
                    {
                        Id = visit_card.Business_Card_ID,
                        Name = visit_card.Name,
                        Description = visit_card.Descriprion,
                        Logo = visit_card.Logo,
                        Services = visit_card.Services,//.Select(i => i.Serv_NM).ToList(),
                        Adress = visit_card.Addresses,
                        Email = visit_card.Emails,
                        Phones = visit_card.Phones
                    };

                    return JsonConvert.SerializeObject(card_item);

                case "account":

                    break;
                case "picture":

                    break;
                case "doc":

                    break;
                case "map":

                    break;
            }

            return "";
        }

        private void MesssageType(char t)
        {
            switch (t)
            {
                case 'm':
                    MessageType = "message";
                    break;
                case 'v':
                    MessageType = "service";
                    break;
                case 'p':
                    MessageType = "picture";
                    break;
            }

        }

        /*
         *  Update message list when callback event fired
         */
        public async Task UpdateMessageList()
        {
            var result = await access.GetRequest(ApiUrl.GetNewMessages + ChatId);
            var data = JsonConvert.DeserializeObject<List<ChatMessages>>(result);
            Items = data;
        }

        /*
         *  Rm selected message and synch with server
         */
        public async Task RemoveMessaage(int id)
        {
            var msg = Items.Find(x => x.MessageId.Equals(id));

            ContentDB.Conv_Msgs.DeleteItems(msg);

            Items.RemoveAt(id);

            await access.GetRequest(ApiUrl.RemoveItem + id);
        }

        /*
         *  Edit selected message and synch with server
         */
        public async Task EditMessaage(int id, string message)
        {
            var msg = Items.Find(x => x.MessageId.Equals(id));
            msg.Message = message;

            ContentDB.Conv_Msgs.UpdateItem(msg);

            var model = new MessageModel
            {
                MessageId = id,
                Message = message
            };

            await access.PostRequest(ApiUrl.EditItem, model);
        }

    }
}