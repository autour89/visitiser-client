using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Visi.Generic.Models;
using Newtonsoft.Json;
using Visi.Resources.Actions.activity;
using Android.Content.PM;

namespace Visi.Profile.Actions.activity
{
    [Activity(Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class AddressActivity : BaseActivity
    {
        private EditText cityStr, addressStr;

        protected override int LayoutResource => Resource.Layout.AddressLayout;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            //Window.RequestFeature(WindowFeatures.NoTitle);

            var btn_ok = FindViewById<Button>(Resource.Id.lk_name_confirm);
            cityStr = FindViewById<EditText>(Resource.Id.lk_city_field);
            addressStr = FindViewById<EditText>(Resource.Id.lk_address_field);

            btn_ok.Click += Handle_Click;

            SetScreenData();
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.lk_name_confirm))
                {
                    var data = new AddressModel
                    {
                        City = cityStr.Text,
                        Address = addressStr.Text
                    };

                    if(data.City.Length > 0 || data.Address.Length> 0){
                        var obj = JsonConvert.SerializeObject(data);
                        var returnIntent = new Intent();
                        returnIntent.PutExtra("Data", obj);
                        SetResult(Result.Ok, returnIntent);
                    }
                    
                    Finish();
                }
            }
        }

        private void SetScreenData()
        {
            var intent_data = Intent.GetStringExtra("Data");

            if (intent_data != null)
            {
                var data = JsonConvert.DeserializeObject<AddressModel>(intent_data);

                if(data.City!=null)
                    cityStr.Text = data.City;
                if(data.Address!=null)
                    addressStr.Text = data.Address;
            }
        }

    }
}