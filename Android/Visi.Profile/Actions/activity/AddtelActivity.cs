using System;
using System.IO;
using System.Threading.Tasks;
using System.Reactive.Linq;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Runtime;
using Android.Views.InputMethods;

using Visi.Profile.Actions.adapter;
using Visi.Profile.Presenter;
using Visi.Generic.Helpers;
using Visi.Resources.Actions.activity;
using Visi.Generic.Services;
using Android.Content.PM;

namespace Visi.Profile.Actions.activity
{
    [Activity(Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class AddtelActivity : BaseActivity
    {
        private AddtelPresenter telPresenter;

        private Spinner countryList;
        private TextView contry_codeText;        
        private EditText phoneNumber;
        private CountryListAdapter countriesAdapter;

        protected override int LayoutResource => Resource.Layout.AddtelLayout;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            var btn_to_code_confirm = FindViewById<Button>(Resource.Id.lk_continue_button_to_code);
            var btn_to_main = FindViewById<Button>(Resource.Id.lk_top_return_to_main_button);
            countryList = FindViewById<Spinner>(Resource.Id.lk_countryList);
            contry_codeText = FindViewById<TextView>(Resource.Id.lk_country_code);
            phoneNumber = FindViewById<EditText>(Resource.Id.lk_add_phone);
            phoneNumber.EditorAction += Handle_EditorAction;

            countryList.ItemSelected += Handle_ItemSelected;
            btn_to_code_confirm.Click += Handle_Click;
            btn_to_main.Click += Handle_Click;

            GetCountries();
            countriesAdapter = new CountryListAdapter(this, telPresenter.DataCountries);
            countryList.Adapter = countriesAdapter;
            SelectCountry();

            var intent_data = Intent.GetStringExtra("phone");

            if (intent_data != null)
            {
                SetPhoneBox(intent_data);
            }

        }

        private void Handle_EditorAction(object sender, TextView.EditorActionEventArgs e)
        {
            switch (e.ActionId)
            {
                case ImeAction.Done:
                    SetNewPhone();
                    break;
            }
        }

        private void Handle_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var id = e.Position;
            contry_codeText.Text = "(" + telPresenter.DataCountries[id].CallingCode + ")";
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.lk_continue_button_to_code))
                {
                    SetNewPhone();
                }
                else if (button.Id.Equals(Resource.Id.lk_top_return_to_main_button))
                {
                    OnBackPressed();
                }
                
            }

        }
        
        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            switch (requestCode)
            {
                case 1:
                    if (data != null)
                    {
                        var phone = data.GetStringExtra("phone");

                        var returnIntent = new Intent();
                        returnIntent.PutExtra("Data", phone);
                        SetResult(Result.Ok, returnIntent);
                        Finish();
                    }
                    break;
            }
        }

        /*
         * action on handling button press for add new phone to account
         */
        private void SetNewPhone()
        {
            var id = countryList.SelectedItemPosition;
            var phone = contry_codeText.Text + " " + phoneNumber.Text;
            telPresenter.SendSMS(id,phone.ParsePhoneStr(HelperStr.PhoneContains));

            var intent = new Intent(this, typeof(CodeconfirmActivity));
            intent.PutExtra("phoneData", telPresenter.SerelizeData);
            StartActivityForResult(intent, 1);
        }

        /*
         * get txt file with countries list from assets folder
         */
        private void GetCountries()
        {
            string content;
            var assets = this.Assets;
            using (var sr = new StreamReader(assets.Open("CountryCodes.json")))
            {
                content = sr.ReadToEnd();
                telPresenter = new AddtelPresenter(content);
            }
        }

        /*
         * set data to controls
         */
        private void SelectCountry()
        {
            int id = 0;
            if (ApplicationSettings.IsConnected)
            {

                var o = Observable.Start(() =>
                {
                    telPresenter.GetCountryInfo();
                    id = telPresenter.DataCountries.FindIndex(x => x.Code.Equals(telPresenter.CountryInfo.CountryCode));
                });
                o.Subscribe();
                o.Wait();
                Observable.Start(() => SetSelection(id)).Subscribe();
                
            }
            else
            {
                id = telPresenter.DataCountries.FindIndex(x => x.EnglishName.Contains("United States"));
                SetSelection(id);
            }

        }

        /*
         * wrapper for setting control data
         */
        private void SetSelection(int id)
        {
            countryList.SetSelection(id);
            contry_codeText.Text = "(" + telPresenter.DataCountries[id].CallingCode + ")";
        }

        /*
         * wrapper for set phone to editbox in case if user clicked back from forward activity
         */
        private void SetPhoneBox(string phone)
        {
            phoneNumber.Text = phone;
        }

    }
}