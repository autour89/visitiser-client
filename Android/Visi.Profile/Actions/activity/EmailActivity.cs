using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;

using Visi.Resources.Actions.activity;

namespace Visi.Profile.Actions.activity
{
    [Activity(Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class EmailActivity : BaseActivity
    {
        private EditText emailStr;

        protected override int LayoutResource => Resource.Layout.EmailLayout;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            //Window.RequestFeature(WindowFeatures.NoTitle);

            var btn_ok = FindViewById<Button>(Resource.Id.lk_name_confirm);
            emailStr = FindViewById<EditText>(Resource.Id.lk_email_edit);

            emailStr.EditorAction += Handle_EditorAction;
            btn_ok.Click += Handle_Click;

            SetScreenData();
        }

        private void Handle_EditorAction(object sender, TextView.EditorActionEventArgs e)
        {
            FinishActivity();
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.lk_name_confirm))
                {
                    FinishActivity();
                }
            }
        }

        private void SetScreenData()
        {
            var intent_data = Intent.GetStringExtra("Data");

            if (intent_data != null)
            {
                emailStr.Text = intent_data;
            }
        }


        private void FinishActivity()
        {
            if (emailStr.Text.Length > 0)
            {
                var returnIntent = new Intent();
                returnIntent.PutExtra("Data", emailStr.Text);
                SetResult(Result.Ok, returnIntent);
            }

            Finish();
        }

    }
}