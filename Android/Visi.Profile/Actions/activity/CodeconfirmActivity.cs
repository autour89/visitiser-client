using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;

using Newtonsoft.Json;
using Visi.Generic.Models;
using Visi.Profile.Presenter;
using Visi.Resources.Actions.activity;
using Android.Content.PM;

namespace Visi.Profile.Actions.activity
{
    [Activity(Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class CodeconfirmActivity : BaseActivity
    {
        private TextView numberTitle;
        private EditText secCode;
        private Button returnBack, confirmAction, changeNumber, sendAgain;

        private CodeconfirmPresenter confirmPresenter;

        protected override int LayoutResource => Resource.Layout.CodeconfirmLayout;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            confirmPresenter = new CodeconfirmPresenter();

            numberTitle = FindViewById<TextView>(Resource.Id.lk_confirm_number_title);
            secCode = FindViewById<EditText>(Resource.Id.lk_securitycode_field);

            returnBack = FindViewById<Button>(Resource.Id.lk_returnBack);
            confirmAction = FindViewById<Button>(Resource.Id.lk_confirm_button_code);
            changeNumber = FindViewById<Button>(Resource.Id.lk_change_button);
            sendAgain = FindViewById<Button>(Resource.Id.lk_sendagain_button);

            secCode.EditorAction += Handle_EditorAction;
            returnBack.Click += Handle_Click;
            confirmAction.Click += Handle_Click;
            changeNumber.Click += Handle_Click;
            sendAgain.Click += Handle_Click;


            var intent_data = Intent.GetStringExtra("phoneData");

            if (intent_data != null)
            {
                confirmPresenter.PhoneData = JsonConvert.DeserializeObject<SecurityModel>(intent_data);
            }

            numberTitle.Text = confirmPresenter.PhoneData.Phone;

        }

        private void Handle_EditorAction(object sender, TextView.EditorActionEventArgs e)
        {
            FinishActivity();
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.lk_returnBack))
                {
                    OnBackPressed();
                }
                else if (button.Id.Equals(Resource.Id.lk_confirm_button_code))
                {                    
                    FinishActivity();
                }
                else if (button.Id.Equals(Resource.Id.lk_change_button))
                {
                    //var addtel_activity = new Intent(this, typeof(AddtelActivity));
                    //addtel_activity.PutExtra("phone", confirmPresenter.PhoneData.Phone);
                    //StartActivity(addtel_activity);
                    //Finish();
                    OnBackPressed();
                }
                else if (button.Id.Equals(Resource.Id.lk_sendagain_button))
                {
                    secCode.Text = "";
                    confirmPresenter.SendSMS();
                }
            }
        }


        private void ViewAlert()
        {
            var msg_phone = GetString(Resource.String.Message_incorrect_security_code);
            var alert = Toast.MakeText(this, msg_phone, ToastLength.Long);
            alert.SetGravity(GravityFlags.CenterVertical & GravityFlags.CenterHorizontal, 0, 0);
            alert.Show();
        }


        private void FinishActivity()
        {
            var code = secCode.Text;
            if (code.Length.Equals(4))
            {
                if (confirmPresenter.PhoneData.SecurityCode.Equals(code))
                {
                    var data = JsonConvert.SerializeObject(confirmPresenter.PhoneData);
                    var returnIntent = new Intent();
                    returnIntent.PutExtra("phone", data);
                    SetResult(Result.Ok, returnIntent);
                    Finish();
                }
                else
                {
                    ViewAlert();
                }
            }
        }


    }
}