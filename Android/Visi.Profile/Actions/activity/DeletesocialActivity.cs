using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Visi.Generic.Models;
using Newtonsoft.Json;
using Visi.Resources.Actions.activity;
using Android.Content.PM;

namespace Visi.Profile.Actions.activity
{
    [Activity(Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class DeletesocialActivity : BaseActivity
    {
        private Dialog confirm_del_social;

        protected override int LayoutResource => Resource.Layout.DeletesocialLayout;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            

            var delete_social_dialog_btn = FindViewById<Button>(Resource.Id.lk_delete_social_btn);
            delete_social_dialog_btn.Click += Delete_social_dialog_btn_Click;
            // Create your application here
        }

        private void Delete_social_dialog_btn_Click(object sender, EventArgs e)
        {
            confirm_delete_social_dialog();
        }



        private void confirm_delete_social_dialog()
        {
            confirm_del_social = new Dialog(this);
            confirm_del_social.RequestWindowFeature(Convert.ToInt32(WindowFeatures.NoTitle));
            confirm_del_social.SetContentView(Resource.Layout.ConfirmDelSocialDialog);
            confirm_del_social.Show();
        }
    }
}