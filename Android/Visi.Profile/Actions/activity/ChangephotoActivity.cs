﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Toolbar = Android.Support.V7.Widget.Toolbar;

using Visi.Resources.Actions.activity;
using System.IO;
using Com.Isseiaoki.Simplecropview;

using Android.Provider;
using Com.Isseiaoki.Simplecropview.Callback;
using Android.Content.PM;

namespace Visi.Profile.Actions.activity
{
    [Activity(Label = "",Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class ChangephotoActivity : BaseActivity
    {
        private CropImageView cropView;

        protected override int LayoutResource => Resource.Layout.ChangephotoLayout;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            var toolbar = FindViewById<Toolbar>(Resource.Id.profile_toolbar);

            SetSupportActionBar(toolbar);


            cropView = FindViewById<CropImageView>(Resource.Id.cropImageView);
            var addPic = FindViewById<ImageButton>(Resource.Id.buttonAdd);
            var actionDone = FindViewById<ImageButton>(Resource.Id.buttonDone);

            addPic.Click += Handle_Click;
            actionDone.Click += Handle_Click;

            SetImage();
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as ImageButton;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.buttonAdd))
                {
                    ChoosePic();
                }
                else if (button.Id.Equals(Resource.Id.buttonDone))
                {
                    CropPic();
                }
            }
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            switch (requestCode)
            {
                case 2:
                    if (resultCode == Result.Ok)
                    {
                        var galleryPictureUri = data.Data;
                        var bitmap = MediaStore.Images.Media.GetBitmap(this.ContentResolver, galleryPictureUri);
                        cropView.SetImageBitmap(bitmap);

                    }
                    break;

            }
        }

        private void SetImage()
        {
            var intent_data = Intent.GetByteArrayExtra("Data");

            if (intent_data != null)
            {
                var bmp = BitmapFactory.DecodeByteArray(intent_data, 0, intent_data.Length);
                cropView.SetImageBitmap(bmp);
            }
            else
            {
                ChoosePic();
            }
        }

        private void ChoosePic()
        {
            var imageIntent = new Intent();
            imageIntent.SetType("image/*");
            imageIntent.SetAction(Intent.ActionGetContent);
            StartActivityForResult(Intent.CreateChooser(imageIntent, "Select Picture"), 2);
        }

        private void CropPic()
        {
            cropView.StartCrop(null, new CropCallBack(), new SaveCallBack());
            var bytes = ImageBytes(cropView.CroppedBitmap);
            var returnIntent = new Intent();
            returnIntent.PutExtra("Data", bytes);
            SetResult(Result.Ok, returnIntent);
            Finish();
        }

        private byte[] ImageBytes(Bitmap bitmap)
        {
            using (var ms = new MemoryStream())
            {
                bitmap.Compress(Bitmap.CompressFormat.Png, 0, ms);

                return ms.ToArray();
            }
        }
    }
}


