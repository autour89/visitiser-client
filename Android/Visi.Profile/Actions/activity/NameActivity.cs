using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Visi.Resources.Actions.activity;

namespace Visi.Profile.Actions.activity
{
    [Activity(Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class NameActivity : BaseActivity
    {
        private EditText editName;

        protected override int LayoutResource => Resource.Layout.NameLayout;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            //Window.RequestFeature(WindowFeatures.NoTitle);

            var btn_ok = FindViewById<Button>(Resource.Id.lk_name_confirm);
            editName = FindViewById<EditText>(Resource.Id.lk_name_edit);

            editName.EditorAction += Handle_EditorAction;
            btn_ok.Click += Handle_Click;

            SetScreenData();
        }

        private void Handle_EditorAction(object sender, TextView.EditorActionEventArgs e)
        {
            FinishActivity();
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.lk_name_confirm))
                {
                    FinishActivity();
                }
            }
        }


        private void SetScreenData()
        {
            var intent_data = Intent.GetStringExtra("Data");

            if (intent_data != null)
            {
                editName.Text = intent_data;
            }
        }


        private void FinishActivity()
        {
            if (editName.Text.Length > 0)
            {
                var returnIntent = new Intent();
                returnIntent.PutExtra("Data", editName.Text);
                SetResult(Result.Ok, returnIntent);
            }

            Finish();
        }

    }
}