﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Visi.Generic.Models;

namespace Visi.Profile.Actions.adapter
{
    
    public class PhoneslistAdapter : RecyclerView.Adapter
    {
        private Context cx;

        public PhoneslistAdapter(Context cx, List<PhoneModel> items) : base()
        {
            this.cx = cx;
            DataHolder = items;
        }

        public List<PhoneModel> DataHolder { get; set; }


        public event EventHandler<int> ItemClick;

        public event EventHandler<int> HandleEvent;

        void OnClick(int position)
        {
            if (ItemClick != null)
                ItemClick(this, position);
        }

        void OnHandleEvent(object sender, int position)
        {
            if (HandleEvent != null)
                HandleEvent(sender, position);
        }

        public override int ItemCount => DataHolder.Count;

        public override long GetItemId(int position)
        {
            return base.GetItemId(position);
        }
        public override int GetItemViewType(int position)
        {
            return base.GetItemViewType(position);
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            holder.IsRecyclable = true;
            var viewholder = holder as PhoneslistViewHolder;

            var data = DataHolder[position];

            viewholder.PhoneTitle.Text = data.Title;
            viewholder.PhoneStr.Text = data.Number; 

        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var id = Resource.Layout.Phones_adapter_layout;
            var itemView = LayoutInflater.From(parent.Context).Inflate(id, parent, false);

            return new PhoneslistViewHolder(itemView, OnHandleEvent, OnClick);
        }

    }

    class PhoneslistViewHolder : RecyclerView.ViewHolder
    {
        private View view;
        private TextView phoneTitle, phoneStr;

        public TextView PhoneTitle => phoneTitle;
        public TextView PhoneStr => phoneStr;

        public PhoneslistViewHolder(View itemView, Action<object, int> handleEvent, Action<int> clickEvent) : base(itemView)
        {
            view = itemView;

            view.Tag = this;
            view.Id = Position;
            view.Click += (sender, e) => clickEvent(base.Position);

            phoneTitle = view.FindViewById<TextView>(Resource.Id.phonenumber_title);
            phoneStr = view.FindViewById<TextView>(Resource.Id.phonenumber_text);
        }



    }

}