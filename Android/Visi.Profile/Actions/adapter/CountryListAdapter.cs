using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Visi.Generic.Models;

namespace Visi.Profile.Actions.adapter
{
    public class CountryListAdapter : BaseAdapter<CountryModel>
    {
        private Context _context;
        private View view;
        public CountryListAdapter(Context cx, List<CountryModel> countries) : base()
        {
            _context = cx;
            Items = countries;
        }

        List<CountryModel> Items { get; set; }
        public override int Count => Items.Count;
        public override CountryModel this[int position] => Items[position];

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            view = convertView;

            CountryListViewHolder viewholder;
            if (view == null)
            {
                var inflater = (LayoutInflater)_context.GetSystemService(Context.LayoutInflaterService);

                view = inflater.Inflate(Resource.Layout.Country_list_layout, parent, false);

                viewholder = new CountryListViewHolder(view);

                view.Tag = viewholder;
            }
            else
            {
                viewholder = (CountryListViewHolder)view.Tag;
            }

            var data = Items[position];
            viewholder.ContryTitle.Text = data.LocaleName + " (" + data.CallingCode + ")";

            return view;
        }


        class CountryListViewHolder : Java.Lang.Object
        {
            private View view;
            private TextView contryTitle;

            public TextView ContryTitle => contryTitle;

            public CountryListViewHolder(View v)
            {
                view = v;

                contryTitle = view.FindViewById<TextView>(Resource.Id.content_str);
            }
        }



    }
}