﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Visi.Profile.Actions.view
{
    public class GenderDialogView : Dialog
    {
        private Context cx;
        private ToggleButton val_male, val_female;

        public GenderDialogView(Context context) : base(context)
        {
            this.cx = context;

            var feature = this.Window;
            feature.RequestFeature(WindowFeatures.NoTitle);

            Initialize();

        }

        private void Initialize()
        {
            SetContentView(Resource.Layout.Gender_dialog);


            val_male = FindViewById<ToggleButton>(Resource.Id.RadioMale);
            val_female = FindViewById<ToggleButton>(Resource.Id.RadioFemale);

            val_male.Click += Handle_Click;
            val_female.Click += Handle_Click;

        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var togglebutton = sender as ToggleButton;

            if (togglebutton != null)
            {

                if (togglebutton.Id.Equals(Resource.Id.RadioMale))
                {
                    val_male.Checked = true;
                    val_female.Checked = false;
                    this.Dismiss();
                }
                if (togglebutton.Id.Equals(Resource.Id.RadioFemale))
                {
                    val_male.Checked = false;
                    val_female.Checked = true;
                    this.Dismiss();
                }

            }
        }

        public ToggleButton MaleToggleBtn => val_male;
        public ToggleButton FemaleToggleBtn => val_female;
    }
}