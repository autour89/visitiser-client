using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Support.V7.App;
using Android.Graphics.Drawables;
using Android.Support.V7.Widget;

using Newtonsoft.Json;
using Visi.Profile.Actions.activity;
using Visi.Profile.Presenter;
using Visi.Profile.Actions.adapter;
using Visi.Generic.Models;
using Visi.Generic.DAL.DataModel;

namespace Visi.Profile.Actions.view
{
    public class ProfileView : FrameLayout
    {
        private ProfileViewPresenter profilePres;

        private Context cx;
        private GenderDialogView gender_dialog;
        private ImageButton changePhoto;
        private ImageView profileImage;
        private TextView fullName, genderStr, dateofbirthStr, emailStr;
        private Dialog dateOfbirthDialog;

        private RecyclerView phonenumsList;
        private PhoneslistAdapter phonesAdapter;
        private string notSpecifiedfield;

        public ProfileView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            cx = context;
            Initialize();
        }

        public ProfileView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
        {
            cx = context;
            Initialize();
        }

        private void Initialize()
        {
            Inflate(cx, Resource.Layout.Profile_view, this);

            var phoneTitle = Resources.GetString(Resource.String.Lk_phone_title);
            notSpecifiedfield = Resources.GetString(Resource.String.Lk_no_title);            

            profileImage = FindViewById<ImageView>(Resource.Id.profile_image);
            changePhoto = FindViewById<ImageButton>(Resource.Id.change_photo);
            var addtel_button = FindViewById<Button>(Resource.Id.lk_add_tel);
            var name_button = FindViewById<FrameLayout>(Resource.Id.lk_name);
            var email_button = FindViewById<FrameLayout>(Resource.Id.lk_email);
            var address_button = FindViewById<FrameLayout>(Resource.Id.lk_address);
            var gender_btn = FindViewById<FrameLayout>(Resource.Id.gender_btn);
            var dateofbith_btn = FindViewById<FrameLayout>(Resource.Id.lk_dateofbirth_set);
            phonenumsList = FindViewById<RecyclerView>(Resource.Id.lk_phones_list);
            fullName = FindViewById<TextView>(Resource.Id.lk_full_name);
            genderStr = FindViewById<TextView>(Resource.Id.gender_text);
            dateofbirthStr = FindViewById<TextView>(Resource.Id.dateofbirth_text);
            emailStr = FindViewById<TextView>(Resource.Id.lk_email_text);

            profilePres = new ProfileViewPresenter();
            profilePres.SetPhoneList(phoneTitle);
            dateOfbirthDialog = new Dialog(cx);
            dateOfbirthDialog.RequestWindowFeature(Convert.ToInt32(WindowFeatures.NoTitle));
            dateOfbirthDialog.SetContentView(Resource.Layout.Dateofbirth_dialog_layout);

            var dialog_ok = dateOfbirthDialog.FindViewById<Button>(Resource.Id.lk_db_ok);
            var dialog_cancel = dateOfbirthDialog.FindViewById<Button>(Resource.Id.lk_db_cancel);

            SetPersonalData();

            dialog_ok.Click += Handle_Click;
            dialog_cancel.Click += Handle_Click;
            gender_btn.Click += Handle_Click;
            dateofbith_btn.Click += Handle_Click;
            name_button.Click += Handle_Click;
            email_button.Click += Handle_Click;
            address_button.Click += Handle_Click;
            addtel_button.Click += Handle_Click;
            changePhoto.Click += Handle_Click;

            var delete_facebook = FindViewById<FrameLayout>(Resource.Id.delete_facebook);
            var add_googlepl = FindViewById<FrameLayout>(Resource.Id.add_googlepl);

            delete_facebook.Click += Delete_facebook_Click;
            add_googlepl.Click += Add_googlepl_Click;

        }

        private void Add_googlepl_Click(object sender, EventArgs e)
        {
            var add_social_activity = new Intent(cx, typeof(AddsocialActivity));
            ((AppCompatActivity)cx).StartActivity(add_social_activity);

        }

        private void Delete_facebook_Click(object sender, EventArgs e)
        {

            var delete_social_activity = new Intent(cx, typeof(DeletesocialActivity));
            ((AppCompatActivity)cx).StartActivity(delete_social_activity);


        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;
            var imagebutton = sender as ImageButton;
            var framelayout = sender as FrameLayout;


            if (imagebutton != null)
            {
                if (imagebutton.Id.Equals(Resource.Id.change_photo))
                {
                    var changPhoto = new Intent(cx, typeof(ChangephotoActivity));
                    if (profilePres.CachedProfile.Logo != null)
                    {
                        changPhoto.PutExtra("Data", profilePres.CachedProfile.Logo);
                    }
                    ((AppCompatActivity)cx).StartActivityForResult(changPhoto, 3);
                }
            }
            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.lk_add_tel))
                {
                    var addtel_activity = new Intent(cx, typeof(AddtelActivity));
                    ((AppCompatActivity)cx).StartActivityForResult(addtel_activity,7);
                }
                else if (button.Id.Equals(Resource.Id.lk_db_ok))
                {
                    var datepicker = dateOfbirthDialog.FindViewById<DatePicker>(Resource.Id.lk_datechooser);
                    var date = datepicker.DateTime;
                    profilePres.CachedProfile.Birth_DT = date;
                    profilePres.UpdateRow();
                    dateOfbirthDialog.Dismiss();
                    SetDatebirthTitle();

                    var datebirth_row = new VisitizerProfile
                    {
                        Id = profilePres.CachedProfile.Id,
                        Birth_DT = profilePres.ProfileData.Birth_DT
                    };
                    profilePres.UpdateSingleField(datebirth_row);

                }
                else if (button.Id.Equals(Resource.Id.lk_db_cancel))
                {
                    dateOfbirthDialog.Dismiss();
                }
            }
            if (framelayout != null)
            {
                if (framelayout.Id.Equals(Resource.Id.lk_name))
                {
                    var name_activity = new Intent(cx, typeof(NameActivity));
                    if(profilePres.CachedProfile.Name!=null)
                        name_activity.PutExtra("Data", profilePres.CachedProfile.Name);
                    ((AppCompatActivity)cx).StartActivityForResult(name_activity, 5);
                }
                else if (framelayout.Id.Equals(Resource.Id.lk_email))
                {
                    var email_activity = new Intent(cx, typeof(EmailActivity));
                    if (profilePres.CachedProfile.Email != null)
                        email_activity.PutExtra("Data", profilePres.CachedProfile.Email);
                     ((AppCompatActivity)cx).StartActivityForResult(email_activity, 6);
                   
                }
                else if (framelayout.Id.Equals(Resource.Id.lk_address))
                {
                    var address_activity = new Intent(cx, typeof(AddressActivity));
                    if (profilePres.CachedProfile.City != null || profilePres.CachedProfile.Address!=null)
                    {
                        var addr = new AddressModel
                        {
                            City = profilePres.CachedProfile.City,
                            Address = profilePres.CachedProfile.Address
                        };
                        var data = JsonConvert.SerializeObject(addr);
                        address_activity.PutExtra("Data", data);
                    }
                    ((AppCompatActivity)cx).StartActivityForResult(address_activity, 4);
                }
                else if (framelayout.Id.Equals(Resource.Id.gender_btn))
                {
                    gender_dialog = new GenderDialogView(cx);

                    var g = profilePres.CachedProfile.Gender;
                    switch (g)
                    {
                        case "m":
                            gender_dialog.MaleToggleBtn.Checked = true;
                            break;
                        case "w":
                            gender_dialog.FemaleToggleBtn.Checked = true;
                            break;
                    }
                    gender_dialog.Show();
                    gender_dialog.DismissEvent += Handle_DismissEvent;
                }
                else if (framelayout.Id.Equals(Resource.Id.lk_dateofbirth_set))
                {
                    dateOfbirthDialog.Show();
                }
                    
            }
        }

        private void Handle_DismissEvent(object sender, EventArgs e)
        {
            var dialog = sender as GenderDialogView;

            if (dialog.MaleToggleBtn.Checked)
            {
                profilePres.CachedProfile.Gender = "m";
            }
            else if (dialog.FemaleToggleBtn.Checked)
            {
                profilePres.CachedProfile.Gender = "w";
            }
            profilePres.UpdateRow();
            var genderTitle = GetGender(profilePres.ProfileData.Gender);
            genderStr.Text = genderTitle;

            var gender_row = new VisitizerProfile
            {
                Id = profilePres.CachedProfile.Id,
                Gender = profilePres.ProfileData.Gender
            };

            profilePres.UpdateSingleField(gender_row);

        }

        public void ResetPhoto(byte[] bytes_img)
        {
            if (bytes_img != null)
            {
                var bmp = BitmapFactory.DecodeByteArray(bytes_img, 0, bytes_img.Length);
                var img = new BitmapDrawable(Resources, bmp);

                profileImage.SetImageDrawable(img);
                profilePres.CachedProfile.Logo = bytes_img;
                profilePres.UpdateRow();
                //profileImage.Background = d;
                //profileImage.SetImageBitmap(bmp);
            }
            else
            {
                var icon = BitmapFactory.DecodeResource(this.Resources, Resource.Drawable.persona);
                profileImage.SetImageBitmap(icon);
            }
        }

        public void ResetField(int requestCode, object data)
        {
            switch (requestCode)
            {
                case 3:
                    ResetPhoto((byte[])data);

                    var photo_row = new VisitizerProfile
                    {
                        Id = profilePres.CachedProfile.Id,
                        Logo = (byte[])data
                    };
                    profilePres.UpdateSingleField(photo_row);

                    break;
                case 4:
                    var obj = JsonConvert.DeserializeObject<AddressModel>((string)data);
                    profilePres.CachedProfile.City = obj.City;
                    profilePres.CachedProfile.Address = obj.Address;
                    profilePres.UpdateRow();

                    var addres_row = new VisitizerProfile
                    {
                        Id = profilePres.CachedProfile.Id,
                        City = obj.City,
                        Address = obj.Address
                    };
                    profilePres.UpdateSingleField(addres_row);

                    break;
                case 5:
                    profilePres.CachedProfile.Name = (string)data;
                    profilePres.UpdateRow();
                    fullName.Text = profilePres.ProfileData.Name;

                    var name_row = new VisitizerProfile
                    {
                        Id = profilePres.CachedProfile.Id,
                        Name = (string)data
                    };
                    profilePres.UpdateSingleField(name_row);

                    break;
                case 6:
                    profilePres.CachedProfile.Email = (string)data;
                    profilePres.UpdateRow();
                    emailStr.Text = profilePres.ProfileData.Email;

                    var email_row = new VisitizerProfile
                    {
                        Id = profilePres.CachedProfile.Id,
                        Email = (string)data
                    };
                    profilePres.UpdateSingleField(email_row);

                    break;
                case 7:
                    var phoneTitle = Resources.GetString(Resource.String.Lk_phone_title);

                    profilePres.AddPhone(phoneTitle, (string)data);
                    phonesAdapter.NotifyDataSetChanged();

                    var phones_row = new VisitizerProfile
                    {
                        Id = profilePres.CachedProfile.Id,
                        PhoneNumbers = profilePres.ProfileData.PhoneNumbers
                    };
                    profilePres.UpdateSingleField(phones_row);


                    break;
            }
        }

        private void SetPersonalData()
        {
            ResetPhoto(profilePres.CachedProfile.Logo);
            SetDatebirthTitle();
            SetField(profilePres.CachedProfile.Name, fullName);
            SetField(profilePres.CachedProfile.Email, emailStr);
            
            var genderTitle = GetGender(profilePres.CachedProfile.Gender);
            genderStr.Text = genderTitle;

            var layoutManager = new LinearLayoutManager(this.Context, LinearLayoutManager.Vertical, false);
            phonenumsList.SetLayoutManager(layoutManager);
            phonesAdapter = new PhoneslistAdapter(cx, profilePres.PhoneList);
            phonenumsList.SetAdapter(phonesAdapter);
        }

        private void SetDatebirthTitle()
        {
            if (!(profilePres.CachedProfile.Birth_DT.Year.Equals(1) && profilePres.CachedProfile.Birth_DT.Month.Equals(1) && profilePres.CachedProfile.Birth_DT.Day.Equals(1)))
            {
                var dateStr = profilePres.GetdateofburthStr();
                dateofbirthStr.Text = dateStr;
            }
            else
            {
                dateofbirthStr.Text = notSpecifiedfield;
            }
        }

        private void SetField(string text, TextView field)
        {
            if (text!=null && text.Length > 0)
                field.Text = text;
            else
                field.Text = notSpecifiedfield;
        }

        private string GetGender(string sex)
        {
            string title = notSpecifiedfield;

            if (sex != null)
            {
                if (sex.Equals("m"))
                    title = Resources.GetString(Resource.String.Lk_man_title);
                else if (sex.Equals("w"))
                    title = Resources.GetString(Resource.String.Lk_woman_title);
            }

            return title;
        }

        public void ResetView()
        {
            ResetPhoto(profilePres.CachedProfile.Logo);
            SetDatebirthTitle();
            SetField(profilePres.CachedProfile.Name, fullName);
            SetField(profilePres.CachedProfile.Email, emailStr);

            var genderTitle = GetGender(profilePres.CachedProfile.Gender);
            genderStr.Text = genderTitle;

            profilePres.EmptyCache();
        }

    }
}


