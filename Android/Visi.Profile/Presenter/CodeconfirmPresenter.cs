﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Visi.Generic.Models;
using Visi.Generic.Helpers;
using Visi.Generic.Services;
using Visi.Generic.Services.ApiClients;

namespace Visi.Profile.Presenter
{
    public class CodeconfirmPresenter
    {
        private Random _random;
        private RestsharpApi apiRest;

        public CodeconfirmPresenter()
        {

        }

        public SecurityModel PhoneData { get; set; }


        public void SendSMS()
        {
            if (Configuration.IsSmsService)
            {
                _random = new Random();
                apiRest = new RestsharpApi(ApplicationSettings.SmsServiceEndPoint);
                apiRest.SetAuthenticator(ApplicationSettings.SmsService_Username, ApplicationSettings.SmsService_Password);

                var data = new SmsModel
                {
                    sender = ApplicationSettings.ApplicationOwner,
                    recipient = PhoneData.Phone,
                    message = PhoneData.SecurityCode,
                    dlr = 1
                };
                apiRest.SendPostAsJson("sms", data);
            }
        }

    }
}