﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reactive.Linq;

using Newtonsoft.Json;
using System.Threading.Tasks;

using Visi.Generic.Models;
using Visi.Generic.Helpers;
using Visi.Generic.Services;
using Visi.Generic.Services.ApiClients;

namespace Visi.Profile.Presenter
{
    public class AddtelPresenter
    {
        private List<CountryModel> countries;
        private List<CountryModel> data_countries;
        private Random _random;
        private SecurityModel phoneData;
        private RestsharpApi apiRest;

        public AddtelPresenter(string data)
        {
            phoneData = new SecurityModel();
            _random = new Random();
            apiRest = new RestsharpApi(ApplicationSettings.SmsServiceEndPoint);
            if (Configuration.IsSmsService)
            {
                apiRest.SetAuthenticator(ApplicationSettings.SmsService_Username, ApplicationSettings.SmsService_Password);
            }

            SetLocaleCountries();
            GluingJson(data);
            CleanModel();
            //GetCountryInfo();
        }

        public List<CountryModel> DataCountries => data_countries;

        public string SerelizeData => JsonConvert.SerializeObject(PhoneData);
        public SecurityModel PhoneData
        {
            private set { phoneData = value; }
            get { return phoneData; }
        }

        private void SetLocaleCountries()
        {
            var locales = Java.Util.Locale.GetAvailableLocales();

            countries = new List<CountryModel>();
            var count_str = new List<string>();

            foreach (var locale in locales)
            {
                var country = new CountryModel();

                country.LocaleName = locale.DisplayCountry;
                try
                {
                    country.Iso3Code = locale.ISO3Country;
                }
                catch (Exception) { }

                if (locale.DisplayCountry.Trim().Length > 0 && !count_str.Contains(country.LocaleName))
                {
                    count_str.Add(locale.DisplayCountry);
                    countries.Add(country);
                }
            }

        }

        private void GluingJson(string content)
        {
            data_countries = JsonConvert.DeserializeObject<List<CountryModel>>(content);


            countries.ForEach(x =>
            {
                var ind = data_countries.FindIndex(r => r.Iso3Code.Equals(x.Iso3Code));

                if (ind > 0)
                    data_countries[ind].LocaleName = x.LocaleName;
            });

        }

        private void CleanModel()
        {
            data_countries.RemoveAll(c => c.LocaleName == null);

            data_countries = data_countries.OrderBy(x=>x.LocaleName).ToList();
        }

        public async Task<int> UserCountryId()
        {
            //var response = await ApiSearch.DeviceLocation();
            //int id = -1;
            //if (response.Length > 1)
            //{
            //    var result = JsonConvert.DeserializeObject<DeviceCountry>(response);
            //    id = data_countries.FindIndex(x => x.Code.Equals(result.countryCode));
            //}

            return 0;
        }

        public void SendSMS(int code, string phone)
        {
            PhoneData.Phone = phone;
            PhoneData.CountryCode = DataCountries[code].CallingCode;

            if (Configuration.IsSmsService)
            {
                PhoneData.SecurityCode = GenerateCode();

                var data = new SmsModel
                {
                    sender = ApplicationSettings.ApplicationOwner,
                    recipient = PhoneData.Phone,
                    message = PhoneData.SecurityCode,
                    dlr = 1
                };
                apiRest.SendPostAsJson("sms", data);
            }
            else
            {
                PhoneData.SecurityCode = "1111";
            }
        }

        private string GenerateCode()
        {
            return _random.Next(0, 9999).ToString("D4");
        }

        public DeviceCountry CountryInfo { get; set; }

        public async Task GetCountryInfo()
        {
            if (ApplicationSettings.IsConnected)
            {
                var api = new RestsharpApi(ApplicationSettings.IpApi);
                var response = await api.GetRequest("json");
                if (response.Length > 1)
                {
                    CountryInfo = JsonConvert.DeserializeObject<DeviceCountry>(response);
                }
            }
        }

    }
}