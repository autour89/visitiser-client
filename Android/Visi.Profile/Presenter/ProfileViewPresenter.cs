﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Visi.Generic.Interfaces;
using Visi.Generic.DAL.DataModel;
using Visi.Generic.Models;
using Visi.Generic.Helpers;
using Visi.Generic.Services;
using Visi.Generic.DAL.Services;
using Visi.Generic.Services.ApiClients;
using Visi.Generic.Services.Synchronization;

namespace Visi.Profile.Presenter
{
    public class ProfileViewPresenter
    {
        private IContentAccess<VisitizerProfile> profileContent;
        private IContentAccess<AccountPhones> profilePhones;
        private VisitizerProfile profileData;
        private RefitApi smartApi;
        private CacheHelper cache;

        private List<PhoneModel> phoneList;
        public ProfileViewPresenter()
        {
            smartApi = RefitApi.Instance(Configuration.ConnectionString);
            cache = CacheHelper.Instance;

            phoneList = new List<PhoneModel>();
            profileContent = ContentDB.Account_Data;
            profilePhones = ContentDB.Account_Phones;

            CachedProfile = profileContent.SelectAllWithChildren<VisitizerProfile>().FirstOrDefault();

            EmptyCache();
        }

        public List<PhoneModel> PhoneList => phoneList;

        public VisitizerProfile ProfileData => profileContent.SelectAllWithChildren<VisitizerProfile>().FirstOrDefault();


        public VisitizerProfile CachedProfile
        {
            set { profileData = value; }
            get { return profileData; }
        }

        public void SetPhoneList(string phoneTitle)
        {
            var items = ProfileData.PhoneNumbers;
            var i = 0;
            items.ForEach(x =>
            {
                i++;
                phoneList.Add(new PhoneModel { Title = phoneTitle + " "+ i + "", Number = x.PhoneNumber });
            });

        }

        public void AddPhone(string phoneTitle, string data)
        {
            var obj = JsonConvert.DeserializeObject<SecurityModel>(data);


            var num = phoneList.Count + 1;
            profilePhones.InsertItem(new AccountPhones { AccountContactId = ProfileData.Id, PhoneNumber = obj.Phone, CountryCode = obj.CountryCode });
            phoneList.Add(new PhoneModel { Title = phoneTitle + " " + num + "", Number = obj.Phone });

        }

        public string GetdateofburthStr()
        {
            return ProfileData.Birth_DT.ToShortDateString();
        }

        public void UpdateRow()
        {
            profileContent.UpdateItem(CachedProfile);
        }


        public void UpdateSingleField(VisitizerProfile profile)
        {
            if (ApplicationSettings.IsConnected)
            {
                smartApi.UpdateProfile(profile);
            }
            else
            {
                AddToCache();
                cache.InsertData("profile", new CachedObj
                {
                    ApiUri = ApiUrl.UpdateData,
                    Data = JsonConvert.SerializeObject(ProfileData),
                    UpdatedAt = new DateTimeOffset(DateTime.Now)
                });
            }
        }


        private void AddToCache()
        {
            var prof_key = cache.GetKey("profile");
        }

        public void EmptyCache()
        {
            var prof_key = cache.GetKey("profile");

            //var prof_keyss = cache.GetKeys();

            if (ApplicationSettings.IsConnected)
            {
                if (prof_key != null)
                {
                    var profile_cache = cache.GetData<CachedObj>(prof_key);

                    var obj = JsonConvert.DeserializeObject<VisitizerProfile>(profile_cache.Data);

                    if (obj != null)
                    {
                        smartApi.UpdateProfile(obj);
                        cache.EmptyCache(prof_key);
                    }
                }
            }
        }

        public void UpdateApi()
        {
            //var prof = JsonConvert.SerializeObject(ProfileData);
            //visiApi.UpdateProfile(prof);
        }

    }
}