using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Globalization;

namespace Visi.Generic.Helpers
{
    public static class CalendarHelper
    {

        /// <summary>
        /// Get dates in specific period 
        /// </summary>
        public static List<DateTime> DateRange(DateTime fromDate, DateTime toDate)
        {
            return Enumerable.Range(0, toDate.Subtract(fromDate).Days + 1)
                             .Select(d => fromDate.AddDays(d)).ToList();
        }

        /// <summary>
        /// Get dates of specific year and month
        /// </summary>
        public static List<DateTime> GetMonthDates(int year, int month)
        {
            return Enumerable.Range(1, DateTime.DaysInMonth(year, month))
                             .Select(day => new DateTime(year, month, day))
                             .ToList();
        }

        /// <summary>
        /// Get count of days in month
        /// </summary>
        public static int DaysInMonth(int year, int month)
        {
            return DateTime.DaysInMonth(year, month);
        }


        /// <summary>
        /// Get first day of months for specific range
        /// </summary>
        public static List<DateTime> GroupDatesRange(DateTime fromstart, DateTime toend)
        {
            var droup_dt = new List<DateTime>();

            for (int year = fromstart.Year; year <= toend.Year; year++)
                for (int month = 1; month <= 12; month++)
                    droup_dt.Add(new DateTime(year, month, 1));

            return droup_dt;
        }

        /// <summary>
        /// Get week day dates of specific month
        /// </summary>
        public static List<DateTime> WeekDayDates(int year, int month)
        {
            return Enumerable.Range(1, DateTime.DaysInMonth(year, month))
                             .Select(day => new DateTime(year, month, day))
                             .Where(dt => dt.DayOfWeek != DayOfWeek.Sunday &&
                                          dt.DayOfWeek != DayOfWeek.Saturday)
                             .ToList();
        }

        /// <summary>
        /// Get week number in current month
        /// </summary>
        public static int WeekNumber(this DateTime value)
        {
            return CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(value, CalendarWeekRule.FirstFourDayWeek, DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek);
        }

        /// <summary>
        /// Get count of weeks in month
        /// </summary>
        public static IEnumerable<DateTime> GetWeeks(DateTime date, DayOfWeek startDay)
        {
            var list = new List<DateTime>();
            DateTime first = new DateTime(date.Year, date.Month, 1);

            for (var i = first; i < first.AddMonths(1); i = i.AddDays(1))
            {
                if (i.DayOfWeek == startDay)
                    list.Add(i);
            }

            return list;
        }

        /// <summary>
        /// Change first day of week to Monday
        /// </summary>
        public static List<string> ChangeFirstDay(List<string> days)
        {
            var item = days[0];
            days.RemoveAt(0);
            days.Insert(days.Count, item);

            return days;
        }

        /// <summary>
        /// Get month's names
        /// </summary>
        public static List<string> MonthNms => DateTimeFormatInfo.CurrentInfo.MonthNames.ToList();

        /// <summary>
        /// Get abbreviated month's names
        /// </summary>
        public static List<string> AbbreviatedMonthNms => DateTimeFormatInfo.CurrentInfo.AbbreviatedMonthNames.ToList();

        /// <summary>
        /// Get week days names
        /// </summary>
        public static List<string> WeekDayNames => DateTimeFormatInfo.CurrentInfo.DayNames.ToList();

        /// <summary>
        /// Get abbreviated week days names
        /// </summary>
        public static List<string> AbbreviatedDayNames => DateTimeFormatInfo.CurrentInfo.AbbreviatedDayNames.ToList();

        /// <summary>
        /// Get first day of week as string, for globalization purpose
        /// </summary>
        public static DayOfWeek FirstDayOfWeekStr => DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek;

        /// <summary>
        /// Get first day of week as number, for globalization purpose
        /// </summary>
        public static int FirstDayOfWeekNum => Convert.ToInt32(DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek);

        /// <summary>
        /// Calculates days left
        /// </summary>
        /// <param name="StartDate">From date</param>
        /// <param name="EndDate">To date</param>
        /// <returns></returns>
        public static double CalculateDaysLeft(DateTime StartDate, DateTime EndDate)
        {
            return (EndDate - StartDate).TotalDays;
        }
    }
}