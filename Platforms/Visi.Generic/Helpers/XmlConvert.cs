﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Visi.Generic.Helpers
{
    public static class XmlConvert
    {
        public static string ElemType(XElement elem,string attr)
        {
            return elem.Attribute(attr).Value;
        }

        public static string ElemValue(XElement elem)
        {
            return elem.Value;
        }

        public static XElement DocRoot(string xml)
        {
            return XDocument.Parse(xml).Root;
        }

        public static List<XElement> ElemList(XElement elem)
        {
            return elem.Elements().ToList();
        }

        public static XElement ElemItem(XElement elem, string name)
        {
            return elem.Elements(name).FirstOrDefault();
        }

        public static XAttribute NewAttr(string key,object value)
        {
            return new XAttribute(key, value);
        }

        public static XElement NewElem(string key, params object[] content)
        {
            return new XElement(key, content);
        }

        public static XElement PackElem(XElement elem, params object[] items)
        {
            elem.Add(items);

            return elem;
        }

        public static XDocument PackDoc(XElement root)
        {
            return new XDocument(root);
        }

        public static XElement RmElem(XElement elem,string name)
        {
            elem.Elements().Where(x => x.Element(name) != null).Remove();

            return elem;
        }

        public static XElement RmElems(XElement elem, string name)
        {
            var nodes = elem.Elements().Where(x => x.Element(name) != null).ToList();

            foreach (var node in nodes)
                node.Remove();

            return elem;
        }

        public static XElement RmAttr(XElement elem, string name)
        {
            elem.Attributes().Where(x => x.Name.Equals(name)).Remove();

            return elem;
        }
    }
}
