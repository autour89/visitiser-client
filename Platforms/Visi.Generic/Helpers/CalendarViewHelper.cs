﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Visi.Generic.Interfaces;
using Visi.Generic.Models;

namespace Visi.Generic.Helpers
{
    public class CalendarViewHelper : ICalendarViewModel
    {
        private static CalendarViewHelper _object;

        private List<DateRangeModel> datesRange;
        private DateTime now = DateTime.Now;
        private int locale = CalendarHelper.FirstDayOfWeekNum;

        CalendarViewHelper()
        {
            datesRange = new List<DateRangeModel>();
        }

        private int CurrentMonth { get; set; }

        public static CalendarViewHelper Instance
        {
            get
            {
                if (_object == null)
                    _object = new CalendarViewHelper();

                return _object;
            }
        }


        /// <summary>
        /// Gluing monthes to fulfilled empty spaces in weeks 
        /// </summary>
        public List<DateTime> GluingMonths(DateTime current, List<DateTime> month)
        {
            List<DateTime> prev_m = null;
            List<DateTime> next_m = null;
            var first_dt = 0;
            var last_dt = 0;

            if (current.Month.Equals(1))
                prev_m = CalendarHelper.GetMonthDates(current.Year - 1, 12);
            else
                prev_m = CalendarHelper.GetMonthDates(current.Year, current.Month - 1);

            if (current.Month.Equals(12))
                next_m = CalendarHelper.GetMonthDates(current.Year + 1, 1);
            else
                next_m = CalendarHelper.GetMonthDates(current.Year, current.Month + 1);

            var num = CalendarHelper.DaysInMonth(prev_m[0].Year, prev_m[0].Month);

            CurrentMonth = month[0].Month;
            switch (locale)
            {
                case 0:
                    first_dt = num - (int)month[0].DayOfWeek;
                    last_dt = 7 - ((int)month[month.Count - 1].DayOfWeek + 1);

                    prev_m = prev_m.Where(day => day.Day > first_dt).ToList();
                    next_m = next_m.Take(last_dt).ToList();
                    month.InsertRange(0, prev_m);
                    month.AddRange(next_m);

                    break;
                case 1:
                    var day_in_week_prev = (int)month[0].DayOfWeek;
                    var day_in_week_cur = (int)month[month.Count - 1].DayOfWeek;

                    if (day_in_week_prev.Equals(0))
                        first_dt = num - 6;
                    else
                        first_dt = num - ((int)month[0].DayOfWeek - 1);
                    if (day_in_week_cur.Equals(0))
                        last_dt = 0;
                    else
                        last_dt = 7 - ((int)month[month.Count - 1].DayOfWeek);

                    prev_m = prev_m.Where(day => day.Day > first_dt).ToList();
                    next_m = next_m.Take(last_dt).ToList();
                    month.InsertRange(0, prev_m);
                    month.AddRange(next_m);

                    break;
            }

            return month;
        }

        /// <summary>
        /// Gluing monthes for day calendar to fulfilled empty spaces in weeks 
        /// </summary>
        private List<DateTime> GluingWeekMonths(DateTime current, List<DateTime> months)
        {
            List<DateTime> prev_m = null;
            var first_dt = 0;

            if (current.Month.Equals(1))
                prev_m = CalendarHelper.GetMonthDates(current.Year - 1, 12);
            else
                prev_m = CalendarHelper.GetMonthDates(current.Year, current.Month - 1);

            var num = CalendarHelper.DaysInMonth(prev_m[0].Year, prev_m[0].Month);

            switch (locale)
            {
                case 0:
                    first_dt = num - (int)months[0].DayOfWeek;

                    prev_m = prev_m.Where(day => day.Day > first_dt).ToList();
                    months.InsertRange(0, prev_m);

                    break;
                case 1:
                    var day_in_week_prev = (int)months[0].DayOfWeek;
                    var day_in_week_cur = (int)months[months.Count - 1].DayOfWeek;

                    if (day_in_week_prev.Equals(0))
                        first_dt = num - 6;
                    else
                        first_dt = num - ((int)months[0].DayOfWeek - 1);

                    prev_m = prev_m.Where(day => day.Day > first_dt).ToList();
                    months.InsertRange(0, prev_m);

                    break;
            }

            return months;
        }
        /// <summary>
        /// Get number of weeks in month
        /// </summary>
        public int GetWeeks(List<DateTime> full_month)
        {
            return full_month.Count / 7;
        }



        public List<DateTime> DatesList(int from, int max)
        {
            var time = DateTime.Now;

            var fromDate = new DateTime(time.Year - from, time.Month, 1);
            var toDate = new DateTime(time.Year + max, time.Month, 1);

            return CalendarHelper.DateRange(fromDate.GetFirstDayOfWeek(), toDate);
        }


        /// <summary>
        /// Fulfilled data model
        /// </summary>
        public List<DateRangeModel> DaysValues(List<DateTime> dates)
        {
            var now = DateTime.Now;
            var list_days = CalendarHelper.AbbreviatedDayNames;
            var list_month = CalendarHelper.MonthNms;
            var list_abriv_month = CalendarHelper.AbbreviatedMonthNms;

            var rangeD = new List<DateRangeModel>();
            var month = dates[0].Month;
            var state = true;
            var isFirst = false;
            var isCurrent = false;

            foreach (var date in dates)
            {
                if (!date.Month.Equals(month))
                {
                    state = !state;
                    month = date.Month;
                }
                if (date.Day.Equals(1))
                    isFirst = true;
                else
                    isFirst = false;

                if (date.Year.Equals(now.Year) && date.Month.Equals(now.Month) && date.Day.Equals(now.Day))
                    isCurrent = true;
                else
                    isCurrent = false;


                rangeD.Add(new DateRangeModel
                {
                    DayOfMonth = date.Day,
                    Month = date.Month,
                    Year = date.Year,
                    DayOfWeek = list_days[(int)date.DayOfWeek],
                    MonthOfYear = list_month[date.Month - 1],
                    AbbreviatedMonthOfYear = list_abriv_month[date.Month - 1],
                    Flag = state,
                    IsFirstDay = isFirst,
                    IsDaySelected = false,
                    IsCurrent = isCurrent
                });
            }

            return rangeD;
        }



        public List<string> ListDays()
        {
            var list_days = new List<string>();

            var locale = CalendarHelper.FirstDayOfWeekNum;

            switch (locale)
            {
                case 0:
                    list_days = CalendarHelper.AbbreviatedDayNames;
                    break;
                case 1:
                    var days = CalendarHelper.AbbreviatedDayNames;
                    list_days = CalendarHelper.ChangeFirstDay(days);
                    break;
            }

            return list_days;
        }

        /// <summary>
        /// Get formatted date model
        /// </summary>
        public static DateRangeModel DateCurrent(int year, int month, int dayOfMonth)
        {
            var mont_nms = CalendarHelper.MonthNms;
            var week_dys = CalendarHelper.AbbreviatedDayNames;
            var dt = new DateTime(year, month, dayOfMonth);
            var d_ofweek = Convert.ToInt32(dt.DayOfWeek);

            var date_m = new DateRangeModel
            {
                Year = year,
                Month = month,
                DayOfMonth = dayOfMonth,
                DayOfWeek = week_dys[d_ofweek],
                MonthOfYear = mont_nms[month - 1]
            };

            return date_m;
        }
    }


    public static class DateTimeExtensions
    {
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(-1 * diff).Date;
        }

        public static DateTime GetFirstDayOfWeek(this DateTime date)
        {
            var firstDayOfWeek = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;

            while (date.DayOfWeek != firstDayOfWeek)
            {
                date = date.AddDays(-1);
            }

            return date;
        }
    }
}
