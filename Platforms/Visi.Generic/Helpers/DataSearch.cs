﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Visi.Generic.Helpers
{
    public class DataSearch<T> where T : new()
    {
        public List<T> Data { get; set; }
        public List<T> SearchFilter(params string[] keywords)
        {
            var predicate = PredicateBuilder.True<T>();

            foreach (string keyword in keywords)
            {
                predicate = predicate.And(p => p.ToString().Contains(keyword));
            }

            return Data.Where(predicate.Compile()).ToList();
        }


        public Expression<Func<T, bool>> Predicate(string[] filter)
        {
            var predicate = PredicateBuilder.True<T>();

            foreach (string keyword in filter)
                predicate = predicate.And(p => p.ToString().Contains(keyword));

            return predicate;
        }

    }
}