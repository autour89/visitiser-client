﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Visi.Generic.Services;

namespace Visi.Generic.Models
{
    public static class ApiSearch
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public static async Task<string> FindAddress(string address)
        {
            string content;

            using (var client = new HttpClient())
            {
                var request = await client.GetAsync(address);

                content = await request.Content.ReadAsStringAsync();
            }

            return content;
        }

        /// <summary>
        /// Get device geo location
        /// </summary>
        /// <returns>json string</returns>
        public static string DeviceLocation()
        {
            var content="";

            try
            {
                using (var client = new HttpClient())
                {
                    //var request = client.GetAsync(Configuration.IpApi);
                    //content = request.Content.ReadAsStringAsync();

                    Task.Run(async () =>
                    {
                        var request = await client.GetAsync(ApplicationSettings.IpApi);
                        content = await request.Content.ReadAsStringAsync();
                    });
                }
            }
            catch (Exception)
            {
            }

            return content;
        }

    }
}
