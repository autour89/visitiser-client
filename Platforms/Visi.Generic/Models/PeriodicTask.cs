﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Visi.Generic.Models
{
    public class PeriodicTask
    {
        public static CancellationTokenSource TokenSource { get; set; }
        public static async Task Run(Action action, TimeSpan period, CancellationToken cancellationToken)
        {

            await Task.Delay(period, cancellationToken);

            if (!cancellationToken.IsCancellationRequested)
                action();
            //while (!CancellationToken.IsCancellationRequested)
            //{
            //    await Task.Delay(period, CancellationToken);

            //    if (!CancellationToken.IsCancellationRequested)
            //        action();
            //}
        }

        public static Task Run(Action action, TimeSpan period)
        {
            return Run(action, period, CancellationToken.None);
        }

    }
}