﻿
using Newtonsoft.Json;

namespace Visi.Generic.Models
{

    public class ContactsModel
    {
        public byte[] Picture { get; set; }
        public int AccountContactId { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public bool IsContact { get; set; }

        public override string ToString()
        {
            return AccountContactId + " " + Name + " " + Number;
        }

    }

    public class CountryModel
    {
        public string EnglishName { get; set; }
        public string LocaleName { get; set; }
        public string Code { get; set; }
        public string Iso3Code { get; set; }
        public string CallingCode { get; set; }
    }

    public class DeviceCountry
    {
        [JsonProperty("country_name")]
        public string Country { get; set; }
        [JsonProperty("country_code")]
        public string CountryCode { get; set; }
        [JsonProperty("latitude")]
        public string lat { get; set; }
        [JsonProperty("longitude")]
        public string lon { get; set; }
    }

    public class SmsModel
    {
        public string sender { get; set; }
        public string recipient { get; set; }
        public string message { get; set; }
        public int dlr { get; set; }
    }


    public class SecurityModel
    {
        public string Phone { get; set; }
        public string CountryCode { get; set; }
        public string SecurityCode { get; set; }
    }





    //    public class RegisterModel : IValidatableObject
    //    {
    //        [DataType(DataType.PhoneNumber, ErrorMessage ="Validation error")]
    //        [Required(ErrorMessage="Is required")]
    //        [Phone(ErrorMessage = "Validation error")]
    //        [RegularExpression(@"^\([+]?\d{3}\)\d{2}-?\d{3}-?\d{4}$", ErrorMessage ="Phone number is wrong")]
    //        public string PhoneNumber { get; set; }

    //        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    //        {
    //            var results = new List<ValidationResult>();

    //            Validator.TryValidateProperty(this.PhoneNumber,
    //                new ValidationContext(this, null, null) { MemberName = "PhoneNumber" },
    //                results);

    //            return results;
    //        }
    //    }
}
