﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Visi.Generic.DAL.DataModel;

namespace Visi.Generic.Models
{

    public class RegisterViewModel
    {
        public string Name { get; set; }
        public string CountryCode { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class AccountData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Sex { get; set; }
        public string City { get; set; }
        public List<AccountPhones> PhoneNumber { get; set; }
        public DateTime Birth_DT { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public byte[] Logo { get; set; }
        public override string ToString()
        {
            return Name + " " + PhoneNumber + " " + Birth_DT + " " + Email;
        }
    }


    //public class VisiProfile
    //{
    //    [JsonProperty("id")]
    //    public int Id { get; set; }
    //    [JsonProperty("name")]
    //    public string Name { get; set; }
    //    [JsonProperty("gender")]
    //    public string Gender { get; set; }
    //    [JsonProperty("birth_dt")]
    //    public DateTime Birth_DT { get; set; }
    //    [JsonProperty("city")]
    //    public string City { get; set; }
    //    [JsonProperty("address")]
    //    public string Address { get; set; }
    //    [JsonProperty("email")]
    //    public string Email { get; set; }
    //    [JsonProperty("logo")]
    //    public byte[] Logo { get; set; }
    //    public string Skype { get; set; }
    //    public string Facebook { get; set; }
    //    public string Site { get; set; }
    //    [JsonProperty("phones")]
    //    public List<Phones> Phones { get; set; }
    //}

    //public class Phones
    //{
    //    public int Id { get; set; }
    //    public int AccountContactId { get; set; }
    //    public string Phone { get; set; }
    //}

    public class AccountModel
    {
        public int UserId { get; set; }
        public int ChatId { get; set; }
        public List<int> Ids { get; set; }
        public List<AccountContacts> Items { get; set; }
    }

    public class ChaListModel
    {
        public int ChatId { get; set; }
        public string ChatName { get; set; }
        public string ChatSnippet { get; set; }
        public List<ChatMessages> Items { get; set; }
    }

    public class MessageModel
    {
        public int Account1 { get; set; }
        public int Account2 { get; set; }
        public int MessageId { get; set; }
        public int SenderId { get; set; }
        public int ChatId { get; set; }
        public string SenderName { get; set; }
        public DateTime MessageDT { get; set; }
        public string Message { get; set; }
        public string MessageType { get; set; }
    }

    public class BusinessCardModel
    {
        public int Business_Card_ID { get; set; }
        public int AccountContactId { get; set; }
        public string Business_Card_NM { get; set; }
        public string Business_Card_Descriprion { get; set; }
        public string Business_Card_Adress { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Business_Card_Email { get; set; }
        public string Business_Card_Phones { get; set; }
        public List<ServicesModel> Services { get; set; }
    }
    public class ServicesModel
    {
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public override string ToString()
        {
            return ServiceName;
        }
    }

    public class AccountChats
    {
        public int ConversationId { get; set; }
    }


    public class BCard_Adresses
    {
        public string Business_Card_City { get; set; }
        public string Business_Card_Adress_NM { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }    

    public class BCard_Links
    {
        public string Name { get; set; }
    }
    public class BCard_Services
    {
        public int Business_Card_Serv_ID { get; set; }
        public string Serv_NM { get; set; }
    }



    public class RenderRequest
    {
        public string Text { get; set; }
        public string Mode { get; set; }
        public string Context { get; set; }
    }


    public class ErrorModel
    {
        public int StatusCode { get; set; }
        public DateTime Time { get; set; }
        public string User { get; set; }
        public string Detail { get; set; }
        public string Message { get; set; }
        public string Source { get; set; }
        public string Type { get; set; }
        public Exception Exception { get; set; }
    }


    public class AccountInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public byte[] Logo { get; set; }
        public override string ToString()
        {
            return Name + " " + PhoneNumber.ParsePhoneStr(HelperStr.PhoneContains) + " " + Email;
        }
    }



}
