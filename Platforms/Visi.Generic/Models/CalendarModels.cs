﻿using System;
using System.Collections.Generic;

namespace Visi.Generic.Models
{
    public class CallNumberModel
    {
        public DateTime DT { get; set; }
        public string Number { get; set; }
    }

    public class DateRangeModel
    {
        public int DayOfMonth { get; set; }
        public string DayOfWeek { get; set; }
        public int HourOfDay { get; set; }
        public int Minutes { get; set; }
        public int Month { get; set; }
        public string MonthOfYear { get; set; }
        public string AbbreviatedMonthOfYear { get; set; }
        public int Year { get; set; }
        public TimeSpan Duration { get; set; }
        public bool Flag { get; set; }
        public bool IsFirstDay { get; set; }
        public bool IsDaySelected { get; set; }
        public bool IsCurrent { get; set; }

    }


    public class PageTableModel
    {
        public int DayOfMonth { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public int ListPosition { get; set; }
    }

    public class EventDateModel
    {
        public DateTime Date { get; set; }
        public int Duration { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public List<string> Members { get; set; }
    }


    public class WeekModel
    {
        public List<DateRangeModel> Week { get; set; }
    }


    public class TimetableModel
    {
        public string Time { get; set; }
        public int Hour { get; set; }
        public string EventText { get; set; }
        public bool CheckboxView { get; set; }
        public bool Checked { get; set; }
        public bool IsPanel { get; set; }
        public bool isHour { get; set; }

    }

    public static class Extensions
    {
        public static bool IsHourEqual(this int hour)
        {
            return true ? hour.Equals(DateTime.Now.Hour) : false;
        }
    }


   


}
