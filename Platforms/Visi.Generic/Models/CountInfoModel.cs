﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Visi.Generic.Models
{
    public class CountInfoModel
    {
        public int DrawId{ get; set; }
        public string InfoTitle { get; set; }
        public string Info { get; set; }
    }
}
