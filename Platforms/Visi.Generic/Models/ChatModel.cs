﻿
namespace Visi.Generic.Models
{
    public class MessageM
    {
        public string Message { get; set; }
    }

    public class FileM
    {
        public string Name { get; set; }
    }
    public class VisitCardM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte[] Logo { get; set; }
        public string Adress { get; set; }
        public string Email { get; set; }
        public string Phones { get; set; }
        public string Services { get; set; }
    }


}
