﻿
namespace Visi.Generic.Models
{
    public class PhoneModel
    {
        public string Title { get; set; }
        public string Number { get; set; }
    }

    public class AddressModel
    {
        public string City { get; set; }
        public string Address { get; set; }
    }
}
