﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Visi
{
    public static class Extensions
    {
        public static string ParsePhoneStr(this string text, IEnumerable<char> chars)
        {
            return new string(text.Where(c => !chars.Contains(c)).ToArray());
        }

        private static string ParsePhone(this string phone)
        {
            var arrPhone = phone.ToCharArray().ToList();

            arrPhone.RemoveAll(x => x == ('(') || x == (')') || x == ('+') || x == (' '));

            return new string(arrPhone.ToArray());
        }

    }

    public struct HelperStr
    {
        public static char[] PhoneContains = { '(', '+', ')', ' ' };
    }

}
