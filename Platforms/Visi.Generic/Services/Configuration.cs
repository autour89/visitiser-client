﻿
#region Preprocessor Directives
#define STAGING
#define PRODUCTION

namespace Visi.Generic.Services
{
    public static class Configuration
    {
#if DEBUG
        //public const string BaseUrl = "http://test.visitizer.com:8080/Visi/";
        public const string ConnectionString = "http://192.168.0.108:59220";
        public const bool IsSmsService = false;
#elif RELEASE
        public const string BaseUrl = "http://test.visitizer.com:8080/Visi/";
        public const bool IsSmsService = false;
#elif TEST
        public const string BaseUrl = "http://192.168.0.102:59220/";
        public const bool IsSmsService = false;
#else
        public const string BaseUrl = "http://192.168.0.102:59220/";
#endif

    }
}

#endregion
