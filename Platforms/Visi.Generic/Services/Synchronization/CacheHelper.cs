﻿using Akavache;
using Akavache.Sqlite3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyIoC;
using System.Reactive.Linq;
using Visi.Generic.Interfaces;
using Visi.Generic.Models;
using Newtonsoft.Json;
using Visi.Generic.Services;

namespace Visi.Generic.Services.Synchronization
{
    public class CacheHelper
    {
        private static CacheHelper instance;
        private IBlobCache cache;
        public CacheHelper()
        {
            CreateBinding();
            cache = BlobCache.LocalMachine;
        }

        public IBlobCache Cache => cache;
        public static CacheHelper Instance
        {
            get
            {
                if (instance == null)
                    instance = new CacheHelper();

                return instance;
            }
        }

        private void CreateBinding()
        {
            var container = TinyIoCContainer.Current;
            container.AutoRegister(t => t.Name.Equals("ISQLite") || t.Name.Contains("SQLite_"));
            var path = container.Resolve<ISQLite>().GetCacheConnectionString();
            BlobCache.ApplicationName = "VisiCache";
            BlobCache.LocalMachine = new SQLitePersistentBlobCache(path);
        }

        public void InsertData(string key, CachedObj obj)
        {
            //Task.Run(async () =>
            //{
            //    await cache.InsertObject(key, obj).GetAwaiter();
            //}).Wait();

            cache.InsertObject(key, obj).Wait();
        }

        public Task<T> GetData2<T>(string key)
        {
            return Task.Run(async () => await cache.GetObject<T>(key));
        }

        public T GetData<T>(string key)
        {
            return cache.GetObject<T>(key).Wait();
        }

        public List<string> KeysCount()
        {
            return cache.GetAllKeys().Wait().ToList();
        }

        public string GetKey(string key)
        {
            return cache.GetAllKeys().Wait().Where(o => o.Equals(key)).FirstOrDefault();
        }

        public List<string> GetKeys()
        {
            return cache.GetAllKeys().Wait().ToList();
        }

        public void EmptyCache(string key)
        {
            cache.Invalidate(key).Wait();
        }

        public void EmptyCache()
        {
            cache.InvalidateAll().Wait();
        }

        public void ReportErrors(ErrorModel data)
        {
            var key = GetKey("errors");

            if (key != null)
            {
                var cache_item = GetData<List<ErrorModel>>("errors");
                cache_item.Add(data);
                var ch_obj = new CachedObj
                {
                    ApiUri = ApiUrl.WriteErrors,
                    Data = JsonConvert.SerializeObject(cache_item,Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                    }),
                    UpdatedAt = new DateTimeOffset(DateTime.Now)
                };

                cache.InsertObject("errors", ch_obj).Wait();
            }
            else
            {
                var cache_data = new List<ErrorModel>();
                cache_data.Add(data);
                var ch_obj = new CachedObj
                {
                    ApiUri = ApiUrl.WriteErrors,
                    Data = JsonConvert.SerializeObject(data, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                    }),
                    UpdatedAt = new DateTimeOffset(DateTime.Now)
                };
                cache.InsertObject("errors", ch_obj).Wait();
            }
        }

    }


    public class CachedObj
    {
        public string ApiUri { get; set; }
        public string Data { get; set; }
        public DateTimeOffset UpdatedAt { get; set; }
    }
}
