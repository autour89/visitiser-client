﻿using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Visi.Generic.DAL.DataModel;
using Visi.Generic.Interfaces;
using Visi.Generic.Models;

namespace Visi.Generic.Services.ApiClients
{
    public class RefitApi
    {
        private HttpClient httpClient;

        private static RefitApi instance;
        public RefitApi(string address)
        {
            httpClient = new HttpClient()
            {
                BaseAddress = new Uri(address)
            };
        }

        public static RefitApi Instance(string address)
        {
            if (instance == null)
                instance = new RefitApi(address);

            return instance;
        }

        public void UpdateProfile(VisitizerProfile data)
        {
            var service = RestService.For<IProfileApi>(httpClient);

            var o = Observable.Start(() => { service.UpdateRequest(data); });
            o.Subscribe();
            //o.Wait();
        }


        public string FindAccountByPhone(AccountInfo data)
        {
            var service = RestService.For<IAccountByPhone>(httpClient);

            //var o = Observable.Start(() => { service.SearchAccount(data); });
            //var ff = o.Subscribe();
            //string s = await service.SearchAccount(data);
            string s = "";
            Task actualTask = new Task(async () =>
            {
                s = await service.SearchAccount(data);
            });

            return s;
        }

        public void SendReport(List<ErrorModel> data)
        {
            var service = RestService.For<IErrorsLog>(httpClient);

            var o = Observable.Start(() => { service.ErrorsRequest(data); });
            o.Subscribe();
            //o.Wait();
        }

        public void SendReport(ErrorModel data)
        {
            var service = RestService.For<IErrorLog>(httpClient);

            var o = Observable.Start(() => { service.ErrorRequest(data); });
            o.Subscribe();
            //o.Wait();
        }


    }
}
