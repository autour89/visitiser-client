﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RestSharp;
using RestSharp.Authenticators;

namespace Visi.Generic.Services.ApiClients
{
    public class RestsharpApi
    {
        private RestClient client;
        private static RestsharpApi instance = null;
        public RestsharpApi(string baseUrl)
        {
            client = new RestClient(baseUrl);
        }

        public RestRequest Request { get; set; }
        public IRestResponse Response { get; set; }

        public static RestsharpApi Instance(string baseUrl)
        {
            if (instance == null)
                instance = new RestsharpApi(baseUrl);

            return instance;
        }

        public void SetAuthenticator(string username, string password)
        {
            client.Authenticator = new HttpBasicAuthenticator(username, password);
        }

        public void SetUri(string url)
        {
            client.BaseUrl = new Uri(url);
        }

        public async Task<string> SendPostAsJson(string api, object data)
        {
            Request = new RestRequest(api, Method.POST);

            Request.AddHeader("Accept", "application/json");
            Request.AddJsonBody(data);

            await SendRequest;

            return Response.Content;
        }

        public async Task<string> PostRequest(string api, object data)
        {
            Request = new RestRequest(api, Method.POST);
            Request.AddObject(data);

            await SendRequest;

            return Response.Content;
        }

        public async Task<string> GetRequest(string api)
        {
            Request = new RestRequest(api, Method.GET);

            await SendRequest;

            return Response.Content;
        }
       
        public async Task<string> SendPostAsBody(string api, object data)
        {
            Request = new RestRequest(api, Method.POST);
            Request.AddBody(data);

            await SendRequest;

            return Response.Content;
        }

        private Task SendRequest => Task.Run(() =>
        {
            Response = client.Execute(Request);
        });


    }
}
