﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Visi.Generic.Models;
using Visi.Generic.Services;
using Newtonsoft.Json;

namespace Visi.Generic.Services.ApiClients
{
    public class HttpClientApi
    {
        private HttpClient client;
        private static HttpClientApi instance = null;

        public HttpClientApi()
        {
            client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            EndpointUri = new Uri(Configuration.ConnectionString);
        }

        public Uri EndpointUri
        {
            get => client.BaseAddress;
            set => client.BaseAddress = value;
        }
        public static HttpClientApi Instance
        {
            get
            {
                if (instance == null)
                    instance = new HttpClientApi();

                return instance;
            }
        }

        public async Task<string> SendPostJsonAsync(string request, object data)
        {
           var response = await client.PostAsJsonAsync(request, data);

            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            response.EnsureSuccessStatusCode();

            string responseBody = await response.Content.ReadAsStringAsync();

            return responseBody;
        }

        public async Task<string> SendPostAsync(string request, object data)
        {
            var query = JsonConvert.SerializeObject(data);
            StringContent queryString = new StringContent(query);
            var response = await client.PostAsync(new Uri(request), queryString);

            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            response.EnsureSuccessStatusCode();

            string responseBody = await response.Content.ReadAsStringAsync();

            return responseBody;
        }

        public async Task<string> SendGetAsync(string request)
        {
            var response = await client.GetAsync(request);

            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            response.EnsureSuccessStatusCode();

            string responseBody = await response.Content.ReadAsStringAsync();

            return responseBody;
        }
    }
}
