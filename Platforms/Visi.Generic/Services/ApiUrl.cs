﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Visi.Generic.Services
{
    public static class ApiUrl
    {
        public const string CreateAccount = "Api/Account/CreateAccount/";

        public const string NewConversations = "/Api/Account/GetNewConversations/";

        public const string UpdateData = "/Api/Profile/UpdateData";

        public const string FindAccntByPhone = "/Api/Account/FindAccntByPhone/";

        public const string FindAccnt = "/Api/Account/FindAccnt/";

        public const string AddToContacts = "/Api/Account/AddToContacts/";
        
        public const string CheckContacts = "/Api/Account/CheckContacts/";

        public const string RemoveFromContacts = "/Api/Account/RemoveFromContacts/";

        public const string GetAllContacts = "/Api/Account/GetAllContacts";
        
        public const string GetNewMessages = "/Api/Messages/GetNewMessages/?chatId=";

        public const string CheckMessages = "/Api/Messages/CheckMessages/";

        public const string RemoveItem = "/Api/Messages/RemoveItem/?Id=";

        public const string EditItem = "/Api/Messages/EditItem/";

        public const string NewMessage = "/Api/Messages/NewMessage";

        public const string GetAllMessages = "/Api/Messages/GetAllMessages";

        public const string AccontConversations = "/Api/Messages/AccontConversations";

        public const string WriteErrors = "/Api/Logger/WriteErrors";

        public const string WriteError = "/Api/Logger/WriteError";

        public const string WriteException = "/Api/Logger/WriteException";
        
    }
}
