﻿
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Visi.Generic.Interfaces;
using Visi.Generic.Models;
using Visi.Generic.Services;

namespace Visi.Generic.Services.SignalR
{
    public class HubTon : IHubTon
    {
        private static IHubTon instance = null;
        public String StatusText { get; set; }
        public bool IsConnected { get; set; }
        private Dictionary<string, string> QueryString { get; set; }
        public HubConnection Connection { get; set; }
        public IHubProxy HubProxy { get; set; }
        protected HubTon(Dictionary<string, string> querystring)
        {
            QueryString = querystring;

            CreateConnection();
        }
        public static IHubTon Instance(Dictionary<string, string> querystring)
        {
            if (instance == null)
            {
                instance = new HubTon(querystring);
            }
            return instance;
        }

        public void CreateConnection()
        {
            Connection = new HubConnection(Configuration.ConnectionString, QueryString);
            HubProxy = Connection.CreateHubProxy("MessengerHub");
        }

        public void ToConnect()
        {
            try
            {
                Task.Run(() =>
                {
                    Connection.Start();
                    IsConnected = true;
                    StatusText = "Connected to server at " + Configuration.ConnectionString + "\r";
                });
            }
            catch (HttpRequestException)
            {
                StatusText = "Unable to connect to server: Start server before connecting clients.";
                IsConnected = false;
                return;
            }
            catch (Exception)
            {
                return;
            }
            
        }

        public void ToDisconnect()
        {
            Connection.Stop();
            StatusText = "You are Disconnect at " + Configuration.ConnectionString + "\r";
        }

    }
}