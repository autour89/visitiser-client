﻿using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Visi.Generic.DAL.DataModel;
using Visi.Generic.Models;
using Visi.Generic.Services;

namespace Visi.Generic.Interfaces
{
    public interface IProfileApi
    {
        [Post(ApiUrl.UpdateData)]
        Task<string> UpdateRequest([Body]VisitizerProfile request);
    }

    public interface IAccountByPhone
    {
        [Post(ApiUrl.FindAccntByPhone)]
        Task<string> SearchAccount(AccountInfo request);
    }

    public interface IErrorsLog
    {
        [Post(ApiUrl.WriteErrors)]
        Task<string> ErrorsRequest([Body]List<ErrorModel> request);
    }

    public interface IErrorLog
    {
        [Post(ApiUrl.WriteErrors)]
        Task<string> ErrorRequest([Body]ErrorModel request);
    }

    public interface IExceptionLog
    {
        [Post(ApiUrl.WriteException)]
        Task<string> ExceptionRequest([Body]ErrorModel request);
    }

}
