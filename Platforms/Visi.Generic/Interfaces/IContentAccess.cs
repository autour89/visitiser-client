﻿using System;
using System.Collections.Generic;

namespace Visi.Generic.Interfaces
{
    public interface IContentAccess<T>
    {
        List<I> SelectData<I>(string query) where I : class, new();
        List<I> SelectAllData<I>() where I : class, new();
        void InsertItem(T item);
        void UpdateItem(T item);
        void InsertList(List<T> items);
        void SaveItem(T item);
        void DeleteItems(T item);
        void DeleteAllItems();
        I SelectWithChildren<I>(int id) where I : class, new();
        List<I> SelectAllWithChildren<I>() where I : class, new();
        void UpdateItemWithChildren(T item);
        void InsertItemWithChildren(T item);
    }
}
