﻿using Microsoft.AspNet.SignalR.Client;

namespace Visi.Generic.Interfaces
{
    public interface IHubTon
    {
        HubConnection Connection { get; set; }
        IHubProxy HubProxy { get; set; }
        bool IsConnected { get; set; }
        string StatusText { get; set; }
        void CreateConnection();
        void ToConnect();
        void ToDisconnect();
    }
}