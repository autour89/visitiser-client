﻿using System;
using System.Collections.Generic;
using Visi.Generic.Models;

namespace Visi.Generic.Interfaces
{
    public interface ICalendarViewModel
    {
        List<DateRangeModel> DaysValues(List<DateTime> dates);
        List<DateTime> GluingMonths(DateTime current, List<DateTime> month);
    }
}