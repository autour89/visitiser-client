﻿using SQLite;

namespace Visi.Generic.Interfaces
{
    public interface ISQLite
    {
        string GetConnectionString();
        string GetCacheConnectionString();
        string GetCacheConnectionString(string key);
    }
}
