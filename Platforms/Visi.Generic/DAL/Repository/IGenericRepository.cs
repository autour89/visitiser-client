﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Visi.Generic.DAL.Repository
{
    public interface IGenericRepository<T>
    {
        Task InsertDataAsync(T data);
        Task InsertListDataAsync(List<T> datatList);
        Task UpdateDataAsync(T data);
        Task DeleteDataAsync(T data);
        Task<List<T>> SelectAllDataAsync();
        Task<List<T>> SelectDataAsync(string query);

        List<T> SelectAll();

        void InsertItem(T item);
        void InsertList(List<T> items);
        void UpdateItem(T item);
        void SaveItem(T item);
        void DeleteRow(T item);
        void DeleteAll();


    }
}
