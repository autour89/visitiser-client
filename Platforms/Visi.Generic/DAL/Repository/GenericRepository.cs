﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

using SQLite;
using SQLiteNetExtensions.Extensions;
using Visi.Generic.DAL.DataAccess;

namespace Visi.Generic.DAL.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : new()
    {
        private static object locker = new object();
        SQLiteAsyncConnection connectionAsync;
        SQLiteConnection connection;

        public GenericRepository(IDbConnection oIDbConnection)
        {
            connectionAsync = oIDbConnection.GetAsyncConnection;
            connection = oIDbConnection.GetConnection;

            connection.CreateTable<T>();
        }

        public async Task UpdateDataAsync(T data)
        {
            await connectionAsync.UpdateAsync(data);
        }
        public async Task DeleteDataAsync(T data)
        {
            await connectionAsync.DeleteAsync(data);
        }
        public async Task<List<T>> SelectDataAsync(string query)
        {
            return await connectionAsync.QueryAsync<T>(query);
        }
        public async Task<List<T>> SelectAllDataAsync()
        {
            return await connectionAsync.Table<T>().ToListAsync();
        }
        public async Task InsertDataAsync(T data)
        {
            await connectionAsync.InsertAsync(data);
        }
        public async Task InsertListDataAsync(List<T> datatList)
        {
            await connectionAsync.InsertAllAsync(datatList);
        }


        public List<T> SelectAll()
        {
            return connection.Table<T>().ToList();
        }

        public void InsertItem(T item)
        {
            connection.Insert(item);
        }

        public void InsertList(List<T> items)
        {
            connection.InsertAll(items);
        }

        public void UpdateItem(T item)
        {
            connection.Update(item);
        }

        public void SaveItem(T item)
        {
            lock (locker)
            {
                connection.Update(item);
            }
        }

        public void DeleteRow(T item)
        {
            connection.Delete(item);
        }

        public void DeleteAll()
        {
            connection.DeleteAll<T>();
        }

        public I SelectWithChildren<I>(int id) where I : class, new()
        {
            return connection.GetWithChildren<I>(id);
        }

        public List<I> SelectAllWithChildren<I>() where I : class, new()
        {
            return connection.GetAllWithChildren<I>();
        }

        public void UpdateItemWithChildren(T item)
        {
            connection.UpdateWithChildren(item);
        }

        public void InsertItemWithChildren(T item)
        {
            connection.InsertWithChildren(item);
        }

    }
}

