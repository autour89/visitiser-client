﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Visi.Generic.DAL.DataModel;
using Visi.Generic.Interfaces;

namespace Visi.Generic.DAL.Services
{
    public class ContentDB
    {
        private static IContentAccess<VisitizerProfile> account_data;
        private static IContentAccess<AccountContacts> account_cont;
        private static IContentAccess<AccountPhones> accnt_phns;
        private static IContentAccess<Conversation> user_conv;
        private static IContentAccess<ChatMessages> conv_msgs;
        private static IContentAccess<BusinessCard> bsns_cards;
        private static IContentAccess<Events> event_data;
        private static IContentAccess<EventMembers> event_member_data;

        private static ContentDB instance = null;

        public ContentDB()
        {
            //account_data = new ContentAccess<VisitizerProfile>();
            //accnt_phns = new ContentAccess<AccountPhones>();

            //account_data.InsertItem(new VisitizerProfile
            //{
            //    Id = 1,
            //    Name = "Alex",
            //    Gender = "n",
            //    Birth_DT = new DateTime()
            //});
            //accnt_phns.InsertItem(new AccountPhones
            //{
            //    AccountContactId = 1,
            //    PhoneNumber = "+380 3475 93458"
            //});

            //account_data.InsertItem(new VisitizerProfile
            //{
            //    Id = 2,
            //    Name = "Ivan"
            //});
            //accnt_phns.InsertItem(new AccountPhones
            //{
            //    AccountContactId = 2,
            //    PhoneNumber = "+380 6834 58769"
            //});

            UserAccount = Account_Data.SelectAllData<VisitizerProfile>().FirstOrDefault();
        }

        //public static ContentDB Instance
        //{
        //    get
        //    {
        //        if (instance == null)
        //            instance = new ContentDB();

        //        return instance;
        //    }
        //}
        public VisitizerProfile UserAccount { get; set; }

        public static IContentAccess<VisitizerProfile> Account_Data
        {
            get
            {
                if (account_data == null)
                    account_data = new ContentAccess<VisitizerProfile>();

                return account_data;
            }
        }

        public static IContentAccess<AccountContacts> Account_Cont
        {
            get
            {
                if (account_cont == null)
                    account_cont = new ContentAccess<AccountContacts>();

                return account_cont;
            }
        }
        public static IContentAccess<AccountPhones> Account_Phones
        {
            get
            {
                if (accnt_phns == null)
                    accnt_phns = new ContentAccess<AccountPhones>();

                return accnt_phns;
            }
        }

        public static IContentAccess<Conversation> User_Conv
        {
            get
            {
                if (user_conv == null)
                    user_conv = new ContentAccess<Conversation>();

                return user_conv;
            }
        }

        public static IContentAccess<ChatMessages> Conv_Msgs
        {
            get
            {
                if (conv_msgs == null)
                    conv_msgs = new ContentAccess<ChatMessages>();

                return conv_msgs;
            }
        }

        public static IContentAccess<BusinessCard> Bsns_Cards
        {
            get
            {
                if (bsns_cards == null)
                    bsns_cards = new ContentAccess<BusinessCard>();

                return bsns_cards;
            }
        }

        public static IContentAccess<Events> EventData
        {
            get
            {
                if (event_data == null)
                    event_data = new ContentAccess<Events>();

                return event_data;
            }
        }

        public static IContentAccess<EventMembers> EventMembersData
        {
            get
            {
                if (event_member_data == null)
                    event_member_data = new ContentAccess<EventMembers>();

                return event_member_data;
            }
        }



    }
}
