﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;
using SQLite;
using TinyIoC;
using Visi.Generic.DAL.DataModel;
using Visi.Generic.Interfaces;

namespace Visi.Generic.DAL.Services
{
    public static class LocalCache
    {
        private static readonly CacheConfiguration Configuration = CacheConfiguration.Instance("cache.sql3");
        private static SQLiteConnection Db => Configuration.Connection;

        public static void WriteToCache<T>(string key, T data, string api, DateTimeOffset timeStamp)
        {
            if (String.IsNullOrEmpty(key) || data == null || timeStamp == DateTimeOffset.MinValue) return;

            var currentValue = Db.Table<CachedObject>().Where(o => o.Key == key).ToList().FirstOrDefault();

            if (currentValue == null)
            {

                var item = new CachedObject
                {
                    Key = key,
                    UpdatedAt = timeStamp,
                    Value = JsonConvert.SerializeObject(data),
                    ApiUri = api
                };
                Db.Insert(item);
            }
            else
            {
                currentValue.Value = JsonConvert.SerializeObject(data);
                currentValue.UpdatedAt = timeStamp;
                currentValue.ApiUri = api;
                Db.Update(currentValue);
            }
        }

        public static DateTimeOffset CacheLastUpdated(string key)
        {
            if (String.IsNullOrEmpty(key)) return DateTimeOffset.MinValue;

            var currentValue = Db.Table<CachedObject>().Where(o => o.Key == key).ToList().FirstOrDefault();
            return currentValue?.UpdatedAt ?? DateTimeOffset.MinValue;
        }

        public static void RemoveCache(string key)
        {
            if (String.IsNullOrEmpty(key)) return;

            var currentValue = Db.Table<CachedObject>().Where(o => o.Key == key).ToList().FirstOrDefault();

            if (currentValue == null) return;

            Db.Delete(currentValue);
        }

        public static T GetFromCache<T>(string key)
        {
            if (String.IsNullOrEmpty(key)) return default(T);

            var currentValue = Db.Table<CachedObject>().Where(o => o.Key == key).ToList().FirstOrDefault();

            return currentValue?.Value == null ? default(T) : JsonConvert.DeserializeObject<T>(currentValue.Value);
        }

        public static List<T> GetAllFromCache<T>() where T : class, new()
        {
            return Db.Table<T>().ToList();
        }

        public static void ClearCache()
        {
            Db.DeleteAll<CachedObject>();
            //Db.DropTable<CachedObject>();
        }

    }


    public class CacheConfiguration
    {
        private SQLiteConnection connection;
        private static CacheConfiguration instance;
        public CacheConfiguration(string key)
        {
            CreateBinding(key);
            connection.CreateTable<CachedObject>();
        }

        public static CacheConfiguration Instance(string key)
        {
            if (instance == null)
                instance = new CacheConfiguration(key);

            return instance;
        }
        private void CreateBinding(string key)
        {
            if (connection == null)
            {
                var container = TinyIoCContainer.Current;
                container.AutoRegister(t => t.Name.Equals("ISQLite") || t.Name.Contains("SQLite_"));
                var dbPath = container.Resolve<ISQLite>().GetCacheConnectionString(key);
                connection = new SQLiteConnection(dbPath);
            }
        }

        public SQLiteConnection Connection => connection;

    }
}
