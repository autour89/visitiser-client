﻿using SQLite;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TinyIoC;
using Visi.Generic.Interfaces;

namespace Visi.Generic.DAL.Services
{
    public class ContentAccess<T> : IContentAccess<T>
    {
        private static object locker = new object();
        private SQLiteConnection db;
        private static ContentAccess<T> instance;

        public ContentAccess()
        {
            CreateBinding();

            // create the tables
            db.CreateTable<T>();

            //db.DropTable<VisitizerProfile>();
            //db.DropTable<AccountPhones>();
            //db.DropTable<EventMembers>();
            //db.DeleteAll<BusinessCard>();
            //db.DeleteAll<Conversation>();
            //db.DeleteAll<ChatMessages>();
            //db.CreateTable<VisitizerProfile>();
            //db.CreateTable<AccountPhones>();

        }

        public static ContentAccess<T> Instance
        {
            get
            {
                if (instance == null)
                    instance = new ContentAccess<T>();
                return instance;
            }
        }

        private void CreateBinding()
        {
            if (db == null)
            {
                var container = TinyIoCContainer.Current;
                container.AutoRegister(t => t.Name.Equals("ISQLite") || t.Name.Contains("SQLite_"));
                var dbPath = container.Resolve<ISQLite>().GetConnectionString();
                db = new SQLiteConnection(dbPath);
            }
        }

        public List<I> SelectData<I>(string query) where I : class, new()
        {
            return db.Query<I>(query).ToList();
        }

        public List<I> SelectAllData<I>() where I : class, new()
        {
            return db.Table<I>().ToList();
        }

        public void InsertItem(T item)
        {
            db.Insert(item);
        }

        public void UpdateItem(T item)
        {
            db.Update(item);
        }

        public void InsertList(List<T> items)
        {
            db.InsertAll(items);
        }
        public void SaveItem(T item)
        {
            lock (locker)
            {
                db.Update(item);
            }
        }

        public void DeleteItems(T item)
        {
            db.Delete(item);
        }

        public void DeleteAllItems()
        {
            db.DeleteAll<T>();
        }

        public I SelectWithChildren<I>(int id) where I : class, new()
        {
            return db.GetWithChildren<I>(id);
        }

        public List<I> SelectAllWithChildren<I>() where I : class, new()
        {
            return db.GetAllWithChildren<I>();
        }

        public void UpdateItemWithChildren(T item)
        {
            db.UpdateWithChildren(item);
        }

        public void InsertItemWithChildren(T item)
        {
            db.InsertWithChildren(item);
        }

    }
}
