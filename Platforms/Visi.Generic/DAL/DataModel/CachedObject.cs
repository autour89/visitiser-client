﻿using System;
using SQLite;

namespace Visi.Generic.DAL.DataModel
{
    [Table("CachedObject")]
    public class CachedObject
    {
        [PrimaryKey]
        public string Key { get; set; }
        public string Value { get; set; }
        public string ApiUri { get; set; }
        public DateTimeOffset UpdatedAt { get; set; }
    }
}
