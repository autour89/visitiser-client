﻿using System;
using System.Collections.Generic;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace Visi.Generic.DAL.DataModel
{

    [Table("VisitizerProfile")]
    public class VisitizerProfile
    {
        [PrimaryKey, NotNull, Indexed]
        public int Id { get; set; }
        [MaxLength(30)]
        public string Name { get; set; }
        public string Gender { get; set; }
        public DateTime Birth_DT { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public byte[] Logo { get; set; }
        public string Skype { get; set; }
        public string Facebook { get; set; }
        public string Site { get; set; }
        [OneToMany]
        public List<AccountContacts> Contacts { get; set; }
        [OneToMany]
        public List<AccountPhones> PhoneNumbers { get; set; }
        [OneToMany]
        public List<BusinessCard> BusinessCards { get; set; }
    }

    [Table("AccountContacts")]
    public class AccountContacts
    {
        [PrimaryKey, AutoIncrement, NotNull, Indexed]
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string ContactStatus { get; set; }
        [ForeignKey(typeof(VisitizerProfile))]
        public int AccountContactId { get; set; }
        public DateTime Flag { get; set; }
        public override string ToString()
        {
            return Name + " " + PhoneNumber;
        }
    }

    [Table("AccountPhones")]
    public class AccountPhones
    {
        [PrimaryKey, AutoIncrement, NotNull, Indexed]
        public int Id { get; set; }
        [ForeignKey(typeof(VisitizerProfile))]
        public int AccountContactId { get; set; }
        [NotNull]
        public string CountryCode { get; set; }
        [NotNull]
        public string PhoneNumber { get; set; }
    }

    [Table("Conversation")]
    public class Conversation
    {
        [PrimaryKey, NotNull, Indexed]
        public int ConversationId { get; set; }
        public string ConversationName { get; set; }
        public DateTime Flag { get; set; }
        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<ChatMessages> Messages { get; set; }
    }

    [Table("ChatMessages")]
    public class ChatMessages
    {
        [PrimaryKey, NotNull, Indexed]
        public int MessageId { get; set; }
        [ForeignKey(typeof(Conversation)), NotNull]
        public int ConversationId { get; set; }
        [NotNull]
        public int SenderId { get; set; }
        public string SenderName { get; set; }
        public string Message { get; set; }
        public string MessageType { get; set; }
        public DateTime MessageDT { get; set; }
        [ManyToOne]
        public Conversation Conversation { get; set; }
        public DateTime Flag { get; set; }
        public override string ToString()
        {
            return MessageDT + " " + SenderName + " " + Message;
        }
    }

    [Table("BusinessCard")]
    public class BusinessCard
    {
        [PrimaryKey, AutoIncrement, NotNull, Indexed]
        public int ID { get; set; }
        public int Business_Card_ID { get; set; }
        [ForeignKey(typeof(VisitizerProfile))]
        public int Account_ID { get; set; }
        public string Card_Type { get; set; }
        public bool IsOffice { get; set; }
        public bool IsOnSite { get; set; }
        public string Name { get; set; }
        public string Descriprion { get; set; }
        public string Addresses { get; set; }
        public string Skype { get; set; }
        public string Facebook { get; set; }
        public string Emails { get; set; }
        public string Phones { get; set; }
        public string Sites { get; set; }
        public string Services { get; set; }
        public byte[] Logo { get; set; }
        public DateTime Created_DT { get; set; }
        public override string ToString()
        {
            return Name + " " + Descriprion;
        }
    }

    [Table("Events")]
    public class Events
    {
        [PrimaryKey, AutoIncrement, NotNull, Indexed]
        public int EventId { get; set; }
        public int ServerEventId { get; set; }
        [NotNull]
        public DateTime Created_DT { get; set; }
        [NotNull]
        public DateTime Start_DT { get; set; }
        [NotNull]
        public DateTime End_DT { get; set; }
        [NotNull]
        public TimeSpan Duration { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int CreatorId { get; set; }
        public int OwnerId { get; set; }
        public string Address { get; set; }
        public string EventStatus { get; set; }
        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<EventMembers> EventMembers { get; set; }
        public Double ToDuration()
        {
            return Convert.ToDouble(Duration.Hours + "," + Duration.Minutes);
        }
        public Double ToTime()
        {
            return Convert.ToDouble(Start_DT.Hour + "," + Start_DT.Minute);
        }
    }

    [Table("EventMembers")]
    public class EventMembers
    {
        [PrimaryKey, AutoIncrement, NotNull, Indexed]
        public int Id { get; set; }
        [ForeignKey(typeof(Events))]
        public int EventId { get; set; }
        public int AccountContactId { get; set; }
        public string MemberInfo { get; set; }
        [ManyToOne]
        public Events Events { get; set; }
    }

    [Table("SyncWithServer")]
    public class SyncWithServer
    {
        [PrimaryKey, AutoIncrement, NotNull]
        public int Id { get; set; }
        public string ProcName { get; set; }
        public int RowId { get; set; }
        public string DescriptionProcess { get; set; }
        public string ApiUrl { get; set; }
        public string Data { get; set; }
        public DateTime Label { get; set; }
    }

}
