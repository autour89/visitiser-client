﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Visi.Generic.DAL.DataAccess
{
    public class DbConnection : IDbConnection
    {
        SQLiteAsyncConnection connAsync;
        SQLiteConnection conn;

        string sqliteFilename = "TodoListDB.db3";
        //string dbPath;

        private static IDbConnection instance;
        public DbConnection(string dbPath)
        {
            //dbPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);

            connAsync = new SQLiteAsyncConnection(dbPath);
            conn = new SQLiteConnection(dbPath);

        }

        public static IDbConnection Instance(string dbPath)
        {
            if (instance == null)
                instance = new DbConnection(dbPath);

            return instance;
        }
        public SQLiteAsyncConnection GetAsyncConnection => connAsync;
        public SQLiteConnection GetConnection => conn;

        public bool IsExistDB => true;

    }
}