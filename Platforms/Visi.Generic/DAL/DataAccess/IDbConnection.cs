﻿using SQLite;

namespace Visi.Generic.DAL.DataAccess
{
    public interface IDbConnection
    {
        SQLiteAsyncConnection GetAsyncConnection { get; }
        SQLiteConnection GetConnection { get; }
        bool IsExistDB { get; }
    }
}
