﻿CREATE TABLE [articles].[Label] (
    [Label_ID]   INT             IDENTITY (1, 1) NOT NULL,
    [Label_CD]   VARCHAR (70)    NOT NULL,
    [Label_DS]   NVARCHAR (100)  NULL,
    [Label_Logo] VARBINARY (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Label_ID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Labels_CD]
    ON [articles].[Label]([Label_CD] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Используется для надписей на разных языках', @level0type = N'SCHEMA', @level0name = N'articles', @level1type = N'TABLE', @level1name = N'Label';

