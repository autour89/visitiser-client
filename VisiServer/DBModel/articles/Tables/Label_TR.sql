﻿CREATE TABLE [articles].[Label_TR] (
    [Label_ID] INT             NOT NULL,
    [Lang_CD]  CHAR (2)        NOT NULL,
    [Label_TR] NVARCHAR (4000) NOT NULL,
    CONSTRAINT [PK_Label_TR] PRIMARY KEY CLUSTERED ([Label_ID] ASC, [Lang_CD] ASC),
    CONSTRAINT [FK_Labels_Lang_Labels] FOREIGN KEY ([Label_ID]) REFERENCES [articles].[Label] ([Label_ID]),
    CONSTRAINT [FK_Labels_Lang_Lang] FOREIGN KEY ([Lang_CD]) REFERENCES [dbo].[Language] ([Lang_CD])
);

