﻿CREATE Procedure [articles].[GetLabel]
(
	@Label_CD varchar(70),
	@Lang_CD char(2),
	@Label nvarchar(500) OUTPUT
)

AS
BEGIN
	DECLARE 
		@Label_ID int;
	DECLARE
		@Inserted_Var TABLE (Label_ID int);
	
	SELECT @Label_ID = Label_ID FROM articles.Label WHERE Label_CD = @Label_CD ;

	IF @Label_ID IS NULL  -- Create new Label	
		BEGIN	
			INSERT INTO articles.Label (Label_CD) 
			OUTPUT inserted.Label_ID INTO @Inserted_Var
			VALUES(@Label_CD);
			
			SELECT 	@Label_ID = Label_ID FROM @Inserted_Var;

			INSERT INTO articles.Label_TR (Label_ID, Lang_CD, Label_TR) VALUES (@Label_ID, @Lang_CD, @Label_CD); 
			SET @Label = @Label_CD;
		END
	ELSE 
		SELECT @Label = Label_TR FROM articles.Label_TR 
		WHERE Label_ID = @Label_ID AND Lang_CD = @Lang_CD;			

	IF @Label IS NULL SET @Label = @Label_CD;

	
END