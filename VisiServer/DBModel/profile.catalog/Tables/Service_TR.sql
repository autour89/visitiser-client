﻿CREATE TABLE [profile.catalog].[Service_TR] (
    [Service_ID] INT                     NOT NULL,
    [Lancg_CD]   CHAR (2)                NOT NULL,
    [Service_TR] [dbo].[NameTranslation] NOT NULL,
    CONSTRAINT [PK_Service_TR] PRIMARY KEY CLUSTERED ([Service_ID] ASC, [Lancg_CD] ASC),
    CONSTRAINT [FK_Service_TR_Language] FOREIGN KEY ([Lancg_CD]) REFERENCES [dbo].[Language] ([Lang_CD]),
    CONSTRAINT [FK_Service_TR_Service] FOREIGN KEY ([Service_ID]) REFERENCES [profile.catalog].[Service] ([Service_ID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Классификатор услуг. Перевод', @level0type = N'SCHEMA', @level0name = N'profile.catalog', @level1type = N'TABLE', @level1name = N'Service_TR';

