﻿CREATE TABLE [profile.catalog].[ServiceSubCategory] (
    [ServiceSubCategory_ID] INT            IDENTITY (1, 1) NOT NULL,
    [ServiceCategory_ID]    INT            NOT NULL,
    [ServiceSubCategory_NM] NVARCHAR (250) NOT NULL,
    [ServiceSubCategory_DT] DATETIME       CONSTRAINT [DF_SubDomain_Created_DT] DEFAULT (getdate()) NOT NULL,
    [IS_Custom]             BIT            NULL,
    CONSTRAINT [PK_ServiceSubCategory] PRIMARY KEY CLUSTERED ([ServiceSubCategory_ID] ASC),
    CONSTRAINT [FK_ServiceSubCategory_ServiceCategory] FOREIGN KEY ([ServiceCategory_ID]) REFERENCES [profile.catalog].[ServiceCategory] ([ServiceCategory_ID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Создана вручную пользователем', @level0type = N'SCHEMA', @level0name = N'profile.catalog', @level1type = N'TABLE', @level1name = N'ServiceSubCategory';

