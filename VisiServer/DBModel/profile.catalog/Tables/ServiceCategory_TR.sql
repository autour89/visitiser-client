﻿CREATE TABLE [profile.catalog].[ServiceCategory_TR] (
    [ServiceCategory_ID]  INT                     NOT NULL,
    [Lang_CD]             CHAR (2)                NOT NULL,
    [ServiceCategory_TR]  [dbo].[NameTranslation] NOT NULL,
    [ServiceCategory_SEO] NCHAR (10)              NULL,
    CONSTRAINT [PK_ServiceCategory_TR] PRIMARY KEY CLUSTERED ([ServiceCategory_ID] ASC, [Lang_CD] ASC),
    CONSTRAINT [FK_ServiceCategory_TR_Language] FOREIGN KEY ([Lang_CD]) REFERENCES [dbo].[Language] ([Lang_CD]),
    CONSTRAINT [FK_ServiceCategory_TR_ServiceCategory] FOREIGN KEY ([ServiceCategory_ID]) REFERENCES [profile.catalog].[ServiceCategory] ([ServiceCategory_ID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SEO Текст для описания услуги', @level0type = N'SCHEMA', @level0name = N'profile.catalog', @level1type = N'TABLE', @level1name = N'ServiceCategory_TR', @level2type = N'COLUMN', @level2name = N'ServiceCategory_SEO';

