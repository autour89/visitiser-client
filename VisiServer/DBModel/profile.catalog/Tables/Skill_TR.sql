﻿CREATE TABLE [profile.catalog].[Skill_TR] (
    [Skill_ID] INT            NOT NULL,
    [Lang_CD]  CHAR (2)       NOT NULL,
    [Skill_TR] NVARCHAR (250) NOT NULL,
    CONSTRAINT [PK_Skill_TR] PRIMARY KEY CLUSTERED ([Skill_ID] ASC, [Lang_CD] ASC),
    CONSTRAINT [FK_Skill_TR_Language] FOREIGN KEY ([Lang_CD]) REFERENCES [dbo].[Language] ([Lang_CD]),
    CONSTRAINT [FK_Skill_TR_Skill] FOREIGN KEY ([Skill_ID]) REFERENCES [profile.catalog].[Skill] ([Skill_ID])
);

