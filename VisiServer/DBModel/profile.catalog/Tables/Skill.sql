﻿CREATE TABLE [profile.catalog].[Skill] (
    [Skill_ID]  INT            NOT NULL,
    [Skill_NM]  NVARCHAR (250) NOT NULL,
    [Skill_URL] VARCHAR (250)  NULL,
    CONSTRAINT [PK_Skill_1] PRIMARY KEY CLUSTERED ([Skill_ID] ASC)
);

