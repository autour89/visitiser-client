﻿CREATE TABLE [profile.catalog].[Service] (
    [Service_ID]            INT            IDENTITY (1, 1) NOT NULL,
    [ServiceSubCategory_ID] INT            NOT NULL,
    [Service_URL]           [dbo].[URL]    NULL,
    [Service_NM]            NVARCHAR (250) NOT NULL,
    CONSTRAINT [PK_Service] PRIMARY KEY CLUSTERED ([Service_ID] ASC),
    CONSTRAINT [FK_Service_ServiceSubCategory] FOREIGN KEY ([ServiceSubCategory_ID]) REFERENCES [profile.catalog].[ServiceSubCategory] ([ServiceSubCategory_ID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Классификатор услуг', @level0type = N'SCHEMA', @level0name = N'profile.catalog', @level1type = N'TABLE', @level1name = N'Service';

