﻿CREATE TABLE [profile.catalog].[ServiceCategory] (
    [ServiceCategory_ID]   INT             IDENTITY (1, 1) NOT NULL,
    [ServiceCategory_URL]  VARCHAR (250)   NULL,
    [ServiceCategory_NM]   NVARCHAR (250)  NOT NULL,
    [ServiceCategory_Logo] VARBINARY (MAX) NULL,
    CONSTRAINT [PK_ServiceCategory] PRIMARY KEY CLUSTERED ([ServiceCategory_ID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Используется для отображения в URL', @level0type = N'SCHEMA', @level0name = N'profile.catalog', @level1type = N'TABLE', @level1name = N'ServiceCategory', @level2type = N'COLUMN', @level2name = N'ServiceCategory_URL';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Виды деятельсности', @level0type = N'SCHEMA', @level0name = N'profile.catalog', @level1type = N'TABLE', @level1name = N'ServiceCategory';


GO
CREATE UNIQUE NONCLUSTERED INDEX [IDX-ServiceCategory-URL]
    ON [profile.catalog].[ServiceCategory]([ServiceCategory_URL] ASC)
    INCLUDE([ServiceCategory_ID], [ServiceCategory_NM]) WHERE ([ServiceCategory_URL] IS NOT NULL);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IDX-ServiceCategory_NM]
    ON [profile.catalog].[ServiceCategory]([ServiceCategory_NM] ASC)
    INCLUDE([ServiceCategory_ID]);

