﻿CREATE TABLE [profile.catalog].[ServiceSubCategory_TR] (
    [ServiceSubCategory_ID]  INT                     NOT NULL,
    [Lang_CD]                CHAR (2)                NOT NULL,
    [ServiceSubCategory_TR]  [dbo].[NameTranslation] NOT NULL,
    [ServiceSubCategory_SEO] NVARCHAR (4000)         NULL,
    CONSTRAINT [PK_ServiceSubCategory_TR] PRIMARY KEY CLUSTERED ([ServiceSubCategory_ID] ASC, [Lang_CD] ASC),
    CONSTRAINT [FK_ServiceSubCategory_TR_Language] FOREIGN KEY ([Lang_CD]) REFERENCES [dbo].[Language] ([Lang_CD]),
    CONSTRAINT [FK_ServiceSubCategory_TR_ServiceSubCategory] FOREIGN KEY ([ServiceSubCategory_ID]) REFERENCES [profile.catalog].[ServiceSubCategory] ([ServiceSubCategory_ID])
);

