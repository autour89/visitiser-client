﻿CREATE TABLE [crm].[Account_Contact] (
    [Account_Contact_ID] INT IDENTITY (1, 1) NOT NULL,
    [Account_ID]         INT NOT NULL,
    [Contact_ID]         INT NOT NULL,
    CONSTRAINT [PK_Account_Contact] PRIMARY KEY CLUSTERED ([Account_Contact_ID] ASC),
    CONSTRAINT [FK_Account_Contacts_ToAccount] FOREIGN KEY ([Account_ID]) REFERENCES [profile].[Account] ([Account_ID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Список контактов эккаунта', @level0type = N'SCHEMA', @level0name = N'crm', @level1type = N'TABLE', @level1name = N'Account_Contact';

