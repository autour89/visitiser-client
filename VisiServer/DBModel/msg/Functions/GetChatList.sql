﻿-- =============================================
-- Author:		Alex Shastun
-- Description:	select id of chat list where both accounts are present
-- =============================================
CREATE FUNCTION [msg].[GetChatList] 
(
	@account1 int, 
	@account2 int
)
RETURNS 
@Table_ChatList TABLE 
(
	[Chat_List_ID] int
)
AS
BEGIN
	insert @Table_ChatList
	select [Chat_List_ID] 
	from msg.Chat_Members  
	where Account_ID in(@account1,@account2) 
	group by [Chat_List_ID]
	having Count(*)=2
	RETURN 
END