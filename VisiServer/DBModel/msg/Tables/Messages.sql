﻿CREATE TABLE [msg].[Messages] (
    [Message_ID]      INT            IDENTITY (1, 1) NOT NULL,
    [Chat_List_ID]    INT            NOT NULL,
    [Sender_ID]       INT            NOT NULL,
    [Message]         NVARCHAR (MAX) NOT NULL,
    [Message_Type]    VARCHAR (10)   NOT NULL,
    [Message_DT]      DATETIME       DEFAULT (getdate()) NOT NULL,
    [Delivery_Status] CHAR (1)       NOT NULL,
    CONSTRAINT [PK_msg_Messages] PRIMARY KEY CLUSTERED ([Message_ID] ASC),
    CONSTRAINT [FK_Messages_Account] FOREIGN KEY ([Sender_ID]) REFERENCES [profile].[Account] ([Account_ID]),
    CONSTRAINT [FK_Messages_Chat_List] FOREIGN KEY ([Chat_List_ID]) REFERENCES [msg].[Chat_List] ([Chat_List_ID])
);


GO
CREATE NONCLUSTERED INDEX [idx-Messages-Chhat_List_ID]
    ON [msg].[Messages]([Chat_List_ID] ASC, [Message_DT] ASC)
    INCLUDE([Message_ID]);

