﻿CREATE TABLE [msg].[Delivery_Message] (
    [Message_ID]      INT      NOT NULL,
    [Device_ID]       INT      NOT NULL,
    [Delivery_Status] CHAR (1) NOT NULL,
    CONSTRAINT [PK_Delivery_Messages] PRIMARY KEY CLUSTERED ([Message_ID] ASC, [Device_ID] ASC),
    CONSTRAINT [FK_Delivery_Messages_Account_Devices] FOREIGN KEY ([Device_ID]) REFERENCES [profile].[Account_Device] ([Account_Device_ID]),
    CONSTRAINT [FK_Delivery_Messages_Messages] FOREIGN KEY ([Message_ID]) REFERENCES [msg].[Messages] ([Message_ID])
);

