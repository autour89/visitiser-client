﻿CREATE TABLE [msg].[Chat_List] (
    [Chat_List_ID] INT            IDENTITY (1, 1) NOT NULL,
    [Chat_List_NM] NVARCHAR (150) NULL,
    [Created_DT]   DATETIME       CONSTRAINT [DF_Chat_List_Created_DT] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Chat_List] PRIMARY KEY CLUSTERED ([Chat_List_ID] ASC)
);

