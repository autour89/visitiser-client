﻿CREATE TABLE [msg].[Chat_Member] (
    [Chat_List_ID] INT      NOT NULL,
    [Account_ID]   INT      NOT NULL,
    [Role_Type]    CHAR (1) CONSTRAINT [DF_Chat_Members_Role_Type] DEFAULT ('a') NULL,
    CONSTRAINT [PK_Chat_Members] PRIMARY KEY CLUSTERED ([Chat_List_ID] ASC, [Account_ID] ASC),
    CONSTRAINT [FK_Chat_Members_Account] FOREIGN KEY ([Account_ID]) REFERENCES [profile].[Account] ([Account_ID]),
    CONSTRAINT [FK_Chat_Members_Chat_List] FOREIGN KEY ([Chat_List_ID]) REFERENCES [msg].[Chat_List] ([Chat_List_ID])
);

