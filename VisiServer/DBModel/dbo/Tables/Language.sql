﻿CREATE TABLE [dbo].[Language] (
    [Lang_CD]   CHAR (2)        NOT NULL,
    [Lang_NM]   NVARCHAR (50)   NOT NULL,
    [Lang_Logo] VARBINARY (MAX) NULL,
    CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED ([Lang_CD] ASC)
);



