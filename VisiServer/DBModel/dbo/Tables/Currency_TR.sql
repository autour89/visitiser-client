﻿CREATE TABLE [dbo].[Currency_TR] (
    [Currency_ID] INT                     NOT NULL,
    [Lang_CD]     CHAR (2)                NOT NULL,
    [Currency_TR] [dbo].[NameTranslation] NOT NULL,
    CONSTRAINT [PK_Currency_TR] PRIMARY KEY CLUSTERED ([Currency_ID] ASC, [Lang_CD] ASC),
    CONSTRAINT [FK_Currency_LG_Curency] FOREIGN KEY ([Currency_ID]) REFERENCES [dbo].[Currency] ([Currency_ID]),
    CONSTRAINT [FK_Currency_LG_Language] FOREIGN KEY ([Lang_CD]) REFERENCES [dbo].[Language] ([Lang_CD])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Курсы валют. перевод', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Currency_TR';

