﻿CREATE TABLE [dbo].[Currency] (
    [Currency_ID]     INT          NOT NULL,
    [Currency_CD]     CHAR (3)     NOT NULL,
    [Currency_Symbol] NVARCHAR (1) NOT NULL,
    CONSTRAINT [PK_Curency] PRIMARY KEY CLUSTERED ([Currency_ID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Список курсов валют', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Currency';

