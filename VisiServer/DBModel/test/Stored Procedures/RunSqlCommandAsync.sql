﻿CREATE PROCEDURE [test].[RunSqlCommandAsync]
(
	 @sql      VARCHAR(4000)
	,@jobname  VARCHAR(200)  = NULL
	,@database VARCHAR(200)  = NULL
	,@owner    VARCHAR(200)  = NULL
)
AS
	BEGIN
	    SET NOCOUNT ON;

	    DECLARE @id UNIQUEIDENTIFIER  
	    --Create unique job name if the name is not specified  
	    IF @jobname IS NULL
	    BEGIN
		   SET @jobname = 'async'
	    END
	    SET @jobname = @jobname+'_'+CONVERT(VARCHAR(64),NEWID())

	    IF @owner IS NULL
	    BEGIN
		   SET @owner = 'sa'
	    END  
  
	    --Create a new job, get job ID  
	    EXECUTE [msdb].dbo.[sp_add_job]
			  @jobname
			 ,@owner_login_name = @owner
			 ,@job_ID = @id OUTPUT  
  
	    --Specify a job server for the job  
	    EXECUTE [msdb].dbo.[sp_add_jobserver]
			  @job_ID = @id  
  
	    --Specify a first step of the job - the SQL command  --(@on_success_action = 3 ... Go to next step)  
	    EXECUTE [msdb].dbo.[sp_add_jobstep]
			  @job_ID = @id
			 ,@step_name = 'Step1'
			 ,@command = @sql
			 ,@database_name = @database
			 ,@on_success_action = 3   
  
	    --Specify next step of the job - delete the job  
	    DECLARE @deletecommand VARCHAR(200)
	    SET @deletecommand = 'execute msdb..sp_delete_job @job_name='''+@jobname+''''
	    EXECUTE [msdb].dbo.[sp_add_jobstep]
			  @job_ID = @id
			 ,@step_name = 'Step2'
			 ,@command = @deletecommand  
  
	    --Start the job  
	    EXECUTE [msdb].dbo.[sp_start_job]
			  @job_ID = @id
	END