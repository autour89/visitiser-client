﻿CREATE PROCEDURE [test].[RunAsync_Test]
	@database nvarchar(30),
	@WorkerTestName nvarchar(20),
	@NumberOfThreads int = 20, 	
	@sql nvarchar(MAX)	

AS

DECLARE	
		@Iterator int = 1;

DECLARE @WorkerName nvarchar(20);

WHILE @Iterator <= @NumberOfThreads BEGIN

	SET @WorkerName = @WorkerTestName+'_'+Convert(varchar(2), @Iterator);
	
	EXEC	[test].[RunSqlCommandAsync]
			@sql = @sql	,		@database = @database

	SET @Iterator += 1;
END


RETURN 0