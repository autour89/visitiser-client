﻿
CREATE PROCEDURE [profile].[GetAccountByPhone]
		@AccountPhone nvarchar(60)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT top 20
		p.Account_ID, p.Account_NM, p.Account_Logo, a.Number 
	FROM 
		profile.Account p JOIN profile.Account_Phones a ON p.Account_ID = a.Account_ID  
	WHERE 
		a.Number  LIKE '%'+@AccountPhone+'%';
END

--set @Phone =  REPLACE(REPLACE(REPLACE(REPLACE(a.Number,' ',''),'(',''),')',''),'+','');

-- =============================================
-- Author:		Shastun Alexandr
-- Create date: 27.07.2017
-- Description:	Get account profile by phone number
-- =============================================