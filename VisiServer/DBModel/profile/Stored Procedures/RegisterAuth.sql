﻿CREATE PROCEDURE [profile].[RegisterAuth]
  (
    @list profile.IdentityList READONLY
  )
AS
  BEGIN
    DECLARE @ID INT
    SELECT @ID = [Account_ID]
    FROM profile.Account_Social as p
      join @list as i
        on i.Identity_NM = p.Provider_NM and i.Identity_ID = p.Provider_User_ID
    if @ID is null
      BEGIN
        DECLARE @t TABLE(ID INT)
        INSERT INTO Account (Account_NM)
        OUTPUT INSERTED.Account_ID INTO @t VALUES ('<new>')
        declare @Account_ID INT
        SELECT @Account_ID = ID
        FROM @t
        INSERT INTO Account_Social (Account_ID, Provider_NM, Provider_User_ID)
          SELECT
            @Account_ID,
            Identity_NM,
            Identity_ID
          FROM @list
        select @Account_ID
      end
    else
      begin
        insert into profile.Account_Social (Account_ID, Provider_NM, Provider_User_ID)
          select
            @ID,
            i.Identity_NM,
            i.Identity_ID
          from @list as i left join profile.Account_Social as p
              on i.Identity_NM = p.Provider_NM and i.Identity_ID = p.Provider_User_ID and p.Account_ID = @ID
          where Account_ID is null
        select @ID
        select top 1
          Account_NM,
          Account_Email,
          Phone.AccountPhoneNumber
        from Account
          left join Account_Phone Phone on Account.Account_ID = Phone.Account_ID
        where Account.Account_ID = @ID
      end
  END
  ;