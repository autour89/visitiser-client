﻿
-- =============================================
-- Author:		Michael Kashchenko
-- Create date: 05/2018
-- Description:	Получить короткий список работ мастера 
-- USAGE : 
--   EXEC [profile].[GetBusinessCardPortfolio__MainScreen] @BusinessCard_ID = 1, @Lang_CD = 'ru', @ItemsCount = 3
-- =============================================

CREATE PROCEDURE [profile].[GetBusinessCardPortfolio__MainScreen](
	@BusinessCard_ID int,
	@Lang_CD char(2),
	@ItemsCount smallint
)

AS
BEGIN

	SELECT TOP (@ItemsCount) 
	   [Portfolio_ID]      
      ,[Portfolio_NM]      
      ,[Portfolio_Logo]      
      ,[Portfolio_DT]
  FROM [profile].[BusinessCard_Portfolio]
  WHERE BusinessCard_ID = @BusinessCard_ID


END