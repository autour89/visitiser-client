﻿/*
USAGE:
	EXEC [profile].[GetBusinessCardServiceLanguage_MainScreen] @BusinessCard_ID = 1, @Lang_CD = 'ru'
*/

CREATE PROCEDURE [profile].[GetBusinessCardServiceLanguage_MainScreen]
	@BusinessCard_ID int,
	@Lang_CD char(2) 
AS BEGIN

  SELECT 
       lg.Lang_NM
  FROM 
		[profile].[BusinessCard_ServiceLanguage] sl
  INNER JOIN 
		[dbo].[Language] lg ON sl.Lang_CD = lg.Lang_CD
  WHERE [BusinessCard_ID] = @BusinessCard_ID
END