﻿/*
Назначение: Получение списка адресов - для экрана - полная визитка

USAGE:
	EXEC [profile].[GetBusinessCardAdresses_MainScreen] @BusinessCard_ID = 1, @Lang_CD = 'ru'
*/
CREATE PROCEDURE [profile].[GetBusinessCardAdresses_MainScreen]
	@BusinessCard_ID int,
	@Lang_CD char(2) 
AS
BEGIN
	SELECT
		 [BusinessCardAdress_ID]
		,[BusinessCardAdress_Type]
		,[BusinessCardAdress_City]
		,[BusinessCardAdress_Adress]
		,[BusinessCardAdress_Geo]
		,[BusinessCardAdress_Radius]
		,[BusinessCardAdress_DS]
	FROM 
		[profile].[BusinessCard_Adress]
	WHERE 
		[BusinessCard_ID] = @BusinessCard_ID
		
END