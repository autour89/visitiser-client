﻿-- USAGE 
-- EXEC [profile].[GetBusinessCardPhones_MainScreen]  @BusinessCard_ID = 1090
CREATE PROCEDURE [profile].[GetBusinessCardPhones_MainScreen]
	@BusinessCard_ID int
AS BEGIN
	SELECT 	
		[Country_Code],
		[AccountPhoneNumber]
	FROM 
		[profile].[BusinessCard_Phone] bph
	INNER JOIN
		[profile].[Account_Phone] accph ON bph.AccountPhone_ID = accph.AccountPhone_ID
	WHERE [BusinessCard_ID] = @BusinessCard_ID
END