﻿CREATE PROCEDURE [profile].[Update]
  (
    @id    int,
    @name  varchar(255),
    @email varchar(255),
    @phone varchar(255)
  )
AS
  BEGIN
    update Account
    set Account_NM = @name, Account_Email = @email
    where Account_ID = @id
    if @phone is not null
      insert into Account_Phones (Account_ID, Number) values (@id, @phone)
    select @id
    select
      @name,
      @email,
      @phone
  END
  ;