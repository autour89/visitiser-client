﻿/*
USAGE 
	EXEC [profile].[UpdateBusinessCardRating] @ReviewedBusinessCard_ID = 1
*/
CREATE PROCEDURE [profile].[UpdateBusinessCardRating] (

@ReviewedBusinessCard_ID int 
-- мой рейтинг влияет на твой без влияния твоего на мой и наоборот
-- сначала считаем чистый рейтинг без влияния рекомендующих
-- так делаем для рекурсивных рекомендаций
-- т.е. убираем из моего расчета твой рейтинг, и вычисляем твой рейтинг, умножая на мой очищенный
-- или всегда умножаем на очищенный рейтинг 
) 
AS BEGIN
WITH tbl_Calc AS (

	SELECT 
		[BusinessCard_ID] = @ReviewedBusinessCard_ID,
		card.Account_ID,
		SUM([Review_Recommendation]) as NumberOfRecomendations,
		COUNT(Review_Mark) as NumberOfReviews,
		CONVERT(decimal(2,1), AVG(CONVERT(decimal(2,1), [Review_Mark]))) as AverageMark,

		SUM(

		20/(convert(decimal(10, 3),  (datediff(month, review_DT, getdate()))+5)+0.2) 
		* IIF(Review_Mark = 3, -0.5, Review_Mark - 3) 
		* iif( Review_Recommendation = 0, 1, 2))  as PoorRating,

		SUM(

		20/(convert(decimal(10, 3),  (datediff(month, review_DT, getdate()))+5)+0.2) 
		* IIF(Review_Mark = 3, -0.5, Review_Mark - 3) 
		+ iif( Review_Recommendation = 0, 1, 2)*ISNULL(bc.PoorRating/100, 1))  as Rating,
	
		getdate() AS [Updating_DT]

	FROM 
		[profile].[Review] rv
	LEFT JOIN 
		[profile].[BusinessCard_Rating] bc ON rv.ReviewedBusinessCard_ID = bc.[BusinessCard_ID]
	LEFT JOIN 
		profile.BusinessCard card ON rv.ReviewedBusinessCard_ID = card.BusinessCard_ID
	WHERE 
		rv.ReviewedBusinessCard_ID = @ReviewedBusinessCard_ID
	GROUP BY card.Account_ID
)

MERGE [profile].[BusinessCard_Rating]  as target
USING 
	tbl_Calc as source 
ON target.[BusinessCard_ID] = source.[BusinessCard_ID]
WHEN MATCHED THEN UPDATE SET
		target.[NumberOfRecomendations] = source.[NumberOfRecomendations],
		target.[NumberOfReviews] = source.[NumberOfReviews],
		target.[AverageMark] = source.[AverageMark],
		target.[PoorRating] = source.[PoorRating],
		target.[Rating] = source.[Rating],
		target.[Updating_DT] = source.[Updating_DT]
WHEN NOT MATCHED THEN INSERT

	(
		[BusinessCard_ID],
		[Account_ID],
		[NumberOfRecomendations],
		[NumberOfReviews],
		[AverageMark],
		[PoorRating],
		[Rating],
		[Updating_DT]
	)
VALUES(source.[BusinessCard_ID],
		source.[Account_ID],		
		source.[NumberOfRecomendations],
		source.[NumberOfReviews],
		source.[AverageMark],
		source.[PoorRating],
		source.[Rating],
		source.[Updating_DT])
;
END