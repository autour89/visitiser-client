﻿/*
USAGE:
	EXEC [profile].[GetBusinessCardPrice_MainScreen] @BusinessCard_ID = 1, @Lang_CD = 'ru'
*/
CREATE PROCEDURE [profile].[GetBusinessCardPrice_MainScreen]
	@BusinessCard_ID int,
	@Lang_CD char(2) 
AS 
BEGIN 
	SELECT TOP (5)
		   [PriceList_ID]
		  ,[BusinessCard_ID]
		  , cur.Currency_CD
		  ,[PriceList_NM]
		  ,[PriceList_DS]
		  ,[Price]
		  ,[Logo]
		  ,[Duration]
		  ,[Unit]		  
	FROM [profile].[BusinessCard_PriceList] pl
	LEFT JOIN [dbo].[Currency] cur ON pl.Currency_ID  = cur.Currency_ID
	WHERE [BusinessCard_ID] = @BusinessCard_ID AND [IsDisable] <> 1
END