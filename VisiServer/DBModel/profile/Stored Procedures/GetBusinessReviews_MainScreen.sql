﻿/*
Назначение: Получение список отзывов - для экрана - полная визитка

USAGE:
	EXEC [profile].[GetBusinessReviews_MainScreen] @BusinessCard_ID = 1, @ItemsCount = 10
*/

CREATE PROCEDURE  [profile].[GetBusinessReviews_MainScreen]
	@BusinessCard_ID int,
	@ItemsCount smallint = 15
AS

/* Берем средний рейтинг двух визиток ? - или сумму, или ту, что релевантна по похожему сервису */


SELECT  TOP(@ItemsCount)
	 rw.[Review_ID]
    ,rw.[ReviewerAccount_ID]
    ,rw.[ReviewedBusinessCard_ID]
    ,rw.[Review_Mark]
    ,rw.[Review_Recommendation]
    ,rw.[Review_DS]
    ,rw.[Review_DT]
	,Reviewer.[Account_NM]
	,Reviewer.[Account_Logo]
	,Reviewer.[Account_Occupation]
	,Reviewer.[Account_Work]
	,Rating.[Rating]
FROM 
	[profile].[Review]  rw
LEFT JOIN 
	[profile].[Account]  Reviewer ON rw.ReviewerAccount_ID = Reviewer.Account_ID
LEFT JOIN 
	[profile].[BusinessCard_Rating] rating ON rw.ReviewerAccount_ID = Rating.Account_ID
WHERE 
	[ReviewedBusinessCard_ID] = @BusinessCard_ID