﻿/*
Назначение: Получение списка Скилов по визитке - для экрана - полная визитка

USAGE:
	EXEC [profile].[GetBusinessCardSkills_MainScreen] @BusinessCard_ID = 1, @Lang_CD = 'ru'
*/
CREATE PROCEDURE [profile].GetBusinessCardSkills_MainScreen
	@BusinessCard_ID int,
	@Lang_CD char(2) 
AS
BEGIN
	SELECT		
		 ISNULL(cat.Skill_NM, sk.[Skill_NM]) AS Skill_NM
		,cat.Skill_ID
		,cat.Skill_URL
	FROM 
		[profile].[BusinessCard_Skill] sk
	LEFT JOIN 
		[profile.catalog].[Skill] cat ON sk.Skill_ID = cat.Skill_ID
	WHERE 
		[BusinessCard_ID] = @BusinessCard_ID
		
END