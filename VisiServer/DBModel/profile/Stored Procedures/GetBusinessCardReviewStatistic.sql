﻿CREATE   PROCEDURE [profile].[GetBusinessCardReviewStatistic] 
	@ReviewedBusinessCard_ID int
AS
SELECT 
      CONVERT(decimal(2,1), AVG(CONVERT(decimal(2,1), [Review_Mark]))) as AverageMark,
      SUM([Review_Recommendation]) as NumberOfRecomendations,
	  COUNT(Review_Mark) as NumberOfReviews
FROM [profile].[Review]
WHERE ReviewedBusinessCard_ID = @ReviewedBusinessCard_ID