﻿/*
Назначение: Получение общей информации о визитке - для экрана - полная визитка

USAGE:
	EXEC [profile].[GetBusinessCardInfo_MainScreen] @BusinessCard_ID = 1, @Lang_CD = 'ru', @Currency_Id = 980
*/

CREATE PROCEDURE [profile].[GetBusinessCardInfo_MainScreen](
@BusinessCard_ID int,
@Lang_CD char(2),
@Currency_Id int = 980 
)
AS BEGIN
	SELECT	TOP 1	
       acc.[Account_ID]
	  ,acc.[Account_NM]
	  ,acc.[Account_Logo]
	  ,acc.Lang_CD
	  ,DATEDIFF(year, acc.[Account_Birth_DT], GETDATE()) AS Age
	  ,bc.[BusinessCard_ID]
      ,bc.[BusinessCard_NM]
      ,bc.[BusinessCard_Type]
      ,bc.[BusinessCard_Short_DS]
      ,bc.[BusinessCard_Full_DS]
      ,bc.[BusinessCard_Logo]
      ,bc.[BusinessCard_Skype]
      ,bc.[BusinessCard_Facebook]
	  ,bc.BusinessCard_Education
	  ,bc.BusinessCard_Logo
	  ,bc.BusinessCard_WWW
	  ,bc.ServiceSubCategory_ID	  
      ,ssc.ServiceSubCategory_NM
	  ,sc.ServiceCategory_NM
	  ,DATEDIFF(year, [BusinessCard_WorkExperienceFrom], GETDATE()) AS ExperienceAge
	  ,pr.Price
	  ,pr.[Unit]
	  ,cur.[Currency_Symbol]
  FROM [profile].[BusinessCard] bc
  LEFT JOIN [profile].[Account] acc on bc.Account_ID = acc.Account_ID
  LEFT JOIN [profile.catalog].ServiceSubCategory ssc ON bc.ServiceSubCategory_ID = ssc.ServiceSubCategory_ID
  LEFT JOIN [profile.catalog].ServiceCategory sc ON sc.ServiceCategory_ID = ssc.ServiceCategory_ID
  LEFT JOIN [profile].[BusinessCard_PriceList] pr ON bc.BusinessCard_ID = pr.BusinessCard_ID
  LEFT JOIN [dbo].[Currency] cur ON pr.Currency_ID = cur.Currency_ID
  WHERE bc.BusinessCard_ID = @BusinessCard_ID
END