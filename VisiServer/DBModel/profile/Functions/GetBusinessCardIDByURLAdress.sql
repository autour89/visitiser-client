﻿-- =============================================
-- Author:		Michael Kashchenko
-- Create date: 05/2018
-- Description:	Find Business card by shor url name 
-- USAGE : SELECT  [profile].GetBusinessCardIDByURLAdress('m.kashchenko')
-- =============================================
CREATE FUNCTION [profile].GetBusinessCardIDByURLAdress
(	
	@URL_Addr  [dbo].[URL]
)
RETURNS int
AS
BEGIN
	
	DECLARE @BusinessCard_ID int

	SELECT @BusinessCard_ID = [BusinessCard_ID] FROM [profile].[BusinessCard] WHERE [BusinessCard_URL] = @URL_Addr
		
	RETURN @BusinessCard_ID

END