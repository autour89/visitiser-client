﻿CREATE TABLE [profile].[BusinessCard_Rating] (
    [BusinessCard_ID]        INT             NOT NULL,
    [Account_ID]             INT             NOT NULL,
    [NumberOfRecomendations] INT             NOT NULL,
    [NumberOfReviews]        INT             NOT NULL,
    [AverageMark]            DECIMAL (2, 1)  NOT NULL,
    [PoorRating]             DECIMAL (10, 2) NOT NULL,
    [Rating]                 DECIMAL (10, 2) NOT NULL,
    [Updating_DT]            DATETIME        NOT NULL,
    CONSTRAINT [PK_BusinessCardRating] PRIMARY KEY CLUSTERED ([BusinessCard_ID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IDX-BusinessCard-Account]
    ON [profile].[BusinessCard_Rating]([Account_ID] ASC);

