﻿CREATE TABLE [profile].[BusinessCard_Portfolio] (
    [Portfolio_ID]    INT             IDENTITY (1, 1) NOT NULL,
    [BusinessCard_ID] INT             NOT NULL,
    [Portfolio_NM]    NVARCHAR (300)  NOT NULL,
    [Portfolio_DS]    NVARCHAR (4000) NULL,
    [Portfolio_Logo]  VARBINARY (MAX) NULL,
    [Portfolio_URL]   NVARCHAR (2048) NULL,
    [Portfolio_DT]    DATE            CONSTRAINT [DF_BusinessCard_Profile_Profile_DT] DEFAULT (getdate()) NULL,
    [CreatedDT]       DATETIME        CONSTRAINT [DF_BusinessCard_Portfolio_Portfolio_CreatedDT] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_BusinessCard_Profile] PRIMARY KEY CLUSTERED ([Portfolio_ID] ASC),
    CONSTRAINT [FK_BusinessCard_Profile_Business_Cards] FOREIGN KEY ([BusinessCard_ID]) REFERENCES [profile].[BusinessCard] ([BusinessCard_ID])
);

