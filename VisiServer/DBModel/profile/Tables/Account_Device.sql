﻿CREATE TABLE [profile].[Account_Device] (
    [Account_Device_ID] INT           IDENTITY (1, 1) NOT NULL,
    [Account_ID]        INT           NOT NULL,
    [Device_UID]        VARCHAR (500) NOT NULL,
    [Created_DT]        DATETIME      CONSTRAINT [DF_Account_Devices_Created_DT] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Account_Devices] PRIMARY KEY CLUSTERED ([Account_Device_ID] ASC),
    CONSTRAINT [FK_Account_Devices_Account] FOREIGN KEY ([Account_ID]) REFERENCES [profile].[Account] ([Account_ID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'All registered Devices for every Account', @level0type = N'SCHEMA', @level0name = N'profile', @level1type = N'TABLE', @level1name = N'Account_Device';

