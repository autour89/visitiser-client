﻿CREATE TABLE [profile].[BusinessCard_Phone] (
    [Account_ID]      INT NOT NULL,
    [BusinessCard_ID] INT NOT NULL,
    [AccountPhone_ID] INT NOT NULL,
    CONSTRAINT [PK_BusinessCard_Phone] PRIMARY KEY CLUSTERED ([Account_ID] ASC, [BusinessCard_ID] ASC, [AccountPhone_ID] ASC),
    CONSTRAINT [FK_BusinessCard_Phone_Account_Phone] FOREIGN KEY ([AccountPhone_ID]) REFERENCES [profile].[Account_Phone] ([AccountPhone_ID]),
    CONSTRAINT [FK_BusinessCard_Phone_Account1] FOREIGN KEY ([Account_ID]) REFERENCES [profile].[Account] ([Account_ID]),
    CONSTRAINT [FK_BusinessCard_Phone_BusinessCard] FOREIGN KEY ([BusinessCard_ID]) REFERENCES [profile].[BusinessCard] ([BusinessCard_ID])
);



