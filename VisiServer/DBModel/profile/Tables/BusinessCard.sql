﻿CREATE TABLE [profile].[BusinessCard] (
    [BusinessCard_ID]                 INT             IDENTITY (1, 1) NOT NULL,
    [Account_ID]                      INT             NOT NULL,
    [ServiceSubCategory_ID]           INT             NOT NULL,
    [BusinessCard_URL]                [dbo].[URL]     NULL,
    [BusinessCard_WWW]                [dbo].[URL]     NULL,
    [BusinessCard_Video]              [dbo].[URL]     NULL,
    [BusinessCard_NM]                 NVARCHAR (150)  NOT NULL,
    [BusinessCard_Type]               CHAR (1)        CONSTRAINT [DF__Business___Card___52593CB8] DEFAULT ('I') NOT NULL,
    [BusinessCard_Short_DS]           NVARCHAR (250)  NULL,
    [BusinessCard_Full_DS]            NVARCHAR (4000) NULL,
    [BusinessCard_Logo]               IMAGE           NULL,
    [BusinessCard_Skype]              NCHAR (150)     NULL,
    [BusinessCard_Facebook]           NCHAR (150)     NULL,
    [BusinessCard_Education]          NVARCHAR (4000) NULL,
    [BusinessCard_CreatedDT]          DATETIME        CONSTRAINT [DF__Business___Busin__5535A963] DEFAULT (getdate()) NOT NULL,
    [BusinessCard_WorkExperienceFrom] DATE            CONSTRAINT [DF_BusinessCard_BusinessCard_WorkExpirienceFrom] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK__BusinessCard] PRIMARY KEY CLUSTERED ([BusinessCard_ID] ASC),
    CONSTRAINT [FK_Business_Cards_Account] FOREIGN KEY ([Account_ID]) REFERENCES [profile].[Account] ([Account_ID]),
    CONSTRAINT [FK_BusinessCard_ServiceSubCategory] FOREIGN KEY ([ServiceSubCategory_ID]) REFERENCES [profile.catalog].[ServiceSubCategory] ([ServiceSubCategory_ID])
);






GO
CREATE UNIQUE NONCLUSTERED INDEX [IDX-BusinessCard-URL]
    ON [profile].[BusinessCard]([BusinessCard_URL] ASC)
    INCLUDE([BusinessCard_ID]) WHERE ([BusinessCard_URL] IS NOT NULL);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'I-individual, B - Business, W - Worker', @level0type = N'SCHEMA', @level0name = N'profile', @level1type = N'TABLE', @level1name = N'BusinessCard', @level2type = N'COLUMN', @level2name = N'BusinessCard_Type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ссылка на ютуб для шапки визитки', @level0type = N'SCHEMA', @level0name = N'profile', @level1type = N'TABLE', @level1name = N'BusinessCard', @level2type = N'COLUMN', @level2name = N'BusinessCard_Video';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Веб сайт визитки', @level0type = N'SCHEMA', @level0name = N'profile', @level1type = N'TABLE', @level1name = N'BusinessCard', @level2type = N'COLUMN', @level2name = N'BusinessCard_WWW';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Короткое веб представление URL для визитки, пользователь создает сам', @level0type = N'SCHEMA', @level0name = N'profile', @level1type = N'TABLE', @level1name = N'BusinessCard', @level2type = N'COLUMN', @level2name = N'BusinessCard_URL';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Визитка', @level0type = N'SCHEMA', @level0name = N'profile', @level1type = N'TABLE', @level1name = N'BusinessCard';

