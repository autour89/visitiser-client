﻿CREATE TABLE [profile].[BusinessCard_Adress] (
    [BusinessCardAdress_ID]     INT               IDENTITY (1, 1) NOT NULL,
    [BusinessCard_ID]           INT               NOT NULL,
    [BusinessCardAdress_Type]   CHAR (1)          NOT NULL,
    [BusinessCardAdress_City]   NVARCHAR (100)    NULL,
    [BusinessCardAdress_Adress] [dbo].[t_Adress]  NULL,
    [BusinessCardAdress_Geo]    [sys].[geography] NULL,
    [BusinessCardAdress_Radius] INT               NULL,
    [BusinessCardAdress_DS]     NVARCHAR (1024)   NULL,
    CONSTRAINT [PK_Businss_Card_Adresses] PRIMARY KEY CLUSTERED ([BusinessCardAdress_ID] ASC),
    CONSTRAINT [FK_Businss_Card_Addresses_ToBusinss_Card] FOREIGN KEY ([BusinessCard_ID]) REFERENCES [profile].[BusinessCard] ([BusinessCard_ID])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'O-online, A-Adress, A - Area(Район)', @level0type = N'SCHEMA', @level0name = N'profile', @level1type = N'TABLE', @level1name = N'BusinessCard_Adress';

