﻿CREATE TABLE [profile].[BusinessCard_Skill] (
    [BusinessCard_Skill_ID] INT            IDENTITY (1, 1) NOT NULL,
    [BusinessCard_ID]       INT            NOT NULL,
    [Skill_ID]              INT            NULL,
    [Skill_NM]              NVARCHAR (250) NULL,
    CONSTRAINT [PK_BusinessCard_Skill] PRIMARY KEY CLUSTERED ([BusinessCard_Skill_ID] ASC),
    CONSTRAINT [FK_BusinessCard_Skill_BusinessCard] FOREIGN KEY ([BusinessCard_ID]) REFERENCES [profile].[BusinessCard] ([BusinessCard_ID]),
    CONSTRAINT [FK_BusinessCard_Skill_Skill] FOREIGN KEY ([Skill_ID]) REFERENCES [profile.catalog].[Skill] ([Skill_ID])
);

