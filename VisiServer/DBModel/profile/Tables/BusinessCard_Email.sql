﻿CREATE TABLE [profile].[BusinessCard_Email] (
    [BusinessCard_Email_ID] INT            IDENTITY (1, 1) NOT NULL,
    [BusinessCard_ID]       INT            NOT NULL,
    [Email]                 NVARCHAR (200) NOT NULL,
    [IsConfirmed]           BIT            CONSTRAINT [DF_BusinessCard_Email_isConfirmed] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Businss_Card_Emails] PRIMARY KEY CLUSTERED ([BusinessCard_Email_ID] ASC),
    CONSTRAINT [FK_Businss_Card_Emails_ToBusinss_Card] FOREIGN KEY ([BusinessCard_ID]) REFERENCES [profile].[BusinessCard] ([BusinessCard_ID])
);

