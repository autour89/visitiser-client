﻿CREATE TABLE [profile].[Account] (
    [Account_ID]           INT               IDENTITY (1, 1) NOT NULL,
    [Account_NM]           NVARCHAR (200)    NOT NULL,
    [Account_URL]          [dbo].[URL]       NULL,
    [Account_Sex]          CHAR (1)          CONSTRAINT [DF_Account_Account_Sex] DEFAULT ('U') NULL,
    [Account_Birth_DT]     DATE              NULL,
    [Account_City]         NCHAR (150)       NULL,
    [Account_Email]        NCHAR (150)       NULL,
    [Account_Facebook]     NCHAR (150)       NULL,
    [Account_Logo]         VARBINARY (MAX)   NULL,
    [Lang_CD]              CHAR (2)          CONSTRAINT [DF__Account__Account__571DF1D5] DEFAULT ('en') NOT NULL,
    [Account_CreatedDT]    DATETIME          CONSTRAINT [DF__Account__Created__5812160E] DEFAULT (getdate()) NULL,
    [Account_Address]      NVARCHAR (300)    NULL,
    [Account_Adress_Geo]   [sys].[geography] NULL,
    [Account_Work]         NVARCHAR (250)    NULL,
    [Account_Occupation]   NVARCHAR (250)    NULL,
    [IS_FacebookConnected] BIT               NULL,
    CONSTRAINT [PK__Account] PRIMARY KEY CLUSTERED ([Account_ID] ASC),
    CONSTRAINT [FK_Account_Language] FOREIGN KEY ([Lang_CD]) REFERENCES [dbo].[Language] ([Lang_CD])
);






GO
CREATE UNIQUE NONCLUSTERED INDEX [IDX-Account-URL]
    ON [profile].[Account]([Account_URL] ASC)
    INCLUDE([Account_ID]) WHERE ([Account_URL] IS NOT NULL);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Table of registered users', @level0type = N'SCHEMA', @level0name = N'profile', @level1type = N'TABLE', @level1name = N'Account';

