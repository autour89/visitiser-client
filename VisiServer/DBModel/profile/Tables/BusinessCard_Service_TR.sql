﻿CREATE TABLE [profile].[BusinessCard_Service_TR] (
    [BusinessCard_ServiceLG_ID] INT      IDENTITY (1, 1) NOT NULL,
    [BusinessCard_ID]           INT      NOT NULL,
    [Lang_CD]                   CHAR (2) NOT NULL,
    CONSTRAINT [PK_BusinessCard_Service_TR] PRIMARY KEY CLUSTERED ([BusinessCard_ServiceLG_ID] ASC),
    CONSTRAINT [FK_BusinessCard_Service_TR_Language] FOREIGN KEY ([Lang_CD]) REFERENCES [dbo].[Language] ([Lang_CD]),
    CONSTRAINT [FK_BusinessCard_ServiceLG_BusinessCard] FOREIGN KEY ([BusinessCard_ID]) REFERENCES [profile].[BusinessCard] ([BusinessCard_ID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'profile', @level1type = N'TABLE', @level1name = N'BusinessCard_Service_TR';

