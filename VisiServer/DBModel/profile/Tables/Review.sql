﻿CREATE TABLE [profile].[Review] (
    [Review_ID]               INT             IDENTITY (1, 1) NOT NULL,
    [ReviewerAccount_ID]      INT             NOT NULL,
    [ReviewedBusinessCard_ID] INT             NOT NULL,
    [Review_Mark]             TINYINT         NOT NULL,
    [Review_Recommendation]   SMALLINT        CONSTRAINT [DF_Review_Review_Recommendation] DEFAULT ((0)) NOT NULL,
    [Review_DS]               NVARCHAR (2048) NULL,
    [Review_DT]               DATETIME        CONSTRAINT [DF_Review_Review_DT] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Review] PRIMARY KEY NONCLUSTERED ([Review_ID] ASC),
    CONSTRAINT [FK_Review_Account] FOREIGN KEY ([ReviewerAccount_ID]) REFERENCES [profile].[Account] ([Account_ID]),
    CONSTRAINT [FK_Review_BusinessCard] FOREIGN KEY ([ReviewedBusinessCard_ID]) REFERENCES [profile].[BusinessCard] ([BusinessCard_ID])
);






GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Отзывы и рекомендации', @level0type = N'SCHEMA', @level0name = N'profile', @level1type = N'TABLE', @level1name = N'Review';


GO
CREATE NONCLUSTERED INDEX [IDX-Review-Revier]
    ON [profile].[Review]([ReviewerAccount_ID] ASC)
    INCLUDE([Review_Mark], [Review_Recommendation]);


GO
CREATE CLUSTERED INDEX [CI-Review]
    ON [profile].[Review]([ReviewedBusinessCard_ID] ASC);

