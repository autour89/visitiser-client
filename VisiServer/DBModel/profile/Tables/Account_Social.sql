﻿CREATE TABLE [profile].[Account_Social] (
    [Account_ID]       INT          NOT NULL,
    [Provider_NM]      VARCHAR (20) NOT NULL,
    [Provider_User_ID] VARCHAR (50) NOT NULL,
    CONSTRAINT [Account_Social_Account_ID_Provider_NM_Provider_User_ID_pk] PRIMARY KEY CLUSTERED ([Account_ID] ASC, [Provider_NM] ASC, [Provider_User_ID] ASC),
    CONSTRAINT [FK_Account_Social_Account] FOREIGN KEY ([Account_ID]) REFERENCES [profile].[Account] ([Account_ID])
);


GO
CREATE NONCLUSTERED INDEX [Account_Social_Provider_User_ID_index]
    ON [profile].[Account_Social]([Provider_User_ID] ASC);

