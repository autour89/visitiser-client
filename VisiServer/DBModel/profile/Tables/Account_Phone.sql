﻿CREATE TABLE [profile].[Account_Phone] (
    [AccountPhone_ID]    INT          IDENTITY (1, 1) NOT NULL,
    [Account_ID]         INT          NOT NULL,
    [Country_Code]       VARCHAR (10) NOT NULL,
    [AccountPhoneNumber] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK__AccountPhone] PRIMARY KEY CLUSTERED ([AccountPhone_ID] ASC),
    CONSTRAINT [FK_Account_Phones_ToAccount] FOREIGN KEY ([Account_ID]) REFERENCES [profile].[Account] ([Account_ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [idx-account_phones-phone]
    ON [profile].[Account_Phone]([AccountPhoneNumber] ASC)
    INCLUDE([AccountPhone_ID], [Account_ID]);

