﻿CREATE TABLE [profile].[BusinessCard_ServiceLanguage] (
    [BusinessCard_ID] INT      NOT NULL,
    [Lang_CD]         CHAR (2) NOT NULL,
    CONSTRAINT [PK_BusinessCard_ServiceLanguage] PRIMARY KEY CLUSTERED ([BusinessCard_ID] ASC, [Lang_CD] ASC),
    CONSTRAINT [FK_BusinessCard_ServiceLanguage_BusinessCard] FOREIGN KEY ([BusinessCard_ID]) REFERENCES [profile].[BusinessCard] ([BusinessCard_ID]),
    CONSTRAINT [FK_BusinessCard_ServiceLanguage_Language] FOREIGN KEY ([Lang_CD]) REFERENCES [dbo].[Language] ([Lang_CD])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Языки, на которых могут оказывать услуги', @level0type = N'SCHEMA', @level0name = N'profile', @level1type = N'TABLE', @level1name = N'BusinessCard_ServiceLanguage';

