﻿CREATE TABLE [profile].[BusinessCard_TR] (
    [BusinessCard_ID]       INT             NOT NULL,
    [Lang_CD]               CHAR (2)        NOT NULL,
    [BusinessCard_NM]       NVARCHAR (150)  NULL,
    [BusinessCard_Short_DS] NVARCHAR (250)  NULL,
    [BusinessCard_Full_DS]  NVARCHAR (4000) NULL,
    [Created_DT]            DATETIME        CONSTRAINT [DF_BusinessCard_TR_CreatedDt] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_BusinessCard_TR] PRIMARY KEY CLUSTERED ([BusinessCard_ID] ASC, [Lang_CD] ASC),
    CONSTRAINT [FK_BusinessCard_TR_BusinessCard] FOREIGN KEY ([BusinessCard_ID]) REFERENCES [profile].[BusinessCard] ([BusinessCard_ID]),
    CONSTRAINT [FK_BusinessCard_TR_Language] FOREIGN KEY ([Lang_CD]) REFERENCES [dbo].[Language] ([Lang_CD])
);

