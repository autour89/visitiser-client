﻿CREATE TABLE [profile].[BusinessCard_PriceList] (
    [PriceList_ID]    INT             IDENTITY (1, 1) NOT NULL,
    [BusinessCard_ID] INT             NOT NULL,
    [Currency_ID]     INT             NULL,
    [PriceList_NM]    NVARCHAR (250)  NOT NULL,
    [PriceList_DS]    NVARCHAR (4000) NULL,
    [Price]           DECIMAL (18, 2) NULL,
    [Logo]            VARBINARY (50)  NULL,
    [Duration]        DECIMAL (10, 2) NULL,
    [Unit]            NVARCHAR (50)   NULL,
    [Created_DT]      DATETIME        CONSTRAINT [DF_BusinessCard_PriceList_BusinessCard_PriceList_DT] DEFAULT (getdate()) NOT NULL,
    [IsDisable]       BIT             CONSTRAINT [DF_BusinessCard_PriceList_IsDisable] DEFAULT ((0)) NOT NULL,
    [SortOrder]       INT             NULL,
    [IsMainService]   BIT             CONSTRAINT [DF_BusinessCard_PriceList_IsMainService] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_BusinessCard_PriceList] PRIMARY KEY CLUSTERED ([PriceList_ID] ASC)
);






GO
CREATE UNIQUE NONCLUSTERED INDEX [IDX-PriceList-MainService]
    ON [profile].[BusinessCard_PriceList]([BusinessCard_ID] ASC)
    INCLUDE([Price], [Unit], [Currency_ID]) WHERE ([IsMainService]=(1));


GO
CREATE NONCLUSTERED INDEX [IDX-PriceList]
    ON [profile].[BusinessCard_PriceList]([BusinessCard_ID] ASC)
    INCLUDE([Price], [Unit]) WHERE ([IsDisable]=(0));

