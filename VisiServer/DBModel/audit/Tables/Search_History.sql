﻿CREATE TABLE [audit].[Search_History] (
    [Search_Id]   INT            IDENTITY (1, 1) NOT NULL,
    [Search_NM]   NVARCHAR (300) NULL,
    [Created_DT]  DATETIME       DEFAULT (getdate()) NULL,
    [Account_ID]  INT            NULL,
    [Source_Type] NCHAR (1)      DEFAULT ('W') NULL,
    [Device_ID]   INT            NULL,
    PRIMARY KEY CLUSTERED ([Search_Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'W- Web, D - Device', @level0type = N'SCHEMA', @level0name = N'audit', @level1type = N'TABLE', @level1name = N'Search_History', @level2type = N'COLUMN', @level2name = N'Source_Type';

