﻿CREATE PROCEDURE calendar.GetEventAdress
	@Event_ID		INT,
	@Location_Type	CHAR(1)			OUTPUT,
	@Adress			CHAR(300)		OUTPUT,
	@Adress_GEO_lat    FLOAT OUTPUT,
	@Adress_GEO_long    FLOAT OUTPUT
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE 		
		@OwnerID int, 
		@Business_Card_Adress_ID int,
		@Adress_GEO GEOGRAPHY;

    SELECT 
		@Location_Type = Location_Type, 
		@OwnerID = events.Owner_ID, 
		@Business_Card_Adress_ID = Events.Business_Card_Adress_ID,
		@Adress = Events.Event_Address, 
		@Adress_GEO = Events.Event_Adress_Geo
	FROM calendar.Events 
	WHERE Events.Event_ID = @Event_ID;
	
	IF @Location_Type = 'P' -- Personal Account
		SELECT 
			@Adress = Account_City,
			@Adress_GEO_lat = Account.Account_Adress_Geo.Lat,
			@Adress_GEO_long = Account.Account_Adress_Geo.Long
		FROM profile.Account
		WHERE Account.Account_ID = @OwnerID

	ELSE IF @Location_Type = 'O' -- Office from business card
		SELECT 
			@Adress = ca.Business_Card_City+' '+ca.Business_Card_Adress_NM,
			@Adress_GEO_lat = ca.Business_Card_Adress_Geo.Lat,
			@Adress_GEO_long = ca.Business_Card_Adress_Geo.Long
		FROM profile.Business_Card_Adresses ca
		WHERE ca.Business_Card_Adress_ID = @Business_Card_Adress_ID

	ELSE IF @Location_Type = 'S' -- Skype (online)
		SELECT 
			@Adress		= null,
			@Adress_GEO_lat = null,
			@Adress_GEO_long = null
	ELSE 
		SELECT 
			@Adress		= null,
			@Adress_GEO_lat = null,
			@Adress_GEO_long = null

END