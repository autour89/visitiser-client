﻿CREATE TABLE [calendar].[Event_Member] (
    [Event_Members_ID] INT             IDENTITY (1, 1) NOT NULL,
    [Event_ID]         INT             NOT NULL,
    [Account_ID]       INT             NULL,
    [Member_Info]      NVARCHAR (1000) NULL,
    [Status_CD]        CHAR (1)        NULL,
    CONSTRAINT [PK_Event_Members] PRIMARY KEY CLUSTERED ([Event_Members_ID] ASC),
    CONSTRAINT [FK_Event_Members_Accounts] FOREIGN KEY ([Account_ID]) REFERENCES [profile].[Account] ([Account_ID])
);

