﻿CREATE TABLE [calendar].[Event] (
    [Event_ID]               INT               IDENTITY (1, 1) NOT NULL,
    [Created_DT]             DATETIME          CONSTRAINT [DF_Events_Created_DT] DEFAULT (getdate()) NOT NULL,
    [Event_Start_DT]         SMALLDATETIME     NOT NULL,
    [Event_End_DT]           SMALLDATETIME     NOT NULL,
    [Event_Duration]         TIME (7)          NOT NULL,
    [Event_NM]               NVARCHAR (300)    NULL,
    [Event_DS]               NVARCHAR (4000)   NULL,
    [Event_Address]          [dbo].[t_Adress]  NULL,
    [Event_Adress_Geo]       [sys].[geography] NULL,
    [Creator_ID]             INT               NOT NULL,
    [Owner_ID]               INT               NULL,
    [Event_Members_ID]       INT               NULL,
    [BusinessCard_ID]        INT               NULL,
    [BusinessCard_Adress_ID] INT               NULL,
    [Location_Type]          CHAR (1)          DEFAULT ('O') NULL,
    [Event_URL]              CHAR (8)          NULL,
    CONSTRAINT [PK_Calendar_Events] PRIMARY KEY CLUSTERED ([Event_ID] ASC),
    CONSTRAINT [FK_Event_Creator_Accounts] FOREIGN KEY ([Creator_ID]) REFERENCES [profile].[Account] ([Account_ID]),
    CONSTRAINT [FK_Event_Members_Accounts_ID] FOREIGN KEY ([Event_Members_ID]) REFERENCES [calendar].[Event_Member] ([Event_Members_ID]),
    CONSTRAINT [FK_Event_Owner_Accounts] FOREIGN KEY ([Owner_ID]) REFERENCES [profile].[Account] ([Account_ID]),
    CONSTRAINT [FK_Events_Business_Card] FOREIGN KEY ([BusinessCard_ID]) REFERENCES [profile].[BusinessCard] ([BusinessCard_ID]),
    CONSTRAINT [FK_Events_Business_Card_Adress] FOREIGN KEY ([BusinessCard_Adress_ID]) REFERENCES [profile].[BusinessCard_Adress] ([BusinessCardAdress_ID])
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [IDX-Business_Card-URL]
    ON [calendar].[Event]([Event_URL] ASC)
    INCLUDE([Event_ID]);


GO
  
-- =============================================
-- Author:		Michael Kashchenko
-- Create date: 2017-05-01
-- Description:	Set Event_URL value from Event_ID
-- =============================================
CREATE TRIGGER calendar.AddShortURLToEvent
   ON  [calendar].[Event]
   AFTER INSERT
AS BEGIN
	DECLARE @Result VARCHAR(8);
	

	WHILE (1=1) BEGIN
		SELECT @Result = LEFT(CONVERT(char(36), NEWID() ), 8);
		IF NOT EXISTS(
			SELECT top 1 Event_ID
			FROM calendar.Events 
			WHERE Event_URL= @Result
		) BREAK
	END

	UPDATE TOP (1) calendar.Events
	SET Event_URL = @Result
	FROM inserted
	WHERE calendar.Events.Event_ID = inserted.Event_ID
END
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'S - online (Skype), O- Office, A - Adress, P - Account', @level0type = N'SCHEMA', @level0name = N'calendar', @level1type = N'TABLE', @level1name = N'Event', @level2type = N'COLUMN', @level2name = N'Location_Type';

