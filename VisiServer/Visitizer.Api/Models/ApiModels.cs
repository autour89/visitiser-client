﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Visitizer.Api.Models
{

    public struct DBase
    {
        public static string СonnectString = System.Configuration.ConfigurationManager.ConnectionStrings["ContextDB"].ToString();
    }
    public struct ServerURLs
    {
        //public static string SignalURI = "http://localhost:59220";
        public static string SignalURI = "http://192.168.0.108:59220/";
        //http://test.visitizer.com:8080/Visi";
        //public static string SignalURI = System.Configuration.ConfigurationManager.AppSettings["SignalURI"];
    }

    public struct HelperStr
    {
        public static char[] PhoneContains = { '(', '+', ')', ' ' };
    }

    //public class VisiProfile
    //{
    //    [JsonProperty("id")]
    //    public int Id { get; set; }
    //    [JsonProperty("name")]
    //    public string Name { get; set; }
    //    [JsonProperty("gender")]
    //    public string Gender { get; set; }
    //    [JsonProperty("birth_dt")]
    //    public DateTime Birth_DT { get; set; }
    //    [JsonProperty("city")]
    //    public string City { get; set; }
    //    [JsonProperty("address")]
    //    public string Address { get; set; }
    //    [JsonProperty("email")]
    //    public string Email { get; set; }
    //    [JsonProperty("logo")]
    //    public byte[] Logo { get; set; }
    //    public string Skype { get; set; }
    //    public string Facebook { get; set; }
    //    public string Site { get; set; }
    //    [JsonProperty("phones")]
    //    public List<Phones> Phones { get; set; }
    //}

    //public class Phones
    //{
    //    public int Id { get; set; }
    //    public int AccountContactId { get; set; }
    //    public string Phone { get; set; }
    //}

    public class VisitizerProfile
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public DateTime Birth_DT { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public byte[] Logo { get; set; }
        public List<AccountPhones> PhoneNumbers { get; set; }
    }


    public class AccountData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Sex { get; set; }
        public string City { get; set; }
        public List<AccountPhones> PhoneNumber { get; set; }
        public DateTime Birth_DT { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public byte[] Logo { get; set; }
        public override string ToString()
        {
            return Name + " " + PhoneNumber.FirstOrDefault() + " " + Birth_DT + " " + Email;
        }
    }


    public class AccountInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public byte[] Logo { get; set; }
        public override string ToString()
        {
            return Name + " " + PhoneNumber.ParsePhoneStr(HelperStr.PhoneContains) + " " + Email;
        }
    }



    public class RegisterViewModel
    {
        public string Name { get; set; }
        public string CountryCode { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class AccountContacts
    {
        public int AccountContactId { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string ContactStatus { get; set; }
        public override string ToString()
        {
            return AccountContactId + " " + Name + " " + PhoneNumber;
        }
    }

    public class AccountModel
    {
        public int UserId { get; set; }
        public int ChatId { get; set; }
        public List<int> Ids { get; set; }
        public List<AccountContacts> Items { get; set; }
    }

    public class Conversation
    {
        public int ConversationId { get; set; }
        public string ConversationName { get; set; }
        public string ChatSnippet { get; set; }
        public List<ChatMessages> ChatMessages { get; set; }
    }

    public class ChatMessages
    {
        public int MessageId { get; set; }
        public int ConversationId { get; set; }
        public int SenderId { get; set; }
        public string SenderName { get; set; }
        public string Message { get; set; }
        public string MessageType { get; set; }
        public DateTime MessageDT { get; set; }
        public override string ToString()
        {
            return MessageDT + " " + SenderName + " " + Message;
        }
    }

    public class ChaListModel
    {
        public int ChatId { get; set; }
        public string ChatName { get; set; }
        public string ChatSnippet { get; set; }
        public List<MessageModel> Items { get; set; }
    }

    public class MessageModel
    {
        public int Account1 { get; set; }
        public int Account2 { get; set; }
        public int MessageId { get; set; }
        public int SenderId { get; set; }
        public int ChatId { get; set; }
        public string SenderName { get; set; }
        public DateTime MessageDT { get; set; }
        public string Message { get; set; }
        public string MessageType { get; set; }
    }


    public class AccountPhones
    {
        public int Id { get; set; }
        public int AccountContactId { get; set; }
        public string CountryCode { get; set; }
        public string PhoneNumber { get; set; }
    }


    public class AccountChats
    {
        public int ConversationId { get; set; }
    }


    public class EventsModel
    {
        public int EventId { get; set; }
        public int ServerEventId { get; set; }
        public DateTime Created_DT { get; set; }
        public DateTime Start_DT { get; set; }
        public DateTime End_DT { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public int CreatorId { get; set; }
        public int OwnerId { get; set; }
        public string EventStatus { get; set; }
        public List<EventMembers> EventMembers { get; set; }
    }

    public class EventMembers
    {
        public int EventId { get; set; }
        public int AccountContactId { get; set; }
    }


    public class BusinessCardModel
    {
        public int Business_Card_ID { get; set; }
        public int AccountContactId { get; set; }
        public string Business_Card_NM { get; set; }
        public string Business_Card_Descriprion { get; set; }
        public string Business_Card_Adress { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Business_Card_Email { get; set; }
        public string Business_Card_Phones { get; set; }
        public List<ServicesModel> Services { get; set; }
    }
    public class ServicesModel
    {
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }

    }

    public class BusinessCard
    {
        public int ID { get; set; }
        public int Business_Card_ID { get; set; }
        public int Account_ID { get; set; }
        public char Card_Type { get; set; }
        public bool IsOffice { get; set; }
        public bool IsOnSite { get; set; }
        public string Name { get; set; }
        public string Descriprion { get; set; }
        public string Addresses { get; set; }
        public string Skype { get; set; }
        public string Facebook { get; set; }
        public string Emails { get; set; }
        public string Phones { get; set; }
        public string Sites { get; set; }
        public string Services { get; set; }
        public byte[] Logo { get; set; }
        public DateTime Created_DT { get; set; }
        public override string ToString()
        {
            return Name + " " + Descriprion;
        }
    }

    public class BCard_Adresses
    {
        public string Business_Card_Adress_NM { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }

    public class BCard_Links
    {
        public string Name { get; set; }
    }
    public class BCard_Services
    {
        public int Business_Card_Serv_ID { get; set; }
        public string Serv_NM { get; set; }
    }



    public class RenderRequest
    {
        public string Text { get; set; }

        public string Mode { get; set; }

        public string Context { get; set; }
    }


    public class ErrorModel
    {
        public int StatusCode { get; set; }
        public DateTime Time { get; set; }
        public string User { get; set; }
        public string Detail { get; set; }
        public string Message { get; set; }
        public string Source { get; set; }
        public string Type { get; set; }
        public Exception Exception { get; }
    }

}



//public class AccountData
//{
//    public int Id { get; set; }
//    public string Name { get; set; }
//    public List<string> PhoneNumber { get; set; }
//    public string Birth_DT { get; set; }
//    public string Skype { get; set; }
//    public string Email { get; set; }
//    public byte[] Logo { get; set; }
//    public override string ToString()
//    {
//        return Name + " " + PhoneNumber + " " + Birth_DT + " " + Skype + " " + Email;
//    }
//}

//public class BusinessCard
//{
//    public int Business_Card_ID { get; set; }
//    public int AccountContactId { get; set; }
//    public string Business_Card_NM { get; set; }
//    public string Business_Card_Descriprion { get; set; }
//    public string Business_Card_Adress { get; set; }
//    public string Business_Card_Email { get; set; }
//    public string Business_Card_Phones { get; set; }
//    public byte[] Business_Card_Logo { get; set; }
//    public List<BusinessCardServices> Services { get; set; }
//    public override string ToString()
//    {
//        return Business_Card_NM + " " + Business_Card_Descriprion;
//    }
//}


//public class BusinessCardServices
//{
//    public int Business_Card_Serv_ID { get; set; }
//    public int Business_Card_ID { get; set; }
//    public int Serv_ID { get; set; }
//    public string Serv_NM { get; set; }
//    public override string ToString()
//    {
//        return Serv_NM;
//    }
//}

