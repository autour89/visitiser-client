﻿using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Visitizer.Api.Models
{
    public class FilterExpression
    {
        //private static FilterExpression<T> instance = null;
        //protected FilterExpression()
        //{
        //}
        //public static FilterExpression<T> NewInstance()
        //{
        //    if (instance == null)
        //    {
        //        instance = new FilterExpression<T>();
        //    }
        //    return instance;
        //}


        public static Expression<Func<T, bool>> Predicate<T>(string[] filter) where T : new()
        {
            var predicate = PredicateBuilder.New<T>();
                
            foreach (string keyword in filter)
                predicate = predicate.And(p => p.ToString().Contains(keyword));

            return predicate;
        }


        public static Expression<Func<T, bool>> Predicate<T>(string filter) where T : new()
        {
            var predicate = PredicateBuilder.New<T>();

            return predicate = predicate.And(p => p.ToString().Contains(filter));
        }

    }
}