﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using Visitizer.Api.App_Start;
using Ninject;

namespace Visitizer.Api
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup

            IKernel kernel = new StandardKernel();
            GlobalConfiguration.Configuration.DependencyResolver = new NinjectResolver(kernel);

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //SqlServerTypes.Utilities.LoadNativeAssemblies(Server.MapPath("~/bin"));

            //var dbContext = new MessengerDbContext();
            //Database.SetInitializer(new DataInitialization());
            //dbContext.Database.Initialize(true);

        }
    }
}