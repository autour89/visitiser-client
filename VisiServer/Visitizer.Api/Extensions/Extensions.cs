﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Visitizer
{
    public static class Extensions
    {
        public static string ParsePhoneStr(this string text, IEnumerable<char> chars)
        {
            return new string(text.Where(c => !chars.Contains(c)).ToArray());
        }
    }
}