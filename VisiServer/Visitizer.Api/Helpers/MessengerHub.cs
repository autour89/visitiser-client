﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Visitizer.Api.Models;
using Visitizer.DataLayer.DataModel;

namespace Visitizer.Api.Helpers
{
    public class MessengerHub : Hub
    {
        public void Send(string name, string message)
        {
            Clients.All.addMessage(name, message);
        }

        public void SendPrivateMessage(int chatId)
        {
            //var acn_ids = (from ch_mr in db.Msg_Chat_Members where ch_mr.Chat_List_ID.Equals(chatId) select ch_mr.Account_ID).ToList();

            //var devices = (from ac_dv in db.Profile_Account_Devices.Where(x => x.Account_ID.Contains(acn_ids)) select ac_dv.Device_UID).ToList();

            //var devices = (from ac_dv in db.Profile_Account_Devices.Where(x => acn_ids.Contains(x.Account_ID)) select ac_dv.Device_UID).ToList();

            //devices.ForEach(x => Clients.Client(x).NewMessages(chatId));
        }

        public override Task OnConnected()
        {
            var userid = Convert.ToInt32(Context.QueryString["userid"]);

            //var model = new Profile_Account_Devices { Account_ID = userid, Device_UID = Context.ConnectionId, Created_DT = DateTime.Now };

            //db.Profile_Account_Devices.InsertOnSubmit(model);
            //db.SubmitChanges();

            return base.OnConnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {
            var clientId = Convert.ToInt32(Context.QueryString["userid"]);

            //var item = (from acdvc in db.Profile_Account_Devices
            //         where acdvc.Account_ID.Equals(clientId) && acdvc.Device_UID.Equals(Context.ConnectionId)
            //         select acdvc).FirstOrDefault();
            //db.Profile_Account_Devices.DeleteOnSubmit(item);

            //db.SubmitChanges();

            return base.OnDisconnected(stopCalled);
        }

    }
}

