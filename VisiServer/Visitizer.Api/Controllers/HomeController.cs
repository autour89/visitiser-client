﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Visitizer.Api.Models;
using Visitizer.DataLayer.DataModel;

namespace Visitizer.Api.Controllers
{
    public class HomeController : Controller
    {
        private List<ApplicationUser> appusers;

        public IDisposable SignalR { get; set; }

        public HomeController()
        {
            Task.Run(() => StartServer());

            //appusers = new List<ApplicationUser>();

            //appusers.Add(new ApplicationUser { NickName = "Ivan", LastName = "Petrov", UserName = "petrov@gmail.com", Email = "petrov@gmail.com", PasswordHash = "petrov123" });

        }

        public ActionResult Index()
        {
            //foreach (var u in appusers)
            //    await UserManager.CreateAsync(u, u.PasswordHash);

            //var t = UserManager.Users.ToList();

            //if (!await RoleManager.RoleExistsAsync(SecurityRoles.Admin))
            //    await RoleManager.CreateAsync(new ApplicationRole(SecurityRoles.Admin));
            //if (!await RoleManager.RoleExistsAsync(SecurityRoles.Manager))
            //    await RoleManager.CreateAsync(new ApplicationRole(SecurityRoles.Manager));
            //if (!await RoleManager.RoleExistsAsync(SecurityRoles.MainManager))
            //    await RoleManager.CreateAsync(new ApplicationRole(SecurityRoles.MainManager));

            //foreach (var user in appusers)
            //{
            //    await UserManager.AddToRoleAsync(user.Id, SecurityRoles.Admin);
            //}

            return View();
        }

        private void StartServer()
        {
            try
            {
                SignalR = WebApp.Start(ServerURLs.SignalURI);
            }
            catch (TargetInvocationException)
            {
                return;
            }
        }

    }
}