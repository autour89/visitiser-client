﻿using Elmah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Visitizer.Api.Models;

namespace Visitizer.Api.Controllers.WebApi
{
    public class LoggerController : ApiController
    {
        private ErrorLog Err;
        public LoggerController()
        {
            Err = ErrorLog.GetDefault(System.Web.HttpContext.Current);
        }

        [HttpPost]
        public void WriteException([FromBody]ErrorModel model)
        {
            ErrorSignal.FromCurrentContext().Raise(model.Exception);
        }

        [HttpPost]
        public void WriteError([FromBody]ErrorModel model)
        {
            if (model.Exception != null)
            {
                Err.Log(new Error(model.Exception)
                {
                    StatusCode = model.StatusCode,
                    User = model.User,
                    Time = model.Time,
                    Source = model.Source,
                    Detail = model.Detail,
                    Message = model.Message
                });
            }
            else
            {
                Err.Log(new Error(new Exception("no exception"))
                {
                    StatusCode = model.StatusCode,
                    User = model.User,
                    Time = model.Time,
                    Source = model.Source,
                    Detail = model.Detail,
                    Message = model.Message
                });
            }


        }



        [HttpPost]
        public void WriteErrors([FromBody]List<ErrorModel> model)
        {
            foreach (var item in model)
            {
                if (item.Exception != null)
                {
                    Err.Log(new Error(item.Exception)
                    {
                        StatusCode = item.StatusCode,
                        User = item.User,
                        Time = item.Time,
                        Source = item.Source,
                        Detail = item.Detail,
                        Message = item.Message
                    });
                }
                else
                {
                    Err.Log(new Error(new Exception("no exception"))
                    {
                        StatusCode = item.StatusCode,
                        User = item.User,
                        Time = item.Time,
                        Source = item.Source,
                        Detail = item.Detail,
                        Message = item.Message
                    });
                }
            }


        }


    }
}
