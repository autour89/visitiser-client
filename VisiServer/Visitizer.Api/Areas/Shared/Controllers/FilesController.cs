﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Visitizer.Api.Areas.Shared.Models;

namespace Visitizer.Api.Areas.Shared.Controllers
{
    public class FilesController : Controller
    {
        public FilesController()
        {
        }

        private string BaseDir => AppDomain.CurrentDomain.BaseDirectory;
        private string AppPath = AppDomain.CurrentDomain.GetData("DataDirectory").ToString();
        public ActionResult Index()
        {
            var files = GetFiles();

            return View(files);
        }

        [HttpGet]
        public ActionResult Download(string name)
        {
            var dir_files = GetFiles();
            var file = dir_files.Where(f => f.Name.Equals(name)).FirstOrDefault();
            var data = GetFile(file.FullName);
            return File(data, "application/force-download", file.Name);
        }

        private byte[] GetFile(string fileName)
        {
            return System.IO.File.ReadAllBytes(fileName);
        }

        private List<FileModel> GetFiles()
        {
            var docs = new List<FileModel>();

            var files = Directory.EnumerateFiles(BaseDir + "/Areas/Shared/Files", "*", SearchOption.TopDirectoryOnly)
           .Select(f => new FileInfo(f)).ToList();


            files.ForEach(f => docs.Add(new FileModel
            {
                Dir = f.DirectoryName,
                FullName = f.FullName,
                Name = f.Name,
                Extension = f.Extension,
                CreationDate = f.CreationTime
            }));

            return docs;
        }

        private void TestFunc()
        {
            Response.Write(string.Format("<script>window.open('{0}','_blank');</script>", "pdf/aaaa.PDF"));

            //var cur_dir = Environment.CurrentDirectory;
            //string[] Documents = Directory.GetFiles(BaseDir);
        }
    }
}