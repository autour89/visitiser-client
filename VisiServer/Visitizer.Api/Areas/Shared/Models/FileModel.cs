﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Visitizer.Api.Areas.Shared.Models
{
    public class FileModel
    {
        public string Dir { get; set; }
        public string FullName { get; set; }
        public string Name { get; set; }
        public string Extension { get; set; }
        public DateTime CreationDate { get; set; }
    }
}