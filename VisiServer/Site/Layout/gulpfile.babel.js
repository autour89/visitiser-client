import gulp from 'gulp4';
import plumber from 'gulp-plumber';
import gulpIf from 'gulp-if';
import gulpStylus from 'gulp-stylus';
import notify from 'gulp-notify';
import sourceMaps from 'gulp-sourcemaps';
import cssNano from 'gulp-cssnano';
import rev from 'gulp-rev';
import {resolver} from 'stylus';
import {obj as combine} from 'stream-combiner2';
import {create} from 'browser-sync';
import {argv} from 'yargs';
import path from 'path';
import uglify from 'gulp-uglify';
import del from 'del';
import revReplace from 'gulp-rev-replace';
import webpackStream, {webpack} from 'webpack-stream';
import named from 'vinyl-named';
import AssetsPlugin from 'assets-webpack-plugin';
import modRewrite from 'connect-modrewrite';
import handlebars from 'gulp-compile-handlebars';
import rename from 'gulp-rename';
import fs from 'fs';
import replace from 'gulp-replace';
import escape from 'escape-html'

const browserSync = create();

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';
const publicDir = './public';

const paths = {
    styles: {
        src: [
            './src/layout/index.styl',
        ],
        dest: `${publicDir}/styles`,
        watch: [
            './src/layout/**/*.styl',
        ],
    },
    js: {
        src: './src/js',
        dest: `${publicDir}/js`,
    },
    html: {
        src: [
            './src/layout/pages/card/card.hbs',
            './src/layout/pages/search/search.hbs',
        ],
        partials: [
            './src/layout/partials'
        ],
        pages: './src/layout/pages',
        common: './src/layout/common',
        watch: [
            './src/layout/**/*.{hbs,json}',
        ],
        dest: `${publicDir}`,
    },
    assets: {
        src: [
            './src/assets/**/*.{html,js}',
        ],

        dest: `${publicDir}`,
    },
    revInPlaceReplace: {
        target: `${publicDir}/*.html`
    }
};

const styles = () => {
    return gulp.src(paths.styles.src)
        .pipe(plumber({
            errorHandler: notify.onError(err => ({
                title: 'Styles',
                message: err.message
            }))
        }))
        .pipe(gulpIf(isDevelopment, sourceMaps.init()))
        .pipe(gulpStylus({
            define: {
                url: resolver()
            },
            // 'include css': true
        }))
        .pipe(gulpIf(isDevelopment, sourceMaps.write()))
        .pipe(gulpIf(!isDevelopment, combine(cssNano(), rev())))
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(gulpIf(!isDevelopment, combine(rev.manifest('css.json'), gulp.dest('manifest'))))
        .pipe(browserSync.stream());
};

const serve = () => {
    browserSync.init({
        server: {
            baseDir: publicDir,
            middleware: [
                modRewrite([
                    '!\\.\\w+($|\\?.*$) /index.html'
                ])
            ]
        }
    });
};

// const serve = () => {
//     browserSync.init({
//         proxy: argv.proxyTarget || 'localhost'
//     });
// };

const webpackTask = callback => {

    let options = {
        entry: {
            app: paths.js.src,
        },
        output: {
            // path: `${__dirname}/${paths.js.dest}`,
            publicPath: '/js/',
            filename: isDevelopment ? '[name].js' : '[name]-[chunkhash:10].js'
        },
        watch: isDevelopment,
        devtool: isDevelopment ? 'cheap-module-source-map' : false,
        module: {
            loaders: [{
                test: /\.js$/,
                include: path.join(__dirname, "src"),
                loader: 'babel-loader?presets[]=env&presets[]=react&presets[]=stage-0'
            }],
            // noParse: []
        },
        plugins: [
            new webpack.NoEmitOnErrorsPlugin() // otherwise error still gives a file
        ]
    };

    if (!isDevelopment) {
        options.plugins.push(
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    // don't show unreachable variables etc
                    warnings: false,
                    unsafe: true
                }
            }),
            new AssetsPlugin({
                filename: 'webpack.json',
                path: `${__dirname}/manifest`,
                processOutput(assets) {
                    for (let key in assets) {
                        assets[key + '.js'] = assets[key].js.slice(options.output.publicPath.length);
                        delete assets[key];
                    }
                    return JSON.stringify(assets);
                }
            }),
            new webpack.EnvironmentPlugin(['NODE_ENV'])
        );

    }

    return gulp.src('src/js/*.js')
        .pipe(plumber({
            errorHandler: notify.onError(err => ({
                title: 'Webpack',
                message: err.message
            }))
        }))
        .pipe(named())
        .pipe(webpackStream(options))
        .pipe(gulpIf(!isDevelopment, uglify()))
        .pipe(gulp.dest(paths.js.dest))
        .pipe(browserSync.stream())
        .on('data', function () {
            callback();
        });
};

const clean = () => {
    return del([
        `${publicDir}/js`,
        `${publicDir}/styles/index.css`,
        `${publicDir}/index.html`,
        'manifest'
    ], {force: true});
};

/*
const assets = () => {
    return gulp.src(
        paths.assets.src,
        // {since: gulp.lastRun(assets)}
    )
        .pipe(plumber({
            errorHandler: notify.onError(err => ({
                title: 'Assets',
                message: err.message
            }))
        }))
        .pipe(gulpIf(!isDevelopment, revReplace({
            replaceInExtensions: ['.html'],
            manifest: gulp.src('manifest/css.json', {allowEmpty: true})
        })))
        .pipe(gulpIf(!isDevelopment, revReplace({
            replaceInExtensions: ['.html'],
            manifest: gulp.src('manifest/webpack.json', {allowEmpty: true})
        })))
        .pipe(gulpIf("*.html", gulp.dest(paths.assets.dest)))
        // .pipe(gulpIf("*-sw.js", gulp.dest(paths.assets.dest)))
        .pipe(browserSync.stream());
};
*/

const html = () => {
    let pagesPath = paths.html.pages;
    let commonPath = paths.html.common;
    let isNew = true;
    let currentPage = '';
    let currentPartialPath = '';
    const errorHandler = notify.onError(err => ({
        title: 'Html',
        message: err.message.replace('\r', '\n')
    }));
    const parseTemplate = path => data => {
        try {
            let template = fs.readFileSync(path, 'utf8').toString();
            template = handlebars.Handlebars.compile(template);
            return template(data);
        } catch (e) {
            errorHandler(e);
            return escape(e.message)
        }
    };
    return gulp.src(
        paths.html.src,
        // {since: gulp.lastRun(assets)}
    )
        .pipe(plumber({errorHandler}))
        .pipe(handlebars({}, {
            ignorePartials: true,
            batch: paths.html.partials,
            helpers: {
                inc: function (part, options) {
                    let name = part.split("/").pop();
                    let page = this.path.split("/").shift();
                    let partPath = `${this.path}/${part}`;
                    currentPartialPath = partPath;
                    let filesPathName = options.hash.common
                        ? `${commonPath}/${part}/${name}.`
                        : `${pagesPath}/${partPath}/${name}.`;
                    let layoutPath = `${filesPathName}hbs`;
                    let stylePath = `${filesPathName}styl`;
                    let dataPath = `${filesPathName}json`;
                    this[part] = fs.existsSync(dataPath)
                        ? (() => {
                            try {
                                return JSON.parse(fs.readFileSync(dataPath, 'utf8'))
                            } catch (e) {
                                errorHandler(e);
                                return {}
                            }
                        })()
                        : {};
                    this[part].path = partPath;
                    if (page !== currentPage) {
                        if (fs.existsSync(`${pagesPath}/${page}/${page}.styl`)) {
                            let func = isNew ? (isNew = false) || fs.writeFileSync : fs.appendFileSync;
                            func(`${pagesPath}/_auto_parts.styl`, `@require "${page}"\n`);
                        }
                        currentPage = page;
                    }
                    if (fs.existsSync(stylePath)) {
                        let path = options.hash.common
                            ? `../common/${part}`
                            : partPath;
                        fs.appendFileSync(`${pagesPath}/_auto_parts.styl`, `@require "${path}"\n`);
                    }
                    return parseTemplate(layoutPath)(this[part]);
                },
                file: function (name) {
                    let path = `${pagesPath}/${currentPartialPath}/${name}.hbs`;
                    return parseTemplate(path)(this);
                }
            }
        }))
        .pipe(rename({
            extname: '.html'
        }))
        .pipe(gulp.dest(paths.html.dest))
        .pipe(browserSync.stream());
};

// const assetsStyles = () => {
//     return gulp.src(paths.assets.styles, {since: gulp.lastRun(assetsStyles)})
//         .pipe(gulp.dest(paths.styles.dest));
// };

const revision = () => {
    return gulp.src(paths.revInPlaceReplace.target, {base: "../"})
        .pipe(replace(/-[a-f|0-9]{10}/g, ''))
        .pipe(revReplace({
            replaceInExtensions: ['.html'],
            manifest: gulp.src('manifest/css.json', {allowEmpty: true}),
        }))
        .pipe(revReplace({
            replaceInExtensions: ['.html'],
            manifest: gulp.src('manifest/webpack.json', {allowEmpty: true}),
        }))
        .pipe(gulp.dest("../"));
};

export const build = gulp.series(
    clean,
    html,
    gulp.parallel(
        styles,
        webpackTask,
        // assetsStyles
    ),
    revision,
    // assets
);

export const dev = gulp.series(
    build,
    gulp.parallel(
        serve,
        function () {
            gulp.watch(paths.styles.watch, gulp.series(styles));
            // gulp.watch(paths.assets.src, gulp.series(assets));
            gulp.watch(paths.html.watch, gulp.series(html));
        }
    )
);

export default dev;
