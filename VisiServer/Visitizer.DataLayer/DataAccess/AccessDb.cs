﻿using System.Data.Entity;

using Mobile.DataLayer.Repository;
using Visitizer.DataLayer.DataModel;

namespace Visitizer.DataLayer.DataAccess
{
    public class AccountAccess : GenericRepository<Account>
    {
        private ContextDB context;

        public AccountAccess(ContextDB context): base(context)
        {
            this.context = context;
        }

        protected override DbSet<Account> dbSet => context.Accounts;

    }



    public class Business_Card_ServicesAccess : GenericRepository<BusinessCard_Service_TR>
    {
        private ContextDB context;
        public Business_Card_ServicesAccess(ContextDB context): base(context)
        {
            this.context = context;
        }
        protected override DbSet<BusinessCard_Service_TR> dbSet => context.BusinessCard_Service_TR;
    }


    public class Business_CardsAccess : GenericRepository<BusinessCard>
    {
        private ContextDB context;
        public Business_CardsAccess(ContextDB context): base(context)
        {
            this.context = context;
        }
        protected override DbSet<BusinessCard> dbSet => context.BusinessCards;
    }


    public class EventMembers : GenericRepository<Event_Member>
    {
        private ContextDB context;
        public EventMembers(ContextDB context): base(context)
        {
            this.context = context;
        }

        protected override DbSet<Event_Member> dbSet => context.Event_Member;
    }


    public class EventsAccess : GenericRepository<Event>
    {
        private ContextDB context;
        public EventsAccess(ContextDB context): base(context)
        {
            this.context = context;
        }

        protected override DbSet<Event> dbSet => context.Events;
    }


    public class MessagesAccess : GenericRepository<Message>
    {
        private ContextDB context;

        public MessagesAccess(ContextDB context): base(context)
        {
            this.context = context;
        }

        protected override DbSet<Message> dbSet => context.Messages;
    }



}
