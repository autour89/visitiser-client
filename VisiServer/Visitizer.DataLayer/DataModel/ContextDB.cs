namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ContextDB : DbContext
    {
        public ContextDB()
            : base("name=ContextDB1")
        {
        }

        public virtual DbSet<Label> Labels { get; set; }
        public virtual DbSet<Label_TR> Label_TR { get; set; }
        public virtual DbSet<Search_History> Search_History { get; set; }
        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<Event_Member> Event_Member { get; set; }
        public virtual DbSet<Account_Contact> Account_Contact { get; set; }
        public virtual DbSet<Currency> Currencies { get; set; }
        public virtual DbSet<Currency_TR> Currency_TR { get; set; }
        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<Chat_List> Chat_List { get; set; }
        public virtual DbSet<Chat_Member> Chat_Member { get; set; }
        public virtual DbSet<Delivery_Message> Delivery_Message { get; set; }
        public virtual DbSet<Message> Messages { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Account_Device> Account_Device { get; set; }
        public virtual DbSet<Account_Phone> Account_Phone { get; set; }
        public virtual DbSet<BusinessCard> BusinessCards { get; set; }
        public virtual DbSet<BusinessCard_Adress> BusinessCard_Adress { get; set; }
        public virtual DbSet<BusinessCard_Email> BusinessCard_Email { get; set; }
        public virtual DbSet<BusinessCard_Phone> BusinessCard_Phone { get; set; }
        public virtual DbSet<BusinessCard_Portfolio> BusinessCard_Portfolio { get; set; }
        public virtual DbSet<BusinessCard_PriceList> BusinessCard_PriceList { get; set; }
        public virtual DbSet<BusinessCard_Service_TR> BusinessCard_Service_TR { get; set; }
        public virtual DbSet<BusinessCard_Skill> BusinessCard_Skill { get; set; }
        public virtual DbSet<BusinessCard_TR> BusinessCard_TR { get; set; }
        public virtual DbSet<Review> Reviews { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<Service_TR> Service_TR { get; set; }
        public virtual DbSet<ServiceCategory> ServiceCategories { get; set; }
        public virtual DbSet<ServiceCategory_TR> ServiceCategory_TR { get; set; }
        public virtual DbSet<ServiceSubCategory> ServiceSubCategories { get; set; }
        public virtual DbSet<ServiceSubCategory_TR> ServiceSubCategory_TR { get; set; }
        public virtual DbSet<Skill> Skills { get; set; }
        public virtual DbSet<Skill_TR> Skill_TR { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Label>()
                .Property(e => e.Label_CD)
                .IsUnicode(false);

            modelBuilder.Entity<Label>()
                .HasMany(e => e.Label_TR)
                .WithRequired(e => e.Label)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Label_TR>()
                .Property(e => e.Lang_CD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Search_History>()
                .Property(e => e.Source_Type)
                .IsFixedLength();

            modelBuilder.Entity<Event>()
                .Property(e => e.Location_Type)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Event>()
                .Property(e => e.Event_URL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Event_Member>()
                .Property(e => e.Status_CD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Currency>()
                .Property(e => e.Currency_CD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Currency>()
                .HasMany(e => e.Currency_TR)
                .WithRequired(e => e.Currency)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Currency_TR>()
                .Property(e => e.Lang_CD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Language>()
                .Property(e => e.Lang_CD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Language>()
                .HasMany(e => e.Label_TR)
                .WithRequired(e => e.Language)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Language>()
                .HasMany(e => e.Currency_TR)
                .WithRequired(e => e.Language)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Language>()
                .HasMany(e => e.Accounts)
                .WithRequired(e => e.Language)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Language>()
                .HasMany(e => e.BusinessCard_Service_TR)
                .WithRequired(e => e.Language)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Language>()
                .HasMany(e => e.BusinessCard_TR)
                .WithRequired(e => e.Language)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Language>()
                .HasMany(e => e.Service_TR)
                .WithRequired(e => e.Language)
                .HasForeignKey(e => e.Lancg_CD)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Language>()
                .HasMany(e => e.ServiceCategory_TR)
                .WithRequired(e => e.Language)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Language>()
                .HasMany(e => e.ServiceSubCategory_TR)
                .WithRequired(e => e.Language)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Language>()
                .HasMany(e => e.Skill_TR)
                .WithRequired(e => e.Language)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Chat_List>()
                .HasMany(e => e.Chat_Member)
                .WithRequired(e => e.Chat_List)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Chat_List>()
                .HasMany(e => e.Messages)
                .WithRequired(e => e.Chat_List)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Chat_Member>()
                .Property(e => e.Role_Type)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Delivery_Message>()
                .Property(e => e.Delivery_Status)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Message>()
                .Property(e => e.Message_Type)
                .IsUnicode(false);

            modelBuilder.Entity<Message>()
                .Property(e => e.Delivery_Status)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Message>()
                .HasMany(e => e.Delivery_Message)
                .WithRequired(e => e.Message)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .Property(e => e.Account_URL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Account>()
                .Property(e => e.Account_Sex)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Account>()
                .Property(e => e.Account_City)
                .IsFixedLength();

            modelBuilder.Entity<Account>()
                .Property(e => e.Account_Email)
                .IsFixedLength();

            modelBuilder.Entity<Account>()
                .Property(e => e.Account_Facebook)
                .IsFixedLength();

            modelBuilder.Entity<Account>()
                .Property(e => e.Lang_CD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Events)
                .WithRequired(e => e.Account)
                .HasForeignKey(e => e.Creator_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Events1)
                .WithOptional(e => e.Account1)
                .HasForeignKey(e => e.Owner_ID);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Account_Contact)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Chat_Member)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Messages)
                .WithRequired(e => e.Account)
                .HasForeignKey(e => e.Sender_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Account_Device)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Account_Phone)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.BusinessCards)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.BusinessCard_Phone)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Reviews)
                .WithOptional(e => e.Account)
                .HasForeignKey(e => e.ReviewerAccount_ID);

            modelBuilder.Entity<Account_Device>()
                .Property(e => e.Device_UID)
                .IsUnicode(false);

            modelBuilder.Entity<Account_Device>()
                .HasMany(e => e.Delivery_Message)
                .WithRequired(e => e.Account_Device)
                .HasForeignKey(e => e.Device_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account_Phone>()
                .Property(e => e.Country_Code)
                .IsUnicode(false);

            modelBuilder.Entity<Account_Phone>()
                .Property(e => e.AccountPhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Account_Phone>()
                .HasMany(e => e.BusinessCard_Phone)
                .WithRequired(e => e.Account_Phone)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BusinessCard>()
                .Property(e => e.BusinessCard_URL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BusinessCard>()
                .Property(e => e.BusinessCard_WWW)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BusinessCard>()
                .Property(e => e.BusinessCard_Video)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BusinessCard>()
                .Property(e => e.BusinessCard_Type)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BusinessCard>()
                .Property(e => e.BusinessCard_Skype)
                .IsFixedLength();

            modelBuilder.Entity<BusinessCard>()
                .Property(e => e.BusinessCard_Facebook)
                .IsFixedLength();

            modelBuilder.Entity<BusinessCard>()
                .HasMany(e => e.BusinessCard_Phone)
                .WithRequired(e => e.BusinessCard)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BusinessCard>()
                .HasMany(e => e.BusinessCard_Portfolio)
                .WithRequired(e => e.BusinessCard)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BusinessCard>()
                .HasMany(e => e.BusinessCard_Service_TR)
                .WithRequired(e => e.BusinessCard)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BusinessCard>()
                .HasMany(e => e.BusinessCard_Skill)
                .WithRequired(e => e.BusinessCard)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BusinessCard>()
                .HasMany(e => e.BusinessCard_TR)
                .WithRequired(e => e.BusinessCard)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BusinessCard>()
                .HasMany(e => e.BusinessCard_Adress)
                .WithRequired(e => e.BusinessCard)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BusinessCard>()
                .HasMany(e => e.BusinessCard_Email)
                .WithRequired(e => e.BusinessCard)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BusinessCard>()
                .HasMany(e => e.Reviews)
                .WithOptional(e => e.BusinessCard)
                .HasForeignKey(e => e.ReviewedBusinessCard_ID);

            modelBuilder.Entity<BusinessCard>()
                .HasMany(e => e.Languages)
                .WithMany(e => e.BusinessCards)
                .Map(m => m.ToTable("BusinessCard_ServiceLanguage", "profile").MapLeftKey("BusinessCard_ID").MapRightKey("Lang_CD"));

            modelBuilder.Entity<BusinessCard_Adress>()
                .Property(e => e.BusinessCardAdress_Type)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BusinessCard_Adress>()
                .HasMany(e => e.Events)
                .WithOptional(e => e.BusinessCard_Adress)
                .HasForeignKey(e => e.BusinessCard_Adress_ID);

            modelBuilder.Entity<BusinessCard_PriceList>()
                .Property(e => e.Duration)
                .HasPrecision(10, 2);

            modelBuilder.Entity<BusinessCard_Service_TR>()
                .Property(e => e.Lang_CD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BusinessCard_TR>()
                .Property(e => e.Lang_CD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Service>()
                .Property(e => e.Service_URL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Service>()
                .HasMany(e => e.Service_TR)
                .WithRequired(e => e.Service)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Service_TR>()
                .Property(e => e.Lancg_CD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ServiceCategory>()
                .Property(e => e.ServiceCategory_URL)
                .IsUnicode(false);

            modelBuilder.Entity<ServiceCategory>()
                .HasMany(e => e.ServiceCategory_TR)
                .WithRequired(e => e.ServiceCategory)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ServiceCategory>()
                .HasMany(e => e.ServiceSubCategories)
                .WithRequired(e => e.ServiceCategory)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ServiceCategory_TR>()
                .Property(e => e.Lang_CD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ServiceCategory_TR>()
                .Property(e => e.ServiceCategory_SEO)
                .IsFixedLength();

            modelBuilder.Entity<ServiceSubCategory>()
                .HasMany(e => e.BusinessCards)
                .WithRequired(e => e.ServiceSubCategory)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ServiceSubCategory>()
                .HasMany(e => e.Services)
                .WithRequired(e => e.ServiceSubCategory)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ServiceSubCategory>()
                .HasMany(e => e.ServiceSubCategory_TR)
                .WithRequired(e => e.ServiceSubCategory)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ServiceSubCategory_TR>()
                .Property(e => e.Lang_CD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Skill>()
                .Property(e => e.Skill_URL)
                .IsUnicode(false);

            modelBuilder.Entity<Skill>()
                .HasMany(e => e.Skill_TR)
                .WithRequired(e => e.Skill)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Skill_TR>()
                .Property(e => e.Lang_CD)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}
