namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Language")]
    public partial class Language
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Language()
        {
            Label_TR = new HashSet<Label_TR>();
            Currency_TR = new HashSet<Currency_TR>();
            Accounts = new HashSet<Account>();
            BusinessCard_Service_TR = new HashSet<BusinessCard_Service_TR>();
            BusinessCard_TR = new HashSet<BusinessCard_TR>();
            Service_TR = new HashSet<Service_TR>();
            ServiceCategory_TR = new HashSet<ServiceCategory_TR>();
            ServiceSubCategory_TR = new HashSet<ServiceSubCategory_TR>();
            Skill_TR = new HashSet<Skill_TR>();
            BusinessCards = new HashSet<BusinessCard>();
        }

        [Key]
        [StringLength(2)]
        public string Lang_CD { get; set; }

        [Required]
        [StringLength(50)]
        public string Lang_NM { get; set; }

        public byte[] Lang_Logo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Label_TR> Label_TR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Currency_TR> Currency_TR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Account> Accounts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BusinessCard_Service_TR> BusinessCard_Service_TR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BusinessCard_TR> BusinessCard_TR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Service_TR> Service_TR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServiceCategory_TR> ServiceCategory_TR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServiceSubCategory_TR> ServiceSubCategory_TR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Skill_TR> Skill_TR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BusinessCard> BusinessCards { get; set; }
    }
}
