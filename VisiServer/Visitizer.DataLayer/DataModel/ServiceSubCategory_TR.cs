namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("[profile.catalog].ServiceSubCategory_TR")]
    public partial class ServiceSubCategory_TR
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ServiceSubCategory_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(2)]
        public string Lang_CD { get; set; }

        [Column("ServiceSubCategory_TR")]
        [Required]
        [StringLength(250)]
        public string ServiceSubCategory_TR1 { get; set; }

        [StringLength(4000)]
        public string ServiceSubCategory_SEO { get; set; }

        public virtual Language Language { get; set; }

        public virtual ServiceSubCategory ServiceSubCategory { get; set; }
    }
}
