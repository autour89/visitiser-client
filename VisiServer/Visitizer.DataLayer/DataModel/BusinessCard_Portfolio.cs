namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("profile.BusinessCard_Portfolio")]
    public partial class BusinessCard_Portfolio
    {
        [Key]
        public int Portfolio_ID { get; set; }

        public int BusinessCard_ID { get; set; }

        [Required]
        [StringLength(300)]
        public string Portfolio_NM { get; set; }

        [StringLength(4000)]
        public string Portfolio_DS { get; set; }

        public byte[] Portfolio_Logo { get; set; }

        [StringLength(2048)]
        public string Portfolio_URL { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Portfolio_DT { get; set; }

        public DateTime CreatedDT { get; set; }

        public virtual BusinessCard BusinessCard { get; set; }
    }
}
