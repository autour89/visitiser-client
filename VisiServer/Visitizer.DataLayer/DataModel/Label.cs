namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("articles.Label")]
    public partial class Label
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Label()
        {
            Label_TR = new HashSet<Label_TR>();
        }

        [Key]
        public int Label_ID { get; set; }

        [Required]
        [StringLength(70)]
        public string Label_CD { get; set; }

        [StringLength(100)]
        public string Label_DS { get; set; }

        public byte[] Label_Logo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Label_TR> Label_TR { get; set; }
    }
}
