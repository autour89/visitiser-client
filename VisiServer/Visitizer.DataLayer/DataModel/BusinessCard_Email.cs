namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("profile.BusinessCard_Email")]
    public partial class BusinessCard_Email
    {
        [Key]
        public int BusinessCard_Email_ID { get; set; }

        public int BusinessCard_ID { get; set; }

        [Required]
        [StringLength(200)]
        public string Email { get; set; }

        public bool IsConfirmed { get; set; }

        public virtual BusinessCard BusinessCard { get; set; }
    }
}
