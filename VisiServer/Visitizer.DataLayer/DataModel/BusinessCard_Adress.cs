namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("profile.BusinessCard_Adress")]
    public partial class BusinessCard_Adress
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BusinessCard_Adress()
        {
            Events = new HashSet<Event>();
        }

        [Key]
        public int BusinessCardAdress_ID { get; set; }

        public int BusinessCard_ID { get; set; }

        [Required]
        [StringLength(1)]
        public string BusinessCardAdress_Type { get; set; }

        [StringLength(100)]
        public string BusinessCardAdress_City { get; set; }

        [StringLength(300)]
        public string BusinessCardAdress_Adress { get; set; }

        public DbGeography BusinessCardAdress_Geo { get; set; }

        public int? BusinessCardAdress_Radius { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Event> Events { get; set; }

        public virtual BusinessCard BusinessCard { get; set; }
    }
}
