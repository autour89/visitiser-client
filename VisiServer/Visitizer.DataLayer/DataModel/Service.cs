namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("[profile.catalog].Service")]
    public partial class Service
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Service()
        {
            Service_TR = new HashSet<Service_TR>();
        }

        [Key]
        public int Service_ID { get; set; }

        public int ServiceSubCategory_ID { get; set; }

        [StringLength(250)]
        public string Service_URL { get; set; }

        [Required]
        [StringLength(250)]
        public string Service_NM { get; set; }

        public virtual ServiceSubCategory ServiceSubCategory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Service_TR> Service_TR { get; set; }
    }
}
