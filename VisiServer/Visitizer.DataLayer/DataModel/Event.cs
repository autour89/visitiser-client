namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("calendar.Event")]
    public partial class Event
    {
        [Key]
        public int Event_ID { get; set; }

        public DateTime Created_DT { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime Event_Start_DT { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime Event_End_DT { get; set; }

        public TimeSpan Event_Duration { get; set; }

        [StringLength(300)]
        public string Event_NM { get; set; }

        [StringLength(4000)]
        public string Event_DS { get; set; }

        [StringLength(300)]
        public string Event_Address { get; set; }

        public DbGeography Event_Adress_Geo { get; set; }

        public int Creator_ID { get; set; }

        public int? Owner_ID { get; set; }

        public int? Event_Members_ID { get; set; }

        public int? BusinessCard_ID { get; set; }

        public int? BusinessCard_Adress_ID { get; set; }

        [StringLength(1)]
        public string Location_Type { get; set; }

        [StringLength(8)]
        public string Event_URL { get; set; }

        public virtual Account Account { get; set; }

        public virtual Event_Member Event_Member { get; set; }

        public virtual Account Account1 { get; set; }

        public virtual BusinessCard BusinessCard { get; set; }

        public virtual BusinessCard_Adress BusinessCard_Adress { get; set; }
    }
}
