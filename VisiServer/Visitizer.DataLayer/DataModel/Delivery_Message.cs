namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("msg.Delivery_Message")]
    public partial class Delivery_Message
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Message_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Device_ID { get; set; }

        [Required]
        [StringLength(1)]
        public string Delivery_Status { get; set; }

        public virtual Account_Device Account_Device { get; set; }

        public virtual Message Message { get; set; }
    }
}
