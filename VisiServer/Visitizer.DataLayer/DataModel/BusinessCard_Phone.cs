namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("profile.BusinessCard_Phone")]
    public partial class BusinessCard_Phone
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Account_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int BusinessCard_ID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccountPhone_ID { get; set; }

        public virtual Account Account { get; set; }

        public virtual Account_Phone Account_Phone { get; set; }

        public virtual BusinessCard BusinessCard { get; set; }
    }
}
