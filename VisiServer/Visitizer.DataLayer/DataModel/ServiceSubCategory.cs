namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("[profile.catalog].ServiceSubCategory")]
    public partial class ServiceSubCategory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ServiceSubCategory()
        {
            BusinessCards = new HashSet<BusinessCard>();
            Services = new HashSet<Service>();
            ServiceSubCategory_TR = new HashSet<ServiceSubCategory_TR>();
        }

        [Key]
        public int ServiceSubCategory_ID { get; set; }

        public int ServiceCategory_ID { get; set; }

        [Required]
        [StringLength(250)]
        public string ServiceSubCategory_NM { get; set; }

        public DateTime ServiceSubCategory_DT { get; set; }

        public bool? IS_Custom { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BusinessCard> BusinessCards { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Service> Services { get; set; }

        public virtual ServiceCategory ServiceCategory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServiceSubCategory_TR> ServiceSubCategory_TR { get; set; }
    }
}
