namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("profile.BusinessCard_TR")]
    public partial class BusinessCard_TR
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int BusinessCard_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(2)]
        public string Lang_CD { get; set; }

        [StringLength(150)]
        public string BusinessCard_NM { get; set; }

        [StringLength(250)]
        public string BusinessCard_Short_DS { get; set; }

        [StringLength(4000)]
        public string BusinessCard_Full_DS { get; set; }

        public DateTime Created_DT { get; set; }

        public virtual Language Language { get; set; }

        public virtual BusinessCard BusinessCard { get; set; }
    }
}
