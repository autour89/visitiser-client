namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("[profile.catalog].ServiceCategory")]
    public partial class ServiceCategory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ServiceCategory()
        {
            ServiceCategory_TR = new HashSet<ServiceCategory_TR>();
            ServiceSubCategories = new HashSet<ServiceSubCategory>();
        }

        [Key]
        public int ServiceCategory_ID { get; set; }

        [StringLength(250)]
        public string ServiceCategory_URL { get; set; }

        [Required]
        [StringLength(250)]
        public string ServiceCategory_NM { get; set; }

        public byte[] ServiceCategory_Logo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServiceCategory_TR> ServiceCategory_TR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServiceSubCategory> ServiceSubCategories { get; set; }
    }
}
