namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("profile.Account")]
    public partial class Account
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Account()
        {
            Events = new HashSet<Event>();
            Events1 = new HashSet<Event>();
            Event_Member = new HashSet<Event_Member>();
            Account_Contact = new HashSet<Account_Contact>();
            Chat_Member = new HashSet<Chat_Member>();
            Messages = new HashSet<Message>();
            Account_Device = new HashSet<Account_Device>();
            Account_Phone = new HashSet<Account_Phone>();
            BusinessCards = new HashSet<BusinessCard>();
            BusinessCard_Phone = new HashSet<BusinessCard_Phone>();
            Reviews = new HashSet<Review>();
        }

        [Key]
        public int Account_ID { get; set; }

        [Required]
        [StringLength(200)]
        public string Account_NM { get; set; }

        [StringLength(250)]
        public string Account_URL { get; set; }

        [StringLength(1)]
        public string Account_Sex { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Account_Birth_DT { get; set; }

        [StringLength(150)]
        public string Account_City { get; set; }

        [StringLength(150)]
        public string Account_Email { get; set; }

        [StringLength(150)]
        public string Account_Facebook { get; set; }

        public byte[] Account_Logo { get; set; }

        [Required]
        [StringLength(2)]
        public string Lang_CD { get; set; }

        public DateTime? Account_CreatedDT { get; set; }

        [StringLength(300)]
        public string Account_Address { get; set; }

        public DbGeography Account_Adress_Geo { get; set; }

        [StringLength(250)]
        public string Account_Work { get; set; }

        [StringLength(250)]
        public string Account_Occupation { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Event> Events { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Event> Events1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Event_Member> Event_Member { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Account_Contact> Account_Contact { get; set; }

        public virtual Language Language { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Chat_Member> Chat_Member { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Message> Messages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Account_Device> Account_Device { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Account_Phone> Account_Phone { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BusinessCard> BusinessCards { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BusinessCard_Phone> BusinessCard_Phone { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Review> Reviews { get; set; }
    }
}
