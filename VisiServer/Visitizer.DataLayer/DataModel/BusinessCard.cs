namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("profile.BusinessCard")]
    public partial class BusinessCard
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BusinessCard()
        {
            Events = new HashSet<Event>();
            BusinessCard_Phone = new HashSet<BusinessCard_Phone>();
            BusinessCard_Portfolio = new HashSet<BusinessCard_Portfolio>();
            BusinessCard_Service_TR = new HashSet<BusinessCard_Service_TR>();
            BusinessCard_Skill = new HashSet<BusinessCard_Skill>();
            BusinessCard_TR = new HashSet<BusinessCard_TR>();
            BusinessCard_Adress = new HashSet<BusinessCard_Adress>();
            BusinessCard_Email = new HashSet<BusinessCard_Email>();
            Reviews = new HashSet<Review>();
            Languages = new HashSet<Language>();
        }

        [Key]
        public int BusinessCard_ID { get; set; }

        public int Account_ID { get; set; }

        public int ServiceSubCategory_ID { get; set; }

        [StringLength(250)]
        public string BusinessCard_URL { get; set; }

        [StringLength(250)]
        public string BusinessCard_WWW { get; set; }

        [StringLength(250)]
        public string BusinessCard_Video { get; set; }

        [Required]
        [StringLength(150)]
        public string BusinessCard_NM { get; set; }

        [Required]
        [StringLength(1)]
        public string BusinessCard_Type { get; set; }

        [StringLength(250)]
        public string BusinessCard_Short_DS { get; set; }

        [StringLength(4000)]
        public string BusinessCard_Full_DS { get; set; }

        [Column(TypeName = "image")]
        public byte[] BusinessCard_Logo { get; set; }

        [StringLength(150)]
        public string BusinessCard_Skype { get; set; }

        [StringLength(150)]
        public string BusinessCard_Facebook { get; set; }

        [StringLength(4000)]
        public string BusinessCard_Education { get; set; }

        public DateTime BusinessCard_CreatedDT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Event> Events { get; set; }

        public virtual Account Account { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BusinessCard_Phone> BusinessCard_Phone { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BusinessCard_Portfolio> BusinessCard_Portfolio { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BusinessCard_Service_TR> BusinessCard_Service_TR { get; set; }

        public virtual ServiceSubCategory ServiceSubCategory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BusinessCard_Skill> BusinessCard_Skill { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BusinessCard_TR> BusinessCard_TR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BusinessCard_Adress> BusinessCard_Adress { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BusinessCard_Email> BusinessCard_Email { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Review> Reviews { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Language> Languages { get; set; }
    }
}
