namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("profile.Account_Phone")]
    public partial class Account_Phone
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Account_Phone()
        {
            BusinessCard_Phone = new HashSet<BusinessCard_Phone>();
        }

        [Key]
        public int AccountPhone_ID { get; set; }

        public int Account_ID { get; set; }

        [Required]
        [StringLength(10)]
        public string Country_Code { get; set; }

        [Required]
        [StringLength(50)]
        public string AccountPhoneNumber { get; set; }

        public virtual Account Account { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BusinessCard_Phone> BusinessCard_Phone { get; set; }
    }
}
