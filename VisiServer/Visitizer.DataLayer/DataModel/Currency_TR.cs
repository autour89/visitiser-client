namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Currency_TR
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Currency_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(2)]
        public string Lang_CD { get; set; }

        [Column("Currency_TR")]
        [Required]
        [StringLength(250)]
        public string Currency_TR1 { get; set; }

        public virtual Currency Currency { get; set; }

        public virtual Language Language { get; set; }
    }
}
