namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("crm.Account_Contact")]
    public partial class Account_Contact
    {
        [Key]
        public int Account_Contact_ID { get; set; }

        public int Account_ID { get; set; }

        public int Contact_ID { get; set; }

        public virtual Account Account { get; set; }
    }
}
