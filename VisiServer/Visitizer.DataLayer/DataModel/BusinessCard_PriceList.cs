namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("profile.BusinessCard_PriceList")]
    public partial class BusinessCard_PriceList
    {
        [Key]
        public int PriceList_ID { get; set; }

        public int BusinessCard_ID { get; set; }

        public int? Currency_ID { get; set; }

        [Required]
        [StringLength(250)]
        public string PriceList_NM { get; set; }

        [StringLength(4000)]
        public string PriceList_DS { get; set; }

        public decimal? Price { get; set; }

        [MaxLength(50)]
        public byte[] Logo { get; set; }

        public decimal? Duration { get; set; }

        [StringLength(50)]
        public string Unit { get; set; }

        public DateTime Created_DT { get; set; }

        public bool IsDisable { get; set; }
    }
}
