namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("[profile.catalog].Skill_TR")]
    public partial class Skill_TR
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Skill_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(2)]
        public string Lang_CD { get; set; }

        [Column("Skill_TR")]
        [Required]
        [StringLength(250)]
        public string Skill_TR1 { get; set; }

        public virtual Language Language { get; set; }

        public virtual Skill Skill { get; set; }
    }
}
