namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("profile.Account_Device")]
    public partial class Account_Device
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Account_Device()
        {
            Delivery_Message = new HashSet<Delivery_Message>();
        }

        [Key]
        public int Account_Device_ID { get; set; }

        public int Account_ID { get; set; }

        [Required]
        [StringLength(500)]
        public string Device_UID { get; set; }

        public DateTime Created_DT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Delivery_Message> Delivery_Message { get; set; }

        public virtual Account Account { get; set; }
    }
}
