namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("profile.Review")]
    public partial class Review
    {
        [Key]
        public int Review_ID { get; set; }

        public int? ReviewerAccount_ID { get; set; }

        public int? ReviewedBusinessCard_ID { get; set; }

        public byte? Review_Mark { get; set; }

        public bool Review_Recommendation { get; set; }

        [StringLength(2048)]
        public string Review_DS { get; set; }

        public DateTime Review_DT { get; set; }

        public virtual Account Account { get; set; }

        public virtual BusinessCard BusinessCard { get; set; }
    }
}
