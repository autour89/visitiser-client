namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("articles.Label_TR")]
    public partial class Label_TR
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Label_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(2)]
        public string Lang_CD { get; set; }

        [Column("Label_TR")]
        [Required]
        [StringLength(4000)]
        public string Label_TR1 { get; set; }

        public virtual Label Label { get; set; }

        public virtual Language Language { get; set; }
    }
}
