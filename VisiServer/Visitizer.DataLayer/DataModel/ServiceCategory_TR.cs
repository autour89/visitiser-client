namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("[profile.catalog].ServiceCategory_TR")]
    public partial class ServiceCategory_TR
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ServiceCategory_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(2)]
        public string Lang_CD { get; set; }

        [Column("ServiceCategory_TR")]
        [Required]
        [StringLength(250)]
        public string ServiceCategory_TR1 { get; set; }

        [StringLength(10)]
        public string ServiceCategory_SEO { get; set; }

        public virtual Language Language { get; set; }

        public virtual ServiceCategory ServiceCategory { get; set; }
    }
}
