namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("profile.BusinessCard_Service_TR")]
    public partial class BusinessCard_Service_TR
    {
        [Key]
        public int BusinessCard_ServiceLG_ID { get; set; }

        public int BusinessCard_ID { get; set; }

        [Required]
        [StringLength(2)]
        public string Lang_CD { get; set; }

        public virtual Language Language { get; set; }

        public virtual BusinessCard BusinessCard { get; set; }
    }
}
