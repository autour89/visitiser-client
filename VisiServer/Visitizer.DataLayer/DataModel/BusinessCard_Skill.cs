namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("profile.BusinessCard_Skill")]
    public partial class BusinessCard_Skill
    {
        [Key]
        public int BusinessCard_Skill_ID { get; set; }

        public int BusinessCard_ID { get; set; }

        public int? Skill_ID { get; set; }

        [StringLength(250)]
        public string Skill_NM { get; set; }

        public virtual BusinessCard BusinessCard { get; set; }

        public virtual Skill Skill { get; set; }
    }
}
