namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("[profile.catalog].Service_TR")]
    public partial class Service_TR
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Service_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(2)]
        public string Lancg_CD { get; set; }

        [Column("Service_TR")]
        [Required]
        [StringLength(250)]
        public string Service_TR1 { get; set; }

        public virtual Language Language { get; set; }

        public virtual Service Service { get; set; }
    }
}
