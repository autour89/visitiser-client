﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Visitizer.DataLayer.DataModel
{
    [Table("UserDevices")]
    public class UserDevice
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        [Required]
        public string DeviceId { get; set; }
        public string DeviceName { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
    }
}
