namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("msg.Chat_Member")]
    public partial class Chat_Member
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Chat_List_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Account_ID { get; set; }

        [StringLength(1)]
        public string Role_Type { get; set; }

        public virtual Chat_List Chat_List { get; set; }

        public virtual Account Account { get; set; }
    }
}
