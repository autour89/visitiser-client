namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("calendar.Event_Member")]
    public partial class Event_Member
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Event_Member()
        {
            Events = new HashSet<Event>();
        }

        [Key]
        public int Event_Members_ID { get; set; }

        public int Event_ID { get; set; }

        public int? Account_ID { get; set; }

        [StringLength(1000)]
        public string Member_Info { get; set; }

        [StringLength(1)]
        public string Status_CD { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Event> Events { get; set; }

        public virtual Account Account { get; set; }
    }
}
