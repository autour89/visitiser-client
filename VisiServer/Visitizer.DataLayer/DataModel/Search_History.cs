namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("audit.Search_History")]
    public partial class Search_History
    {
        [Key]
        public int Search_Id { get; set; }

        [StringLength(300)]
        public string Search_NM { get; set; }

        public DateTime? Created_DT { get; set; }

        public int? Account_ID { get; set; }

        [StringLength(1)]
        public string Source_Type { get; set; }

        public int? Device_ID { get; set; }
    }
}
