namespace Visitizer.DataLayer.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("msg.Messages")]
    public partial class Message
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Message()
        {
            Delivery_Message = new HashSet<Delivery_Message>();
        }

        [Key]
        public int Message_ID { get; set; }

        public int Chat_List_ID { get; set; }

        public int Sender_ID { get; set; }

        [Column("Message")]
        [Required]
        public string Message1 { get; set; }

        [Required]
        [StringLength(10)]
        public string Message_Type { get; set; }

        public DateTime Message_DT { get; set; }

        [Required]
        [StringLength(1)]
        public string Delivery_Status { get; set; }

        public virtual Chat_List Chat_List { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Delivery_Message> Delivery_Message { get; set; }

        public virtual Account Account { get; set; }
    }
}
