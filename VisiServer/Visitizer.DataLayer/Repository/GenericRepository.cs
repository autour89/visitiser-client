﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Visitizer.DataLayer.DataModel;
namespace Mobile.DataLayer.Repository
{
    public abstract class GenericRepository<T> : IGenericRepository<T> where T : class, new()
    {
        protected DbContext context;
        protected abstract DbSet<T> dbSet { get; }

        public GenericRepository() { }
        public GenericRepository(ContextDB context)
        {
            this.context = context;
        }

        public IEnumerable<T> GetAll()
        {
            return dbSet;
        }

        public IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return dbSet.Where(predicate.Compile());
        }

        public T Get(int id)
        {
            return dbSet.Find(id);
        }

        public void AddOrUpdate(T obj)
        {
            dbSet.AddOrUpdate(obj);
            try
            {
                context.SaveChanges();
            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString());
            }
        }

        public void Delete(T obj)
        {
            dbSet.Remove(obj);
            context.SaveChanges();
        }

    }
}

