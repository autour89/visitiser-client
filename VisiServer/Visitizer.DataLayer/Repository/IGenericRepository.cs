﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Mobile.DataLayer.Repository
{
    public interface IGenericRepository<T>
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate);
        //void Save(T obj);
        T Get(int id);
        void AddOrUpdate(T obj);
        void Delete(T obj);
    }
}
